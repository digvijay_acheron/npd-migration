package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.monster.npd.migration.model.MPMProject;

public interface MPMProjectRepository extends JpaRepository<MPMProject, Integer> {
	
	@Query("SELECT MAX(p.id) FROM MPMProject p")
    Integer maxMPMProjectId();
	
	@Query(value = "SELECT NEXT VALUE FOR dbo.projectsequence", nativeQuery = true)
    public Integer getCurrentVal();

}
