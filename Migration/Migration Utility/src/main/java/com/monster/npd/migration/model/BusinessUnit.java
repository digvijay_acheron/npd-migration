package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "shrdnpdlookupdomainmodelbusiness_unit")
@Entity //O2NPDLookUpDomainModelBusiness_Unit
public class BusinessUnit {
	 	@Id
	    @Column(name = "Id", updatable = false)
	    private Integer id;

	    @Column(name = "DISPLAY_NAME", updatable = false)
	    private String displayName;
	    
	    @Column(name = "name", updatable = false)
	    private String name;
	    
	    
	    @OneToOne
	    @JoinColumn(name = "R_PO_REGION_Id", referencedColumnName = "Id", insertable = false, updatable = false)
	    private Region region;

}
