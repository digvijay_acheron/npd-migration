package com.monster.npd.migration.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProjectOutputDetails {
	
	String otmmFolderId;
	Integer mpmProjectId;
	Integer npdProjectId;
	
	MPMProject mpmProject;
	NPDProject npdProject;
	ProjectTeamList projectTeamList;

}
