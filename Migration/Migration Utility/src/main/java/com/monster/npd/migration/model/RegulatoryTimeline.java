package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@Table(name = "shrdnpdlookupdomainmodelregulatory_timeline")
@Entity
public class RegulatoryTimeline {

	  @Id
	    @Column(name = "Id", updatable = false)
	    private String id;

	    @Column(name = "registrationpreparationduration", updatable = false)
	    private Integer registrationPreparationDuration;
	    
	    @Column(name = "preproductionregistartionduration", updatable = false)
	    private Integer preproductionRegistartionDuration;
	    
	    @Column(name = "postproductionduration", updatable = false)
	    private Integer postProductionDuration;
	    
	    @OneToOne
	    @JoinColumn(name = "R_PO_MANUFACTURINGLOCATION_Id", referencedColumnName = "Id", insertable = false, updatable = false)
	    private DraftManufacturingLocation manufacturingLocationId;
	    
	    @OneToOne
	    @JoinColumn(name = "R_PO_MARKET_Id", referencedColumnName = "Id", insertable = false, updatable = false)
	    private Market marketId;
}
