package com.monster.npd.migration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.MPMPriority;

@Repository
public interface MPMPriorityRepository extends JpaRepository<MPMPriority, Integer> {
  
    List<MPMPriority> findByOrganizationid(int organizationid);
}
