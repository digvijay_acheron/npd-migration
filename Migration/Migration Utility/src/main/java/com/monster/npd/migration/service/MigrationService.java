package com.monster.npd.migration.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.monster.npd.migration.model.AuditLog;
import com.monster.npd.migration.model.MaxId;
import com.monster.npd.migration.repository.AuditLogRepository;

@Service
public class MigrationService {
	
//	private static final Logger logger = LoggerFactory.getLogger(MigrationService.class);
	private static final Logger logger = LogManager.getLogger(MigrationService.class);
	@Autowired
	Environment env;

	@Autowired
	private EventUserModal eventUserModal;
	
	@Autowired
	private ConfigService configService;

	@Autowired
	MigrationUtilityService migrationUtilityService;
	
	@Autowired
	RequestMigrationService requestMigrationService;
	
	@Autowired
	ProjectMigrationService projectMigrationService;
	
	@Autowired
	TaskMigrationService taskMigrationService;
	
	@Autowired
	AuditLogRepository auditLogRepository;
	
	

	/**
	 * migrate Data method to call soap Api
	 * 
	 * @param samlart - to access soap Api in Appworks
	 * 
	 * @return migrated message
	 */
	
	public void kickOffAsyncTasks() throws InterruptedException {
		int total = 50;
		int i = 0;
		while(i < total) {
			 Collection<Future<Integer>> results = new ArrayList<>(10);	  

			  //kick off all threads
			  for ( int idx = i; idx < i+10; idx++) {
//			    results.add(requestMigrationService.call(idx));
			  }

			  // wait for all threads
			  results.forEach(result -> {
			    try {
			      result.get();
			    } catch (InterruptedException | ExecutionException e) {
			      //handle thread error
			    }
			  });
			  i = i + 10;
			  logger.info("Batch Completed");
		}
		
		logger.info("All Completed");
		}


	public void migrateData(String samlart, String UserName, String Password) throws InterruptedException {
		try {
			   
			eventUserModal.processAllSheets(env.getProperty("excel.path"), samlart);
			configService.getAllConfigs();
			logger.info("processed sheet");
			int size = EventUserModal.getRequestList().size();	
			int total = Integer.parseInt(env.getProperty("npd.batch.total"));
			int idx = Integer.parseInt(env.getProperty("npd.batch.start"));
			int batch = 1;
			int batchSize = Integer.parseInt(env.getProperty("npd.batch.size"));
			logger.info("total records to be processed " + total);
			logger.info("Records starts from " + idx);
			logger.info("Records to be processed parallely " + batchSize);
			while(idx < total) {
				 Collection<Future<Integer>> results = new ArrayList<>(batchSize);	  
				 for (int i = idx; i < idx+batchSize; i++) {
//					 if(!EventUserModal.getRequestList().get(i).getReferenceId().equalsIgnoreCase("0") || 
//						!EventUserModal.getProjectDetailsList().get(i).getProjectEndDate()
//						.equalsIgnoreCase("1900-09-01T00:00:00Z")) {
//						 results.add(requestMigrationService.migrateAll(EventUserModal.getRequestList().get(i),
//							EventUserModal.getTasksList().get(i), EventUserModal.getProjectDetailsList().get(i), 
//							UserName, Password));
//					 }	
					 
					 if(EventUserModal.getRequestList().get(i).getReferenceId().equalsIgnoreCase("EM00577")) {
						 results.add(requestMigrationService.migrateAll(EventUserModal.getRequestList().get(i),
									EventUserModal.getTasksList().get(i), EventUserModal.getProjectDetailsList().get(i), 
									UserName, Password));
					 }
				 }
				 results.forEach(result -> {
					    try {
					      result.get();
//					      logger.info("result " + result.get());
					    } catch (InterruptedException | ExecutionException e) {
					      logger.info("Thread error..");
					    }
				 });
				 idx = idx + 10;
				 batch = batch+1;
				 logger.info("Batch Completed " +(batch-1));
			}
			logger.info("All Completed");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}

//migrationUtilityService.soapCall(samlart, index, EventUserModal.getRequestList());
//logger.info("Starting..." + requestResponse);
   
//while (true) { 
//    if (future.isDone()) {
//        System.out.println("Result from asynchronous process - " + future.get());
//        break;
//    }
//    System.out.println("Continue doing something else. ");
//    Thread.sleep(1000);
//}

//try{
//}catch(Exception e) {
//logger.info("Exception " + e.getMessage());
//AuditLog auditLog = new AuditLog();
//auditLog.setId(ConfigService.getMaxAuditLogId());
//auditLog.setReferenceid(EventUserModal.getRequestList().get(1).getReferenceId());
//auditLog.setStatus("Failed");
//auditLog.setMessage(e.getMessage());
//
//AuditLog response = auditLogRepository.save(auditLog);
//logger.info("Added " + response.getId() + response.getMessage());
//}
//AuditLog request = auditLogRepository.findById(147757).orElse(null);
//request.setMessage("Sucessssssssss");
//System.out.println("market " + EventUserModal.getRequestList().get(0).getLeadMarketId());
//System.out.println("req " + EventUserModal.getRequestList().get(0).getR_PO_REQUEST_TYPE());


//for (int index = 0; index < size; index += 10) { 
//logger.info("cp 0 date" + EventUserModal.getProjectDetailsList().get(i).getProjectEndDate());
