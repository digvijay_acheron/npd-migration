package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.AuditLog;

@Repository
public interface AuditLogRepository extends JpaRepository<AuditLog, Integer> {
	
	@Query("SELECT MAX(p.id) FROM AuditLog p")
    Integer maxAuditLogId();
	
	@Query(value = "SELECT NEXT VALUE FOR dbo.audit_log_seq", nativeQuery = true)
    public Integer getCurrentVal();

}
