package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "o10opentextentityidentitycomponentsidentity")
@Entity //O2OpenTextEntityIdentityComponentsPerson
public class Users {
	
	 	@Id
	    @Column(name = "Id", updatable = false)
	    private String id;

	    @Column(name = "fullname", updatable = false)
	    private String fullname;
	    
	    @Column(name = "identitydisplayname", updatable = false)
	    private String identitydisplayname;

}
