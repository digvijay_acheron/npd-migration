package com.monster.npd.migration.model;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@Table(name = "shrdnpdsubmissionsecondary_pack")
@Entity
public class SecondaryPack {

		@Id
		@GeneratedValue(generator = "sec_pack_seq", strategy = GenerationType.SEQUENCE)
		@SequenceGenerator(name = "sec_pack_seq", sequenceName = "sec_pack_seq",allocationSize=1)
	    @Column(name = "Id")
	    private Integer id;

	    @Column(name = "required")
	    private Boolean required;
	    
	    @Column(name = "regsneeded")
	    private Boolean regsneeded;
	    
	    @Column(name = "mpmjobno")
	    private Integer mpmjobno;
	    
	    @Column(name = "approveddielineavailable")
	    private Boolean approveddielineavailable;
	    
	    @Column(name = "itemnumber")
	    private Integer itemnumber;
	    
	    @Column(name = "colourapproval")
	    private String colourapproval;
	    
	    @Column(name = "invoiceapproved")
	    private String invoiceapproved;
	    
	    @Column(name = "statuscomments")
	    private String statuscomments;
	    
	    @Column(name = "datedevelopmentcompleted")
	    private Date datedevelopmentcompleted;
	    
	    @Column(name = "dateproofingcompleted")
	    private Date dateproofingcompleted;
	    
	    @OneToOne
	    @JoinColumn(name = "r_po_print_supplier_id", referencedColumnName = "Id", insertable = false)
	    private PrintSupplier printSupplier;
	    
	    @OneToOne
	    @JoinColumn(name = "r_po_pack_format_id", referencedColumnName = "Id", insertable = false)
	    private SecondaryPackLeadTimes secondaryPackLeadTime;
	    
	    @Column(name = "mpmjobstate")
	    private String mpmJobState;
	    
//	    @ManyToMany(mappedBy = "secondaryPack", fetch = FetchType.LAZY)
//	    private Set<NPDProject> npdProject = new HashSet<NPDProject>();
	    
	    @OneToMany(mappedBy = "secondaryPack", cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	    private Set<NPDProjectSecondaryPack> npdProjectSecondaryPack = new HashSet<NPDProjectSecondaryPack>();
	    
	    
	    @Column(name = "s_organizationid")
	    private int organizationid;

}