package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "shrdnpdsubmissionnpd_projectsecondary_pack")
@Entity
@IdClass(NPDProjectSecondaryPackId.class) 
public class NPDProjectSecondaryPack {
	
	@Id
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "npd_projectidA5E8B0A5F45E5D19")
    private NPDProject npdProject;

	@Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "secondary_pack_id")
    private SecondaryPack secondaryPack;

    
    @Column(name = "s_organizationid")
    private int organizationid;

}
