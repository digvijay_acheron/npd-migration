package com.monster.npd.migration.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.monster.npd.migration.model.APIConstants;
import com.monster.npd.migration.model.PmOwner;

/**
 * 
 * @author JeevaR
 *
 */
@Service
public class UtilService {

	@Autowired
	Environment env;

	private static final Logger logger = LogManager.getLogger(UtilService.class);

	/**
	 * method to check a string is null or empty
	 * 
	 * @param value - a string value to be checked whether it is null or empty
	 * @return
	 */
	public boolean isNullOrEmpty(String value) {
		if (value == null || value.isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * method to check object is null or empty
	 * 
	 * @param obj - a object value to be checked whether it is null or empty
	 * @return
	 */
	public boolean isNullOrEmpty(Object obj) {
		return obj == null ? true : false;
	}

	/**
	 * method to convert soap message to string
	 * 
	 * @param soapMessage - soap message that needs to be converted into string
	 *                    value
	 * @return soap message in string format
	 * @throws SOAPException
	 * @throws IOException
	 */
	public String convertSOAPMessageToString(SOAPMessage soapMessage) throws SOAPException, IOException {
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		soapMessage.writeTo(bout);
		String message = bout.toString("UTF-8");
		return message;
	}

	/**
	 * method convert StackTrace To String
	 * 
	 * @param pThrowable
	 * @return
	 */
	public String convertStackTraceToString(Throwable pThrowable) {
		if (pThrowable == null) {
			return null;
		} else {
			StringWriter sw = new StringWriter();
			pThrowable.printStackTrace(new PrintWriter(sw));
			return sw.toString();
		}
	}

	/**
	 * method to form OTDS API
	 * 
	 * @param s_serviceName
	 * @return return OTDSAPI
	 */
	public String formOTDSAPI(String s_serviceName) {
		logger.info("Get host name");
		String hostName = env.getProperty("otds.server.host");
		logger.info("Get Api Ext");
		String apiExt = env.getProperty("otds.api.ext");
		System.out.println(hostName + apiExt + "/" + s_serviceName);
		if (!isNullOrEmpty(hostName) && !isNullOrEmpty(apiExt) && !isNullOrEmpty(s_serviceName)) {

			return hostName + apiExt + "/" + s_serviceName;
		}
		return null;
	}

	/**
	 * method to form Appworks Gateway Url
	 * 
	 * @param s_samlArt
	 * @return Appworks Gateway Url
	 */
	public String formAppworksGatewayUrl(String s_samlArt) {
		String gatewayUrl = env.getProperty("appworks.gateway.url");
		if (!isNullOrEmpty(gatewayUrl) && !isNullOrEmpty(s_samlArt)) {
			System.out.println(
					"formAppworksGatewayUrl " + gatewayUrl + "?" + env.getProperty("samlart") + "=" + s_samlArt);
			return gatewayUrl + "?" + APIConstants.SAMLart.getValue() + "=" + s_samlArt;
		}
		return null;
	}

	/**
	 * method to find the next monday of given date
	 * 
	 * @param date
	 * @return the next monday of given date
	 */
	public static String getNextMonday(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
		GregorianCalendar gc = new GregorianCalendar(1900, 0, 0, 0, 0, 0);
		gc.add(Calendar.DATE, Integer.parseInt(date) - 1);
		Date newDate = gc.getTime();
		String ndate = sdf.format(newDate);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
		// convert String to LocalDate
		LocalDate localDate = LocalDate.parse(ndate, formatter);

		localDate = localDate.with(TemporalAdjusters.nextOrSame(DayOfWeek.MONDAY));
		String dayinMonth = localDate.getDayOfMonth() / 10 == 0 ? "0" + localDate.getDayOfMonth()
		: localDate.getDayOfMonth() + "";
String month = localDate.getMonthValue() / 10 == 0 ? "0" + localDate.getMonthValue()
		: localDate.getMonthValue() + "";
		String nextMonday = localDate.getYear() + "-" + month + "-" + dayinMonth
				+ "T00:00:00Z";
		return nextMonday;

	}

	/**
	 * method to find the next sunday of given date
	 * 
	 * @param date
	 * @return the next monday of given date
	 */
	public static String getNextSunday(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
		GregorianCalendar gc = new GregorianCalendar(1900, 0, 0);
		gc.add(Calendar.DATE, Integer.parseInt(date) - 1);
		Date newDate = gc.getTime();
		String ndate = sdf.format(newDate);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
		// convert String to LocalDate
		LocalDate localDate = LocalDate.parse(ndate, formatter);
		localDate = localDate.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
		String dayinMonth = localDate.getDayOfMonth() / 10 == 0 ? "0" + localDate.getDayOfMonth()
				: localDate.getDayOfMonth() + "";
//		System.out.println(dayinMonth);
		String month = localDate.getMonthValue() / 10 == 0 ? "0" + localDate.getMonthValue()
				: localDate.getMonthValue() + "";
//		System.out.println(month);
		String nextSunday = localDate.getYear() + "-" + month + "-" + dayinMonth + "T00:00:00Z";
		return nextSunday;

	}

	/**
	 * method to convert numeric value into date
	 * 
	 * @param date -numeric value to be converted to date
	 * @return date converted from numeric value
	 */
	public static String getValidDate(String value) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
		GregorianCalendar gc = new GregorianCalendar(1900, 0, 0);
		gc.add(Calendar.DATE, Integer.parseInt(value) - 1);
		Date newDate = gc.getTime();
		String date = sdf.format(newDate);
		
		return date;
	}

	/**
	 * method to get Pm identity Id by passing pm name
	 * 
	 * @param r_PO_CORP_PM
	 * @return PM identity Id
	 */
	public String getPmOwnerIdentityId(String pmName) {
		String pmId = "";
		if (pmName.contains("Christoffer")) {
			pmId = PmOwner.Christoffer.getId();
		} else if (pmName.contains("Antonios")) {
			pmId = PmOwner.Antonios.getId();
		} else if (pmName.contains("Donald")) {
			pmId = PmOwner.Donald.getId();
		} else if (pmName.contains("Hans")) {
			pmId = PmOwner.Hans.getId();
		} else if (pmName.contains("Tiffany")) {
			pmId = PmOwner.Tiffany.getId();
		} else if (pmName.contains("Mike")) {
			pmId = PmOwner.Mike.getId();
		} else if (pmName.contains("Martin")) {
			pmId = PmOwner.Martin.getId();
		} else if (pmName.contains("Jenny")) {
			pmId = PmOwner.Jenny.getId();
		} else if (pmName.contains("Ivonne")) {
			pmId = PmOwner.Ivonne.getId();
		} else if (pmName.contains("Mark")) {
			pmId = PmOwner.Mark.getId();
		} else if (pmName.contains("Toan")) {
			pmId = PmOwner.Toan.getId();
		} else if (pmName.contains("Rashad")) {
			pmId = PmOwner.Rashad.getId();
		} else if (pmName.contains("Chris H")) {
			pmId = PmOwner.ChrisH.getId();
		} else if (pmName.contains("Josh")) {
			pmId = PmOwner.Josh.getId();
		} else if (pmName.contains("Steven")) {
			pmId = PmOwner.Steven.getId();
		} else if (pmName.contains("Doug")) {
			pmId = PmOwner.Doug.getId();
		} else if (pmName.contains("Ed")) {
			pmId = PmOwner.Ed.getId();
		} else if (pmName.contains("Kammie")) {
			pmId = PmOwner.Kammie.getId();
		} else if (pmName.contains("Peichi")) {
			pmId = PmOwner.Peichi.getId();
		} else if (pmName.contains("Leila")) {
			pmId = PmOwner.Leila.getId();
		} else if (pmName.contains("Marcus")) {
			pmId = PmOwner.Marcus.getId();
		} else if (pmName.contains("Emmanuel")) {
			pmId = PmOwner.Emmanuel.getId();
		} else if (pmName.contains("Carly")) {
			pmId = PmOwner.Carly.getId();
		} else if (pmName.contains("Lamiss")) {
			pmId = PmOwner.Lamiss.getId();
		} else if (pmName.contains("Shilpi")) {
			pmId = PmOwner.Shilpi.getId();
		} else if (pmName.contains("Regional")) {
			pmId = PmOwner.Regional.getId();
		} else if (pmName.contains("Niamh")) {
			pmId = PmOwner.Niamh.getId();
		} else if (pmName.contains("Albert")) {
			pmId = PmOwner.Albert.getId();
		} else if (pmName.contains("Susan")) {
			pmId = PmOwner.Susan.getId();
		} else if (pmName.contains("Ben")) {
			pmId = PmOwner.Ben.getId();
		} else if (pmName.contains("Alice")) {
			pmId = PmOwner.Alice.getId();
		} else if (pmName.contains("Christine")) {
			pmId = PmOwner.Christine.getId();
		} else if (pmName.contains("Corp")) {
			pmId = PmOwner.Corp.getId();
		} else if (pmName.contains("Corp West")) {
			pmId = PmOwner.CorpWest.getId();
		} else if (pmName.contains("Fatima")) {
			pmId = PmOwner.Fatima.getId();
		} else if (pmName.contains("Payal")) {
			pmId = PmOwner.Payal.getId();
		} else if (pmName.contains("Charlie")) {
			pmId = PmOwner.Charlie.getId();
		} else if (pmName.contains("Natalie")) {
			pmId = PmOwner.Natalie.getId();
		} else if (pmName.contains("Dom Smith")) {
			pmId = PmOwner.DomSmith.getId();
		} else if (pmName.contains("Dominic Smith")) {
			pmId = PmOwner.DominicSmith.getId();
		} else if (pmName.contains("Dom")) {
			pmId = PmOwner.Dom.getId();
		} else if (pmName.contains("Silva")) {
			pmId = PmOwner.Silva.getId();
		} else if (pmName.contains("alex")) {
			pmId = PmOwner.alex.getId();
		} else if (pmName.contains("Hayden Rawson")) {
			pmId = PmOwner.HaydenRawson.getId();
		} else if (pmName.contains("Paul Goolde")) {
			pmId = PmOwner.PaulGoolde.getId();
		} else if (pmName.contains("Dan Fratila")) {
			pmId = PmOwner.DanFratila.getId();
		} else if (pmName.contains("Vladislav Sivkov")) {
			pmId = PmOwner.VladislavSivkov.getId();
		} else if (pmName.contains("Louise")) {
			pmId = PmOwner.Louise.getId();
		} else if (pmName.contains("Damianos")) {
			pmId = PmOwner.Damianos.getId();
		} else if (pmName.contains("Sonay")) {
			pmId = PmOwner.Sonay.getId();
		} else if (pmName.contains("Chris Wallace")) {
			pmId = PmOwner.ChrisWallace.getId();
		} else if (pmName.contains("ANTON")) {
			pmId = PmOwner.ANTON.getId();
		} else if (pmName.contains("Omar Alsamhoury")) {
			pmId = PmOwner.OmarAlsamhoury.getId();
		} else if (pmName.contains("Antonio Clementi")) {
			pmId = PmOwner.AntonioClementi.getId();
		} else if (pmName.contains("Azamat")) {
			pmId = PmOwner.Azamat.getId();
		} else if (pmName.contains("Azamat Kabdrakhman")) {
			pmId = PmOwner.AzamatKabdrakhman.getId();
		} else if (pmName.contains("Christof Gemke")) {
			pmId = PmOwner.ChristofGemke.getId();
		}

		else if (pmName.contains("Christoff")) {
			pmId = PmOwner.Christoff.getId();
		} else if (pmName.contains("Dimitris Smailos Louise")) {
			pmId = PmOwner.DimitrisSmailosLouise.getId();
		} else if (pmName.contains("Sergio Bravo")) {
			pmId = PmOwner.SergioBravo.getId();
		} else if (pmName.contains("Neil Thompson")) {
			pmId = PmOwner.NeilThompson.getId();
		} else if (pmName.contains("Joerg")) {
			pmId = PmOwner.Joerg.getId();
		} else if (pmName.contains("Tom Groothuijse")) {
			pmId = PmOwner.TomGroothuijse.getId();
		} else if (pmName.contains("Anton")) {
			pmId = PmOwner.Anton.getId();
		} else if (pmName.contains("Alex")) {
			pmId = PmOwner.Alex.getId();
		} else if (pmName.contains("Mirek")) {
			pmId = PmOwner.Mirek.getId();
		} else if (pmName.contains("SaraFernandez")) {
			pmId = PmOwner.SaraFernandez.getId();
		} else if (pmName.contains("Juan")) {
			pmId = PmOwner.Juan.getId();
		} else if (pmName.contains("Patrick Wiklund")) {
			pmId = PmOwner.PatrickWiklund.getId();
		} else if (pmName.contains("Thomas")) {
			pmId = PmOwner.Thomas.getId();
		} else if (pmName.contains("Shumyl")) {
			pmId = PmOwner.Shumyl.getId();
		} else if (pmName.contains("Hamad Sherazi")) {
			pmId = PmOwner.HamadSherazi.getId();
		} else if (pmName.contains("Patrick")) {
			pmId = PmOwner.Patrick.getId();
		} else if (pmName.contains("ErO LtiHA")) {
			pmId = PmOwner.ErOLtiHA.getId();
		} else if (pmName.contains("Jorg")) {
			pmId = PmOwner.Jorg.getId();
		} else if (pmName.contains("Sara")) {
			pmId = PmOwner.Sara.getId();
		} else if (pmName.contains("Dimitris")) {
			pmId = PmOwner.Dimitris.getId();
		} else if (pmName.contains("louise")) {
			pmId = PmOwner.louise.getId();
		} else if (pmName.contains("Fred")) {
			pmId = PmOwner.Fred.getId();
		} else if (pmName.contains("Hayden")) {
			pmId = PmOwner.Hayden.getId();
		} else if (pmName.contains("Hisham")) {
			pmId = PmOwner.Hisham.getId();
		} else if (pmName.contains("Ivan")) {
			pmId = PmOwner.Ivan.getId();
		} else if (pmName.contains("Jorge")) {
			pmId = PmOwner.Jorge.getId();
		} else if (pmName.contains("Juan Abajo")) {
			pmId = PmOwner.JuanAbajo.getId();
		} else if (pmName.contains("Julie")) {
			pmId = PmOwner.Julie.getId();
		} else if (pmName.contains("Juiia")) {
			pmId = PmOwner.Juiia.getId();
		} else if (pmName.contains("Luc")) {
			pmId = PmOwner.Luc.getId();
		} else if (pmName.contains("Matt")) {
			pmId = PmOwner.Matt.getId();
		} else if (pmName.contains("Matt Howard")) {
			pmId = PmOwner.MattHoward.getId();
		} else if (pmName.contains("Mutlu")) {
			pmId = PmOwner.Mutlu.getId();
		}

		else if (pmName.contains("Neil")) {
			pmId = PmOwner.Neil.getId();
		} else if (pmName.contains("Niels")) {
			pmId = PmOwner.Niels.getId();
		} else if (pmName.contains("Paul")) {
			pmId = PmOwner.Paul.getId();
		}

		else if (pmName.contains("Paul Goold")) {
			pmId = PmOwner.PaulGoold.getId();
		} else if (pmName.contains("Philippe")) {
			pmId = PmOwner.Philippe.getId();
		} else if (pmName.contains("Roman")) {
			pmId = PmOwner.Roman.getId();
		} else if (pmName.contains("Rusian")) {
			pmId = PmOwner.Rusian.getId();
		} else if (pmName.contains("Sara Fernadez")) {
			pmId = PmOwner.SaraFernadez.getId();
		} else if (pmName.contains("Sonay Nustekin")) {
			pmId = PmOwner.SonayNustekin.getId();
		} else if (pmName.contains("Tatjana")) {
			pmId = PmOwner.Tatjana.getId();
		} else if (pmName.contains("Tom")) {
			pmId = PmOwner.Tom.getId();
		} else if (pmName.contains("Tom Groothijuse")) {
			pmId = PmOwner.TomGroothijuse.getId();
		} else if (pmName.contains("Tony")) {
			pmId = PmOwner.Tony.getId();
		} else if (pmName.contains("Uche")) {
			pmId = PmOwner.Uche.getId();
		} else if (pmName.contains("Ushmi")) {
			pmId = PmOwner.Ushmi.getId();
		} else if (pmName.contains("Cartrice")) {
			pmId = PmOwner.Cartrice.getId();
		}

		else if (pmName.contains("Corp East")) {
			pmId = PmOwner.CorpEast.getId();
		} else if (pmName.contains("Harpreet")) {
			pmId = PmOwner.Harpreet.getId();
		} else if (pmName.contains("Justyna")) {
			pmId = PmOwner.Justyna.getId();
		}

		else if (pmName.contains("Kari")) {
			pmId = PmOwner.Kari.getId();
		} else if (pmName.contains("Komal")) {
			pmId = PmOwner.Komal.getId();
		} else if (pmName.contains("Naz")) {
			pmId = PmOwner.Naz.getId();
		} else if (pmName.contains("Peta")) {
			pmId = PmOwner.Peta.getId();
		} else if (pmName.contains("SharedCU")) {
			pmId = PmOwner.SharedCU.getId();
		} else if (pmName.contains("TBD")) {
			pmId = PmOwner.TBD.getId();
		} else if (pmName.contains("US Kari")) {
			pmId = PmOwner.USKari.getId();
		} else if (pmName.contains("US Marilyn")) {
			pmId = PmOwner.USMarilyn.getId();
		} else if (pmName.contains("US PS")) {
			pmId = PmOwner.USPS.getId();
		} else if (pmName.contains("Bre")) {
			pmId = PmOwner.Bre.getId();
		} else if (pmName.contains("Britney")) {
			pmId = PmOwner.Britney.getId();
		} else if (pmName.contains("Jade")) {
			pmId = PmOwner.Jade.getId();
		} else if (pmName.contains("KOREY")) {
			pmId = PmOwner.KOREY.getId();
		} else if (pmName.contains("Patricia")) {
			pmId = PmOwner.Patricia.getId();
		} else if (pmName.contains("RJ")) {
			pmId = PmOwner.RJ.getId();
		} else if (pmName.contains("Robert")) {
			pmId = PmOwner.Robert.getId();
		} else if (pmName.contains("wo")) {
			pmId = PmOwner.wo.getId();
		} else if (pmName.contains("Corp GFG")) {
			pmId = PmOwner.CorpGFG.getId();
		} else if (pmName.contains("Damian")) {
			pmId = PmOwner.Damian.getId();
		} else if (pmName.contains("Hilda")) {
			pmId = PmOwner.Hilda.getId();
		} else if (pmName.contains("Justine")) {
			pmId = PmOwner.Justine.getId();
		}

		else if (pmName.contains("Samantha")) {
			pmId = PmOwner.Samantha.getId();
		} else if (pmName.contains("Thu")) {
			pmId = PmOwner.Thu.getId();
		}
		return pmId;
	}

}
