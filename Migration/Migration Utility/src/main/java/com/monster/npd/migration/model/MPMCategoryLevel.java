package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@Table(name = "shrdacheronmpmcorempm_category_level")
@Entity
@ToString
public class MPMCategoryLevel {
	
	@Id
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(name = "category_level_name", updatable = false)
    private String category_level_name;
    
    @Column(name = "is_active", updatable = false)
    private String is_active;
        
    @Column(name = "metadata_model_id", updatable = false)
    private String metadata_model_id;
    
     @Column(name = "asset_type", updatable = false)
    private String asset_type;
         
    @Column(name = "template_id", updatable = false)
    private String template_id;
    
    @Column(name = "category_level_type", updatable = false)
    private String category_level_type;
    
    @Column(name = "securtiy_policy_ids", updatable = false)
    private String securtiy_policy_ids; 
    
    @Column(name = "field_group_id", updatable = false)
    private String field_group_id;
    
    @Column(name = "r_po_category_id", updatable = false)
    private String r_po_category_id;

}
