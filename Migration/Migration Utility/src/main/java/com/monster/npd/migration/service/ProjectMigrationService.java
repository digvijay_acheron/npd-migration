package com.monster.npd.migration.service;



import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.monster.npd.migration.model.AuditUsers;
import com.monster.npd.migration.model.CanSupplierData;
import com.monster.npd.migration.model.DraftManufacturingLocation;
import com.monster.npd.migration.model.EarlyManufacturingSite;
import com.monster.npd.migration.model.Folder;
import com.monster.npd.migration.model.FolderResource;
import com.monster.npd.migration.model.FolderResourceContainer;
import com.monster.npd.migration.model.MPMProject;
import com.monster.npd.migration.model.Metadata;
import com.monster.npd.migration.model.MetadataField;
import com.monster.npd.migration.model.MetadataValue;
import com.monster.npd.migration.model.MetadataValueContainer;
import com.monster.npd.migration.model.NPDProject;
import com.monster.npd.migration.model.NPDProjectSecondaryPack;
import com.monster.npd.migration.model.OpenTextIdentity;
import com.monster.npd.migration.model.PrintSupplier;
import com.monster.npd.migration.model.ProjectClassifierData;
import com.monster.npd.migration.model.ProjectDetails;
import com.monster.npd.migration.model.ProjectOutputDetails;
import com.monster.npd.migration.model.ProjectTeamList;
import com.monster.npd.migration.model.ProjectType;
import com.monster.npd.migration.model.Request;
import com.monster.npd.migration.model.RequestData;
import com.monster.npd.migration.model.SecondaryPack;
import com.monster.npd.migration.model.SecondaryPackLeadTimes;
import com.monster.npd.migration.model.SecurityPolicyList;
import com.monster.npd.migration.model.Sessions;
import com.monster.npd.migration.model.TaskDetails;
import com.monster.npd.migration.model.TastingRequirement;
import com.monster.npd.migration.model.TechQualUsers;
import com.monster.npd.migration.model.TrialProtocolWrittenBy;
import com.monster.npd.migration.model.TrialSupervisionNPD;
import com.monster.npd.migration.model.TrialSupervisionQuality;
import com.monster.npd.migration.repository.MPMProjectRepository;
import com.monster.npd.migration.repository.NPDProjectRepository;
import com.monster.npd.migration.repository.NPDProjectSecondaryPackRepo;
import com.monster.npd.migration.repository.ProjectSolrRepository;
import com.monster.npd.migration.repository.SecondaryPackRepository;
import com.monster.npd.migration.repository.TaskSolrRepository;

@Service
public class ProjectMigrationService {
	
//	private static final Logger logger = LoggerFactory.getLogger(ProjectMigrationService.class);
	private static final Logger logger = LogManager.getLogger(ProjectMigrationService.class);
	
	@Autowired
	private ProjectSolrRepository projectSolrRepository;
	
	@Autowired
	private MPMProjectRepository MPMProjectRepository;
	
	@Autowired
	private TaskSolrRepository taskSolrRepository;
	
	@Autowired
	SecondaryPackRepository secondaryPackRepository;
	
	@Autowired
	NPDProjectSecondaryPackRepo NPDProjectSecondaryPackRepo;
	
	@Autowired
	NPDProjectRepository NPDProjectRepository;
	
	@Autowired
	WorkflowRulesService workflowRulesService;
	
	@Autowired
	SolrIndexingService solrIndexingService;
	
	@Autowired
	OTMMAPIService otmmAPIService;
	
	@Autowired
	UtilService util;
	
	@Autowired
	Environment env;
	
	@PersistenceContext
	EntityManager entityManager;
	
	

//	@Transactional
	public ProjectOutputDetails createProject(String UserName, String Password, Request request, ProjectDetails projectDetail,
			  String market, String businessUnit, RequestData requestData, TaskDetails taskData
			  , Integer noOfLinkedMarkets) throws ParseException {
		
//		logger.info("Next val " + MPMProjectRepository.getCurrentVal());
		
//		Integer id = MPMProjectRepository.getCurrentVal();
		
				Date dueDate= !util.isNullOrEmpty(projectDetail.getProjectEndDate()) 
						&& projectDetail.getProjectEndDate().indexOf("T") > 0
				? Date.valueOf(projectDetail.getProjectEndDate().split("T")[0]) : null;
				
				Integer statusId = request.getRequestStatus().equalsIgnoreCase("InProgress") ? 
						Integer.parseInt(env.getProperty("npd.project.initial.status.id")) :
							Integer.parseInt(env.getProperty("npd.project.completed.status.id"));
		    	MPMProject project = new MPMProject();
				project.setProjectName(request.getProjectName());
				project.setDescription(request.getTechnicalDescription());
				project.setStartDate(request.getSCreateddate());
				project.setDueDate(dueDate);
				project.setEndDate(dueDate);
				project.setIsActive(true); 
				project.setIsStandardStatus(true);
				project.setProjectType("PROJECT");
				project.setCategoryId(Integer.parseInt(env.getProperty("npd.project.category.id")));
				project.setPriorityId(Integer.parseInt(env.getProperty("npd.project.high.priority.id")));
				project.setProjectOwnerId(request.getE2ePM().getId());
				project.setStatusId(statusId);
				project.setTeamId(Integer.parseInt(env.getProperty("npd.project.team.id")));
				project.setIsDeleted(false);
				project.setOriginalStartDate(request.getSCreateddate());
				project.setOriginalDueDate(dueDate);
				project.setIsCustomWorkflow(true);
				project.setVersion(0);
//				project.setId(id);
				project.setIsOnHold(false);
				project.setIsApprovalFlow(false);
				project.setIsBulkUpdate(false);
				project.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
				
	
		
		
		
		String folderId = createOTMMFolder(project, request.getE2ePM().getIdentityDisplayName(), UserName, Password);
		logger.info("OTMM Folder Created Successfully..");
		project.setOtmmFolderId(folderId);
		MPMProject response = MPMProjectRepository.save(project);
		logger.info("Project Created Successfully.." + project.getId());
		
		
		

		List<SecondaryPack> secondaryPackList = createSecondaryPack(projectDetail);
		ProjectOutputDetails projectOutput = createNpdProject(response.getId(), request.getId(), projectDetail, 
				secondaryPackList, response, request, requestData, taskData, noOfLinkedMarkets);
		
		NPDProject npdProject = projectOutput.getNpdProject();
		
		for(SecondaryPack secPack : secondaryPackList) {
			SecondaryPack secPackresponse =  secondaryPackRepository.save(secPack);
			NPDProjectSecondaryPack npdProjectSecondaryPack = new NPDProjectSecondaryPack();
			NPDProject npdPro = NPDProjectRepository.findById(npdProject.getId()).orElse(null);
			logger.info(" inside npdPro " + npdPro.getCorppm() + npdPro.getId() );
			npdProjectSecondaryPack.setSecondaryPack(secPackresponse);
			npdProjectSecondaryPack.setNpdProject(npdProject);
			npdProjectSecondaryPack.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			secPackresponse.getNpdProjectSecondaryPack().add(npdProjectSecondaryPack);
			npdPro.getNpdProjectSecondaryPack().add(npdProjectSecondaryPack);
			
			NPDProjectRepository.save(npdPro);
//			entityManager.flush();
			secondaryPackRepository.save(secPackresponse);
//			entityManager.flush();
			NPDProjectSecondaryPackRepo.save(npdProjectSecondaryPack);
//			entityManager.flush();
		}


		ProjectOutputDetails projectOutputDetails =  new ProjectOutputDetails();
		projectOutputDetails.setOtmmFolderId(folderId);
		projectOutputDetails.setNpdProjectId(npdProject.getId());
		projectOutputDetails.setMpmProjectId(response.getId());
		projectOutputDetails.setMpmProject(response);
		projectOutputDetails.setNpdProject(npdProject);
		projectOutputDetails.setProjectTeamList(projectOutput.getProjectTeamList());
		return projectOutputDetails;
	}
	
	@Transactional
	public List<SecondaryPack> createSecondaryPack(ProjectDetails projectDetail) {
		List<SecondaryPack> secondaryPackList =  new ArrayList<SecondaryPack>();
		
		if(!util.isNullOrEmpty(projectDetail.getRequire1()) 
				&& projectDetail.getRequire1().equalsIgnoreCase("true")) {
			SecondaryPack secondaryPack1 = new SecondaryPack();
			secondaryPack1.setApproveddielineavailable(!util.isNullOrEmpty(projectDetail.getApprovedDielineAvailable1()) 
					&& projectDetail.getApprovedDielineAvailable1().equalsIgnoreCase("true") ?  true : false );
			secondaryPack1.setColourapproval(projectDetail.getColourApproval1());
			secondaryPack1.setDatedevelopmentcompleted(!util.isNullOrEmpty(projectDetail.getDateDevelopmentCompleted1()) 
					&& projectDetail.getDateDevelopmentCompleted1().indexOf("T") > 0 ? 
					Date.valueOf(projectDetail.getDateDevelopmentCompleted1().split("T")[0]) : null); 
			secondaryPack1.setDateproofingcompleted(!util.isNullOrEmpty(projectDetail.getDateProofingCompletedSecondaryPackaging1()) 
					&& projectDetail.getDateProofingCompletedSecondaryPackaging1().indexOf("T") > 0 ? 
					Date.valueOf(projectDetail.getDateProofingCompletedSecondaryPackaging1().split("T")[0]) : null);
			secondaryPack1.setInvoiceapproved(projectDetail.getInvoiceApproval1());
			secondaryPack1.setItemnumber(!util.isNullOrEmpty(projectDetail.getSecPackItemNumber1()) ?
					Integer.parseInt(projectDetail.getSecPackItemNumber1()) :  null );
//			secondaryPack1.setMpmjobno(!util.isNullOrEmpty(projectDetail.getMPMJobNo1()) ?
//					Integer.parseInt(projectDetail.getMPMJobNo1()) : null);
			PrintSupplier printSupplier = ConfigService.getPrintSupplierList().stream()
					.filter(obj -> Objects.nonNull(obj.getSupplier()))
					.filter(obj -> obj.getSupplier().equalsIgnoreCase(projectDetail.getPrintSupplier1())).findAny().orElse(null);
			secondaryPack1.setPrintSupplier(printSupplier);
			secondaryPack1.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			SecondaryPackLeadTimes secondaryPackLeadTimes = ConfigService.getSecondaryPackLeadTimesList().stream()
					.filter(obj -> Objects.nonNull(obj.getPackType()))
					.filter(obj -> obj.getPackType().equalsIgnoreCase(projectDetail.getPackFormat1())).findAny().orElse(null);
			secondaryPack1.setSecondaryPackLeadTime(secondaryPackLeadTimes);
			
			secondaryPack1.setRegsneeded(!util.isNullOrEmpty(projectDetail.getRegsNeeded1()) 
					&& projectDetail.getRegsNeeded1().equalsIgnoreCase("true") ?  true : false );
			secondaryPack1.setRequired(!util.isNullOrEmpty(projectDetail.getRequire1()) 
					&& projectDetail.getRequire1().equalsIgnoreCase("true") ?  true : false );
			
			secondaryPack1.setStatuscomments(projectDetail.getStatusComments1());
//			secondaryPack1.setId(secondaryPackRepository.getCurrentVal());
			secondaryPackList.add(secondaryPack1);
//			secondaryPackRepository.save(secondaryPack1);
//			entityManager.flush();
			logger.info("Sec Pack Details 1 " + secondaryPack1);
		}
		
		if(!util.isNullOrEmpty(projectDetail.getRequire2()) 
				&& projectDetail.getRequire2().equalsIgnoreCase("true")) {
		SecondaryPack secondaryPack2 = new SecondaryPack();
		secondaryPack2.setApproveddielineavailable(!util.isNullOrEmpty(projectDetail.getApprovedDielineAvailable2()) 
				&& projectDetail.getApprovedDielineAvailable2().equalsIgnoreCase("true") ?  true : false );
		secondaryPack2.setColourapproval(projectDetail.getColourApproval2());
		secondaryPack2.setDatedevelopmentcompleted(!util.isNullOrEmpty(projectDetail.getDateDevelopmentCompleted2()) 
				&& projectDetail.getDateDevelopmentCompleted2().indexOf("T") > 0 ? 
				Date.valueOf(projectDetail.getDateDevelopmentCompleted2().split("T")[0]) : null);
		secondaryPack2.setDateproofingcompleted(!util.isNullOrEmpty(projectDetail.getDateProofingCompletedSecondaryPackaging2())
				&& projectDetail.getDateProofingCompletedSecondaryPackaging2().indexOf("T") > 0 ? 
				Date.valueOf(projectDetail.getDateProofingCompletedSecondaryPackaging2().split("T")[0]) : null);
		secondaryPack2.setInvoiceapproved(projectDetail.getInvoiceApproval2());
		secondaryPack2.setItemnumber(!util.isNullOrEmpty(projectDetail.getSecPackItemNumber2()) ?
				Integer.parseInt(projectDetail.getSecPackItemNumber2()) :  null );
//		secondaryPack2.setMpmjobno(!util.isNullOrEmpty(projectDetail.getMPMJobNo2()) ?
//				Integer.parseInt(projectDetail.getMPMJobNo2()) : null);
		PrintSupplier printSupplier2 = ConfigService.getPrintSupplierList().stream()
				.filter(obj -> Objects.nonNull(obj.getSupplier()))
				.filter(obj -> obj.getSupplier().equalsIgnoreCase(projectDetail.getPrintSupplier2())).findAny().orElse(null);
		secondaryPack2.setPrintSupplier(printSupplier2);
		secondaryPack2.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
		
		SecondaryPackLeadTimes secondaryPackLeadTimes2 = ConfigService.getSecondaryPackLeadTimesList().stream()
				.filter(obj -> Objects.nonNull(obj.getPackType()))
				.filter(obj -> obj.getPackType().equalsIgnoreCase(projectDetail.getPackFormat2())).findAny().orElse(null);
		secondaryPack2.setSecondaryPackLeadTime(secondaryPackLeadTimes2);
		
		secondaryPack2.setRegsneeded(!util.isNullOrEmpty(projectDetail.getRegsNeeded2()) 
				&& projectDetail.getRegsNeeded2().equalsIgnoreCase("true") ?  true : false );
		secondaryPack2.setRequired(!util.isNullOrEmpty(projectDetail.getRequire2()) 
				&& projectDetail.getRequire2().equalsIgnoreCase("true") ?  true : false );
		
		secondaryPack2.setStatuscomments(projectDetail.getStatusComments2());
//		secondaryPack2.setId(secondaryPackRepository.getCurrentVal());
		secondaryPackList.add(secondaryPack2);
//		secondaryPackRepository.save(secondaryPack2);
//		entityManager.flush();
		logger.info("Sec Pack Details 2 " + secondaryPack2);
		}
		
		if(!util.isNullOrEmpty(projectDetail.getRequire3()) 
				&& projectDetail.getRequire3().equalsIgnoreCase("true")) {
		SecondaryPack secondaryPack3 = new SecondaryPack();
		secondaryPack3.setApproveddielineavailable(!util.isNullOrEmpty(projectDetail.getApprovedDielineAvailable3()) 
				&& projectDetail.getApprovedDielineAvailable3().equalsIgnoreCase("true") ?  true : false );
		secondaryPack3.setColourapproval(projectDetail.getColourApproval3());
		secondaryPack3.setDatedevelopmentcompleted(!util.isNullOrEmpty(projectDetail.getDateProofingCompletedSecondaryPackaging3()) 
				&& projectDetail.getDateProofingCompletedSecondaryPackaging3().indexOf("T") > 0 ? 
				Date.valueOf(projectDetail.getDateProofingCompletedSecondaryPackaging3().split("T")[0]) : null);
		secondaryPack3.setDateproofingcompleted(!util.isNullOrEmpty(projectDetail.getDateProofingCompletedSecondaryPackaging3()) 
				&& projectDetail.getDateProofingCompletedSecondaryPackaging3().indexOf("T") > 0 ? 
				Date.valueOf(projectDetail.getDateProofingCompletedSecondaryPackaging3().split("T")[0]) : null);
		secondaryPack3.setInvoiceapproved(projectDetail.getInvoiceApproval3());
		secondaryPack3.setItemnumber(!util.isNullOrEmpty(projectDetail.getSecPackItemNumber3()) ?
				Integer.parseInt(projectDetail.getSecPackItemNumber3()) :  null );
//		secondaryPack3.setMpmjobno(!util.isNullOrEmpty(projectDetail.getMPMJobNo3()) ?
//				Integer.parseInt(projectDetail.getMPMJobNo3()) : null);
		PrintSupplier printSupplier3 = ConfigService.getPrintSupplierList().stream()
				.filter(obj -> Objects.nonNull(obj.getSupplier()))
				.filter(obj -> obj.getSupplier().equalsIgnoreCase(projectDetail.getPrintSupplier3())).findAny().orElse(null);
		secondaryPack3.setPrintSupplier(printSupplier3);
		secondaryPack3.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
		
		SecondaryPackLeadTimes secondaryPackLeadTimes3 = ConfigService.getSecondaryPackLeadTimesList().stream()
				.filter(obj -> Objects.nonNull(obj.getPackType()))
				.filter(obj -> obj.getPackType().equalsIgnoreCase(projectDetail.getPackFormat3())).findAny().orElse(null);
		secondaryPack3.setSecondaryPackLeadTime(secondaryPackLeadTimes3);
		
		secondaryPack3.setRegsneeded(!util.isNullOrEmpty(projectDetail.getRegsNeeded3()) 
				&& projectDetail.getRegsNeeded3().equalsIgnoreCase("true") ?  true : false );
		secondaryPack3.setRequired(!util.isNullOrEmpty(projectDetail.getRequire3()) 
				&& projectDetail.getRequire3().equalsIgnoreCase("true") ?  true : false );
		
		secondaryPack3.setStatuscomments(projectDetail.getStatusComments3());
//		secondaryPack3.setId(secondaryPackRepository.getCurrentVal());
		secondaryPackList.add(secondaryPack3);
//		secondaryPackRepository.save(secondaryPack3);
//		entityManager.flush();
		logger.info("Sec Pack Details 3 " + secondaryPack3);
		}
		
		return secondaryPackList;
		
	}
	
	public ProjectOutputDetails createNpdProject(Integer projectId, Integer requestId, ProjectDetails projectDetail, 
			List<SecondaryPack> secondaryPackList, MPMProject project, Request request, RequestData requestData
			,TaskDetails taskData, Integer noOfLinkedMarkets) {
		ProjectOutputDetails projectOutputDetails = new ProjectOutputDetails();
		ProjectTeamList projectTeamList =  new ProjectTeamList();
//		Integer count = 0;
		NPDProject npdProject = new NPDProject();
//		npdProject.setId(NPDProjectRepository.getCurrentVal());
		npdProject.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
		npdProject.setMpmprojectid(String.valueOf(projectId));
		npdProject.setR_po_request_id(requestId);
		npdProject.setRequestid(requestId);
		npdProject.setTimelinerisk(projectDetail.getTimelineRisk());
		npdProject.setBottlercommunicationstatus(projectDetail.getBottlerCommunicationStatus());
		npdProject.setDescription(project.getDescription());
		npdProject.setIsfgproject(projectDetail.getFGOrConcModel().equalsIgnoreCase("true") ?  true : false);
		npdProject.setRisksummary(projectDetail.getRiskSummary());
		npdProject.setStatusandcriticalitems(projectDetail.getStatusAndCriticalItems());
		npdProject.setOpsaligneddpweek(projectDetail.getComTechOpsAlignedDate());
		npdProject.setComtechopsagreeddpweek(projectDetail.getTechOpsAgreedLatestView());
		npdProject.setNooflinkedmarkets(!util.isNullOrEmpty(noOfLinkedMarkets) 
				|| noOfLinkedMarkets == 0 ?
				String.valueOf(noOfLinkedMarkets) : "0");
		
		DraftManufacturingLocation finalManufacturingLocation = ConfigService.getDraftManufacturingLocationList().stream()
				.filter(obj -> Objects.nonNull(obj.getMarket()))
				.filter(obj -> Objects.nonNull(obj.getMarket().getName()))
				.filter(obj -> obj.getMarket().getName().equalsIgnoreCase(projectDetail.getFinalCountryOfManufacture()))
				.findAny().orElse(null);
		
		EarlyManufacturingSite finalManufacturingSite = ConfigService.getEarlyManufacturingSiteList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(projectDetail.getFinalManufacturingSite()))
				.findAny().orElse(null);
//		logger.info("getFinalProjectType " + projectDetail.getFinalProjectType());
		ProjectType projectType = ConfigService.getProjectTypeList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(projectDetail.getFinalProjectType())).findAny().orElse(null);
//		logger.info("projectType value  " + projectType.getDisplayName());
		
		ProjectClassifierData projectClassifier = ConfigService.getProjectClassifierList().stream()
				.filter(obj -> Objects.nonNull(obj.getProjectClassificationNumber()))
				.filter(obj -> obj.getProjectClassificationNumber().equalsIgnoreCase(projectDetail.getFinalProjectClassification()))
				.findAny().orElse(null);
		
		if(!util.isNullOrEmpty(finalManufacturingLocation)) {
			npdProject.setFinalmanufacturinglocation(finalManufacturingLocation.getMarket().getDisplayName());
			npdProject.setFinalmanufacturinglocationid(finalManufacturingLocation.getId());
			npdProject.setFinalmanufacturinglocationitemid(env.getProperty("npd.manufacturingLocation.itemId").concat(".")
					.concat(String.valueOf(finalManufacturingLocation.getId())));
		}
		
		if(!util.isNullOrEmpty(finalManufacturingSite)) {
			npdProject.setFinalmanufacturingsite(finalManufacturingSite.getDisplayName());
			npdProject.setFinalmanufacturingsiteid(finalManufacturingSite.getId());
			npdProject.setFinalmanufacturingsiteitemid(env.getProperty("npd.manufacturingSite.itemId").concat(".")
					.concat(String.valueOf(finalManufacturingSite.getId())));
		}
		
		if(!util.isNullOrEmpty(projectClassifier)) {
			npdProject.setProjectfinalclassification(Integer.parseInt(projectClassifier.getProjectClassificationNumber()));
			npdProject.setFinalprojectclassificationdescription(projectClassifier.getProjectClassificationDescription());
		}
		
		Boolean possibleNotfReq = null;
		
		if(!util.isNullOrEmpty(projectType)) {
			npdProject.setNewprojecttype(projectType.getDisplayName());
			npdProject.setNewprojecttypeitemid(env.getProperty("npd.projectType.itemId").concat(".")
					.concat(String.valueOf(projectType.getId())));
			possibleNotfReq = projectType.getDisplayName().equalsIgnoreCase("Commercial New SKU") ||
					projectType.getDisplayName().equalsIgnoreCase("Lift & Shift") ? true : false;
		}
		
		if(!util.isNullOrEmpty(npdProject.getProjectfinalclassification())) {
			npdProject.setNewfg(projectDetail.getFinalNewFG().equalsIgnoreCase("true") ? true :
				projectDetail.getFinalNewFG().equalsIgnoreCase("false") ? false : null);
		
			npdProject.setNewrdformula(projectDetail.getFinalnewRDFormula().equalsIgnoreCase("true") ? true :
				projectDetail.getFinalnewRDFormula().equalsIgnoreCase("false") ? false : null);
			
			npdProject.setNewhbcformula(projectDetail.getFinalNewHBCFormula().equalsIgnoreCase("true") ? true :
				projectDetail.getFinalNewHBCFormula().equalsIgnoreCase("false") ? false : null);
			
			npdProject.setNewprimarypackaging(projectDetail.getFinalNewPrimaryPackaging().equalsIgnoreCase("true") ? true :
				projectDetail.getFinalNewPrimaryPackaging().equalsIgnoreCase("false") ? false : null);
			
			npdProject.setNewsecondarypackaging(projectDetail.getFinalSecondaryPackaging());
			
			npdProject.setNewregdossier(request.getRegistrationDossier());
			
			npdProject.setNewpostproduction(request.getPostProductionRegistartion());
			
			npdProject.setNewpreproduction(request.getPreProductionRegistartion());
			
			possibleNotfReq = projectDetail.getFinalnewRDFormula().equalsIgnoreCase("true") || 
					projectDetail.getFinalNewPrimaryPackaging().equalsIgnoreCase("true") ||
					possibleNotfReq ==  true ? true : false;
//			npdProject.setNewclassificationrational(projectDetail);
		}
		
		OpenTextIdentity opsPlanner = ConfigService.getUsersList().stream()
				.filter(obj -> Objects.nonNull(obj.getId()))
				.filter(obj -> obj.getId() == Integer.parseInt(util.getPmOwnerIdentityId("Samantha"))).findAny().orElse(null);
		
		OpenTextIdentity gfg = ConfigService.getUsersList().stream()
				.filter(obj -> Objects.nonNull(obj.getId()))
				.filter(obj -> obj.getId() == Integer.parseInt(util.getPmOwnerIdentityId("Samantha"))).findAny().orElse(null);
		
		OpenTextIdentity projectSpecialist = ConfigService.getUsersList().stream()
				.filter(obj -> Objects.nonNull(obj.getId()))
				.filter(obj -> obj.getId() == Integer.parseInt(util.getPmOwnerIdentityId("Samantha"))).findAny().orElse(null);
		
		OpenTextIdentity regTech = ConfigService.getUsersList().stream()
				.filter(obj -> Objects.nonNull(obj.getId()))
				.filter(obj -> obj.getId() == Integer.parseInt(util.getPmOwnerIdentityId("Samantha"))).findAny().orElse(null);
		
		OpenTextIdentity regLead = ConfigService.getUsersList().stream()
				.filter(obj -> Objects.nonNull(obj.getId()))
				.filter(obj -> obj.getId() == Integer.parseInt(util.getPmOwnerIdentityId("Samantha"))).findAny().orElse(null);
		
		OpenTextIdentity commercialLead = ConfigService.getUsersList().stream()
				.filter(obj -> Objects.nonNull(obj.getId()))
				.filter(obj -> obj.getId() == Integer.parseInt(util.getPmOwnerIdentityId("Samantha"))).findAny().orElse(null);
		
		npdProject.setCorppm(request.getCorpPM().getIdentityDisplayName());
		npdProject.setCorppmitemid(env.getProperty("npd.users.itemId").concat(".").concat(String.valueOf(request.getCorpPM().getId())));
		npdProject.setE2epm(request.getE2ePM().getIdentityDisplayName());
		npdProject.setE2epmitemid(env.getProperty("npd.users.itemId").concat(".").concat(String.valueOf(request.getE2ePM().getId())));
		npdProject.setOpspm(request.getOpsPM().getIdentityDisplayName());
		npdProject.setOpspmitemid(env.getProperty("npd.users.itemId").concat(".").concat(String.valueOf(request.getOpsPM().getId())));
		
		projectTeamList.setE2ePm(request.getE2ePM());
		projectTeamList.setCorpPm(request.getCorpPM());
		projectTeamList.setOpsPm(request.getOpsPM());
		
		
		if(!util.isNullOrEmpty(opsPlanner)) {
			npdProject.setOpsplannerid(opsPlanner.getId());
			npdProject.setOpsplanneritemid(env.getProperty("npd.users.itemId").concat(".").concat(String.valueOf(opsPlanner.getId())));
			npdProject.setOpsplannername(opsPlanner.getIdentityDisplayName());
			projectTeamList.setOpsPlanner(opsPlanner);
		}
		
		if(!util.isNullOrEmpty(gfg)) {
			npdProject.setGfgid(gfg.getId());
			npdProject.setGfgitemid(env.getProperty("npd.users.itemId").concat(".").concat(String.valueOf(gfg.getId())));
			npdProject.setGfgname(gfg.getIdentityDisplayName());
			projectTeamList.setGfg(gfg);
		}
		
		if(!util.isNullOrEmpty(projectSpecialist)) {
			npdProject.setProjectspecialistid(projectSpecialist.getId());
			npdProject.setProjectspecialistname(projectSpecialist.getIdentityDisplayName());
			npdProject.setProjectspecialistitemid(env.getProperty("npd.users.itemId").concat(".").concat(String.valueOf(projectSpecialist.getId())));
			projectTeamList.setProjectSpecialist(projectSpecialist);
		}
		
		if(!util.isNullOrEmpty(regTech)) {
			npdProject.setRegionaltechnicalmanagerid(regTech.getId());
			npdProject.setRegionaltechnicalmanageritemid(env.getProperty("npd.users.itemId").concat(".").concat(String.valueOf(regTech.getId())));
			npdProject.setRegionaltechnicalmanagername(regTech.getIdentityDisplayName());
			projectTeamList.setRtm(regTech);
		}
		 
		logger.info("regionaltech id  " +  regTech.getId() + " - " +regTech.getIdentityDisplayName());
		
		if(!util.isNullOrEmpty(regLead)) {
			npdProject.setRegulatoryleadid(regLead.getId());
			npdProject.setRegulatoryleaditemid(env.getProperty("npd.users.itemId").concat(".").concat(String.valueOf(regLead.getId())));
			npdProject.setRegulatoryleadname(regLead.getIdentityDisplayName());
			projectTeamList.setRegLead(regLead);
		}
		
		if(!util.isNullOrEmpty(commercialLead)) {
			npdProject.setCommercialleadid(commercialLead.getId());
			npdProject.setCommercialleaditemid(env.getProperty("npd.users.itemId").concat(".").concat(String.valueOf(commercialLead.getId())));
			npdProject.setCommercialleadname(commercialLead.getIdentityDisplayName());	
			projectTeamList.setCommercialLead(commercialLead);
		}	
		
		npdProject.setPmreportingprojecttype(request.getCpProjectType().getDisplayName());
		npdProject.setPmreportingprojecttypeitemid(!util.isNullOrEmpty(request.getCpProjectType().getId()) ? env.getProperty("npd.cpProjectType.itemId").concat(".")
				.concat(String.valueOf(request.getCpProjectType().getId())) : "");
		npdProject.setPmreportingsubprojecttypeitemid(!util.isNullOrEmpty(request.getProjectSubType().getId()) ? env.getProperty("npd.subProjectType.itemId").concat(".")
				.concat(String.valueOf(request.getProjectSubType().getId())) : "");
		npdProject.setPmreportingsubprojecttype(request.getProjectSubType().getDisplayName());
		npdProject.setPmreportingprojectdetail(request.getProjectDetail().getDisplayName());
		npdProject.setPmreportingprojectdetailitemid(!util.isNullOrEmpty(request.getProjectDetail().getId()) ? env.getProperty("npd.projectDetail.itemId").concat(".")
				.concat(String.valueOf(request.getProjectDetail().getId())) : "");
		npdProject.setPmreportingdeliveryquarter(!util.isNullOrEmpty(request.getPMDeliveryQuarter()) 
				&& !util.isNullOrEmpty(request.getPMDeliveryQuarter().getDisplayName()) ? 
				request.getPMDeliveryQuarter().getDisplayName() : "");
		npdProject.setPmreportingdeliveryquarteritemid(!util.isNullOrEmpty(request.getPMDeliveryQuarter().getId()) 
				? env.getProperty("npd.deliveryQuarter.itemId").concat(".")
				.concat(String.valueOf(request.getPMDeliveryQuarter().getId())) : "");
		npdProject.setPmreportingother1(projectDetail.getOther1());
		npdProject.setPmreportingother2(projectDetail.getOther2());
		npdProject.setPmreportingcomments(projectDetail.getComments());
		
		npdProject.setNsvy1annualised(requestData.getNSV());
		npdProject.setVolume1st3months(requestData.getVolume3Months());
		npdProject.setVolumey1annualised(requestData.getVolume1Year());
		npdProject.setCp1agreementdate(projectDetail.getCP1DeliveryWeek());
		Integer cp1Phasedelay = !util.isNullOrEmpty(projectDetail.getProgramManagerCP1PhaseDelay()) &&
				Integer.parseInt(projectDetail.getProgramManagerCP1PhaseDelay()) > 0 ? 
						Integer.parseInt(projectDetail.getProgramManagerCP1PhaseDelay()) / 7 : 0;		
		npdProject.setProgrammemanagercp1phasedelay(cp1Phasedelay);
		
		npdProject.setCp1reviseddate(projectDetail.getCP1RevisedDate());
		npdProject.setNpdwasontrackfororiginalcp1date(projectDetail.getNPDWasOnTrackForOriginalCp1Date().equalsIgnoreCase("true") ? true : false);
		npdProject.setBottlerpushedoutproductionwithoutlaunchimpact(projectDetail.getBottlerPushedOutProductionWithoutLaunchImpact().equalsIgnoreCase("true") ?  true : false);
		npdProject.setMeccommercialbottlerrequesteddelaytolaunch(projectDetail.getMECCommercialBottlerRequestedDelayToLaunch().equalsIgnoreCase("true") ?  true : false);
		npdProject.setOthertbc(projectDetail.getOtherTbc().equalsIgnoreCase("true") ?  true : false);
		npdProject.setOther2tbc(projectDetail.getOther2Tbc().equalsIgnoreCase("true") ?  true : false);
		npdProject.setHasthechangebeenalignedviacpmtg(projectDetail.getHasTheChangeBeenAlignedViaCPMtg().equalsIgnoreCase("true") ?  true : false);
		npdProject.setBriefdescriptionofdelayprogrammemanager(projectDetail.getBriefDescOfDelay());
		
		npdProject.setWinshuttlenumber(projectDetail.getWinshuttleProjectNumber());
		npdProject.setFgmaterialcode(projectDetail.getFGMaterialCode());
		npdProject.setFormulahbcnumber(projectDetail.getFormulaHBC());
		npdProject.setCansleeveprimarybottlematerial(projectDetail.getCanSleeveOrPrimaryBottler());
		
		npdProject.setEantargetweek(projectDetail.getEANCodeSharedWithBottlerTarget());
		npdProject.setEanactualweek(projectDetail.getEANCodesSharedWithBottlerActual());
		npdProject.setProdinfotargetweek(projectDetail.getProdInfoSheetSharedTarget());
		npdProject.setProdinfoactualweek(projectDetail.getProdInfoSheetSharedActual());
		npdProject.setLabeluploadedactualdate(projectDetail.getLabelUplaodedToCanCompanyDate());
		npdProject.setBottlerspecificcomments(projectDetail.getBottlerSpecificComments());
		npdProject.setRegulatorycomments(projectDetail.getRegulatoryComments());
		npdProject.setHowmanypieces(Integer.parseInt(projectDetail.getNoOfPiecesRequired()));
		
		npdProject.setSiteauditrequired(projectDetail.getSiteAuditRequired().equalsIgnoreCase("true") ? true : false);
		npdProject.setAuditdate(projectDetail.getAuditDate());
		npdProject.setSiteapproved(projectDetail.getSiteApproved().equalsIgnoreCase("true") ? true : false);
		npdProject.setPlantspecificcomments(projectDetail.getPlantSpecificBatchingInstructionsComments());
		npdProject.setPlantspecificreapplicationonly(projectDetail.getPlantSpecificBatchingInstructionsReapplicationOnly());
		npdProject.setIsplantspecificreapplicationonly(projectDetail.getPlantSpecificBatchingInstructionsReapplicationOnly().equalsIgnoreCase("true") ? true : false);
		npdProject.setProtocolissued(projectDetail.getProtocolIssued());
		npdProject.setQaqcreleasedate(projectDetail.getQAQAReleaseDate());
		npdProject.setTrialorcommentsoractionsrequired(projectDetail.getTrialOverallComments());
		npdProject.setTechqualcompleted(projectDetail.getSiteSetupCompleted());	
		npdProject.setOnsiteremotenotrequired(projectDetail.getOnsiteRemoteNotRequired());		
		
//		logger.info("auditUser"  + " " + projectDetail.getWhoWillAudit());
//		logger.info("techQualUsers" + " " + projectDetail.getWhoWillComplete());
//		logger.info("trialProtocolWrittenBy" + " " + projectDetail.getTrialProtocolWrittenBy());
//		logger.info("trialSupervisionNPD" + " " + projectDetail.getTrialNPD());
//		logger.info("trialSupervisionQuality"  + " " + projectDetail.getTrialQuality());
//		logger.info("tastingRequirement"  + " " + projectDetail.getTastingRequirement());
		AuditUsers auditUser = ConfigService.getAuditUsersList().stream()
				.filter(obj -> Objects.nonNull(obj.getUsers()))
				.filter(obj -> obj.getUsers().equalsIgnoreCase(projectDetail.getWhoWillAudit())).findAny().orElse(null);
		TechQualUsers techQualUsers = ConfigService.getTechQualUsersList().stream()
				.filter(obj -> Objects.nonNull(obj.getUsers()))
				.filter(obj -> obj.getUsers().equalsIgnoreCase(projectDetail.getWhoWillComplete())).findAny().orElse(null);
		TrialProtocolWrittenBy trialProtocolWrittenBy = ConfigService.getTrialProtocolWrittenByList().stream()
				.filter(obj -> Objects.nonNull(obj.getUsers()))
				.filter(obj -> obj.getUsers().equalsIgnoreCase(projectDetail.getTrialProtocolWrittenBy())).findAny().orElse(null);
		TrialSupervisionNPD trialSupervisionNPD = ConfigService.getTrialSupervisionNPDList().stream()
				.filter(obj -> Objects.nonNull(obj.getUsers()))
				.filter(obj -> obj.getUsers().equalsIgnoreCase(projectDetail.getTrialNPD())).findAny().orElse(null);
		TrialSupervisionQuality trialSupervisionQuality = ConfigService.getTrialSupervisionQualityList().stream()
				.filter(obj -> Objects.nonNull(obj.getUsers()))
				.filter(obj -> obj.getUsers().equalsIgnoreCase(projectDetail.getTrialQuality())).findAny().orElse(null);
		TastingRequirement tastingRequirement = ConfigService.getTastingRequirementList().stream()
				.filter(obj -> Objects.nonNull(obj.getTastingRequirement()))
				.filter(obj -> obj.getTastingRequirement().equalsIgnoreCase(projectDetail.getTastingRequirement())).findAny().orElse(null);
//		logger.info("auditUser" + auditUser.getUsers() + " " + projectDetail.getWhoWillAudit());
//		logger.info("techQualUsers" + techQualUsers.getUsers() + " " + projectDetail.getWhoWillComplete());
//		logger.info("trialProtocolWrittenBy" + trialProtocolWrittenBy.getUsers() + " " + projectDetail.getTrialProtocolWrittenBy());
//		logger.info("trialSupervisionNPD" + trialSupervisionNPD.getUsers() + " " + projectDetail.getTrialNPD());
//		logger.info("trialSupervisionQuality" + trialSupervisionQuality.getUsers() + " " + projectDetail.getTrialQuality());
//		logger.info("tastingRequirement" + auditUser.getUsers() + " " + projectDetail.getTastingRequirement());
		if(!util.isNullOrEmpty(auditUser)) {
			npdProject.setWhowillaudit(auditUser.getUsers());
			npdProject.setWhowillaudititemid(env.getProperty("npd.auditUsers.itemId").concat(".")
					.concat(String.valueOf(auditUser.getId())));
		}
		if(!util.isNullOrEmpty(techQualUsers)) {
			npdProject.setTechqualwhowillcomplete(techQualUsers.getUsers());
			npdProject.setTechqualwhowillcompleteitemid(env.getProperty("npd.techQualUsers.itemId").concat(".")
					.concat(String.valueOf(techQualUsers.getId())));		
		}
		if(!util.isNullOrEmpty(trialProtocolWrittenBy)) {
			npdProject.setTrialprotocolwrittenby(trialProtocolWrittenBy.getUsers());
			npdProject.setTrialprotocolwrittenbyitemid(env.getProperty("npd.trialProtocolWrittenBy.itemId").concat(".")
					.concat(String.valueOf(trialProtocolWrittenBy.getId())));
		}
		if(!util.isNullOrEmpty(trialSupervisionNPD)) {
			npdProject.setTrialsupervisionnpd(trialSupervisionNPD.getUsers());
			npdProject.setTrialsupervisionnpditemid(env.getProperty("npd.trialSupervisionNPD.itemId").concat(".")
					.concat(String.valueOf(trialSupervisionNPD.getId())));
		}
		if(!util.isNullOrEmpty(trialSupervisionQuality)) {
			npdProject.setTrialsupervisionquality(trialSupervisionQuality.getUsers());
			npdProject.setTrialsupervisionqualityitemid(env.getProperty("npd.trialSupervisionQuality.itemId").concat(".")
					.concat(String.valueOf(trialSupervisionQuality.getId())));
		}
		if(!util.isNullOrEmpty(tastingRequirement)) {
			npdProject.setTastingrequirement(tastingRequirement.getTastingRequirement());
			npdProject.setTastingrequirementitemid(env.getProperty("npd.tastingRequirement.itemId").concat(".")
					.concat(String.valueOf(tastingRequirement.getId())));
		}
		
		npdProject.setTrialmaterialsconfirmeddeliveryweek(projectDetail.getTrialMaterialsConfirmedDeliveryWeek());
		npdProject.setTrialdateconfirmedwithbottler(projectDetail.getTrialDateConfirmedWithBottler());
//		npdProject.setFirstprodmaterialsconfirmeddelivery(projectDetail); No mapping field
		npdProject.setFinalagreed1stprofconfirmedwithbottler(projectDetail.getFinalAgreed1stProd());		
		npdProject.setAllowproductionschedulelessthan8weeks(projectDetail.getAllowProdSchedule().equalsIgnoreCase("true") ?  true : false);
		npdProject.setLocalbominsap(projectDetail.getLocalBOMInSAp());		
		npdProject.setBomsentactualdate(projectDetail.getBomSentActualDate());
		npdProject.setPalletlabelreceivedactualweek(projectDetail.getPalletLabelReceivedActual());
		npdProject.setPalletlabelsentactualweek(projectDetail.getPalletLabelSentActual());
		npdProject.setPoreceived(projectDetail.getPOReceivedDate());	
		npdProject.setBottlerandcanactualweek(projectDetail.getBottlerActualWeek());
		npdProject.setItemnumber(projectDetail.getBottlerSpecificItemNumber());
		npdProject.setPalletlabelformat(projectDetail.getPalletLabelFormat());
		npdProject.setTraylabelformat(projectDetail.getTrayLabelFormat());
		npdProject.setHbcissuancetype(projectDetail.getHBCIssuanceType());
		
		npdProject.setPossiblenotificationrequirement(!util.isNullOrEmpty(possibleNotfReq) &&  possibleNotfReq == true ? "true" : "false");
		npdProject.setRequiresprimarycanaw(!util.isNullOrEmpty(taskData.getTask17Duration()) 
				&& Integer.parseInt(taskData.getTask17Duration())  > 0 ? true : false);
		npdProject.setCanprimaryawstart(!util.isNullOrEmpty(taskData.getTask17IsActive()) 
				&& taskData.getTask17IsActive().equalsIgnoreCase("true") ? true : false);
		npdProject.setCansecondaryawstart(!util.isNullOrEmpty(taskData.getTask18IsActive()) 
				&& taskData.getTask18IsActive().equalsIgnoreCase("true") ? true : false);
		npdProject.setRequiressecondaryaw(!util.isNullOrEmpty(taskData.getTask18Duration()) 
				&& Integer.parseInt(taskData.getTask18Duration())  > 0 ? true : false);
		npdProject.setAllawtaskscompleted(taskData.getTask17IsCompleted().equalsIgnoreCase("true")
				&& taskData.getTask20IsCompleted().equalsIgnoreCase("true") ?  true : false);
		
		npdProject.setTrialorfirstprodincludedinplan((!util.isNullOrEmpty(taskData.getTask39Duration()) &&
				Integer.parseInt(taskData.getTask39Duration()) > 0 ) ||
				(!util.isNullOrEmpty(taskData.getTask39Duration()) && Integer.parseInt(taskData.getTask39Duration()) > 0)
				? "true" : "false");
		
		
		logger.info("NameOfCanCompanys() " + projectDetail.getNameOfCanCompanys());
		Integer noOfCanCompany = 0;
		int count = 0;
		String canCompanyIds = "";
		List<String> canCompanyValuesList = new ArrayList<String>(Arrays.asList(projectDetail.getNameOfCanCompanys().split(",")));
		for(String canCompany : canCompanyValuesList) {
			noOfCanCompany++;
			count++;
			CanSupplierData canSupplierData = ConfigService.getCanSupplierDataList().stream()
					.filter(obj -> Objects.nonNull(obj.getCanCompany()))
					.filter(obj -> obj.getCanCompany().equalsIgnoreCase(canCompany)).findAny().orElse(null);
			
			if(!util.isNullOrEmpty(canSupplierData)) {
				canCompanyIds = canCompanyIds.concat(String.valueOf(canSupplierData.getId()));
			}
			if(count != canCompanyValuesList.size()) {
				canCompanyIds = canCompanyIds.concat(",");
			}
		}
		logger.info("noOfCanCompany " + noOfCanCompany);
		logger.info("canCompanyIds " + canCompanyIds);
		
		npdProject.setCancompanies(projectDetail.getNameOfCanCompanys());
		npdProject.setCancompanyids(canCompanyIds);
		npdProject.setNameofcancompany(projectDetail.getNameOfCanCompanys());
		npdProject.setNumberofcancompaniesrequired(noOfCanCompany);
		
		
// ---------------------------------------------------------------------------------


//		

//		npdProject.setOther2tbc(other2tbc);
//		npdProject.setOthertbc(othertbc);
//		npdProject.setHasthechangebeenalignedviacpmtg(hasthechangebeenalignedviacpmtg);
//		npdProject.setComtechopsagreeddpweek(comtechopsagreeddpweek);
//		npdProject.setCp1actualdate(cp1actualdate);
//		npdProject.setCp2actualdate(cp2actualdate);
//		npdProject.setCp3actualdate(cp3actualdate);
//		npdProject.setCp4actualdate(cp4actualdate);
//		npdProject.setCurrentcommercialaligneddate(currentcommercialaligneddate);
//		npdProject.setDescription(description);
//		npdProject.setEarliesttrialor1stproddate(earliesttrialor1stproddate);
//		npdProject.setGovernanceapproach(governanceapproach);
//		npdProject.setHbcrpemsebcreated(hbcrpemsebcreated);
//		npdProject.setIscp1completed(iscp1completed);
//		npdProject.setIsfgproject(isfgproject);
//		npdProject.setIsprojectmigrationcompleted(isprojectmigrationcompleted);
//		npdProject.setIsprojectmigrationcompleted(isprojectmigrationcompleted);
//		npdProject.setIssecondarypackagingdevelopmentactive(issecondarypackagingdevelopmentactive);
//		npdProject.setIssecondarypackagingproofingactive(issecondarypackagingproofingactive);
//		npdProject.setIstask4completed(istask4completed);
//		npdProject.setIstask5completed(istask5completed);
//		npdProject.setLeadmarket(leadmarket);
//		npdProject.setLeadmarketid(leadmarketid);
//		npdProject.setLeadtimemarket(leadtimemarket);
//		npdProject.setNoofpiecesrequired(noofpiecesrequired);
//		npdProject.setNoofweeksincrease(noofweeksincrease);
//		npdProject.setNpdwasontrackfororiginalcp1date(npdwasontrackfororiginalcp1date);
//		npdProject.setOpsaligneddpweek(opsaligneddpweek);
//		npdProject.setOpsaligneddpyear(opsaligneddpyear);
//		
//		npdProject.setPalletlabelreceivedtargetweek(palletlabelreceivedtargetweek);
//		npdProject.setPalletlabelsenttargetweek(palletlabelsenttargetweek);
//		npdProject.setPlantspecificreapplicationonly(plantspecificreapplicationonly);
//		npdProject.setPlantspecificresponsibleforcompletion(plantspecificresponsibleforcompletion);
//		npdProject.setPmslacktime(pmslacktime);
//		npdProject.setPoreceivedactualweek(poreceivedactualweek);
//		npdProject.setPrintsupplier(printsupplier);
//		npdProject.setPrintsupplieritemid(printsupplieritemid);
//		npdProject.setProjectbusinessid(projectbusinessid);
//		npdProject.setProjecthealth(projecthealth);
//		npdProject.setProjectspecialistid(projectspecialistid);
//		npdProject.setProjectspecialistitemid(projectspecialistitemid);
//		npdProject.setProjectspecialistname(projectspecialistname);
//		npdProject.setR_po_secondary_pack_lead_time_id(r_po_secondary_pack_lead_time_id);
//		npdProject.setRag(rag);

//		npdProject.setRisksummary(risksummary);
//		npdProject.setSecondarypackrequired(secondarypackrequired);

//		npdProject.setSiteauditrequired(siteauditrequired);
//		npdProject.setStatusandcriticalitems(statusandcriticalitems);
//		npdProject.setTargetcp2date(targetcp2date);
//		npdProject.setTechorqualsupervisionrequired(techorqualsupervisionrequired);
//		npdProject.setTechqualweekdue(techqualweekdue);
//		
//		npdProject.setTrialandorcommercialproductionincluded(trialandorcommercialproductionincluded);
//		npdProject.setTrialorfirstprodincludedinplan(trialorfirstprodincludedinplan);

//		npdProject.getSecondaryPack().addAll(secondaryPackList);
		logger.info("Npd Project Data " + npdProject);
		NPDProject npdProjectResponse = NPDProjectRepository.save(npdProject);
		
		projectOutputDetails.setNpdProject(npdProjectResponse);
		projectOutputDetails.setProjectTeamList(projectTeamList);
		return projectOutputDetails;
	}
	
	
	public String createOTMMFolder(MPMProject project, String ownerName, String UserName, String Password) {
		String otmmFolderId = "";
		Sessions sessions = otmmAPIService.createSession(UserName ,Password);
		
		List<SecurityPolicyList> security_policy_list =  new ArrayList<SecurityPolicyList>();
		SecurityPolicyList securityPolicyList =  new SecurityPolicyList();
		securityPolicyList.setId(env.getProperty("npd.otmm.folder.sec.policy.id"));
		security_policy_list.add(securityPolicyList);
		
		FolderResourceContainer folderResourceContainer = new FolderResourceContainer();
		FolderResource folderResource = new FolderResource();
		Folder folder = new Folder();
		Metadata metadata =  new Metadata();
		List<MetadataField> metadata_element_list = new ArrayList<MetadataField>() ;
		
		MetadataField projectNameMetadataField = new MetadataField();
		projectNameMetadataField.setId("MPM.PROJECT.NAME");
		projectNameMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer projectNameValue = new MetadataValueContainer();
		MetadataValue projectNameMetadataValue = new MetadataValue();
		projectNameMetadataValue.setType("string");
		projectNameMetadataValue.setValue(project.getProjectName());
		projectNameValue.setValue(projectNameMetadataValue);
		projectNameMetadataField.setValue(projectNameValue);
		metadata_element_list.add(projectNameMetadataField);
		
		MetadataField priorityMetadataField = new MetadataField();
		priorityMetadataField.setId("MPM.PROJECT.PRIORITY");
		priorityMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer priorityValue = new MetadataValueContainer();
		MetadataValue priorityMetadataValue = new MetadataValue();
		priorityMetadataValue.setType("string");
		priorityMetadataValue.setValue("HIGH");
		priorityValue.setValue(priorityMetadataValue);
		priorityMetadataField.setValue(priorityValue);
		metadata_element_list.add(priorityMetadataField);
		
		MetadataField projectTypeMetadataField = new MetadataField();
		projectTypeMetadataField.setId("MPM.PROJECT.TYPE");
		projectTypeMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer projectTypeValue = new MetadataValueContainer();
		MetadataValue projectTypeMetadataValue = new MetadataValue();
		projectTypeMetadataValue.setType("string");
		projectTypeMetadataValue.setValue("PROJECT");
		projectTypeValue.setValue(projectTypeMetadataValue);
		projectTypeMetadataField.setValue(projectTypeValue);
		metadata_element_list.add(projectTypeMetadataField);
		
		MetadataField projectIdMetadataField = new MetadataField();
		projectIdMetadataField.setId("MPM.PROJECT.PROJECT_ID");
		projectIdMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer projectIdValue = new MetadataValueContainer();
		MetadataValue projectIdMetadataValue = new MetadataValue();
		projectIdMetadataValue.setType("string");
		projectIdMetadataValue.setValue(String.valueOf(project.getId()));
		projectIdValue.setValue(projectIdMetadataValue);
		projectIdMetadataField.setValue(projectIdValue);
		metadata_element_list.add(projectIdMetadataField);
		
		MetadataField progressMetadataField = new MetadataField();
		progressMetadataField.setId("MPM.PROJECT.PROGRESS");
		progressMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer progressValue = new MetadataValueContainer();
		MetadataValue progressMetadataValue = new MetadataValue();
		progressMetadataValue.setType("string");
		progressMetadataValue.setValue("0");
		progressValue.setValue(progressMetadataValue);
		progressMetadataField.setValue(progressValue);
		metadata_element_list.add(progressMetadataField);
		
		MetadataField descriptionMetadataField = new MetadataField();
		descriptionMetadataField.setId("MPM.PROJECT.DESCRIPTION");
		descriptionMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer descriptionValue = new MetadataValueContainer();
		MetadataValue descriptionMetadataValue = new MetadataValue();
		descriptionMetadataValue.setType("string");
		descriptionMetadataValue.setValue(project.getDescription());
		descriptionValue.setValue(descriptionMetadataValue);
		descriptionMetadataField.setValue(descriptionValue);
		metadata_element_list.add(descriptionMetadataField);
		
		MetadataField dueDateMetadataField = new MetadataField();
		dueDateMetadataField.setId("MPM.PROJECT.DUE_DATE");
		dueDateMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer dueDateValue = new MetadataValueContainer();
		MetadataValue dueDateMetadataValue = new MetadataValue();
		dueDateMetadataValue.setType("dateTime");
		dueDateMetadataValue.setValue(!util.isNullOrEmpty(project.getDueDate()) ?
				project.getDueDate().toString().concat("T00:00:00Z") : "");
		dueDateValue.setValue(dueDateMetadataValue);
		dueDateMetadataField.setValue(dueDateValue);
		metadata_element_list.add(dueDateMetadataField);
		
		MetadataField endDateMetadataField = new MetadataField();
		endDateMetadataField.setId("MPM.PROJECT.END_DATE");
		endDateMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer endDateValue = new MetadataValueContainer();
		MetadataValue endDateMetadataValue = new MetadataValue();
		endDateMetadataValue.setType("dateTime");
		endDateMetadataValue.setValue(!util.isNullOrEmpty(project.getDueDate()) ?
				project.getDueDate().toString().concat("T00:00:00Z") : "");
		endDateValue.setValue(endDateMetadataValue);
		endDateMetadataField.setValue(endDateValue);
		metadata_element_list.add(endDateMetadataField);
		
		MetadataField isActiveMetadataField = new MetadataField();
		isActiveMetadataField.setId("MPM.PROJECT.IS_ACTIVE");
		isActiveMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer isActiveValue = new MetadataValueContainer();
		MetadataValue isActiveMetadataValue = new MetadataValue();
		isActiveMetadataValue.setType("string");
		isActiveMetadataValue.setValue("true");
		isActiveValue.setValue(isActiveMetadataValue);
		isActiveMetadataField.setValue(isActiveValue);
		metadata_element_list.add(isActiveMetadataField);
		
		MetadataField isDeletedMetadataField = new MetadataField();
		isDeletedMetadataField.setId("MPM.PROJECT.IS_DELETED");
		isDeletedMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer isDeletedValue = new MetadataValueContainer();
		MetadataValue isDeletedMetadataValue = new MetadataValue();
		isDeletedMetadataValue.setType("string");
		isDeletedMetadataValue.setValue("false");
		isDeletedValue.setValue(isDeletedMetadataValue);
		isDeletedMetadataField.setValue(isDeletedValue);
		metadata_element_list.add(isDeletedMetadataField);
		
		MetadataField startDateMetadataField = new MetadataField();
		startDateMetadataField.setId("MPM.PROJECT.START_DATE");
		startDateMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer startDateValue = new MetadataValueContainer();
		MetadataValue startDateMetadataValue = new MetadataValue();
		startDateMetadataValue.setType("dateTime");
		startDateMetadataValue.setValue(project.getStartDate().toString().concat("T00:00:00Z"));
		startDateValue.setValue(startDateMetadataValue);
		startDateMetadataField.setValue(startDateValue);
		metadata_element_list.add(startDateMetadataField);
		
		MetadataField categoryMetadataField = new MetadataField();
		categoryMetadataField.setId("MPM.PROJECT.CATEGORY");
		categoryMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer categoryValue = new MetadataValueContainer();
		MetadataValue categoryMetadataValue = new MetadataValue();
		categoryMetadataValue.setType("string");
		categoryMetadataValue.setValue(env.getProperty("npd.project.category.name"));
		categoryValue.setValue(categoryMetadataValue);
		categoryMetadataField.setValue(categoryValue);
		metadata_element_list.add(categoryMetadataField);
		
		MetadataField ownerMetadataField = new MetadataField();
		ownerMetadataField.setId("MPM.PROJECT.PROJECT_OWNER");
		ownerMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer ownerValue = new MetadataValueContainer();
		MetadataValue ownerMetadataValue = new MetadataValue();
		ownerMetadataValue.setType("string");
		ownerMetadataValue.setValue(ownerName);
		ownerValue.setValue(ownerMetadataValue);
		ownerMetadataField.setValue(ownerValue);
		metadata_element_list.add(ownerMetadataField);
		
		MetadataField statusMetadataField = new MetadataField();
		statusMetadataField.setId("MPM.PROJECT.STATUS");
		statusMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer statusValue = new MetadataValueContainer();
		MetadataValue statusMetadataValue = new MetadataValue();
		statusMetadataValue.setType("string");
		statusMetadataValue.setValue(env.getProperty("npd.project.initial.status.name"));
		statusValue.setValue(statusMetadataValue);
		statusMetadataField.setValue(statusValue);
		metadata_element_list.add(statusMetadataField);
		
		MetadataField statusTypeMetadataField = new MetadataField();
		statusTypeMetadataField.setId("MPM.PROJECT.STATUS_TYPE");
		statusTypeMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer statusTypeValue = new MetadataValueContainer();
		MetadataValue statusTypeMetadataValue = new MetadataValue();
		statusTypeMetadataValue.setType("string");
		statusTypeMetadataValue.setValue(env.getProperty("npd.project.initial.status.type"));
		statusTypeValue.setValue(statusTypeMetadataValue);
		statusTypeMetadataField.setValue(statusTypeValue);
		metadata_element_list.add(statusTypeMetadataField);
		
		MetadataField teamMetadataField = new MetadataField();
		teamMetadataField.setId("MPM.PROJECT.TEAM");
		teamMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer teamValue = new MetadataValueContainer();
		MetadataValue teamMetadataValue = new MetadataValue();
		teamMetadataValue.setType("string");
		teamMetadataValue.setValue(env.getProperty("npd.project.team.name"));
		teamValue.setValue(teamMetadataValue);
		teamMetadataField.setValue(teamValue);
		metadata_element_list.add(teamMetadataField);
		
		MetadataField itemIdMetadataField = new MetadataField();
		itemIdMetadataField.setId("MPM.PROJECT.ITEM_ID");
		itemIdMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer itemIdValue = new MetadataValueContainer();
		MetadataValue itemIdMetadataValue = new MetadataValue();
		itemIdMetadataValue.setType("string");
		itemIdMetadataValue.setValue(env.getProperty("npd.project.itemId").concat(".").concat(String.valueOf(project.getId())));
		itemIdValue.setValue(itemIdMetadataValue);
		itemIdMetadataField.setValue(itemIdValue);
		metadata_element_list.add(itemIdMetadataField);
		
		MetadataField originalStartDateMetadataField = new MetadataField();
		originalStartDateMetadataField.setId("MPM.PROJECT.ORIGINAL_START_DATE");
		originalStartDateMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer originalStartDateValue = new MetadataValueContainer();
		MetadataValue originalStartDateMetadataValue = new MetadataValue();
		originalStartDateMetadataValue.setType("dateTime");
		originalStartDateMetadataValue.setValue(project.getStartDate().toString().concat("T00:00:00Z"));
		originalStartDateValue.setValue(originalStartDateMetadataValue);
		originalStartDateMetadataField.setValue(originalStartDateValue);
		metadata_element_list.add(originalStartDateMetadataField);
		
		MetadataField originalDueDateMetadataField = new MetadataField();
		originalDueDateMetadataField.setId("MPM.PROJECT.ORIGINAL_DUE_DATE");
		originalDueDateMetadataField.setType("com.artesia.metadata.MetadataField");
		MetadataValueContainer originalDueDateValue = new MetadataValueContainer();
		MetadataValue originalDueDateMetadataValue = new MetadataValue();
		originalDueDateMetadataValue.setType("dateTime");
		originalDueDateMetadataValue.setValue(!util.isNullOrEmpty(project.getDueDate()) ?
				project.getDueDate().toString().concat("T00:00:00Z") : "");
		originalDueDateValue.setValue(originalDueDateMetadataValue);
		originalDueDateMetadataField.setValue(originalDueDateValue);
		metadata_element_list.add(originalDueDateMetadataField);
		
		metadata.setMetadata_element_list(metadata_element_list);

		folder.setContainer_type_id("MEC.NPD.FOLDER");
		folder.setMetadata(metadata);
		folder.setMetadata_model_id("MEC.MODEL.NPD");
		folder.setName(project.getProjectName());
		folder.setSecurity_policy_list(security_policy_list);
		
		folderResource.setFolder(folder);
		folderResourceContainer.setFolder_resource(folderResource);
		
		
		try {
   			String createFolderResponse  = otmmAPIService.createFolder("", sessions, 
   					folderResourceContainer, env.getProperty("npd.otmm.rootFolder.id"));
   			logger.info("folder id : " + createFolderResponse);
   			ObjectMapper objMapper = new ObjectMapper();
   			FolderResourceContainer response = objMapper.readValue(createFolderResponse, FolderResourceContainer.class);
   			otmmFolderId = response.getFolder_resource().getFolder().getAsset_id();
		}catch(Exception e)
		{
			
		}
		return otmmFolderId;
	}
	


}
