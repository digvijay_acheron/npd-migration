package com.monster.npd.migration.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.monster.npd.migration.model.MPMDeliverable;
import com.monster.npd.migration.model.MPMDeliverableSchema;
import com.monster.npd.migration.model.MPMStatus;
import com.monster.npd.migration.model.MPMTaskSchema;
import com.monster.npd.migration.model.MPMTeamRole;
import com.monster.npd.migration.model.OpenTextIdentity;
import com.monster.npd.migration.repository.DeliverableSolrRepository;
import com.monster.npd.migration.repository.MPMDeliverableRepository;

@Service
public class DeliverableMigrationService {
	
//	private static final Logger logger = LoggerFactory.getLogger(DeliverableMigrationService.class);
	private static final Logger logger = LogManager.getLogger(DeliverableMigrationService.class);
	
	@Autowired
	Environment env;
	
	@Autowired
	MPMDeliverableRepository MPMDeliverableRepository;
	
	@Autowired
	DeliverableSolrRepository deliverableSolrRepository;
	
	@Autowired
	EntityManager entityManager;
	
	@Autowired
	UtilService util;
	
	@Transactional
	public void createDeliverable(List<MPMTaskSchema> taskSchemaList, Integer projectId) {
		
		List<MPMDeliverable> MPMDeliverableList = new ArrayList<MPMDeliverable>();
		
		for(MPMTaskSchema MPMTaskSchema : taskSchemaList) {
			Integer statusId = 0;
			if(MPMTaskSchema.getTask_status_type().equalsIgnoreCase("INITIAL")) {
				statusId = Integer.parseInt(env.getProperty("npd.deliverable.initial.status.id"));
			}else {
				statusId = Integer.parseInt(env.getProperty("npd.deliverable.completed.status.id"));
			}
		
			
			MPMDeliverable deliverable1 = new MPMDeliverable();
			deliverable1.setDescription(MPMTaskSchema.getTask_description());
			deliverable1.setDueDate(Date.valueOf(MPMTaskSchema.getTask_due_date().split("T")[0]));
			deliverable1.setIsActive(MPMTaskSchema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			deliverable1.setIsDeleted(false);
			deliverable1.setIterationCount(0);
			deliverable1.setName(MPMTaskSchema.getTask_name());
			deliverable1.setStartDate(Date.valueOf(MPMTaskSchema.getTask_start_date().split("T")[0]));
			deliverable1.setDeliverableType("COMPLETE");
			deliverable1.setOwnerAssignmentType("ROLE");
			deliverable1.setPriorityId(Integer.parseInt(env.getProperty("npd.deliverable.high.priority.id")));
			deliverable1.setStatusId(statusId);
			deliverable1.setTaskId(Integer.parseInt(MPMTaskSchema.getId()));
			deliverable1.setProjectId(projectId);
			deliverable1.setOwnerRoleId(MPMTaskSchema.getTask_role_id());
			deliverable1.setOwnerUserId(MPMTaskSchema.getTask_owner_id());
			deliverable1.setActiveUserId(MPMTaskSchema.getTask_owner_id());
			deliverable1.setActiveRoleId(MPMTaskSchema.getTask_role_id());
			deliverable1.setIsPMAssigned(true);
			deliverable1.setOriginalStartDate(Date.valueOf(MPMTaskSchema.getOriginal_task_start_date().split("T")[0]));
			deliverable1.setOriginalDueDate(Date.valueOf(MPMTaskSchema.getOriginal_task_due_date().split("T")[0]));
			deliverable1.setDeliverableDuration(MPMTaskSchema.getTask_duration()*7);
			deliverable1.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
//			deliverable1.setId(MPMDeliverableRepository.getCurrentVal());
			MPMDeliverable deliverableResponse = MPMDeliverableRepository.save(deliverable1);
			entityManager.flush();
			MPMDeliverableList.add(deliverableResponse);
		}
		
		
//		List<MPMDeliverable> deliverableResponseList = MPMDeliverableRepository.saveAll(MPMDeliverableList);
		logger.info("Deliverables created Successfully..");
		
		indexDeliverable(MPMDeliverableList, env.getProperty("npd.project.itemId").concat(".").concat(String.valueOf(projectId)));
		
		
	}
	
	public void indexDeliverable(List<MPMDeliverable> deliverableResponseList, String projectItemId) {
		
		List<MPMDeliverableSchema> deliverableSchemaList = new ArrayList<MPMDeliverableSchema>();
		
		for(MPMDeliverable deliverable:deliverableResponseList ) {
			
			MPMTeamRole delMPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == deliverable.getActiveRoleId()).findAny().orElse(null);
			
	
			
			OpenTextIdentity delOwner = ConfigService.getUsersList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == deliverable.getActiveUserId()).findAny().orElse(null);
			
		
			
			MPMDeliverableSchema deliverableSchema = new MPMDeliverableSchema();
			deliverableSchema.setCampaign_item_id("NA");
			deliverableSchema.setContent_type("MPM_DELIVERABLE");
			deliverableSchema.setDeliverable_active_user_cn(!util.isNullOrEmpty(delOwner) ? 
					delOwner.getUserId() : "NA");
			deliverableSchema.setDeliverable_active_user_id(!util.isNullOrEmpty(delOwner) ? 
					delOwner.getId() : 0);
			deliverableSchema.setDeliverable_active_user_name(!util.isNullOrEmpty(delOwner) ? 
					delOwner.getIdentityDisplayName() : "NA");
			deliverableSchema.setDeliverable_approver_active_user_cn("NA");
			deliverableSchema.setDeliverable_approver_active_user_id(0);
			deliverableSchema.setDeliverable_approver_active_user_name("NA");
			deliverableSchema.setDeliverable_approver_cn("NA");
			deliverableSchema.setDeliverable_approver_cn_facet("NA");
			deliverableSchema.setDeliverable_approver_id(0);
			deliverableSchema.setDeliverable_approver_name("NA");
			deliverableSchema.setDeliverable_approver_role_id(0);
			deliverableSchema.setDeliverable_approver_role_name("NA");
			deliverableSchema.setDeliverable_approver_role_name("NA");
			deliverableSchema.setDeliverable_assignment_type("ROLE");
			deliverableSchema.setDeliverable_description(deliverable.getDescription());
			deliverableSchema.setDeliverable_due_date(deliverable.getDueDate().toString().concat("T00:00:00Z"));
			deliverableSchema.setDeliverable_name(deliverable.getName());
			deliverableSchema.setDeliverable_owner_cn(!util.isNullOrEmpty(delOwner) ? 
					delOwner.getUserId() : "NA");
			deliverableSchema.setDeliverable_owner_id(!util.isNullOrEmpty(delOwner) ? 
					delOwner.getId() : 0);
			deliverableSchema.setDeliverable_owner_name(!util.isNullOrEmpty(delOwner) ? 
					delOwner.getIdentityDisplayName() : "NA");
			deliverableSchema.setDeliverable_owner_role_id(delMPMRole.getId());
			deliverableSchema.setDeliverable_owner_role_name(delMPMRole.getName());
			deliverableSchema.setDeliverable_priority_id(deliverable.getPriorityId());
			deliverableSchema.setDeliverable_priority_value("HIGH");
			deliverableSchema.setDeliverable_start_date(deliverable.getStartDate().toString().concat("T00:00:00Z"));
			deliverableSchema.setDeliverable_status_id(deliverable.getStatusId());
			
			MPMStatus deliverableStatus = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(deliverable.getStatusId()))).findAny().orElse(null);
			
			deliverableSchema.setDeliverable_status_type(deliverableStatus.getStatusType());
			deliverableSchema.setDeliverable_status_value(deliverableStatus.getName());
			deliverableSchema.setDeliverable_time_spent(0);
			deliverableSchema.setDeliverable_type("COMPLETE");
			deliverableSchema.setExpected_deliverable_duration(0);
			deliverableSchema.setId(deliverableSchema.getId());
			deliverableSchema.setIs_active_deliverable(deliverable.getIsActive());
			deliverableSchema.setIs_deleted_deliverable(false);
			deliverableSchema.setIs_pm_assigned(true);
			deliverableSchema.setIs_uploaded(false);
			deliverableSchema.setItem_id(env.getProperty("npd.deliverable.itemId").concat(".").concat(String.valueOf(deliverable.getId())));
			deliverableSchema.setIteration_count("1");
			deliverableSchema.setOriginal_deliverable_due_date(deliverable.getDueDate().toString().concat("T00:00:00Z"));
			deliverableSchema.setOriginal_deliverable_start_date(deliverable.getStartDate().toString().concat("T00:00:00Z"));
			deliverableSchema.setProject_item_id(projectItemId);
			deliverableSchema.setTask_item_id(env.getProperty("npd.task.itemId").concat(".").concat(String.valueOf(deliverable.getTaskId())));
			deliverableSchema.setUploaded_asset_id("NA");
			deliverableSchemaList.add(deliverableSchema);
		}
		
		try {
			deliverableSolrRepository.saveAll(deliverableSchemaList);
			logger.info("All deliverables indexed successfully");
		}catch(Exception e) {
			logger.info("Error during deliverable index " + e.getMessage());
		}
		
		
		
	}

}
