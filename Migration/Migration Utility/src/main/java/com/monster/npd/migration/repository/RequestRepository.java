package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.Request;

@Repository
public interface RequestRepository extends JpaRepository<Request, Integer> {

	@Query("SELECT MAX(p.id) FROM Request p")
    Integer maxRequestId();
	
	@Query(value = "SELECT NEXT VALUE FOR dbo.request_seq", nativeQuery = true)
    public Integer getCurrentVal();
}
