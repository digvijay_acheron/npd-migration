package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.SecondaryPack;

@Repository
public interface SecondaryPackRepository extends JpaRepository<SecondaryPack, Integer> {
	
	@Query("SELECT MAX(p.id) FROM SecondaryPack p")
    Integer maxSecondaryPackId();
	
	@Query(value = "SELECT NEXT VALUE FOR dbo.sec_pack_seq", nativeQuery = true)
    public Integer getCurrentVal();

}
