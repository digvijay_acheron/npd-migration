package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.MPMDeliverable;

@Repository
public interface MPMDeliverableRepository extends JpaRepository<MPMDeliverable, Integer>  {
	
	@Query("SELECT MAX(p.id) FROM MPMDeliverable p")
    Integer maxDeliverableId();
	
	@Query(value = "SELECT NEXT VALUE FOR dbo.deliverable_seq", nativeQuery = true)
    public Integer getCurrentVal();

}
