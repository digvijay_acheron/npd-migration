package com.monster.npd.migration.service;

import java.util.List;
import java.util.Objects;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.monster.npd.migration.model.MPMProject;
import com.monster.npd.migration.model.MPMProjectSchema;
import com.monster.npd.migration.model.MPMTaskSchema;
import com.monster.npd.migration.model.NPDProject;
import com.monster.npd.migration.model.OpenTextIdentity;
import com.monster.npd.migration.model.ProjectDetails;
import com.monster.npd.migration.model.Request;
import com.monster.npd.migration.repository.MPMProjectRepository;
import com.monster.npd.migration.repository.ProjectSolrRepository;

import lombok.Setter;


@Service
public class ProjectIndex {
	
//	private static final Logger logger = LoggerFactory.getLogger(ProjectIndex.class);
	private static final Logger logger = LogManager.getLogger(ProjectIndex.class);
	
	@Autowired
	private ProjectSolrRepository projectSolrRepository;
	
	@Autowired
	private MPMProjectRepository MPMProjectRepository;
	
	@Autowired
	UtilService util;
	
	@Autowired
	Environment env;
	
	public String indexProject(MPMProject project, Request request, ProjectDetails projectDetail,
			String otmmFolderId, String npdProjectItemId, String market, String BusinessUnit,
			List<MPMTaskSchema> taskSchemaList, NPDProject npdProject) throws Exception {		
		MPMProjectSchema projectSchema = new MPMProjectSchema();
		
		projectSchema.setItem_id(env.getProperty("npd.project.itemId").concat(".").concat(String.valueOf(project.getId())));
		projectSchema.setContent_type("MPM_PROJECT");
		projectSchema.setPROJECT_NAME(project.getProjectName());		
		projectSchema.setPROJECT_NAME_facet(project.getProjectName());
		projectSchema.setPROJECT_STATUS_ID(project.getStatusId());
		projectSchema.setPROJECT_PRIORITY_VALUE("HIGH");
		projectSchema.setPROJECT_PRIORITY_VALUE_facet("HIGH");
		projectSchema.setPROJECT_PRIORITY_ID(Integer.parseInt(env.getProperty("npd.project.high.priority.id")));
		
		projectSchema.setPROJECT_TYPE("PROJECT");
		projectSchema.setID(String.valueOf(project.getId()));
		projectSchema.setPROJECT_MILESTONE_PROGRESS("0");
		projectSchema.setPROJECT_MILESTONE_PROGRESS_facet("0");
		projectSchema.setPROJECT_DESCRIPTION(project.getDescription());
		projectSchema.setNPD_PROJECT_DESCRIPTION(project.getDescription());
		projectSchema.setNPD_PROJECT_DESCRIPTION_facet(project.getDescription());
		projectSchema.setPROJECT_DUE_DATE(project.getDueDate().toString().concat("T00:00:00Z"));
		projectSchema.setPROJECT_END_DATE(project.getDueDate().toString().concat("T00:00:00Z"));
		projectSchema.setIS_ACTIVE_PROJECT("true");
		projectSchema.setIS_DELETED_PROJECT("false");
		projectSchema.setNPD_PROJECT_IS_FG_PROJECT(projectDetail.getFGOrConcModel());
		projectSchema.setNPD_PROJECT_IS_FG_PROJECT_facet(projectDetail.getFGOrConcModel());
		
		logger.info("folder id " + otmmFolderId);
		projectSchema.setOTMM_FOLDER_ID(otmmFolderId);
		projectSchema.setPROJECT_START_DATE(project.getStartDate().toString().concat("T00:00:00Z"));
		projectSchema.setPROJECT_CATEGORY_ID(Integer.parseInt(env.getProperty("npd.project.category.id")));
		projectSchema.setPROJECT_CATEGORY_VALUE(env.getProperty("npd.project.category.name"));
		OpenTextIdentity projectOwner = ConfigService.getUsersList().stream()
				.filter(obj -> Objects.nonNull(obj.getId()))
				.filter(obj -> obj.getId() == Integer.parseInt(util.getPmOwnerIdentityId("Samantha"))).findAny().orElse(null);
		projectSchema.setPROJECT_OWNER_ID(project.getProjectOwnerId());
		projectSchema.setPROJECT_OWNER_CN(projectOwner.getUserId());
		
		projectSchema.setPROJECT_OWNER_NAME(projectOwner.getIdentityDisplayName());
		projectSchema.setPROJECT_STATUS_VALUE(env.getProperty("npd.project.initial.status.name"));
		projectSchema.setPROJECT_STATUS_VALUE_facet(env.getProperty("npd.project.initial.status.name"));
		projectSchema.setPROJECT_STATUS_TYPE(env.getProperty("npd.project.initial.status.type"));
		projectSchema.setPROJECT_TEAM_ID(Integer.parseInt(env.getProperty("npd.project.team.id")));
		projectSchema.setPROJECT_TEAM_VALUE(env.getProperty("npd.project.team.name"));
		projectSchema.setORIGINAL_PROJECT_START_DATE(project.getOriginalStartDate().toString().concat("T00:00:00Z"));
		projectSchema.setORIGINAL_PROJECT_DUE_DATE(project.getOriginalDueDate().toString().concat("T00:00:00Z"));
		projectSchema.setIS_CUSTOM_WORKFLOW("true");	
		projectSchema.setIS_STATNDARED_STATUS_PROJECT("NA");
		projectSchema.setPROJECT_TEMPLATE_REF_ID("NA");
		projectSchema.setEXPECTED_PROJECT_DURATION(0);
		projectSchema.setPROJECT_TIME_SPENT(0);
		projectSchema.setPROJECT_CAMPAIGN_ITEM_ID("NA");
		projectSchema.setPROJECT_CATEGORY_METADATA("NA");
		projectSchema.setPROJECT_CATEGORY_METADATA_ID(0);
		projectSchema.setNPD_PROJECT_SECONDARY_PACK_LEAD_TIME("1");
		projectSchema.setPR_REQUEST_ITEM_ID(env.getProperty("npd.request.itemId").concat(".").concat(String.valueOf(request.getId())));
		projectSchema.setPR_POST_LAUNCH_ANALYSIS("false");
		projectSchema.setIS_ON_HOLD("NA");
		projectSchema.setIS_ON_HOLD("NA");
		
		projectSchema.setNPD_PROJECT_BUSINESS_ID("EM".concat(String.valueOf(request.getId())));
		projectSchema.setNPD_PROJECT_BUSINESS_ID_facet("EM".concat(String.valueOf(request.getId())));
//		projectSchema.setNPD_PROJECT_SECONDARY_PACK_LEAD_TIME(NPD_PROJECT_SECONDARY_PACK_LEAD_TIME);
//		projectSchema.setNPD_PROJECT_SECONDARY_PACK_LEAD_TIME_facet(NPD_PROJECT_SECONDARY_PACK_LEAD_TIME);

		projectSchema.setNPD_PROJECT_ITEM_ID(npdProjectItemId);
		
		projectSchema.setNPD_PROJECT_ITEM_ID_facet(npdProjectItemId);
//		logger.info(" desc " + request.getProjectClassifierData().getProjectClassificationDescription());
		projectSchema.setPR_EARLY_PROJECT_CLASSIFICATION_DESC(request.getProjectClassifierData().getProjectClassificationDescription());
		projectSchema.setPR_EARLY_PROJECT_CLASSIFICATION_DESC_facet(request.getProjectClassifierData().getProjectClassificationDescription());
		projectSchema.setPR_EARLY_PROJECT_CLASSIFICATION(request.getProjectClassifierData().getProjectClassificationNumber());
		projectSchema.setPR_EARLY_PROJECT_CLASSIFICATION_facet(request.getProjectClassifierData().getProjectClassificationNumber());
		projectSchema.setPR_BRAND_string(request.getBrand().getDisplayName());
		projectSchema.setPR_BRAND_string_facet(request.getBrand().getDisplayName());
		projectSchema.setPR_PROJECT_TYPE(request.getProjectType().getDisplayName());
		projectSchema.setPR_PROJECT_TYPE_facet(request.getProjectType().getDisplayName());
		projectSchema.setNPD_PROJECT_COM_TECH_OPS_AGREED_DP_WEEK(projectDetail.getTechOpsAgreedLatestView());
		projectSchema.setNPD_PROJECT_COM_TECH_OPS_AGREED_DP_WEEK_facet(projectDetail.getTechOpsAgreedLatestView());
		
		projectSchema.setPR_MARKET(market);
		projectSchema.setPR_MARKET_facet(market);
		projectSchema.setPR_BUSINESS_UNIT(BusinessUnit);
		projectSchema.setPR_BUSINESS_UNIT_facet(BusinessUnit);
		projectSchema.setPR_BOTTLER(request.getBottler().getDisplayName());
		projectSchema.setPR_BOTTLER_facet(request.getBottler().getDisplayName());
		projectSchema.setPR_PLATFORM(request.getPlatform().getDisplayName());
		projectSchema.setPR_PLATFORM_facet(request.getPlatform().getDisplayName());
		projectSchema.setPR_IS_SVP_ALIGNED(!util.isNullOrEmpty(request.getIsSvpAligned()) && 
				request.getIsSvpAligned()  == true ? "true" : "false");
		
		projectSchema.setPR_VARIANT(request.getVariant().getDisplayName());
		projectSchema.setPR_VARIANT_facet(request.getVariant().getDisplayName());
		projectSchema.setPR_SKU_DETAILS(request.getSkuDetails().getDisplayName());
		projectSchema.setPR_SKU_DETAILS_facet(request.getSkuDetails().getDisplayName());
		projectSchema.setPR_CASE_PACK_SIZE(Integer.parseInt(request.getCasePackSize().getDisplayName()));
		projectSchema.setPR_CONSUMER_UNIT_SIZE(Integer.parseInt(request.getConsumerUnitSize().getName()));
		projectSchema.setPR_PRIMARY_PACKAGING_TYPE(request.getPrimaryPackaging().getDisplayName());
		projectSchema.setPR_PRIMARY_PACKAGING_TYPE_facet(request.getPrimaryPackaging().getDisplayName());
		
		projectSchema.setPR_SECONDARY_PACKAGING_TYPE(request.getSecondaryPackagingType());
		projectSchema.setPR_SECONDARY_PACKAGING_TYPE_facet(request.getSecondaryPackagingType());
		projectSchema.setPR_REQUEST_ID_string("EM".concat(String.valueOf(request.getId())).concat("R"));
		projectSchema.setPR_REQUEST_ID_string_facet("EM".concat(String.valueOf(request.getId())).concat("R"));
		projectSchema.setPR_REQUEST_TYPE(request.getRequestType().getDisplayName());
		projectSchema.setPR_REQUEST_TYPE_VALUE(request.getRequestType().getDisplayName());
		logger.info("location " + request.getDraftManufacturingLocation().getMarket().getDisplayName());
		projectSchema.setPR_EARLY_PROJECT_MANUFACTURING_LOCATION(request.getDraftManufacturingLocation().getMarket().getDisplayName());
		projectSchema.setPR_EARLY_PROJECT_MANUFACTURING_LOCATION_facet(request.getDraftManufacturingLocation().getMarket().getDisplayName());
		projectSchema.setPR_EARLY_PROJECT_MANUFACTURING_LOCATION_SITE(request.getEarlyManufacturingSite().getDisplayName());
		
		projectSchema.setPR_EARLY_PROJECT_MANUFACTURING_LOCATION_SITE_facet(request.getEarlyManufacturingSite().getDisplayName());
		projectSchema.setPR_EARLY_PROJECT_TYPE(request.getProjectType().getDisplayName());
		projectSchema.setPR_EARLY_PROJECT_TYPE_facet(request.getProjectType().getDisplayName());
		projectSchema.setNPD_PROJECT_BOTTLER_COMMUNICATION_STATUS(projectDetail.getBottlerCommunicationStatus());
		projectSchema.setNPD_PROJECT_BOTTLER_COMMUNICATION_STATUS_facet(projectDetail.getBottlerCommunicationStatus());
		projectSchema.setNPD_PROJECT_OPS_ALIGNED_DP_WEEK(projectDetail.getComTechOpsAlignedDate());
		projectSchema.setNPD_PROJECT_OPS_ALIGNED_DP_WEEK_facet(projectDetail.getComTechOpsAlignedDate());
		
		projectSchema.setNPD_PROJECT_EARLY_MANUFACTURING_LOCATION_ITEM_ID(env.getProperty("npd.manufacturingLocation.itemId").concat(".").concat(String.valueOf(request.getDraftManufacturingLocation().getId())));
		projectSchema.setNPD_PROJECT_EARLY_MANUFACTURING_SITE_ITEM_ID(env.getProperty("npd.manufacturingSite.itemId").concat(".").concat(String.valueOf(request.getEarlyManufacturingSite().getId())));
		projectSchema.setNPD_PROJECT_EARLY_PROJECT_TYPE_ITEM_ID(env.getProperty("npd.projectType.itemId").concat(".").concat(String.valueOf(request.getProjectType().getId())));
		
		projectSchema.setNPD_PROJECT_TIMELINE_RISK(projectDetail.getTimelineRisk());
		projectSchema.setNPD_PROJECT_TIMELINE_RISK_facet(projectDetail.getTimelineRisk());
		projectSchema.setNPD_PROJECT_RISK_SUMMARY(projectDetail.getRiskSummary());
		projectSchema.setNPD_PROJECT_RISK_SUMMARY_facet(projectDetail.getRiskSummary());
		projectSchema.setNPD_PROJECT_STATUS_CRITICAL_ITEMS(projectDetail.getStatusAndCriticalItems());
		projectSchema.setNPD_PROJECT_STATUS_CRITICAL_ITEMS_facet(projectDetail.getStatusAndCriticalItems());
		projectSchema.setNPD_PROJECT_NO_OF_LINKED_MARKETS(npdProject.getNooflinkedmarkets());
		projectSchema.setNPD_PROJECT_NO_OF_LINKED_MARKETS_facet(npdProject.getNooflinkedmarkets());
		
		projectSchema.setNPD_PROJECT_PROJECT_FINAL_PROJECT_CLASSIFICATION(String.valueOf(npdProject.getProjectfinalclassification()));
		projectSchema.setNPD_PROJECT_PROJECT_FINAL_PROJECT_CLASSIFICATION_DESC(npdProject.getFinalprojectclassificationdescription());
		projectSchema.setNPD_PROJECT_PROJECT_FINAL_PROJECT_CLASSIFICATION_DESC_facet(npdProject.getFinalprojectclassificationdescription());
		projectSchema.setNPD_PROJECT_FINAL_MANUFACTURING_LOCATION(npdProject.getFinalmanufacturinglocation());
		projectSchema.setNPD_PROJECT_FINAL_MANUFACTURING_LOCATION_facet(npdProject.getFinalmanufacturinglocation());
		projectSchema.setNPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ID(npdProject.getFinalmanufacturinglocationid());
		projectSchema.setNPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ITEM_ID(npdProject.getFinalmanufacturinglocationitemid());
		projectSchema.setNPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ITEM_ID_facet(npdProject.getFinalmanufacturinglocationitemid());
		projectSchema.setNPD_PROJECT_FINAL_MANUFACTURING_SITE(npdProject.getFinalmanufacturingsite());
		projectSchema.setNPD_PROJECT_FINAL_MANUFACTURING_SITE_facet(npdProject.getFinalmanufacturingsite());
		projectSchema.setNPD_PROJECT_FINAL_MANUFACTURING_SITE_ID(npdProject.getFinalmanufacturingsiteid());
		projectSchema.setNPD_PROJECT_FINAL_MANUFACTURING_SITE_ITEM_ID(npdProject.getFinalmanufacturingsiteitemid());
		projectSchema.setNPD_PROJECT_FINAL_MANUFACTURING_SITE_ITEM_ID_facet(npdProject.getFinalmanufacturingsiteitemid());
		projectSchema.setNPD_PROJECT_NEW_PROJECT_TYPE(npdProject.getNewprojecttype());
		projectSchema.setNPD_PROJECT_NEW_PROJECT_TYPE_facet(npdProject.getNewprojecttype());
		projectSchema.setNPD_PROJECT_NEW_PROJECT_TYPE_ITEM_ID(npdProject.getNewprojecttypeitemid());
		projectSchema.setNPD_PROJECT_NEW_PROJECT_TYPE_ITEM_ID_facet(npdProject.getNewprojecttypeitemid());
		projectSchema.setNPD_PROJECT_NEW_FG(!util.isNullOrEmpty(npdProject.getNewfg()) && 
				npdProject.getNewfg() == true ? "true"  : "false" );	
		projectSchema.setNPD_PROJECT_NEW_FG_facet(!util.isNullOrEmpty(npdProject.getNewfg()) && 
				npdProject.getNewfg() == true ? "true"  : "false" );		
		projectSchema.setNPD_PROJECT_NEW_RD_FORMULA(!util.isNullOrEmpty(npdProject.getNewrdformula()) && 
				npdProject.getNewrdformula() == true ? "true"  : "false" );	
		projectSchema.setNPD_PROJECT_NEW_RD_FORMULA_facet(!util.isNullOrEmpty(npdProject.getNewrdformula()) && 
				npdProject.getNewrdformula() == true ? "true"  : "false" );		
		projectSchema.setNPD_PROJECT_NEW_HBC_FORMULA(!util.isNullOrEmpty(npdProject.getNewhbcformula()) && 
				npdProject.getNewhbcformula() == true ? "true"  : "false" );		
		projectSchema.setNPD_PROJECT_NEW_HBC_FORMULA_facet(!util.isNullOrEmpty(npdProject.getNewhbcformula()) && 
				npdProject.getNewhbcformula() == true ? "true"  : "false" );	
		projectSchema.setNPD_PROJECT_NEW_PRIMARY_PACK(!util.isNullOrEmpty(npdProject.getNewprimarypackaging()) && 
				npdProject.getNewprimarypackaging() == true ? "true"  : "false" );	
		projectSchema.setNPD_PROJECT_NEW_PRIMARY_PACK_facet(!util.isNullOrEmpty(npdProject.getNewprimarypackaging()) && 
				npdProject.getNewprimarypackaging() == true ? "true"  : "false" );	
		projectSchema.setNPD_PROJECT_NEW_SECONDARY_PACK(npdProject.getNewsecondarypackaging());
		projectSchema.setNPD_PROJECT_NEW_SECONDARY_PACK_facet(npdProject.getNewsecondarypackaging());
		projectSchema.setNPD_PROJECT_NEW_REGISTRATION_DOSSIER(!util.isNullOrEmpty(npdProject.getNewregdossier()) && 
				npdProject.getNewregdossier() == true ? "true"  : "false" );
		projectSchema.setNPD_PROJECT_NEW_REGISTRATION_DOSSIER_facet(!util.isNullOrEmpty(npdProject.getNewregdossier()) && 
				npdProject.getNewregdossier() == true ? "true"  : "false" );
		projectSchema.setNPD_PROJECT_NEW_PRE_PRODUCTION(!util.isNullOrEmpty(npdProject.getNewpreproduction()) && 
				npdProject.getNewpreproduction() == true ? "true"  : "false" );
		projectSchema.setNPD_PROJECT_NEW_PRE_PRODUCTION_facet(!util.isNullOrEmpty(npdProject.getNewpreproduction()) && 
				npdProject.getNewpreproduction() == true ? "true"  : "false" );
		projectSchema.setNPD_PROJECT_NEW_POST_PRODUCTION(!util.isNullOrEmpty(npdProject.getNewpostproduction()) && 
				npdProject.getNewpostproduction() == true ? "true"  : "false" );
		projectSchema.setNPD_PROJECT_NEW_POST_PRODUCTION_facet(!util.isNullOrEmpty(npdProject.getNewpostproduction()) && 
				npdProject.getNewpostproduction() == true ? "true"  : "false" );
		projectSchema.setNPD_PROJECT_NEW_CLASSIFICATION_RATIONAL(projectDetail.getClassificationRational());		
		projectSchema.setNPD_PROJECT_NEW_CLASSIFICATION_RATIONAL_facet(projectDetail.getClassificationRational());
		
		
		projectSchema.setNPD_PROJECT_OPS_PM_ID(String.valueOf(request.getOpsPM().getId()));
		projectSchema.setNPD_PROJECT_OPS_PM_ID_facet(String.valueOf(request.getOpsPM().getId()));
		projectSchema.setNPD_PROJECT_OPS_PM_ITEM_ID(npdProject.getOpspmitemid());
		projectSchema.setNPD_PROJECT_OPS_PM_ITEM_ID_facet(npdProject.getOpspmitemid());
		projectSchema.setNPD_PROJECT_OPS_PM_NAME(npdProject.getOpspm());
		projectSchema.setNPD_PROJECT_OPS_PM_NAME_facet(npdProject.getOpspm());	
		projectSchema.setNPD_PROJECT_E2E_PM_ID(String.valueOf(request.getE2ePM().getId()));
		projectSchema.setNPD_PROJECT_E2E_PM_ID_facet(String.valueOf(request.getE2ePM().getId()));
		projectSchema.setNPD_PROJECT_E2E_PM_ITEM_ID(npdProject.getE2epmitemid());
		projectSchema.setNPD_PROJECT_E2E_PM_ITEM_ID_facet(npdProject.getE2epmitemid());
		projectSchema.setNPD_PROJECT_E2E_PM_NAME(npdProject.getE2epm());
		projectSchema.setNPD_PROJECT_E2E_PM_NAME_facet(npdProject.getE2epm());		
		projectSchema.setNPD_PROJECT_CORP_PM_ID(String.valueOf(request.getCorpPM().getId()));
		projectSchema.setNPD_PROJECT_CORP_PM_ID_facet(String.valueOf(request.getCorpPM().getId()));
		projectSchema.setNPD_PROJECT_CORP_PM_ITEM_ID(npdProject.getCorppmitemid());
		projectSchema.setNPD_PROJECT_CORP_PM_ITEM_ID_facet(npdProject.getCorppmitemid());
		projectSchema.setNPD_PROJECT_CORP_PM_NAME(npdProject.getCorppm());
		projectSchema.setNPD_PROJECT_CORP_PM_NAME_facet(npdProject.getCorppm());	
		
		projectSchema.setNPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_ID(npdProject.getRegionaltechnicalmanagerid());
		projectSchema.setNPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_ITEM_ID(npdProject.getRegionaltechnicalmanageritemid());
		projectSchema.setNPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_ITEM_ID_facet(npdProject.getRegionaltechnicalmanageritemid());
		projectSchema.setNPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_NAME(npdProject.getRegionaltechnicalmanagername());
		projectSchema.setNPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_NAME_facet(npdProject.getRegionaltechnicalmanagername());
		projectSchema.setNPD_PROJECT_OPS_PLANNER_ID(npdProject.getOpsplannerid());
		projectSchema.setNPD_PROJECT_OPS_PLANNER_ITEM_ID(npdProject.getOpsplanneritemid());
		projectSchema.setNPD_PROJECT_OPS_PLANNER_ITEM_ID_facet(npdProject.getOpsplanneritemid());
		projectSchema.setNPD_PROJECT_OPS_PLANNER_NAME(npdProject.getOpsplannername());
		projectSchema.setNPD_PROJECT_OPS_PLANNER_NAME_facet(npdProject.getOpsplannername());
		projectSchema.setNPD_PROJECT_GFG_ID(npdProject.getGfgid());
		projectSchema.setNPD_PROJECT_GFG_ITEM_ID(npdProject.getGfgitemid());
		projectSchema.setNPD_PROJECT_GFG_ITEM_ID_facet(npdProject.getGfgitemid());
		projectSchema.setNPD_PROJECT_GFG_NAME(npdProject.getGfgname());
		projectSchema.setNPD_PROJECT_GFG_NAME_facet(npdProject.getGfgname());
		projectSchema.setNPD_PROJECT_PR_SPECIALIST_ID(npdProject.getProjectspecialistid());//		
		projectSchema.setNPD_PROJECT_PR_SPECIALIST_ITEM_ID(npdProject.getProjectspecialistitemid());
		projectSchema.setNPD_PROJECT_PR_SPECIALIST_ITEM_ID_facet(npdProject.getProjectspecialistitemid());
		projectSchema.setNPD_PROJECT_PR_SPECIALIST_NAME(npdProject.getProjectspecialistname());
		projectSchema.setNPD_PROJECT_PR_SPECIALIST_NAME_facet(npdProject.getProjectspecialistname());
		projectSchema.setNPD_PROJECT_REGULATORY_LEAD_ID(npdProject.getRegulatoryleadid());
		projectSchema.setNPD_PROJECT_REGULATORY_LEAD_ITEM_ID(npdProject.getRegulatoryleaditemid());
		projectSchema.setNPD_PROJECT_REGULATORY_LEAD_ITEM_ID_facet(npdProject.getRegulatoryleaditemid());//		
		projectSchema.setNPD_PROJECT_REGULATORY_LEAD_NAME(npdProject.getRegulatoryleadname());
		projectSchema.setNPD_PROJECT_REGULATORY_LEAD_NAME_facet(npdProject.getRegulatoryleadname());
		projectSchema.setNPD_PROJECT_COMMERCIAL_LEAD_ID(npdProject.getCommercialleadid());
		projectSchema.setNPD_PROJECT_COMMERCIAL_LEAD_ITEM_ID(npdProject.getCommercialleaditemid());
		projectSchema.setNPD_PROJECT_COMMERCIAL_LEAD_ITEM_ID_facet(npdProject.getCommercialleaditemid());
		projectSchema.setNPD_PROJECT_COMMERCIAL_LEAD_NAME(npdProject.getCommercialleadname());
		projectSchema.setNPD_PROJECT_COMMERCIAL_LEAD_NAME_facet(npdProject.getCommercialleadname());
		
		projectSchema.setNPD_PROJECT_PM_REPORTING_PROJECT_TYPE(npdProject.getPmreportingprojecttype());
		projectSchema.setNPD_PROJECT_PM_REPORTING_PROJECT_TYPE_facet(npdProject.getPmreportingprojecttype());
		projectSchema.setNPD_PROJECT_PM_REPORTING_PROJECT_TYPE_ITEM_ID(npdProject.getPmreportingprojecttypeitemid());
		projectSchema.setNPD_PROJECT_PM_REPORTING_PROJECT_TYPE_ITEM_ID_facet(npdProject.getPmreportingprojecttypeitemid());
		projectSchema.setNPD_PROJECT_PM_REPORTING_SUB_PROJECT_TYPE(npdProject.getPmreportingsubprojecttype());
		projectSchema.setNPD_PROJECT_PM_REPORTING_SUB_PROJECT_TYPE_facet(npdProject.getPmreportingsubprojecttype());
		projectSchema.setNPD_PROJECT_PM_REPORTING_SUB_PROJECT_TYPE_ITEM_ID(npdProject.getPmreportingsubprojecttypeitemid());
		projectSchema.setNPD_PROJECT_PM_REPORTING_SUB_PROJECT_TYPE_ITEM_ID_facet(npdProject.getPmreportingsubprojecttypeitemid());
		projectSchema.setNPD_PROJECT_PM_REPORTING_PROJECT_DETAIL(npdProject.getPmreportingprojectdetail());
		projectSchema.setNPD_PROJECT_PM_REPORTING_PROJECT_DETAIL_facet(npdProject.getPmreportingprojectdetail());
		projectSchema.setNPD_PROJECT_PM_REPORTING_PROJECT_DETAIL_ITEM_ID(npdProject.getPmreportingprojectdetailitemid());
		projectSchema.setNPD_PROJECT_PM_REPORTING_PROJECT_DETAIL_ITEM_ID_facet(npdProject.getPmreportingprojectdetailitemid());
		projectSchema.setNPD_PROJECT_PM_REPORTING_DELIVERY_QUARTER(npdProject.getPmreportingdeliveryquarter());
		projectSchema.setNPD_PROJECT_PM_REPORTING_DELIVERY_QUARTER_facet(npdProject.getPmreportingdeliveryquarter());
		projectSchema.setNPD_PROJECT_PM_REPORTING_DELIVERY_QUARTER_ITEM_ID(npdProject.getPmreportingdeliveryquarteritemid());
		projectSchema.setNPD_PROJECT_PM_REPORTING_DELIVERY_QUARTER_ITEM_ID_facet(npdProject.getPmreportingdeliveryquarteritemid());
		projectSchema.setNPD_PROJECT_PM_REPORTING_OTHER1(projectDetail.getOther1());
		projectSchema.setNPD_PROJECT_PM_REPORTING_OTHER1_facet(projectDetail.getOther1());
		projectSchema.setNPD_PROJECT_PM_REPORTING_OTHER2(projectDetail.getOther2());
		projectSchema.setNPD_PROJECT_PM_REPORTING_OTHER2_facet(projectDetail.getOther2());
		
		projectSchema.setNPD_PROJECT_NSV_Y1_ANNUALISED(npdProject.getNsvy1annualised());
		projectSchema.setNPD_PROJECT_NSV_Y1_ANNUALISED_facet(npdProject.getNsvy1annualised());
		projectSchema.setNPD_PROJECT_VOLUME_Y1_ANNUALISED(npdProject.getVolumey1annualised());
		projectSchema.setNPD_PROJECT_VOLUME_Y1_ANNUALISED_facet(npdProject.getVolumey1annualised());
		projectSchema.setNPD_PROJECT_VOLUME_1ST_3_MONTHS(npdProject.getVolume1st3months());
		projectSchema.setNPD_PROJECT_VOLUME_1ST_3_MONTHS_facet(npdProject.getVolume1st3months());
		projectSchema.setNPD_PROJECT_CP1_AGREEMENT_DATE(npdProject.getCp1agreementdate());
		projectSchema.setNPD_PROJECT_CP1_AGREEMENT_DATE_facet(npdProject.getCp1agreementdate());
		projectSchema.setNPD_PROJECT_PM_CP1_PHASE_DELAY(npdProject.getProgrammemanagercp1phasedelay());
		
		projectSchema.setNPD_PROJECT_CP1_REVISED_DATE(npdProject.getCp1reviseddate());
		projectSchema.setNPD_PROJECT_CP1_REVISED_DATE_facet(npdProject.getCp1reviseddate());
		projectSchema.setNPD_PROJECT_ON_TRACK_ORIGINAL_CP1_DATE(projectDetail.getNPDWasOnTrackForOriginalCp1Date());
		projectSchema.setNPD_PROJECT_ON_TRACK_ORIGINAL_CP1_DATE_facet(projectDetail.getNPDWasOnTrackForOriginalCp1Date());
		projectSchema.setNPD_PROJECT_BOTTLER_PUSHED_OUT_PRODUCTION(projectDetail.getBottlerPushedOutProductionWithoutLaunchImpact());
		projectSchema.setNPD_PROJECT_BOTTLER_PUSHED_OUT_PRODUCTION_facet(projectDetail.getBottlerPushedOutProductionWithoutLaunchImpact());
		projectSchema.setNPD_PROJECT_MEC_COMMERCIAL_BOTTLER_REQUESTED(projectDetail.getMECCommercialBottlerRequestedDelayToLaunch());
		projectSchema.setNPD_PROJECT_MEC_COMMERCIAL_BOTTLER_REQUESTED_facet(projectDetail.getMECCommercialBottlerRequestedDelayToLaunch());
		projectSchema.setNPD_PROJECT_OTHER_TBC(projectDetail.getOther1());
		projectSchema.setNPD_PROJECT_OTHER_TBC_facet(projectDetail.getOther1());
		projectSchema.setNPD_PROJECT_OTHER2_TBC(projectDetail.getOther2());
		projectSchema.setNPD_PROJECT_OTHER2_TBC_facet(projectDetail.getOther2());
		projectSchema.setNPD_PROJECT_HAS_CHANGE_BEEN_ALIGNED(projectDetail.getHasTheChangeBeenAlignedViaCPMtg());		
		projectSchema.setNPD_PROJECT_HAS_CHANGE_BEEN_ALIGNED_facet(projectDetail.getHasTheChangeBeenAlignedViaCPMtg());
		projectSchema.setNPD_PROJECT_BRIEF_DESC_OF_DELAY(projectDetail.getBriefDescOfDelay());
		projectSchema.setNPD_PROJECT_BRIEF_DESC_OF_DELAY_facet(projectDetail.getBriefDescOfDelay());
		
		projectSchema.setNPD_PROJECT_WINSHUTTLE_NUMBER(projectDetail.getWinshuttleProjectNumber());
		projectSchema.setNPD_PROJECT_WINSHUTTLE_NUMBER_facet(projectDetail.getWinshuttleProjectNumber());
		projectSchema.setNPD_PROJECT_FG_MATERIAL_CODE(projectDetail.getFGMaterialCode());
		projectSchema.setNPD_PROJECT_FG_MATERIAL_CODE_facet(projectDetail.getFGMaterialCode());
		projectSchema.setNPD_PROJECT_FORMULA_HBC_NUM(projectDetail.getFormulaHBC());
		projectSchema.setNPD_PROJECT_FORMULA_HBC_NUM_facet(projectDetail.getFormulaHBC());
		projectSchema.setNPD_PROJECT_HBC_ISSUANCE_TYPE(projectDetail.getHBCIssuanceType());
		projectSchema.setNPD_PROJECT_HBC_ISSUANCE_TYPE_facet(projectDetail.getHBCIssuanceType());
		projectSchema.setNPD_PROJECT_CAN_SLEEVE_PRIMARY_BOTTLE_MATERIAL(projectDetail.getCanSleeveOrPrimaryBottler());
		projectSchema.setNPD_PROJECT_CAN_SLEEVE_PRIMARY_BOTTLE_MATERIAL_facet(projectDetail.getCanSleeveOrPrimaryBottler());
		projectSchema.setNPD_PROJECT_PALLET_LABEL_FORMAT(projectDetail.getPalletLabelFormat());
		projectSchema.setNPD_PROJECT_PALLET_LABEL_FORMAT_facet(projectDetail.getPalletLabelFormat());
		projectSchema.setNPD_PROJECT_TRAY_LABEL_FORMAT(projectDetail.getTrayLabelFormat());
		projectSchema.setNPD_PROJECT_TRAY_LABEL_FORMAT_facet(projectDetail.getTrayLabelFormat());
		
		projectSchema.setNPD_PROJECT_CAN_PRIMARY_AW_STARTED(npdProject.getCanprimaryawstart() == true ? "true" : "false");
		projectSchema.setNPD_PROJECT_CAN_PRIMARY_AW_STARTED_facet(npdProject.getCanprimaryawstart() == true ? "true" : "false");
		projectSchema.setNPD_PROJECT_CAN_SECONDARY_AW_STARTED(npdProject.getCansecondaryawstart() == true ? "true" : "false");
		projectSchema.setNPD_PROJECT_CAN_SECONDARY_AW_STARTED_facet(npdProject.getCansecondaryawstart() == true ? "true" : "false");
		projectSchema.setNPD_PROJECT_REQUIRES_PRIMARY_CAN_AW(npdProject.getRequiresprimarycanaw() == true ? "true" : "false");
		projectSchema.setNPD_PROJECT_REQUIRES_PRIMARY_CAN_AW_facet(npdProject.getRequiresprimarycanaw() == true ? "true" : "false");
		projectSchema.setNPD_PROJECT_REQUIRES_SECONDARY_AW(npdProject.getRequiressecondaryaw() == true ? "true" : "false");
		projectSchema.setNPD_PROJECT_REQUIRES_SECONDARY_AW_facet(npdProject.getRequiressecondaryaw() == true ? "true" : "false");
		projectSchema.setNPD_PROJECT_ALL_AW_TASKS_COMPLETED(npdProject.getAllawtaskscompleted() == true ? "true" : "false");
		projectSchema.setNPD_PROJECT_ALL_AW_TASKS_COMPLETED_facet(npdProject.getAllawtaskscompleted() == true ? "true" : "false");
		projectSchema.setNPD_PROJECT_TRIAL_OR_FIRST_PROD_INCLUDED_IN_PLAN(npdProject.getTrialorfirstprodincludedinplan());
		projectSchema.setNPD_PROJECT_TRIAL_OR_FIRST_PROD_INCLUDED_IN_PLAN_facet(npdProject.getTrialorfirstprodincludedinplan());
		
		projectSchema.setNPD_PROJECT_POSSIBLE_NOTF_REQUIREMENT(npdProject.getPossiblenotificationrequirement());
		projectSchema.setNPD_PROJECT_POSSIBLE_NOTF_REQUIREMENT_facet(npdProject.getPossiblenotificationrequirement());	
		
		projectSchema.setNPD_PROJECT_EAN_ACTUAL_WEEK(projectDetail.getEANCodesSharedWithBottlerActual());
		projectSchema.setNPD_PROJECT_EAN_ACTUAL_WEEK_facet(projectDetail.getEANCodesSharedWithBottlerActual());
		projectSchema.setNPD_PROJECT_PROD_INFO_ACTUAL_WEEK(projectDetail.getProdInfoSheetSharedActual());
		projectSchema.setNPD_PROJECT_PROD_INFO_ACTUAL_WEEK_facet(projectDetail.getProdInfoSheetSharedActual());
		projectSchema.setNPD_PROJECT_LABEL_UPLOADED_ACTUAL_DATE(projectDetail.getLabelUplaodedToCanCompanyDate());
		projectSchema.setNPD_PROJECT_LABEL_UPLOADED_ACTUAL_DATE_facet(projectDetail.getLabelUplaodedToCanCompanyDate());
		projectSchema.setNPD_PROJECT_BOTTLER_SPECIFIC_COMMENTS(projectDetail.getBottlerSpecificComments());
		projectSchema.setNPD_PROJECT_BOTTLER_SPECIFIC_COMMENTS_facet(projectDetail.getBottlerSpecificComments());
		
		projectSchema.setNPD_PROJECT_REGULATORY_COMMENTS(projectDetail.getRegulatoryComments());
		projectSchema.setNPD_PROJECT_REGULATORY_COMMENTS_facet(projectDetail.getRegulatoryComments());
		projectSchema.setNPD_PROJECT_HOW_MANY_PIECES(!util.isNullOrEmpty(projectDetail.getNoOfPiecesRequired()) ? 
				Integer.parseInt(projectDetail.getNoOfPiecesRequired()) : 0);
		
		projectSchema.setNPD_PROJECT_SITE_AUDIT_REQUIRED(projectDetail.getSiteAuditRequired());
		projectSchema.setNPD_PROJECT_SITE_AUDIT_REQUIRED_facet(projectDetail.getSiteAuditRequired());
		projectSchema.setNPD_PROJECT_AUDIT_DATE(npdProject.getAuditdate());
		projectSchema.setNPD_PROJECT_AUDIT_DATE_facet(npdProject.getAuditdate());
		projectSchema.setNPD_PROJECT_SITE_APPROVED(projectDetail.getSiteApproved());
		projectSchema.setNPD_PROJECT_SITE_APPROVED_facet(projectDetail.getSiteApproved());
		projectSchema.setNPD_PROJECT_PLANT_SPECIFIC_COMMENTS(projectDetail.getPlantSpecificBatchingInstructionsComments());
		projectSchema.setNPD_PROJECT_PLANT_SPECIFIC_COMMENTS_facet(projectDetail.getPlantSpecificBatchingInstructionsComments());
		projectSchema.setNPD_PROJECT_PLANT_SPECIFIC_REAPPLICATION_ONLY(projectDetail.getPlantSpecificBatchingInstructionsReapplicationOnly());
		projectSchema.setNPD_PROJECT_PLANT_SPECIFIC_REAPPLICATION_ONLY_facet(projectDetail.getPlantSpecificBatchingInstructionsReapplicationOnly());
		projectSchema.setNPD_PROJECT_PROTOCOL_ISSUED(projectDetail.getProtocolIssued());
		projectSchema.setNPD_PROJECT_PROTOCOL_ISSUED_facet(projectDetail.getProtocolIssued());
		projectSchema.setNPD_PROJECT_QA_QC_RELEASE_DATE(projectDetail.getQAQAReleaseDate());		
		projectSchema.setNPD_PROJECT_QA_QC_RELEASE_DATE_facet(projectDetail.getQAQAReleaseDate());
		projectSchema.setNPD_PROJECT_TRIAL_OR_COMMENTS_OR_ACTIONS_REQUIRED(npdProject.getTrialorcommentsoractionsrequired());
		projectSchema.setNPD_PROJECT_TRIAL_OR_COMMENTS_OR_ACTIONS_REQUIRED_facet(npdProject.getTrialorcommentsoractionsrequired());
		projectSchema.setNPD_PROJECT_TECH_QUAL_COMPLETED(npdProject.getTechqualcompleted());
		projectSchema.setNPD_PROJECT_TECH_QUAL_COMPLETED_facet(npdProject.getTechqualcompleted());
		projectSchema.setNPD_PROJECT_ONSITE_REMOTE_NOT_REQUIRED(projectDetail.getOnsiteRemoteNotRequired());
		projectSchema.setNPD_PROJECT_ONSITE_REMOTE_NOT_REQUIRED_facet(projectDetail.getOnsiteRemoteNotRequired());
		
		projectSchema.setNPD_PROJECT_TRIAL_MATERIALS_CONFIRMED_DELIVERY_WEEK(projectDetail.getTrialMaterialsConfirmedDeliveryWeek());
		projectSchema.setNPD_PROJECT_TRIAL_MATERIALS_CONFIRMED_DELIVERY_WEEK_facet(projectDetail.getTrialMaterialsConfirmedDeliveryWeek());
		projectSchema.setNPD_PROJECT_TRIAL_DATE_CONFIRMED_WITH_BOTTLER_facet(npdProject.getTrialdateconfirmedwithbottler());
		projectSchema.setNPD_PROJECT_TRIAL_DATE_CONFIRMED_WITH_BOTTLER(npdProject.getTrialdateconfirmedwithbottler());
		projectSchema.setNPD_PROJECT_FINAL_AGREED_1ST_PROD_CONFIRMED_WITH_BOTTLER(projectDetail.getFinalAgreed1stProd());
		projectSchema.setNPD_PROJECT_FINAL_AGREED_1ST_PROD_CONFIRMED_WITH_BOTTLER_facet(projectDetail.getFinalAgreed1stProd());
		projectSchema.setNPD_PROJECT_ALLOW_PROD_SCHEDULE_LESS_THAN_8(projectDetail.getAllowProdSchedule());
		projectSchema.setNPD_PROJECT_ALLOW_PROD_SCHEDULE_LESS_THAN_8_facet(projectDetail.getAllowProdSchedule());
		projectSchema.setNPD_PROJECT_LOCAL_BOM_IN_SAP(projectDetail.getLocalBOMInSAp());
		projectSchema.setNPD_PROJECT_LOCAL_BOM_IN_SAP_facet(projectDetail.getLocalBOMInSAp());
		projectSchema.setNPD_PROJECT_BOM_SENT_ACTUAL_DATE(projectDetail.getBottlerActualWeek());
		projectSchema.setNPD_PROJECT_BOM_SENT_ACTUAL_DATE_facet(projectDetail.getBottlerActualWeek());
		projectSchema.setNPD_PROJECT_PALLET_LABEL_RECEIVED_ACTUAL_WEEK(projectDetail.getPalletLabelReceivedActual());
		projectSchema.setNPD_PROJECT_PALLET_LABEL_RECEIVED_ACTUAL_WEEK_facet(projectDetail.getPalletLabelReceivedActual());
		projectSchema.setNPD_PROJECT_PO_RECEIVED_ACTUAL_WEEK(projectDetail.getPOReceivedDate());
		projectSchema.setNPD_PROJECT_PO_RECEIVED_ACTUAL_WEEK_facet(projectDetail.getPOReceivedDate());
		projectSchema.setNPD_PROJECT_BOTTLER_CAN_ACTUAL_WEEK(projectDetail.getBottlerActualWeek());
		projectSchema.setNPD_PROJECT_BOTTLER_CAN_ACTUAL_WEEK_facet(projectDetail.getBottlerActualWeek());
		projectSchema.setNPD_PROJECT_ITEM_NUMBER(projectDetail.getBottlerSpecificItemNumber());
		projectSchema.setNPD_PROJECT_ITEM_NUMBER_facet(projectDetail.getBottlerSpecificItemNumber());
		projectSchema.setNPD_PROJECT_NAME_OF_CAN_COMPANY("NA");		
		projectSchema.setNPD_PROJECT_NAME_OF_CAN_COMPANY_facet("NA");
		projectSchema.setNPD_PROJECT_NUMBER_OF_CAN_COMPANIES(npdProject.getNumberofcancompaniesrequired());
		projectSchema.setNPD_PROJECT_NAME_OF_CAN_COMPANY_IDS(npdProject.getCancompanyids());
		projectSchema.setNPD_PROJECT_NAME_OF_CAN_COMPANY_IDS_facet(npdProject.getCancompanyids());
		projectSchema.setNPD_PROJECT_NAME_OF_CAN_COMPANY_VALUES(projectDetail.getNameOfCanCompanys());
		projectSchema.setNPD_PROJECT_NAME_OF_CAN_COMPANY_VALUES_facet(projectDetail.getNameOfCanCompanys());
		
		projectSchema.setNPD_PROJECT_WHO_WILL_AUDIT(npdProject.getWhowillaudit());
		projectSchema.setNPD_PROJECT_WHO_WILL_AUDIT_ITEM_ID(npdProject.getWhowillaudititemid());
		projectSchema.setNPD_PROJECT_WHO_WILL_AUDIT_ITEM_ID_facet(npdProject.getWhowillaudititemid());
		projectSchema.setNPD_PROJECT_TECH_QUAL_WHO_WILL_COMPLETE(npdProject.getTechqualwhowillcomplete());
		projectSchema.setNPD_PROJECT_TECH_QUAL_WHO_WILL_COMPLETE_facet(npdProject.getTechqualwhowillcomplete());
		projectSchema.setNPD_PROJECT_TECH_QUAL_WHO_WILL_COMPLETE_ITEM_ID(npdProject.getTechqualwhowillcompleteitemid());
		projectSchema.setNPD_PROJECT_TECH_QUAL_WHO_WILL_COMPLETE_ITEM_ID_facet(npdProject.getTechqualwhowillcompleteitemid());
		projectSchema.setNPD_PROJECT_TRIAL_PROTOCOL_WRITTEN_BY(npdProject.getTrialprotocolwrittenby());
		projectSchema.setNPD_PROJECT_TRIAL_PROTOCOL_WRITTEN_BY_facet(npdProject.getTrialprotocolwrittenby());
		projectSchema.setNPD_PROJECT_TRIAL_PROTOCOL_WRITTEN_BY_ITEM_ID(npdProject.getTrialprotocolwrittenbyitemid());
		projectSchema.setNPD_PROJECT_TRIAL_PROTOCOL_WRITTEN_BY_ITEM_ID_facet(npdProject.getTrialprotocolwrittenbyitemid());
		projectSchema.setNPD_PROJECT_TRIAL_SUPERVISION_QUALITY(npdProject.getTrialsupervisionquality());
		projectSchema.setNPD_PROJECT_TRIAL_SUPERVISION_QUALITY_facet(npdProject.getTrialsupervisionquality());
		projectSchema.setNPD_PROJECT_TRIAL_SUPERVISION_QUALITY_ITEM_ID(npdProject.getTrialsupervisionqualityitemid());
		projectSchema.setNPD_PROJECT_TRIAL_SUPERVISION_QUALITY_ITEM_ID_facet(npdProject.getTrialsupervisionqualityitemid());
		projectSchema.setNPD_PROJECT_TRIAL_SUPERVISION_NPD(npdProject.getTrialsupervisionnpd());
		projectSchema.setNPD_PROJECT_TRIAL_SUPERVISION_NPD_facet(npdProject.getTrialsupervisionnpd());	
		projectSchema.setNPD_PROJECT_TRIAL_SUPERVISION_NPD_ITEM_ID(npdProject.getTrialsupervisionnpditemid());
		projectSchema.setNPD_PROJECT_TRIAL_SUPERVISION_NPD_ITEM_ID_facet(npdProject.getTrialsupervisionnpditemid());
		projectSchema.setNPD_PROJECT_TASTING_REQUIREMENT(npdProject.getTastingrequirement());
		projectSchema.setNPD_PROJECT_TASTING_REQUIREMENT_facet(npdProject.getTastingrequirement());	
		projectSchema.setNPD_PROJECT_TASTING_REQUIREMENT_ITEM_ID(npdProject.getTastingrequirementitemid());
		projectSchema.setNPD_PROJECT_TASTING_REQUIREMENT_ITEM_ID_facet(npdProject.getTastingrequirementitemid());
		
		

//		---------------------------------------------------------
		
//		projectSchema.setNPD_PROJECT_TRIAL_COMMERCIAL_PROD_INCLUDED(projectDetail.getTrialOrFirstProductionIncludedInThePlan());
//		projectSchema.setNPD_PROJECT_TRIAL_COMMERCIAL_PROD_INCLUDED_facet(projectDetail.getTrialOrFirstProductionIncludedInThePlan());		
//		projectSchema.setNPD_PROJECT_PROGRAM_MANAGER_SLACK_TIME(Integer.parseInt(projectDetail.getProgramManagerSlackTime()));
//		projectSchema.setNPD_PROJECT_NO_OF_PIECES_REQUIRED(!util.isNullOrEmpty(projectDetail.getNoOfPiecesRequired()) ? 
//				Integer.parseInt(projectDetail.getNoOfPiecesRequired()) : 0);	
//		projectSchema.setNPD_PROJECT_TARGET_CP2_DATE(projectDetail.getc);
//		projectSchema.setNPD_PROJECT_TARGET_CP2_DATE_facet(NPD_PROJECT_TAGET_CP2_DATE_facet);

//		projectSchema.setNPD_PROJECT_RAG(projectDetail.getTimelineRisk());
//		projectSchema.setNPD_PROJECT_RAG_facet(projectDetail.getTimelineRisk());	
//		projectSchema.setNPD_PROJECT_HEALTH(NPD_PROJECT_HEALTH);
//		projectSchema.setNPD_PROJECT_HEALTH_facet(NPD_PROJECT_HEALTH_facet);
//		projectSchema.setNPD_PROJECT_NO_OF_WEEKS_INCREASED(!util.isNullOrEmpty(projectDetail.getNoOfWeeksIncreaseVsPrior()) ? 
//				Integer.parseInt(projectDetail.getNoOfWeeksIncreaseVsPrior()) : 0);		
//		projectSchema.setNPD_PROJECT_EAN_TARGET_WEEK(projectDetail.getEANCodeSharedWithBottlerTarget());	
//		projectSchema.setNPD_PROJECT_EAN_TARGET_WEEK_facet(projectDetail.getEANCodeSharedWithBottlerTarget());
//		logger.info(" projectDetail.getProdInfoSheetSharedActual()" + projectDetail.getProdInfoSheetSharedActual());	
//		projectSchema.setNPD_PROJECT_PROD_INFO_TARGET_WEEK(projectDetail.getProdInfoSheetSharedTarget());
//		projectSchema.setNPD_PROJECT_PROD_INFO_TARGET_WEEK_facet(projectDetail.getProdInfoSheetSharedTarget());
//		projectSchema.setNPD_PROJECT_BOTTLER_CAN_TARGET_WEEK(NPD_PROJECT_BOTTLER_CAN_TARGET_WEEK);
//		projectSchema.setNPD_PROJECT_BOTTLER_CAN_TARGET_WEEK_facet(NPD_PROJECT_BOTTLER_CAN_TARGET_WEEK_facet);
//		projectSchema.setNPD_PROJECT_NUMBER_OF_CAN_COMPANIES(!util.isNullOrEmpty(projectDetail.getNoOfCanCompaniesReq()) ?
//				Integer.parseInt(projectDetail.getNoOfCanCompaniesReq()) : 0);
//		projectSchema.setNPD_PROJECT_PALLET_LABEL_RECEIVED_TARGET_WEEK(NPD_PROJECT_PALLET_LABEL_RECEIVED_TARGET_WEEK);
//		projectSchema.setNPD_PROJECT_PALLET_LABEL_RECEIVED_TARGET_WEEK_facet(NPD_PROJECT_PALLET_LABEL_RECEIVED_TARGET_WEEK_facet);
//		projectSchema.setNPD_PROJECT_PALLET_LABEL_SENT_ACTUAL_WEEK(projectDetail.getPalletLabelSentActual());
//		projectSchema.setNPD_PROJECT_PALLET_LABEL_SENT_ACTUAL_WEEK_facet(projectDetail.getPalletLabelSentActual());		
//		projectSchema.setNPD_PROJECT_PALLET_LABEL_SENT_TARGET_WEEK(NPD_PROJECT_PALLET_LABEL_SENT_TARGET_WEEK);
//		projectSchema.setNPD_PROJECT_PALLET_LABEL_SENT_TARGET_WEEK_facet(NPD_PROJECT_PALLET_LABEL_SENT_TARGET_WEEK_facet);
//		projectSchema.setNPD_PROJECT_JOB_ID(projectDetail.getJobID());
//		projectSchema.setNPD_PROJECT_JOB_ID_facet(projectDetail.getJobID());
//		projectSchema.setNPD_PROJECT_JOB_STATUS(projectDetail.);
//		projectSchema.setNPD_PROJECT_JOB_STATUS_facet(NPD_PROJECT_JOB_STATUS_facet);
//		projectSchema.setNPD_PROJECT_JOB_NAME(NPD_PROJECT_JOB_NAME);
//		projectSchema.setNPD_PROJECT_JOB_NAME_facet(NPD_PROJECT_JOB_NAME_facet);
//		projectSchema.setNPD_PROJECT_NSV_Y1_ANNUALISED(request.get);
//		projectSchema.setNPD_PROJECT_NSV_Y1_ANNUALISED_facet(NPD_PROJECT_NSV_Y1_ANNUALISED_facet);
//		projectSchema.setNPD_PROJECT_VOLUME_1ST_3_MONTHS(NPD_PROJECT_VOLUME_1ST_3_MONTHS);
//		projectSchema.setNPD_PROJECT_VOLUME_1ST_3_MONTHS_facet(NPD_PROJECT_VOLUME_1ST_3_MONTHS_facet);
//		projectSchema.setNPD_PROJECT_VOLUME_Y1_ANNUALISED(NPD_PROJECT_VOLUME_Y1_ANNUALISED);
//		projectSchema.setNPD_PROJECT_VOLUME_Y1_ANNUALISED_facet(NPD_PROJECT_VOLUME_Y1_ANNUALISED_facet);
//		projectSchema.setNPD_PROJECT_CP1_AGREEMENT_DATE(projectDetail.getCP1DeliveryWeek());
//		projectSchema.setNPD_PROJECT_CP1_AGREEMENT_DATE_facet(projectDetail.getCP1DeliveryWeek());	
//		projectSchema.setNPD_PROJECT_LEAD_TIME_TARGET(NPD_PROJECT_LEAD_TIME_TARGET);
//		projectSchema.setNPD_PROJECT_LEAD_TIME_TARGET_facet(NPD_PROJECT_LEAD_TIME_TARGET_facet);
//		projectSchema.setNPD_PROJECT_CP1_ACTUAL_DATE(NPD_PROJECT_CP1_ACTUAL_DATE);
//		projectSchema.setNPD_PROJECT_CP1_ACTUAL_DATE_facet(NPD_PROJECT_CP1_ACTUAL_DATE_facet);	
//		projectSchema.setNPD_PROJECT_EARLIEST_TRIAL_OR_1ST_PROD_DATE(projectDetail.getEarliestTrialOr1stProd());
//		projectSchema.setNPD_PROJECT_EARLIEST_TRIAL_OR_1ST_PROD_DATE_facet(projectDetail.getEarliestTrialOr1stProd());

//		projectSchema.setNPD_PROJECT_HBC_RPE_MSEB_CREATED(projectDetail.getHBCRPEMESBCreated());
//		projectSchema.setNPD_PROJECT_HBC_RPE_MSEB_CREATED_facet(projectDetail.getHBCRPEMESBCreated());
//		projectSchema.setNPD_PROJECT_PLANT_SPECIFIC_RESPONSIBLE_FOR_COMPLETION(projectDetail.getWhoWillComplete());
//		projectSchema.setNPD_PROJECT_PLANT_SPECIFIC_RESPONSIBLE_FOR_COMPLETION_facet(projectDetail.getWhoWillComplete());	
//		projectSchema.setNPD_PROJECT_TECHNICAL_OR_QUALITY_SUPERVISION_REQ(projectDetail.getTechnicalOrQualitySupervisionRequired());
//		projectSchema.setNPD_PROJECT_TECHNICAL_OR_QUALITY_SUPERVISION_REQ_facet(projectDetail.getTechnicalOrQualitySupervisionRequired());	
//		projectSchema.setNPD_PROJECT_PRINT_SUPPLIER(NPD_PROJECT_PRINT_SUPPLIER);
//		projectSchema.setNPD_PROJECT_PRINT_SUPPLIER_facet(NPD_PROJECT_PRINT_SUPPLIER_facet);
//		projectSchema.setNPD_PROJECT_PRINT_SUPPLIER_ITEM_ID(NPD_PROJECT_PRINT_SUPPLIER_ITEM_ID);
//		projectSchema.setNPD_PROJECT_PRINT_SUPPLIER_ITEM_ID_facet(NPD_PROJECT_PRINT_SUPPLIER_ITEM_ID_facet);
//		projectSchema.setNPD_PROJECT_TECH_QUAL_WEEK_DUE(NPD_PROJECT_TECH_QUAL_WEEK_DUE);
//		projectSchema.setNPD_PROJECT_TECH_QUAL_WEEK_DUE_facet(NPD_PROJECT_TECH_QUAL_WEEK_DUE_facet);		
//		projectSchema.set();
//		projectSchema.setNPD_PROJECT_LOCAL_BOM_IN_SAP("");
//		projectSchema.setNPD_PROJECT_NSV_Y1_ANNUALISED();
//		projectSchema.setNPD_PROJECT_VOLUME_Y1_ANNUALISED();
//		projectSchema.setNPD_PROJECT_VOLUME_1ST_3_MONTHS();
//		projectSchema.setNPD_PROJECT_1ST_PROD_MATERIALS_CONFIRMED_DELIVERY_WEEK(projectDetail.get);
//		projectSchema.setNPD_PROJECT_1ST_PROD_MATERIALS_CONFIRMED_DELIVERY_WEEK_facet(NPD_PROJECT_1ST_PROD_MATERIALS_CONFIRMED_DELIVERY_WEEK_facet);
//		projectSchema.setNPD_PROJECT_PM_REPORTING_COMMENTS_facet("");
//		projectSchema.setNPD_PROJECT_PM_REPORTING_COMMENTS("");
//		projectSchema.seNPD_PROJECT_DESCRIPTION_facet();
		
//		NPD_PROJECT_E2E_PM_ID
//		NPD_PROJECT_CORP_PM_ID
//		NPD_PROJECT_OPS_PM_ID

		
		
		MPMProjectSchema responseSchema = projectSolrRepository.save(projectSchema);
		
		logger.error("Project Indexed Successfully.." + responseSchema.getItem_id() );
		return responseSchema.getItem_id();

		
	}
	

}
