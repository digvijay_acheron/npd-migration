package com.monster.npd.migration.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class MetadataField {
	@JsonInclude(Include.NON_NULL)
	String id;
	@JsonInclude(Include.NON_NULL)
	String type;
	@JsonInclude(Include.NON_NULL)
	MetadataValueContainer value;
	
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("metadata_element_list")
	List<MetadataField> metadataElementList;
}