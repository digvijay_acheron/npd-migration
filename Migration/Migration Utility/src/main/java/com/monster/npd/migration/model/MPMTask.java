package com.monster.npd.migration.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Table(name = "shrdacheronmpmcoretask")
@Entity
@ToString
public class MPMTask {
	
	@Id
	@GeneratedValue(generator = "mpm_task_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "mpm_task_seq", sequenceName = "mpm_task_seq",allocationSize=1)
    @Column(name = "id")
    private Integer id;
	
    @Column(name = "action_type")
    private String actionType;

    @Column(name = "actual_due_date")
    private Date actualDueDate;
    
    @Column(name = "actual_start_date")
    private Date actualStartDate;
    
    @Column(name = "assignment_type")
    private String assignmentType;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "due_date")
    private Date dueDate;

    @Column(name = "end_date")
    private Date endDate;
    
    @Column(name = "expected_duration")
    private Integer expectedDuration;
    
    @Column(name = "is_active")
    private Boolean isActive;
    
    @Column(name = "is_approval_task")
    private Boolean isApprovalTask; 
    
    @Column(name = "is_bulk_update")
    private Boolean isBulkUpdate;
    
    @Column(name = "is_deleted")
    private Boolean isDeleted;
    
    @Column(name = "is_milestone")
    private Boolean isMilestone;
    
    @Column(name = "is_on_hold")
    private Boolean isOnHold;
    
    @Column(name = "is_subproject")
    private Boolean isSubproject;
    
    @Column(name = "iteration_count")
    private Integer iterationCount;
    
    @Column(name = "max_deliverable_date")
    private Date maxDeliverableDate;

    @Column(name = "min_deliverable_date")
    private Date minDeliverableDate;

    @Column(name = "name")
    private String name;
    
    @Column(name = "original_due_date")
    private Date originalDueDate;

    @Column(name = "original_start_date")
    private Date originalStartDate;
    
    @Column(name = "parent_task_id")
    private Integer parentTaskId;
    
    @Column(name = "progress")
    private Integer progress;
    
    @Column(name = "project_flow_type")
    private String projectFlowType;
    
    @Column(name = "reference_id")
    private String referenceId;
       
    @Column(name = "reference_link_id")
    private String referenceLinkId;
    
    @Column(name = "revision_review_required")
    private Boolean revisionReviewRequired;
    
    @Column(name = "root_parent_task_id")
    private Integer rootParentTaskId;
    
    @Column(name = "start_date")
    private Date startDate;
    
    @Column(name = "task_duration")
    private Integer taskDuration;
    
    @Column(name = "task_duration_type")
    private String taskDurationType;
    
    @Column(name = "task_ref_id")
    private String taskRefId;
    
    @Column(name = "task_type")
    private String taskType;
    
    @Column(name = "time_spent")
    private Integer timeSpent;
    
    @Column(name = "view_other_comments")
    private Boolean viewOtherComments;
    
    //RelationShips
        
//    @Column(name = "r_po_active_user_id_id")
//    private Integer activeUserId;
    
    @OneToOne
    @JoinColumn(name = "r_po_active_user_id_id", referencedColumnName = "Id")
    private OpenTextIdentity activeUserId;
    
    @Column(name = "r_po_campaign_id")
    private Integer campaignId;
    
//    @Column(name = "r_po_owner_id_id")
//    private Integer ownerId;
    
    @OneToOne
    @JoinColumn(name = "r_po_owner_id_id", referencedColumnName = "Id")
    private OpenTextIdentity ownerId;
    
    @Column(name = "r_po_priority_id")
    private Integer priorityId;
    
    @Column(name = "r_po_project_id")
    private Integer projectId;
    
    @Column(name = "r_po_status_id")
    private Integer statusId;
    
    @Column(name = "r_po_subproject_id")
    private Integer subprojectId;
    
//    @Column(name = "r_po_team_role_id")
//    private Integer teamRoleId;
    
    @OneToOne
    @JoinColumn(name = "r_po_team_role_id", referencedColumnName = "Id")
    private MPMTeamRole teamRoleId;
	
	// Tracking Data
    @Column(name = "lastmodifiedby_id")
    private Integer lastModifiedBy;
    
    @Column(name = "createdby_id")
    private Integer createdBy;
	
    @Column(name = "s_createddate")
    private Date createdDate;
    
    @Column(name = "s_lastmodifieddate")
    private Date lastModifiedDate;
    
    @Column(name = "s_item_status")
    private Integer itemStatus;
    
    @Column(name = "s_is_temporary_copy")
    private Boolean temporaryCopy;
    
    //@Column(name = "s_temporary_copy_data")
    //private String temporaryCopyData;
    
    @Column(name = "s_organizationid")
    private int organizationid;

}
