package com.monster.npd.migration.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.monster.npd.migration.controller.MigrationController;
import com.monster.npd.migration.model.MPMTask;
import com.monster.npd.migration.model.TaskRoleMappingDependencies;
import com.monster.npd.migration.model.WorkflowRules;
import com.monster.npd.migration.repository.MPMTaskRepository;
import com.monster.npd.migration.repository.TaskRoleMappingRepository;
import com.monster.npd.migration.repository.WorkflowRulesRepository;

@Service
public class WorkflowRulesService {
	
//	private static final Logger logger = LoggerFactory.getLogger(WorkflowRulesService.class);
	private static final Logger logger = LogManager.getLogger(WorkflowRulesService.class);
	
	@Autowired
	TaskRoleMappingRepository taskRoleMappingRepository;
	
	@Autowired
	MPMTaskRepository MPMTaskRepository;
	
	@Autowired
	WorkflowRulesRepository workflowRulesRepository;
	
	@Autowired
	Environment env;
	
	@Autowired
	UtilService utilService;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public List<TaskRoleMappingDependencies> getWorkflowRulesConfig(Integer projectClassificationNumber, 
			List<TaskRoleMappingDependencies> taskRuleConfigList) {
		LinkedHashMap<String, ArrayList<String>> projectTasksMap = new LinkedHashMap<String, ArrayList<String>>();
		LinkedHashMap<String, ArrayList<String>> allTaskRoleMap = new LinkedHashMap<String, ArrayList<String>>();
		LinkedHashMap<String, ArrayList<String>> workflowRulesMap = new LinkedHashMap<String, ArrayList<String>>();
		List<TaskRoleMappingDependencies> workflowRules = new ArrayList<TaskRoleMappingDependencies>();
		for(TaskRoleMappingDependencies tempTaskRuleConfig : taskRuleConfigList) {
			ArrayList<String> dependencyTasksList = new ArrayList<String>();
			projectTasksMap.put(tempTaskRuleConfig.getTask_name(), dependencyTasksList);
		}
		
		List<TaskRoleMappingDependencies> taskRoleMappingList = taskRoleMappingRepository.findAll();
//		logger.error("all task role " + taskRoleMappingList);
		
//		for(TaskRoleMappingDependencies df: taskRoleMappingList) {
//			logger.error("task : " + df.getTask_name() + " pre: " + df.getPredecessor_task_name());
//		}
		
		for(TaskRoleMappingDependencies taskRole : taskRoleMappingList) {
			ArrayList<String> dependencyTasksList = new ArrayList<String>();
//			logger.error("Task_name :" + taskRole.getTask_name() + " Predecessor_task_name :"+ taskRole.getPredecessor_task_name());
			if (allTaskRoleMap.containsKey(taskRole.getTask_name())) {
				
				ArrayList<String> tempDependencyTasksList = allTaskRoleMap.get(taskRole.getTask_name());
				dependencyTasksList.add(taskRole.getPredecessor_task_name());
				dependencyTasksList.addAll(tempDependencyTasksList);
				allTaskRoleMap.put(taskRole.getTask_name(), dependencyTasksList);
//				logger.error("Already exists - Predecessor_task_name() " + taskRole.getPredecessor_task_name());
				
			} else {
//				logger.error("Adding predec " + taskRole.getPredecessor_task_name());
				dependencyTasksList.add(taskRole.getPredecessor_task_name());
				allTaskRoleMap.put(taskRole.getTask_name(), dependencyTasksList);
			}
			
//			logger.error("dependencyTasksList " + dependencyTasksList);
		}
		
//		logger.error("allTaskRoleMap :" + allTaskRoleMap);
//		logger.error("projectTasksMap :" + projectTasksMap);
		
		for (Entry<String, ArrayList<String>> entry : projectTasksMap.entrySet()) {
//			logger.error("entry.getKey() :" + entry.getKey());
			if (allTaskRoleMap.containsKey(entry.getKey())) {
				ArrayList<String> dependencyTasksList = new ArrayList<String>();							
				ArrayList<String> tempDependencyTasksList = new ArrayList<String>();
				
				allTaskRoleMap.get(entry.getKey()).stream().forEach(dependentTask -> {
//					logger.error("dependentTask :" + dependentTask);
					if(projectTasksMap.containsKey(dependentTask)) {							
						tempDependencyTasksList.add(dependentTask);	
//						logger.error("AAdded :" + dependentTask);
					}
				});
				
				if(workflowRulesMap.containsKey(entry.getKey())) {
//					logger.error("Already exists :" );
					dependencyTasksList = workflowRulesMap.get(entry.getKey());
					tempDependencyTasksList.addAll(dependencyTasksList);
				}	
//				logger.error("dependencyTasksList :" + dependencyTasksList);
//				logger.error("tempDependencyTasksList :" + tempDependencyTasksList);
				workflowRulesMap.put(entry.getKey(), tempDependencyTasksList);
				
			}
			
		}
		
//		logger.error("workflowRulesMap :" + workflowRulesMap);
		
		for (Entry<String, ArrayList<String>> entry : workflowRulesMap.entrySet()) {
			
			ArrayList<String> dependencyTasksList = entry.getValue();
			List<String> uniqueDependencyTasksList = dependencyTasksList.stream().distinct().collect(Collectors.toList());
			uniqueDependencyTasksList.forEach(dependecyTask -> {
				TaskRoleMappingDependencies workflowRule = new TaskRoleMappingDependencies();
				workflowRule.setTask_name(entry.getKey());
				workflowRule.setPredecessor_task_name(dependecyTask);
//				logger.error("TASK :" + workflowRule.getTask_name());
//				logger.error("PREDECESSOR TASK :" + workflowRule.getPredecessor_task_name());
				workflowRules.add(workflowRule);
			});
			
		}
////		logger.error("Final :" + workflowRules);
//		for(TaskRoleMappingDependencies df: workflowRules) {
//		logger.error("task : " + df.getTask_name() + " pre: " + df.getPredecessor_task_name());
//	}
		return workflowRules;
		
	}
	
	@Transactional
	public void createWorkflowRules(Integer projectId, Integer projectClassificationNumber) {
		logger.info("insideee" +projectId +  projectClassificationNumber);
//		int count = 2400;
		List<TaskRoleMappingDependencies> taskRuleConfigList = new ArrayList<TaskRoleMappingDependencies>();
		List<MPMTask> taskList = MPMTaskRepository.findByProjectId(projectId);
		List<MPMTask> initialTaskList =  new ArrayList<MPMTask>();
		List<WorkflowRules> workflowRulesList = new ArrayList<WorkflowRules>();
		for(MPMTask task : taskList ) {
			TaskRoleMappingDependencies taskRoleMappingDependencies = new TaskRoleMappingDependencies();
			taskRoleMappingDependencies.setTask_name(task.getName());
			taskRuleConfigList.add(taskRoleMappingDependencies);
		}
		
		initialTaskList = taskList.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 2")).collect(Collectors.toList());
		
		
//		logger.info(" size " + initialTaskList.size());
//		if(utilService.isNullOrEmpty(initialTaskList)) {
		if(initialTaskList.size() == 0) {
//			logger.info("yess" );
			initialTaskList = taskList.stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase("TASK 3") || obj.getName().equalsIgnoreCase("TASK 4") ||
							obj.getName().equalsIgnoreCase("TASK 5") || obj.getName().equalsIgnoreCase("TASK 6"))
					.collect(Collectors.toList());
		}
		
		if(initialTaskList.size() == 0) {
//			logger.info("yess 2" );
			initialTaskList = taskList.stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase("TASK 7"))
					.collect(Collectors.toList());
		}
//		logger.info("Initial taskList " + initialTaskList);
		
		int forCount = 0;
		List<TaskRoleMappingDependencies> workflowRules = getWorkflowRulesConfig(projectClassificationNumber, taskRuleConfigList);
		for(TaskRoleMappingDependencies rule : workflowRules) {
			forCount++;
			MPMTask task = taskList.stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(rule.getTask_name())).findAny().orElse(null);
			
			MPMTask targetTask = taskList.stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(rule.getPredecessor_task_name())).findAny().orElse(null);
			
			if(task!= null && targetTask!=null && !utilService.isNullOrEmpty(task.getId()) 
					&& !utilService.isNullOrEmpty(targetTask.getId())) {
				
				MPMTask initialTask = initialTaskList.stream()
						.filter(obj -> Objects.nonNull(obj.getName()))
						.filter(obj -> obj.getName().equalsIgnoreCase(targetTask.getName())).findAny().orElse(null);
				
//				for(MPMTask iniTask : initialTaskList ) {
//					logger.info("iniTask " + iniTask.getName() + " " + iniTask.getId());
//				}
//				
//				logger.info("task " + task.getId() + task.getName() + " targetTask " + targetTask.getId());
//				
//				if(initialTask != null) {
//					logger.info("initialTask " + initialTask.getId());
//				}
						
				Boolean isInitial = false;
				
				if(!utilService.isNullOrEmpty(initialTask)) {
//					logger.info("init task " + initialTask.getId() + " task " + targetTask.getId() );
					isInitial = true;
				}
				WorkflowRules workflowRule = new WorkflowRules();
				workflowRule.setTrigger_type("STATUS");
				workflowRule.setTarget_type("TASK");
				workflowRule.setIs_initial_rule(isInitial);
				workflowRule.setR_po_project_id(projectId);
				workflowRule.setR_po_task_id(targetTask.getId());
				workflowRule.setR_po_status_id(Integer.parseInt(env.getProperty("npd.task.completed.status.id")));
				workflowRule.setR_po_target_task_id(task.getId());
//				workflowRule.setId(workflowRulesRepository.getCurrentVal());
				workflowRule.setS_organizationid(Integer.parseInt(env.getProperty("npd.org.id")));
//				logger.info("env id "+ workflowRule.getS_organizationid());
				workflowRulesRepository.save(workflowRule);
				entityManager.flush();
//				workflowRulesList.add(workflowRule);
				
				 if ( (forCount % 50) == 0) {
			            entityManager.flush();
			            entityManager.clear();
			        }
			}
						
		}
		
		entityManager.flush();
		
//		List<WorkflowRules> response =  workflowRulesRepository.saveAll(workflowRulesList);
		logger.info("All workflow rules created successfully .. " );
	}

}
