package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@Table(name = "shrdnpdlookupdomainmodelproject_type")
@Entity
public class ProjectType {

	  @Id
	    @Column(name = "Id", updatable = false)
	    private String id;

	    @Column(name = "display_name", updatable = false)
	    private String displayName;
	    
	    @Column(name = "name", updatable = false)
	    private String name;
	    
}