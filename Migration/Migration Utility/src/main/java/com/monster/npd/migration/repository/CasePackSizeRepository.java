package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.CasePackSize;

@Repository
public interface CasePackSizeRepository extends JpaRepository<CasePackSize, Integer> {
	
	CasePackSize findByDisplayName(String name);

}
