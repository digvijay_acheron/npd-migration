package com.monster.npd.migration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.MPMTask;

@Repository
public interface MPMTaskRepository extends JpaRepository<MPMTask, Integer> {
	
	List<MPMTask> findByProjectId(Integer projectId);
	
	@Query("SELECT MAX(p.id) FROM MPMTask p")
    Integer maxMPMTaskId();
	
	@Query(value = "SELECT NEXT VALUE FOR dbo.mpm_task_seq", nativeQuery = true)
    public Integer getCurrentVal();
	
}
