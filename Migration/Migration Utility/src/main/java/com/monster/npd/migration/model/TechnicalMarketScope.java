package com.monster.npd.migration.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@Table(name = "shrdnpdsubmissiontechnical_market_scope")
@Entity
public class TechnicalMarketScope {

		@Id
		@GeneratedValue(generator = "tech_market_seq", strategy = GenerationType.SEQUENCE)
		@SequenceGenerator(name = "tech_market_seq", sequenceName = "tech_market_seq",allocationSize=1)
		 @Column(name = "Id")
	    private Integer id;
	
	    @Column(name = "targetdpinwarehouse")
	    private String targetDPInWarehouse;

	  	@Column(name = "LEAD_MARKET")
	    private Boolean leadMarket;
	  	
	  	@OneToOne
	    @JoinColumn(name = "R_PO_MARKET_Id", referencedColumnName = "Id")
	    private Market market;
	    
//	    @ManyToMany(mappedBy = "technicalMarketScope", fetch = FetchType.LAZY)
//	    private Set<Request> requests = new HashSet<Request>();
	  	
	  	@OneToMany(mappedBy = "technicalMarketScope", cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	    private Set<RequestTechnicalMarketScope> requestTechnicalMarketScope = new HashSet<RequestTechnicalMarketScope>();
	    
	    @Column(name = "s_organizationid")
	    private int organizationid;
}