package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.SKUDetails;

@Repository
public interface SKUDetailsRepository extends JpaRepository<SKUDetails, Integer> {
	
	SKUDetails findByDisplayName(String name);

}
