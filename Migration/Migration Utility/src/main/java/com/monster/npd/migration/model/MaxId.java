package com.monster.npd.migration.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MaxId {
	
	Integer auditLogMaxId;
	Integer requestMaxId;
	Integer mpmProjectMaxId;
	Integer npdProjectMaxId;
	Integer mpmTaskMaxId;
	Integer npdTaskMaxId;
	Integer deliverableMaxId;
	Integer workflowRuleMaxId;
	Integer techMarkerScopeMaxId;
	Integer comMarkerScopeMaxId;
	String taskRoleName;
	
	String projectItemId;
	Integer requestId;
	Integer projectId;
	List<MPMTaskSchema> taskSchemaList;
	
}
