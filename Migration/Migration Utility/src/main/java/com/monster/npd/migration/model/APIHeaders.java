package com.monster.npd.migration.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum APIHeaders {
	
	CONTENT_TYPE("Content-Type"), 
    APPLICATION_JSON("application/json"),
    APPLICATION_JSON_CHARSET("application/json;charset=UTF-8"),
    X_REQUESTED_BY("X-Requested-By"),
    COOKIE("Cookie"),
    OTDSTICKET("OTDSTicket");
	
	 @Getter
	 private String value;

}
