package com.monster.npd.migration.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.monster.npd.migration.controller.MigrationController;
import com.monster.npd.migration.repository.MPMTaskRepository;
import com.monster.npd.migration.repository.NPDTaskRepository;
import com.monster.npd.migration.repository.TaskSolrRepository;
import com.monster.npd.migration.service.ConfigService;
import com.monster.npd.migration.service.SolrIndexingService;
import com.monster.npd.migration.service.TaskMigrationService;
import com.monster.npd.migration.service.UtilService;

@Service
public class TaskIndexingService {
	
private static final Logger logger = LoggerFactory.getLogger(TaskIndexingService.class);
	
	@Autowired
	private TaskSolrRepository taskSolrRepository;
	
	@Autowired
	private MPMTaskRepository MPMTaskRepository;
	
	@Autowired
	SolrIndexingService solrIndexingService;
	
	@Autowired
	Environment env;
	
	@Autowired
	UtilService util;

	@Autowired
	NPDTaskRepository NPDTaskRepository;
	
	@PersistenceContext
	private EntityManager entityManager; 
	
@Transactional	
public List<MPMTaskSchema> indexTask(List<MPMTask> taskResponse, String projectItemId, TaskDetails taskDetails) throws SolrException, SolrServerException, Exception {
		
//		int npdTaskCount = 0;
		
		String taskItemIdPrefix = env.getProperty("npd.task.itemId");
		List<MPMTaskSchema> taskSchemaList = new ArrayList<MPMTaskSchema>();
		
		MPMTask task2 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 2")).findAny().orElse(null);
		if(task2 != null && !util.isNullOrEmpty(task2.getId())) {
			MPMTaskSchema task2Schema = new MPMTaskSchema();
			task2Schema.setContent_type("MPM_TASK");
			task2Schema.setId(String.valueOf(task2.getId()));
			task2Schema.setIs_active_task(String.valueOf(task2.getIsActive()));
			task2Schema.setIs_approval_task("false");
			task2Schema.setIs_deleted_task("false");
			task2Schema.setIs_milestone("false");
			task2Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task2.getId())));
			task2Schema.setOriginal_task_due_date(task2.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task2Schema.setOriginal_task_start_date(task2.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task2Schema.setProject_item_id(projectItemId);
			
			task2Schema.setTask_active_user_cn(!util.isNullOrEmpty(task2.getOwnerId()) && 
					!util.isNullOrEmpty(task2.getOwnerId().getUserId()) ? task2.getOwnerId().getUserId() : "");
			task2Schema.setTask_active_user_id(!util.isNullOrEmpty(task2.getOwnerId()) && 
					!util.isNullOrEmpty(task2.getOwnerId().getId()) ? task2.getOwnerId().getId() : 0);
			task2Schema.setTask_active_user_name(task2.getOwnerId().getIdentityDisplayName());
			task2Schema.setTask_owner_cn(!util.isNullOrEmpty(task2.getOwnerId()) && 
					!util.isNullOrEmpty(task2.getOwnerId().getUserId()) ? task2.getOwnerId().getUserId() : "");
			task2Schema.setTask_owner_id(!util.isNullOrEmpty(task2.getOwnerId()) && 
					!util.isNullOrEmpty(task2.getOwnerId().getId()) ? task2.getOwnerId().getId() : 0);
			task2Schema.setTask_owner_name(!util.isNullOrEmpty(task2.getOwnerId()) && 
					!util.isNullOrEmpty(task2.getOwnerId().getIdentityDisplayName()) ? 
							task2.getOwnerId().getIdentityDisplayName() : "");
			task2Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task2.getOwnerId()) && 
					!util.isNullOrEmpty(task2.getOwnerId().getIdentityDisplayName()) ?
							task2.getOwnerId().getIdentityDisplayName() : "");
			
			task2Schema.setTask_description(task2.getDescription());
			task2Schema.setTask_description_facet(task2.getDescription());
			task2Schema.setTask_due_date(task2.getDueDate().toString().concat("T00:00:00Z"));
			task2Schema.setTask_duration(task2.getTaskDuration());
			task2Schema.setTask_duration_type("weeks");
			task2Schema.setTask_milestone_progress("0");
			task2Schema.setTask_name(task2.getName());
			task2Schema.setTask_priority_id(task2.getPriorityId());
			task2Schema.setTask_priority_value("HIGH");
			task2Schema.setTask_priority_value_facet("HIGH");
			task2Schema.setTask_role_id(task2.getTeamRoleId().getId());
			task2Schema.setTask_role_value(task2.getTeamRoleId().getName());
			task2Schema.setTask_role_value_facet(task2.getTeamRoleId().getName());
			task2Schema.setTask_start_date(task2.getStartDate().toString().concat("T00:00:00Z"));
			task2Schema.setTask_status_id(task2.getStatusId());
			
			MPMStatus task2Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task2.getStatusId()))).findAny().orElse(null);
					
			task2Schema.setTask_status_type(task2Status.getStatusType());
			task2Schema.setTask_status_value(task2Status.getName());
			task2Schema.setTask_status_value_facet(task2Status.getName());
			task2Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task2Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task2Schema.setNPD_TASK_CAN_START(String.valueOf(task2.getIsActive()));
			task2Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask2ActualStartDate());
			task2Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask2ActualDueDate());
			
			
			taskSchemaList.add(task2Schema);
			
			
			NPDTask npdTask2 = new NPDTask();
//			npdTask2.setId(NPDTaskRepository.getCurrentVal());
			npdTask2.setCanstart(task2Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask2.setActualduedate(taskDetails.getTask2ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask2ActualDueDate().split("T")[0]) : null);
			npdTask2.setActualstartdate(taskDetails.getTask2ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask2ActualStartDate().split("T")[0]) : null);
			npdTask2.setTask_id(Integer.parseInt(task2Schema.getId()));
			npdTask2.setTask_item_id(task2Schema.getItem_id());
			npdTask2.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask2);
			entityManager.flush();
			
			
		}
		
		MPMTask task3 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 3")).findAny().orElse(null);
		if(task3 != null && !util.isNullOrEmpty(task3.getId())) {
			
			
			
			MPMTaskSchema task3Schema = new MPMTaskSchema();
			task3Schema.setContent_type("MPM_TASK");
			task3Schema.setId(String.valueOf(task3.getId()));
			task3Schema.setIs_active_task(String.valueOf(task3.getIsActive()));
			task3Schema.setIs_approval_task("false");
			task3Schema.setIs_deleted_task("false");
			task3Schema.setIs_milestone("false");
			task3Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task3.getId())));
			task3Schema.setOriginal_task_due_date(task3.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task3Schema.setOriginal_task_start_date(task3.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task3Schema.setProject_item_id(projectItemId);
			task3Schema.setTask_description(task3.getDescription());
			task3Schema.setTask_description_facet(task3.getDescription());
			task3Schema.setTask_due_date(task3.getDueDate().toString().concat("T00:00:00Z"));
			task3Schema.setTask_duration(task3.getTaskDuration());
			task3Schema.setTask_duration_type("weeks");
			task3Schema.setTask_milestone_progress("0");
			task3Schema.setTask_name(task3.getName());
			task3Schema.setTask_priority_id(task3.getPriorityId());
			task3Schema.setTask_priority_value("HIGH");
			task3Schema.setTask_priority_value_facet("HIGH");
			task3Schema.setTask_role_id(task3.getTeamRoleId().getId());
			task3Schema.setTask_role_value(task3.getTeamRoleId().getName());
			task3Schema.setTask_role_value_facet(task3.getTeamRoleId().getName());
			task3Schema.setTask_start_date(task3.getStartDate().toString().concat("T00:00:00Z"));
			task3Schema.setTask_status_id(task3.getStatusId());
			
			task3Schema.setTask_active_user_cn(!util.isNullOrEmpty(task3.getOwnerId()) && 
					!util.isNullOrEmpty(task3.getOwnerId().getUserId()) ? task3.getOwnerId().getUserId() : "");
			task3Schema.setTask_active_user_id(!util.isNullOrEmpty(task3.getOwnerId()) && 
					!util.isNullOrEmpty(task3.getOwnerId().getId()) ? task3.getOwnerId().getId() : 0);
			task3Schema.setTask_active_user_name(!util.isNullOrEmpty(task3.getOwnerId()) && 
					!util.isNullOrEmpty(task3.getOwnerId().getIdentityDisplayName()) ? 
					task3.getOwnerId().getIdentityDisplayName() : "");
			task3Schema.setTask_owner_cn(!util.isNullOrEmpty(task3.getOwnerId()) && 
					!util.isNullOrEmpty(task3.getOwnerId().getUserId()) ? task3.getOwnerId().getUserId() : "");
			task3Schema.setTask_owner_id(!util.isNullOrEmpty(task3.getOwnerId()) && 
					!util.isNullOrEmpty(task3.getOwnerId().getId()) ? task3.getOwnerId().getId() : 0);
			task3Schema.setTask_owner_name(!util.isNullOrEmpty(task3.getOwnerId()) && 
					!util.isNullOrEmpty(task3.getOwnerId().getIdentityDisplayName()) ? 
					task3.getOwnerId().getIdentityDisplayName() : "");
			task3Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task3.getOwnerId()) && 
					!util.isNullOrEmpty(task3.getOwnerId().getIdentityDisplayName()) ? 
					task3.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task3Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task3.getStatusId()))).findAny().orElse(null);
					
			task3Schema.setTask_status_type(task3Status.getStatusType());
			task3Schema.setTask_status_value(task3Status.getName());
			task3Schema.setTask_status_value_facet(task3Status.getName());
			task3Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task3Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task3Schema.setNPD_TASK_CAN_START(String.valueOf(task3.getIsActive()));
			task3Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask3ActualStartDate());
			task3Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask3ActualDueDate());
			taskSchemaList.add(task3Schema);
			
			
			
			NPDTask npdTask3 = new NPDTask();
//			npdTask3.setId(NPDTaskRepository.getCurrentVal());
			npdTask3.setCanstart(task3Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask3.setActualduedate(taskDetails.getTask3ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask3ActualDueDate().split("T")[0]) : null);
			npdTask3.setActualstartdate(taskDetails.getTask3ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask3ActualStartDate().split("T")[0]) : null);
			npdTask3.setTask_id(Integer.parseInt(task3Schema.getId()));
			npdTask3.setTask_item_id(task3Schema.getItem_id());
			npdTask3.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask3);
			entityManager.flush();
			
		}
		
		MPMTask task4 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 4")).findAny().orElse(null);
		if(task4 != null && !util.isNullOrEmpty(task4.getId())) {
			logger.info("Coming inside task 4 " + task4.getId());
			
			MPMTaskSchema task4Schema = new MPMTaskSchema();
			task4Schema.setContent_type("MPM_TASK");
			task4Schema.setId(String.valueOf(task4.getId()));
			task4Schema.setIs_active_task(String.valueOf(task4.getIsActive()));
			task4Schema.setIs_approval_task("false");
			task4Schema.setIs_deleted_task("false");
			task4Schema.setIs_milestone("false");
			task4Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task4.getId())));
			task4Schema.setOriginal_task_due_date(task4.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task4Schema.setOriginal_task_start_date(task4.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task4Schema.setProject_item_id(projectItemId);
			task4Schema.setTask_description(task4.getDescription());
			task4Schema.setTask_description_facet(task4.getDescription());
			task4Schema.setTask_due_date(task4.getDueDate().toString().concat("T00:00:00Z"));
			task4Schema.setTask_duration(task4.getTaskDuration());
			task4Schema.setTask_duration_type("weeks");
			task4Schema.setTask_milestone_progress("0");
			task4Schema.setTask_name(task4.getName());
			task4Schema.setTask_priority_id(task4.getPriorityId());
			task4Schema.setTask_priority_value("HIGH");
			task4Schema.setTask_priority_value_facet("HIGH");
			task4Schema.setTask_role_id(task4.getTeamRoleId().getId());
			task4Schema.setTask_role_value(task4.getTeamRoleId().getName());
			task4Schema.setTask_role_value_facet(task4.getTeamRoleId().getName());
			task4Schema.setTask_start_date(task4.getStartDate().toString().concat("T00:00:00Z"));
			task4Schema.setTask_status_id(task4.getStatusId());
			task4Schema.setTask_active_user_cn(!util.isNullOrEmpty(task4.getOwnerId()) && 
					!util.isNullOrEmpty(task4.getOwnerId().getUserId()) ? task4.getOwnerId().getUserId() : "");
			task4Schema.setTask_active_user_id(!util.isNullOrEmpty(task4.getOwnerId()) && 
					!util.isNullOrEmpty(task4.getOwnerId().getId()) ? task4.getOwnerId().getId() : 0);
			task4Schema.setTask_active_user_name(!util.isNullOrEmpty(task4.getOwnerId()) && 
					!util.isNullOrEmpty(task4.getOwnerId().getIdentityDisplayName()) ? 
					task4.getOwnerId().getIdentityDisplayName() : "");
			task4Schema.setTask_owner_cn(!util.isNullOrEmpty(task4.getOwnerId()) && 
					!util.isNullOrEmpty(task4.getOwnerId().getUserId()) ? task4.getOwnerId().getUserId() : "");
			task4Schema.setTask_owner_id(!util.isNullOrEmpty(task4.getOwnerId()) && 
					!util.isNullOrEmpty(task4.getOwnerId().getId()) ? task4.getOwnerId().getId() : 0);
			task4Schema.setTask_owner_name(!util.isNullOrEmpty(task4.getOwnerId()) && 
					!util.isNullOrEmpty(task4.getOwnerId().getIdentityDisplayName()) ? 
					task4.getOwnerId().getIdentityDisplayName() : "");
			task4Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task4.getOwnerId()) && 
					!util.isNullOrEmpty(task4.getOwnerId().getIdentityDisplayName()) ? 
					task4.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task4Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task4.getStatusId()))).findAny().orElse(null);
					
			task4Schema.setTask_status_type(task4Status.getStatusType());
			task4Schema.setTask_status_value(task4Status.getName());
			task4Schema.setTask_status_value_facet(task4Status.getName());
			task4Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task4Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task4Schema.setNPD_TASK_CAN_START(String.valueOf(task4.getIsActive()));
			task4Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask4ActualStartDate());
			task4Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask4ActualDueDate());
			taskSchemaList.add(task4Schema);
			
			
			NPDTask npdTask4 = new NPDTask();
//			npdTask4.setId(NPDTaskRepository.getCurrentVal());
			npdTask4.setCanstart(task4Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask4.setActualduedate(taskDetails.getTask4ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask4ActualDueDate().split("T")[0]) : null);
			npdTask4.setActualstartdate(taskDetails.getTask4ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask4ActualStartDate().split("T")[0]) : null);
			npdTask4.setTask_id(Integer.parseInt(task4Schema.getId()));
			npdTask4.setTask_item_id(task4Schema.getItem_id());
			npdTask4.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask4);
			entityManager.flush();
			
		}
		
		MPMTask task5 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 5")).findAny().orElse(null);
		if(task5 != null && !util.isNullOrEmpty(task5.getId())) {
			MPMTaskSchema task5Schema = new MPMTaskSchema();
			task5Schema.setContent_type("MPM_TASK");
			task5Schema.setId(String.valueOf(task5.getId()));
			task5Schema.setIs_active_task(String.valueOf(task5.getIsActive()));
			task5Schema.setIs_approval_task("false");
			task5Schema.setIs_deleted_task("false");
			task5Schema.setIs_milestone("false");
			task5Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task5.getId())));
			task5Schema.setOriginal_task_due_date(task5.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task5Schema.setOriginal_task_start_date(task5.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task5Schema.setProject_item_id(projectItemId);
			task5Schema.setTask_description(task5.getDescription());
			task5Schema.setTask_description_facet(task5.getDescription());
			task5Schema.setTask_due_date(task5.getDueDate().toString().concat("T00:00:00Z"));
			task5Schema.setTask_duration(task5.getTaskDuration());
			task5Schema.setTask_duration_type("weeks");
			task5Schema.setTask_milestone_progress("0");
			task5Schema.setTask_name(task5.getName());
			task5Schema.setTask_priority_id(task5.getPriorityId());
			task5Schema.setTask_priority_value("HIGH");
			task5Schema.setTask_priority_value_facet("HIGH");
			task5Schema.setTask_role_id(task5.getTeamRoleId().getId());
			task5Schema.setTask_role_value(task5.getTeamRoleId().getName());
			task5Schema.setTask_role_value_facet(task5.getTeamRoleId().getName());
			task5Schema.setTask_start_date(task5.getStartDate().toString().concat("T00:00:00Z"));
			task5Schema.setTask_status_id(task5.getStatusId());
			task5Schema.setTask_active_user_cn(!util.isNullOrEmpty(task5.getOwnerId()) && 
					!util.isNullOrEmpty(task5.getOwnerId().getUserId()) ? task5.getOwnerId().getUserId() : "");
			task5Schema.setTask_active_user_id(!util.isNullOrEmpty(task5.getOwnerId()) && 
					!util.isNullOrEmpty(task5.getOwnerId().getId()) ? task5.getOwnerId().getId() : 0);
			task5Schema.setTask_active_user_name(!util.isNullOrEmpty(task5.getOwnerId()) && 
					!util.isNullOrEmpty(task5.getOwnerId().getIdentityDisplayName()) ? 
					task5.getOwnerId().getIdentityDisplayName() : "");
			task5Schema.setTask_owner_cn(!util.isNullOrEmpty(task5.getOwnerId()) && 
					!util.isNullOrEmpty(task5.getOwnerId().getUserId()) ? task5.getOwnerId().getUserId() : "");
			task5Schema.setTask_owner_id(!util.isNullOrEmpty(task5.getOwnerId()) && 
					!util.isNullOrEmpty(task5.getOwnerId().getId()) ? task5.getOwnerId().getId() : 0);
			task5Schema.setTask_owner_name(!util.isNullOrEmpty(task5.getOwnerId()) && 
					!util.isNullOrEmpty(task5.getOwnerId().getIdentityDisplayName()) ? 
					task5.getOwnerId().getIdentityDisplayName() : "");
			task5Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task5.getOwnerId()) && 
					!util.isNullOrEmpty(task5.getOwnerId().getIdentityDisplayName()) ? 
					task5.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task5Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task5.getStatusId()))).findAny().orElse(null);
					
			task5Schema.setTask_status_type(task5Status.getStatusType());
			task5Schema.setTask_status_value(task5Status.getName());
			task5Schema.setTask_status_value_facet(task5Status.getName());
			task5Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task5Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task5Schema.setNPD_TASK_CAN_START(String.valueOf(task5.getIsActive()));
			task5Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask5ActualStartDate());
			task5Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask5ActualDueDate());
			taskSchemaList.add(task5Schema);
			
			
			NPDTask npdTask5 = new NPDTask();
//			npdTask5.setId(NPDTaskRepository.getCurrentVal());
			npdTask5.setCanstart(task5Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask5.setActualduedate(taskDetails.getTask5ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask5ActualDueDate().split("T")[0]) : null);
			npdTask5.setActualstartdate(taskDetails.getTask5ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask5ActualStartDate().split("T")[0]) : null);
			npdTask5.setTask_id(Integer.parseInt(task5Schema.getId()));
			npdTask5.setTask_item_id(task5Schema.getItem_id());
			npdTask5.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask5);
			entityManager.flush();
			
		}
		
		MPMTask task6 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 6")).findAny().orElse(null);
		if(task6 != null && !util.isNullOrEmpty(task6.getId())) {
			MPMTaskSchema task6Schema = new MPMTaskSchema();
			task6Schema.setContent_type("MPM_TASK");
			task6Schema.setId(String.valueOf(task6.getId()));
			task6Schema.setIs_active_task(String.valueOf(task6.getIsActive()));
			task6Schema.setIs_approval_task("false");
			task6Schema.setIs_deleted_task("false");
			task6Schema.setIs_milestone("false");
			task6Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task6.getId())));
			task6Schema.setOriginal_task_due_date(task6.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task6Schema.setOriginal_task_start_date(task6.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task6Schema.setProject_item_id(projectItemId);
			task6Schema.setTask_description(task6.getDescription());
			task6Schema.setTask_description_facet(task6.getDescription());
			task6Schema.setTask_due_date(task6.getDueDate().toString().concat("T00:00:00Z"));
			task6Schema.setTask_duration(task6.getTaskDuration());
			task6Schema.setTask_duration_type("weeks");
			task6Schema.setTask_milestone_progress("0");
			task6Schema.setTask_name(task6.getName());
			task6Schema.setTask_priority_id(task6.getPriorityId());
			task6Schema.setTask_priority_value("HIGH");
			task6Schema.setTask_priority_value_facet("HIGH");
			task6Schema.setTask_role_id(task6.getTeamRoleId().getId());
			task6Schema.setTask_role_value(task6.getTeamRoleId().getName());
			task6Schema.setTask_role_value_facet(task6.getTeamRoleId().getName());
			task6Schema.setTask_start_date(task6.getStartDate().toString().concat("T00:00:00Z"));
			task6Schema.setTask_status_id(task6.getStatusId());
			task6Schema.setTask_active_user_cn(!util.isNullOrEmpty(task6.getOwnerId()) && 
					!util.isNullOrEmpty(task6.getOwnerId().getUserId()) ? task6.getOwnerId().getUserId() : "");
			task6Schema.setTask_active_user_id(!util.isNullOrEmpty(task6.getOwnerId()) && 
					!util.isNullOrEmpty(task6.getOwnerId().getId()) ? task6.getOwnerId().getId() : 0);
			task6Schema.setTask_active_user_name(!util.isNullOrEmpty(task6.getOwnerId()) && 
					!util.isNullOrEmpty(task6.getOwnerId().getIdentityDisplayName()) ? 
					task6.getOwnerId().getIdentityDisplayName() : "");
			task6Schema.setTask_owner_cn(!util.isNullOrEmpty(task6.getOwnerId()) && 
					!util.isNullOrEmpty(task6.getOwnerId().getUserId()) ? task6.getOwnerId().getUserId() : "");
			task6Schema.setTask_owner_id(!util.isNullOrEmpty(task6.getOwnerId()) && 
					!util.isNullOrEmpty(task6.getOwnerId().getId()) ? task6.getOwnerId().getId() : 0);
			task6Schema.setTask_owner_name(!util.isNullOrEmpty(task6.getOwnerId()) && 
					!util.isNullOrEmpty(task6.getOwnerId().getIdentityDisplayName()) ? 
					task6.getOwnerId().getIdentityDisplayName() : "");
			task6Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task6.getOwnerId()) && 
					!util.isNullOrEmpty(task6.getOwnerId().getIdentityDisplayName()) ? 
					task6.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task6Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task6.getStatusId()))).findAny().orElse(null);
					
			task6Schema.setTask_status_type(task6Status.getStatusType());
			task6Schema.setTask_status_value(task6Status.getName());
			task6Schema.setTask_status_value_facet(task6Status.getName());
			task6Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task6Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task6Schema.setNPD_TASK_CAN_START(String.valueOf(task6.getIsActive()));
			task6Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask6ActualStartDate());
			task6Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask6ActualDueDate());
			taskSchemaList.add(task6Schema);
			
			
			NPDTask npdTask6 = new NPDTask();
//			npdTask6.setId(NPDTaskRepository.getCurrentVal());
			npdTask6.setCanstart(task6Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask6.setActualduedate(taskDetails.getTask6ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask6ActualDueDate().split("T")[0]) : null);
			npdTask6.setActualstartdate(taskDetails.getTask6ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask6ActualStartDate().split("T")[0]) : null);
			npdTask6.setTask_id(Integer.parseInt(task6Schema.getId()));
			npdTask6.setTask_item_id(task6Schema.getItem_id());
			npdTask6.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask6);
			entityManager.flush();
			
		}
		MPMTask task7 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 7")).findAny().orElse(null);
		if(task7 != null && !util.isNullOrEmpty(task7.getId())) {
			MPMTaskSchema task7Schema = new MPMTaskSchema();
			task7Schema.setContent_type("MPM_TASK");
			task7Schema.setId(String.valueOf(task7.getId()));
			task7Schema.setIs_active_task(String.valueOf(task7.getIsActive()));
			task7Schema.setIs_approval_task("false");
			task7Schema.setIs_deleted_task("false");
			task7Schema.setIs_milestone("false");
			task7Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task7.getId())));
			task7Schema.setOriginal_task_due_date(task7.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task7Schema.setOriginal_task_start_date(task7.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task7Schema.setProject_item_id(projectItemId);
			task7Schema.setTask_description(task7.getDescription());
			task7Schema.setTask_description_facet(task7.getDescription());
			task7Schema.setTask_due_date(task7.getDueDate().toString().concat("T00:00:00Z"));
			task7Schema.setTask_duration(task7.getTaskDuration());
			task7Schema.setTask_duration_type("weeks");
			task7Schema.setTask_milestone_progress("0");
			task7Schema.setTask_name(task7.getName());
			task7Schema.setTask_priority_id(task7.getPriorityId());
			task7Schema.setTask_priority_value("HIGH");
			task7Schema.setTask_priority_value_facet("HIGH");
			task7Schema.setTask_role_id(task7.getTeamRoleId().getId());
			task7Schema.setTask_role_value(task7.getTeamRoleId().getName());
			task7Schema.setTask_role_value_facet(task7.getTeamRoleId().getName());
			task7Schema.setTask_start_date(task7.getStartDate().toString().concat("T00:00:00Z"));
			task7Schema.setTask_status_id(task7.getStatusId());
			task7Schema.setTask_active_user_cn(!util.isNullOrEmpty(task7.getOwnerId()) && 
					!util.isNullOrEmpty(task7.getOwnerId().getUserId()) ? task7.getOwnerId().getUserId() : "");
			task7Schema.setTask_active_user_id(!util.isNullOrEmpty(task7.getOwnerId()) && 
					!util.isNullOrEmpty(task7.getOwnerId().getId()) ? task7.getOwnerId().getId() : 0);
			task7Schema.setTask_active_user_name(!util.isNullOrEmpty(task7.getOwnerId()) && 
					!util.isNullOrEmpty(task7.getOwnerId().getIdentityDisplayName()) ? 
					task7.getOwnerId().getIdentityDisplayName() : "");
			task7Schema.setTask_owner_cn(!util.isNullOrEmpty(task7.getOwnerId()) && 
					!util.isNullOrEmpty(task7.getOwnerId().getUserId()) ? task7.getOwnerId().getUserId() : "");
			task7Schema.setTask_owner_id(!util.isNullOrEmpty(task7.getOwnerId()) && 
					!util.isNullOrEmpty(task7.getOwnerId().getId()) ? task7.getOwnerId().getId() : 0);
			task7Schema.setTask_owner_name(!util.isNullOrEmpty(task7.getOwnerId()) && 
					!util.isNullOrEmpty(task7.getOwnerId().getIdentityDisplayName()) ? 
					task7.getOwnerId().getIdentityDisplayName() : "");
			task7Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task7.getOwnerId()) && 
					!util.isNullOrEmpty(task7.getOwnerId().getIdentityDisplayName()) ? 
					task7.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task7Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task7.getStatusId()))).findAny().orElse(null);
					
			task7Schema.setTask_status_type(task7Status.getStatusType());
			task7Schema.setTask_status_value(task7Status.getName());
			task7Schema.setTask_status_value_facet(task7Status.getName());
			task7Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task7Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task7Schema.setNPD_TASK_PROCESS_AUTHORITY_APPROVAL(taskDetails.getTask7IsProcessAuthorityApproval());
			task7Schema.setNPD_TASK_PROCESS_AUTHORITY_APPROVAL_facet(taskDetails.getTask7IsProcessAuthorityApproval());
			task7Schema.setNPD_TASK_SCOPING_DOCUMENT_REQUIREMENT(taskDetails.getTask7ScopingDocumentRequirement());
			task7Schema.setNPD_TASK_SCOPING_DOCUMENT_REQUIREMENT_facet(taskDetails.getTask7ScopingDocumentRequirement());
			
			task7Schema.setNPD_TASK_CAN_START(String.valueOf(task7.getIsActive()));;
			task7Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask7ActualStartDate());
			task7Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask7ActualDueDate());
			
			taskSchemaList.add(task7Schema);
			
			
			NPDTask npdTask7 = new NPDTask();
//			npdTask7.setId(NPDTaskRepository.getCurrentVal());
			npdTask7.setCanstart(task7Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask7.setActualduedate(taskDetails.getTask7ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask7ActualDueDate().split("T")[0]) : null);
			npdTask7.setActualstartdate(taskDetails.getTask7ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask7ActualStartDate().split("T")[0]) : null);
			npdTask7.setTask_id(Integer.parseInt(task7Schema.getId()));
			npdTask7.setTask_item_id(task7Schema.getItem_id());
			npdTask7.setScopingdocumentrequirement(taskDetails.getTask7ScopingDocumentRequirement());
			npdTask7.setProcessAuthorityRequired(taskDetails.getTask7IsProcessAuthorityApproval().equalsIgnoreCase("true") 
					? true : false);
			npdTask7.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask7);
			entityManager.flush();
			
		}
		
		MPMTask task9 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 9")).findAny().orElse(null);
		if(task9 != null && !util.isNullOrEmpty(task9.getId())) {
			MPMTaskSchema task9Schema = new MPMTaskSchema();
			task9Schema.setContent_type("MPM_TASK");
			task9Schema.setId(String.valueOf(task9.getId()));
			task9Schema.setIs_active_task(String.valueOf(task9.getIsActive()));
			task9Schema.setIs_approval_task("false");
			task9Schema.setIs_deleted_task("false");
			task9Schema.setIs_milestone("false");
			task9Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task9.getId())));
			task9Schema.setOriginal_task_due_date(task9.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task9Schema.setOriginal_task_start_date(task9.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task9Schema.setProject_item_id(projectItemId);
			task9Schema.setTask_description(task9.getDescription());
			task9Schema.setTask_description_facet(task9.getDescription());
			task9Schema.setTask_due_date(task9.getDueDate().toString().concat("T00:00:00Z"));
			task9Schema.setTask_duration(task9.getTaskDuration());
			task9Schema.setTask_duration_type("weeks");
			task9Schema.setTask_milestone_progress("0");
			task9Schema.setTask_name(task9.getName());
			task9Schema.setTask_priority_id(task9.getPriorityId());
			task9Schema.setTask_priority_value("HIGH");
			task9Schema.setTask_priority_value_facet("HIGH");
			task9Schema.setTask_role_id(task9.getTeamRoleId().getId());
			task9Schema.setTask_role_value(task9.getTeamRoleId().getName());
			task9Schema.setTask_role_value_facet(task9.getTeamRoleId().getName());
			task9Schema.setTask_start_date(task9.getStartDate().toString().concat("T00:00:00Z"));
			task9Schema.setTask_status_id(task9.getStatusId());
			task9Schema.setTask_active_user_cn(!util.isNullOrEmpty(task9.getOwnerId()) && 
					!util.isNullOrEmpty(task9.getOwnerId().getUserId()) ? task9.getOwnerId().getUserId() : "");
			task9Schema.setTask_active_user_id(!util.isNullOrEmpty(task9.getOwnerId()) && 
					!util.isNullOrEmpty(task9.getOwnerId().getId()) ? task9.getOwnerId().getId() : 0);
			task9Schema.setTask_active_user_name(!util.isNullOrEmpty(task9.getOwnerId()) && 
					!util.isNullOrEmpty(task9.getOwnerId().getIdentityDisplayName()) ? 
					task9.getOwnerId().getIdentityDisplayName() : "");
			task9Schema.setTask_owner_cn(!util.isNullOrEmpty(task9.getOwnerId()) && 
					!util.isNullOrEmpty(task9.getOwnerId().getUserId()) ? task9.getOwnerId().getUserId() : "");
			task9Schema.setTask_owner_id(!util.isNullOrEmpty(task9.getOwnerId()) && 
					!util.isNullOrEmpty(task9.getOwnerId().getId()) ? task9.getOwnerId().getId() : 0);
			task9Schema.setTask_owner_name(!util.isNullOrEmpty(task9.getOwnerId()) && 
					!util.isNullOrEmpty(task9.getOwnerId().getIdentityDisplayName()) ? 
					task9.getOwnerId().getIdentityDisplayName() : "");
			task9Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task9.getOwnerId()) && 
					!util.isNullOrEmpty(task9.getOwnerId().getIdentityDisplayName()) ? 
					task9.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task9Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task9.getStatusId()))).findAny().orElse(null);
					
			task9Schema.setTask_status_type(task9Status.getStatusType());
			task9Schema.setTask_status_value(task9Status.getName());
			task9Schema.setTask_status_value_facet(task9Status.getName());
			task9Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task9Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task9Schema.setNPD_TASK_CAN_START(String.valueOf(task9.getIsActive()));;
			task9Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask9ActualStartDate());
			task9Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask9ActualDueDate());
			
//			task9Schema.setNPD_TASK_WINSHUTTLE_PROJECT(!util.isNullOrEmpty(taskDetails.getTask9WinshuttleProjectNo()) ? 
//					Integer.parseInt(taskDetails.getTask9WinshuttleProjectNo()) : 0);
			taskSchemaList.add(task9Schema);
			
			
			NPDTask npdTask9 = new NPDTask();
//			npdTask9.setId(NPDTaskRepository.getCurrentVal());
			npdTask9.setCanstart(task9Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask9.setActualduedate(taskDetails.getTask9ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask9ActualDueDate().split("T")[0]) : null);
			npdTask9.setActualstartdate(taskDetails.getTask9ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask9ActualStartDate().split("T")[0]) : null);
			npdTask9.setTask_id(Integer.parseInt(task9Schema.getId()));
			npdTask9.setTask_item_id(task9Schema.getItem_id());
			npdTask9.setWinshuttleproject(taskDetails.getTask9WinshuttleProjectNo());
			npdTask9.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask9);
			entityManager.flush();
			
		}
		
		MPMTask task10 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 10")).findAny().orElse(null);
		if(task10 != null && !util.isNullOrEmpty(task10.getId())) {
			MPMTaskSchema task10Schema = new MPMTaskSchema();
			task10Schema.setContent_type("MPM_TASK");
			task10Schema.setId(String.valueOf(task10.getId()));
			task10Schema.setIs_active_task(String.valueOf(task10.getIsActive()));
			task10Schema.setIs_approval_task("false");
			task10Schema.setIs_deleted_task("false");
			task10Schema.setIs_milestone("false");
			task10Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task10.getId())));
			task10Schema.setOriginal_task_due_date(task10.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task10Schema.setOriginal_task_start_date(task10.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task10Schema.setProject_item_id(projectItemId);
			task10Schema.setTask_description(task10.getDescription());
			task10Schema.setTask_description_facet(task10.getDescription());
			task10Schema.setTask_due_date(task10.getDueDate().toString().concat("T00:00:00Z"));
			task10Schema.setTask_duration(task10.getTaskDuration());
			task10Schema.setTask_duration_type("weeks");
			task10Schema.setTask_milestone_progress("0");
			task10Schema.setTask_name(task10.getName());
			task10Schema.setTask_priority_id(task10.getPriorityId());
			task10Schema.setTask_priority_value("HIGH");
			task10Schema.setTask_priority_value_facet("HIGH");
			task10Schema.setTask_role_id(task10.getTeamRoleId().getId());
			task10Schema.setTask_role_value(task10.getTeamRoleId().getName());
			task10Schema.setTask_role_value_facet(task10.getTeamRoleId().getName());
			task10Schema.setTask_start_date(task10.getStartDate().toString().concat("T00:00:00Z"));
			task10Schema.setTask_status_id(task10.getStatusId());
			task10Schema.setTask_active_user_cn(!util.isNullOrEmpty(task10.getOwnerId()) && 
					!util.isNullOrEmpty(task10.getOwnerId().getUserId()) ? task10.getOwnerId().getUserId() : "");
			task10Schema.setTask_active_user_id(!util.isNullOrEmpty(task10.getOwnerId()) && 
					!util.isNullOrEmpty(task10.getOwnerId().getId()) ? task10.getOwnerId().getId() : 0);
			task10Schema.setTask_active_user_name(!util.isNullOrEmpty(task10.getOwnerId()) && 
					!util.isNullOrEmpty(task10.getOwnerId().getIdentityDisplayName()) ? 
					task10.getOwnerId().getIdentityDisplayName() : "");
			task10Schema.setTask_owner_cn(!util.isNullOrEmpty(task10.getOwnerId()) && 
					!util.isNullOrEmpty(task10.getOwnerId().getUserId()) ? task10.getOwnerId().getUserId() : "");
			task10Schema.setTask_owner_id(!util.isNullOrEmpty(task10.getOwnerId()) && 
					!util.isNullOrEmpty(task10.getOwnerId().getId()) ? task10.getOwnerId().getId() : 0);
			task10Schema.setTask_owner_name(!util.isNullOrEmpty(task10.getOwnerId()) && 
					!util.isNullOrEmpty(task10.getOwnerId().getIdentityDisplayName()) ? 
					task10.getOwnerId().getIdentityDisplayName() : "");
			task10Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task10.getOwnerId()) && 
					!util.isNullOrEmpty(task10.getOwnerId().getIdentityDisplayName()) ? 
					task10.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task10Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task10.getStatusId()))).findAny().orElse(null);
					
			task10Schema.setTask_status_type(task10Status.getStatusType());
			task10Schema.setTask_status_value(task10Status.getName());
			task10Schema.setTask_status_value_facet(task10Status.getName());
			task10Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task10Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task10Schema.setNPD_TASK_REQUIRED_COMPLETE(taskDetails.getTask10RequiredComplete());
			
			task10Schema.setNPD_TASK_CAN_START(String.valueOf(task10.getIsActive()));;
			task10Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask10ActualStartDate());
			task10Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask10ActualDueDate());
			taskSchemaList.add(task10Schema);
			
			
			NPDTask npdTask10 = new NPDTask();
//			npdTask10.setId(NPDTaskRepository.getCurrentVal());
			npdTask10.setCanstart(task10Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask10.setActualduedate(taskDetails.getTask10ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask10ActualDueDate().split("T")[0]) : null);
			npdTask10.setActualstartdate(taskDetails.getTask10ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask10ActualStartDate().split("T")[0]) : null);
			npdTask10.setTask_id(Integer.parseInt(task10Schema.getId()));
			npdTask10.setTask_item_id(task10Schema.getItem_id());
			npdTask10.setRequiredcomplete(taskDetails.getTask10RequiredComplete());
			npdTask10.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask10);
			entityManager.flush();
			
		}
		
		MPMTask task11 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 11")).findAny().orElse(null);
		if(task11 != null && !util.isNullOrEmpty(task11.getId())) {
			MPMTaskSchema task11Schema = new MPMTaskSchema();
			task11Schema.setContent_type("MPM_TASK");
			task11Schema.setId(String.valueOf(task11.getId()));
			task11Schema.setIs_active_task(String.valueOf(task11.getIsActive()));
			task11Schema.setIs_approval_task("false");
			task11Schema.setIs_deleted_task("false");
			task11Schema.setIs_milestone("false");
			task11Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task11.getId())));
			task11Schema.setOriginal_task_due_date(task11.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task11Schema.setOriginal_task_start_date(task11.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task11Schema.setProject_item_id(projectItemId);
			task11Schema.setTask_description(task11.getDescription());
			task11Schema.setTask_description_facet(task11.getDescription());
			task11Schema.setTask_due_date(task11.getDueDate().toString().concat("T00:00:00Z"));
			task11Schema.setTask_duration(task11.getTaskDuration());
			task11Schema.setTask_duration_type("weeks");
			task11Schema.setTask_milestone_progress("0");
			task11Schema.setTask_name(task11.getName());
			task11Schema.setTask_priority_id(task11.getPriorityId());
			task11Schema.setTask_priority_value("HIGH");
			task11Schema.setTask_priority_value_facet("HIGH");
			task11Schema.setTask_role_id(task11.getTeamRoleId().getId());
			task11Schema.setTask_role_value(task11.getTeamRoleId().getName());
			task11Schema.setTask_role_value_facet(task11.getTeamRoleId().getName());
			task11Schema.setTask_start_date(task11.getStartDate().toString().concat("T00:00:00Z"));
			task11Schema.setTask_status_id(task11.getStatusId());
			task11Schema.setTask_active_user_cn(!util.isNullOrEmpty(task11.getOwnerId()) && 
					!util.isNullOrEmpty(task11.getOwnerId().getUserId()) ? task11.getOwnerId().getUserId() : "");
			task11Schema.setTask_active_user_id(!util.isNullOrEmpty(task11.getOwnerId()) && 
					!util.isNullOrEmpty(task11.getOwnerId().getId()) ? task11.getOwnerId().getId() : 0);
			task11Schema.setTask_active_user_name(!util.isNullOrEmpty(task11.getOwnerId()) && 
					!util.isNullOrEmpty(task11.getOwnerId().getIdentityDisplayName()) ? 
					task11.getOwnerId().getIdentityDisplayName() : "");
			task11Schema.setTask_owner_cn(!util.isNullOrEmpty(task11.getOwnerId()) && 
					!util.isNullOrEmpty(task11.getOwnerId().getUserId()) ? task11.getOwnerId().getUserId() : "");
			task11Schema.setTask_owner_id(!util.isNullOrEmpty(task11.getOwnerId()) && 
					!util.isNullOrEmpty(task11.getOwnerId().getId()) ? task11.getOwnerId().getId() : 0);
			task11Schema.setTask_owner_name(!util.isNullOrEmpty(task11.getOwnerId()) && 
					!util.isNullOrEmpty(task11.getOwnerId().getIdentityDisplayName()) ? 
					task11.getOwnerId().getIdentityDisplayName() : "");
			task11Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task11.getOwnerId()) && 
					!util.isNullOrEmpty(task11.getOwnerId().getIdentityDisplayName()) ? 
					task11.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task11Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task11.getStatusId()))).findAny().orElse(null);
					
			task11Schema.setTask_status_type(task11Status.getStatusType());
			task11Schema.setTask_status_value(task11Status.getName());
			task11Schema.setTask_status_value_facet(task11Status.getName());
			task11Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task11Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task11Schema.setNPD_TASK_CAN_START(String.valueOf(task11.getIsActive()));;
			task11Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask11ActualStartDate());
			task11Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask11ActualDueDate());
			taskSchemaList.add(task11Schema);
			
			
			NPDTask npdTask11 = new NPDTask();
//			npdTask11.setId(NPDTaskRepository.getCurrentVal());
			npdTask11.setCanstart(task11Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask11.setActualduedate(taskDetails.getTask11ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask11ActualDueDate().split("T")[0]) : null);
			npdTask11.setActualstartdate(taskDetails.getTask11ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask11ActualStartDate().split("T")[0]) : null);
			npdTask11.setTask_id(Integer.parseInt(task11Schema.getId()));
			npdTask11.setTask_item_id(task11Schema.getItem_id());
			npdTask11.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask11);
			entityManager.flush();
			
		}
		
		MPMTask task12 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 12")).findAny().orElse(null);
		if(task12 != null && !util.isNullOrEmpty(task12.getId())) {
			MPMTaskSchema task12Schema = new MPMTaskSchema();
			task12Schema.setContent_type("MPM_TASK");
			task12Schema.setId(String.valueOf(task12.getId()));
			task12Schema.setIs_active_task(String.valueOf(task12.getIsActive()));
			task12Schema.setIs_approval_task("false");
			task12Schema.setIs_deleted_task("false");
			task12Schema.setIs_milestone("false");
			task12Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task12.getId())));
			task12Schema.setOriginal_task_due_date(task12.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task12Schema.setOriginal_task_start_date(task12.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task12Schema.setProject_item_id(projectItemId);
			task12Schema.setTask_description(task12.getDescription());
			task12Schema.setTask_description_facet(task12.getDescription());
			task12Schema.setTask_due_date(task12.getDueDate().toString().concat("T00:00:00Z"));
			task12Schema.setTask_duration(task12.getTaskDuration());
			task12Schema.setTask_duration_type("weeks");
			task12Schema.setTask_milestone_progress("0");
			task12Schema.setTask_name(task12.getName());
			task12Schema.setTask_priority_id(task12.getPriorityId());
			task12Schema.setTask_priority_value("HIGH");
			task12Schema.setTask_priority_value_facet("HIGH");
			task12Schema.setTask_role_id(task12.getTeamRoleId().getId());
			task12Schema.setTask_role_value(task12.getTeamRoleId().getName());
			task12Schema.setTask_role_value_facet(task12.getTeamRoleId().getName());
			task12Schema.setTask_start_date(task12.getStartDate().toString().concat("T00:00:00Z"));
			task12Schema.setTask_status_id(task12.getStatusId());
			task12Schema.setTask_active_user_cn(!util.isNullOrEmpty(task12.getOwnerId()) && 
					!util.isNullOrEmpty(task12.getOwnerId().getUserId()) ? task12.getOwnerId().getUserId() : "");
			task12Schema.setTask_active_user_id(!util.isNullOrEmpty(task12.getOwnerId()) && 
					!util.isNullOrEmpty(task12.getOwnerId().getId()) ? task12.getOwnerId().getId() : 0);
			task12Schema.setTask_active_user_name(!util.isNullOrEmpty(task12.getOwnerId()) && 
					!util.isNullOrEmpty(task12.getOwnerId().getIdentityDisplayName()) ? 
					task12.getOwnerId().getIdentityDisplayName() : "");
			task12Schema.setTask_owner_cn(!util.isNullOrEmpty(task12.getOwnerId()) && 
					!util.isNullOrEmpty(task12.getOwnerId().getUserId()) ? task12.getOwnerId().getUserId() : "");
			task12Schema.setTask_owner_id(!util.isNullOrEmpty(task12.getOwnerId()) && 
					!util.isNullOrEmpty(task12.getOwnerId().getId()) ? task12.getOwnerId().getId() : 0);
			task12Schema.setTask_owner_name(!util.isNullOrEmpty(task12.getOwnerId()) && 
					!util.isNullOrEmpty(task12.getOwnerId().getIdentityDisplayName()) ? 
					task12.getOwnerId().getIdentityDisplayName() : "");
			task12Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task12.getOwnerId()) && 
					!util.isNullOrEmpty(task12.getOwnerId().getIdentityDisplayName()) ? 
					task12.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task12Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task12.getStatusId()))).findAny().orElse(null);
					
			task12Schema.setTask_status_type(task12Status.getStatusType());
			task12Schema.setTask_status_value(task12Status.getName());
			task12Schema.setTask_status_value_facet(task12Status.getName());
			task12Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task12Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task12Schema.setNPD_TASK_CAN_START(String.valueOf(task12.getIsActive()));;
			task12Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask12ActualStartDate());
			task12Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask12ActualDueDate());
			taskSchemaList.add(task12Schema);
			
			
			NPDTask npdTask12 = new NPDTask();
//			npdTask12.setId(NPDTaskRepository.getCurrentVal());
			npdTask12.setCanstart(task12Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask12.setActualduedate(taskDetails.getTask12ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask12ActualDueDate().split("T")[0]) : null);
			npdTask12.setActualstartdate(taskDetails.getTask12ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask12ActualStartDate().split("T")[0]) : null);
			npdTask12.setTask_id(Integer.parseInt(task12Schema.getId()));
			npdTask12.setTask_item_id(task12Schema.getItem_id());
			npdTask12.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask12);
			entityManager.flush();
			
		}
		MPMTask task13 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 13")).findAny().orElse(null);
		if(task13 != null && !util.isNullOrEmpty(task13.getId())) {
			MPMTaskSchema task13Schema = new MPMTaskSchema();
			task13Schema.setContent_type("MPM_TASK");
			task13Schema.setId(String.valueOf(task13.getId()));
			task13Schema.setIs_active_task(String.valueOf(task13.getIsActive()));
			task13Schema.setIs_approval_task("false");
			task13Schema.setIs_deleted_task("false");
			task13Schema.setIs_milestone("false");
			task13Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task13.getId())));
			task13Schema.setOriginal_task_due_date(task13.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task13Schema.setOriginal_task_start_date(task13.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task13Schema.setProject_item_id(projectItemId);
			task13Schema.setTask_description(task13.getDescription());
			task13Schema.setTask_description_facet(task13.getDescription());
			task13Schema.setTask_due_date(task13.getDueDate().toString().concat("T00:00:00Z"));
			task13Schema.setTask_duration(task13.getTaskDuration());
			task13Schema.setTask_duration_type("weeks");
			task13Schema.setTask_milestone_progress("0");
			task13Schema.setTask_name(task13.getName());
			task13Schema.setTask_priority_id(task13.getPriorityId());
			task13Schema.setTask_priority_value("HIGH");
			task13Schema.setTask_priority_value_facet("HIGH");
			task13Schema.setTask_role_id(task13.getTeamRoleId().getId());
			task13Schema.setTask_role_value(task13.getTeamRoleId().getName());
			task13Schema.setTask_role_value_facet(task13.getTeamRoleId().getName());
			task13Schema.setTask_start_date(task13.getStartDate().toString().concat("T00:00:00Z"));
			task13Schema.setTask_status_id(task13.getStatusId());
			task13Schema.setTask_active_user_cn(!util.isNullOrEmpty(task13.getOwnerId()) && 
					!util.isNullOrEmpty(task13.getOwnerId().getUserId()) ? task13.getOwnerId().getUserId() : "");
			task13Schema.setTask_active_user_id(!util.isNullOrEmpty(task13.getOwnerId()) && 
					!util.isNullOrEmpty(task13.getOwnerId().getId()) ? task13.getOwnerId().getId() : 0);
			task13Schema.setTask_active_user_name(!util.isNullOrEmpty(task13.getOwnerId()) && 
					!util.isNullOrEmpty(task13.getOwnerId().getIdentityDisplayName()) ? 
					task13.getOwnerId().getIdentityDisplayName() : "");
			task13Schema.setTask_owner_cn(!util.isNullOrEmpty(task13.getOwnerId()) && 
					!util.isNullOrEmpty(task13.getOwnerId().getUserId()) ? task13.getOwnerId().getUserId() : "");
			task13Schema.setTask_owner_id(!util.isNullOrEmpty(task13.getOwnerId()) && 
					!util.isNullOrEmpty(task13.getOwnerId().getId()) ? task13.getOwnerId().getId() : 0);
			task13Schema.setTask_owner_name(!util.isNullOrEmpty(task13.getOwnerId()) && 
					!util.isNullOrEmpty(task13.getOwnerId().getIdentityDisplayName()) ? 
					task13.getOwnerId().getIdentityDisplayName() : "");
			task13Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task13.getOwnerId()) && 
					!util.isNullOrEmpty(task13.getOwnerId().getIdentityDisplayName()) ? 
					task13.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task13Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task13.getStatusId()))).findAny().orElse(null);
					
			task13Schema.setTask_status_type(task13Status.getStatusType());
			task13Schema.setTask_status_value(task13Status.getName());
			task13Schema.setTask_status_value_facet(task13Status.getName());
			task13Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task13Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task13Schema.setNPD_TASK_TRANSACTION_SCHEME_ANALYSIS_REQUIRED(taskDetails.getTask13IsTransactionSchemeAnalysisRequired());
			task13Schema.setNPD_TASK_TRANSACTION_SCHEME_ANALYSIS_REQUIRED_facet(taskDetails.getTask13IsTransactionSchemeAnalysisRequired());
			task13Schema.setNPD_TASK_TRANSACTION_SCHEME_ANALYSIS_REQUIRED(taskDetails.getTask13IsNewRMCosting());
			task13Schema.setNPD_TASK_TRANSACTION_SCHEME_ANALYSIS_REQUIRED_facet(taskDetails.getTask13IsNewRMCosting());
			
			task13Schema.setNPD_TASK_CAN_START(String.valueOf(task13.getIsActive()));;
			task13Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask13ActualStartDate());
			task13Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask13ActualDueDate());
			
			taskSchemaList.add(task13Schema);
			
			
			NPDTask npdTask13 = new NPDTask();
//			npdTask13.setId(NPDTaskRepository.getCurrentVal());
			npdTask13.setCanstart(task13Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask13.setActualduedate(taskDetails.getTask13ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask13ActualDueDate().split("T")[0]) : null);
			npdTask13.setActualstartdate(taskDetails.getTask13ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask13ActualStartDate().split("T")[0]) : null);
			npdTask13.setTask_id(Integer.parseInt(task13Schema.getId()));
			npdTask13.setTask_item_id(task13Schema.getItem_id());
			npdTask13.setTransactionSchemeAnalysisrequired(taskDetails.getTask13IsTransactionSchemeAnalysisRequired().equalsIgnoreCase("true") 
					? true : false);
			npdTask13.setNonProprietaryMaterialcostingRequired(taskDetails.getTask13IsNewRMCosting().equalsIgnoreCase("true") 
					? true : false);
			npdTask13.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask13);
			entityManager.flush();
			
		}
		
		MPMTask task13a = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 13A")).findAny().orElse(null);
		if(task13a != null && !util.isNullOrEmpty(task13a.getId())) {
			MPMTaskSchema task13aSchema = new MPMTaskSchema();
			task13aSchema.setContent_type("MPM_TASK");
			task13aSchema.setId(String.valueOf(task13a.getId()));
			task13aSchema.setIs_active_task(String.valueOf(task13a.getIsActive()));
			task13aSchema.setIs_approval_task("false");
			task13aSchema.setIs_deleted_task("false");
			task13aSchema.setIs_milestone("false");
			task13aSchema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task13a.getId())));
			task13aSchema.setOriginal_task_due_date(task13a.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task13aSchema.setOriginal_task_start_date(task13a.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task13aSchema.setProject_item_id(projectItemId);
			task13aSchema.setTask_description(task13a.getDescription());
			task13aSchema.setTask_description_facet(task13a.getDescription());
			task13aSchema.setTask_due_date(task13a.getDueDate().toString().concat("T00:00:00Z"));
			task13aSchema.setTask_duration(task13a.getTaskDuration());
			task13aSchema.setTask_duration_type("weeks");
			task13aSchema.setTask_milestone_progress("0");
			task13aSchema.setTask_name(task13a.getName());
			task13aSchema.setTask_priority_id(task13a.getPriorityId());
			task13aSchema.setTask_priority_value("HIGH");
			task13aSchema.setTask_priority_value_facet("HIGH");
			task13aSchema.setTask_role_id(task13a.getTeamRoleId().getId());
			task13aSchema.setTask_role_value(task13a.getTeamRoleId().getName());
			task13aSchema.setTask_role_value_facet(task13a.getTeamRoleId().getName());
			task13aSchema.setTask_start_date(task13a.getStartDate().toString().concat("T00:00:00Z"));
			task13aSchema.setTask_status_id(task13a.getStatusId());
			task13aSchema.setTask_active_user_cn(!util.isNullOrEmpty(task13a.getOwnerId()) && 
					!util.isNullOrEmpty(task13a.getOwnerId().getUserId()) ? task13a.getOwnerId().getUserId() : "");
			task13aSchema.setTask_active_user_id(!util.isNullOrEmpty(task13a.getOwnerId()) && 
					!util.isNullOrEmpty(task13a.getOwnerId().getId()) ? task13a.getOwnerId().getId() : 0);
			task13aSchema.setTask_active_user_name(!util.isNullOrEmpty(task13a.getOwnerId()) && 
					!util.isNullOrEmpty(task13a.getOwnerId().getIdentityDisplayName()) ? 
					task13a.getOwnerId().getIdentityDisplayName() : "");
			task13aSchema.setTask_owner_cn(!util.isNullOrEmpty(task13a.getOwnerId()) && 
					!util.isNullOrEmpty(task13a.getOwnerId().getUserId()) ? task13a.getOwnerId().getUserId() : "");
			task13aSchema.setTask_owner_id(!util.isNullOrEmpty(task13a.getOwnerId()) && 
					!util.isNullOrEmpty(task13a.getOwnerId().getId()) ? task13a.getOwnerId().getId() : 0);
			task13aSchema.setTask_owner_name(!util.isNullOrEmpty(task13a.getOwnerId()) && 
					!util.isNullOrEmpty(task13a.getOwnerId().getIdentityDisplayName()) ? 
					task13a.getOwnerId().getIdentityDisplayName() : "");
			task13aSchema.setTask_owner_name_facet(!util.isNullOrEmpty(task13a.getOwnerId()) && 
					!util.isNullOrEmpty(task13a.getOwnerId().getIdentityDisplayName()) ? 
					task13a.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task13aStatus = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task13a.getStatusId()))).findAny().orElse(null);
					
			task13aSchema.setTask_status_type(task13aStatus.getStatusType());
			task13aSchema.setTask_status_value(task13aStatus.getName());
			task13aSchema.setTask_status_value_facet(task13aStatus.getName());
			task13aSchema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task13aSchema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task13aSchema.setNPD_TASK_CAN_START(String.valueOf(task13a.getIsActive()));;
			task13aSchema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask13aActualStartDate());
			task13aSchema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask13aActualDueDate());
			taskSchemaList.add(task13aSchema);
			
			
			NPDTask npdTask13a = new NPDTask();
//			npdTask13a.setId(NPDTaskRepository.getCurrentVal());
			npdTask13a.setCanstart(task13aSchema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask13a.setActualduedate(taskDetails.getTask13aActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask13aActualDueDate().split("T")[0]) : null);
			npdTask13a.setActualstartdate(taskDetails.getTask13aActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask13aActualStartDate().split("T")[0]) : null);
			npdTask13a.setTask_id(Integer.parseInt(task13aSchema.getId()));
			npdTask13a.setTask_item_id(task13aSchema.getItem_id());
			npdTask13a.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			
			NPDTaskRepository.save(npdTask13a);
			entityManager.flush();
			
		}
		
		MPMTask task14 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 14")).findAny().orElse(null);
		if(task14 != null && !util.isNullOrEmpty(task14.getId())) {
			MPMTaskSchema task14Schema = new MPMTaskSchema();
			task14Schema.setContent_type("MPM_TASK");
			task14Schema.setId(String.valueOf(task14.getId()));
			task14Schema.setIs_active_task(String.valueOf(task14.getIsActive()));
			task14Schema.setIs_approval_task("false");
			task14Schema.setIs_deleted_task("false");
			task14Schema.setIs_milestone("false");
			task14Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task14.getId())));
			task14Schema.setOriginal_task_due_date(task14.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task14Schema.setOriginal_task_start_date(task14.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task14Schema.setProject_item_id(projectItemId);
			task14Schema.setTask_description(task14.getDescription());
			task14Schema.setTask_description_facet(task14.getDescription());
			task14Schema.setTask_due_date(task14.getDueDate().toString().concat("T00:00:00Z"));
			task14Schema.setTask_duration(task14.getTaskDuration());
			task14Schema.setTask_duration_type("weeks");
			task14Schema.setTask_milestone_progress("0");
			task14Schema.setTask_name(task14.getName());
			task14Schema.setTask_priority_id(task14.getPriorityId());
			task14Schema.setTask_priority_value("HIGH");
			task14Schema.setTask_priority_value_facet("HIGH");
			task14Schema.setTask_role_id(task14.getTeamRoleId().getId());
			task14Schema.setTask_role_value(task14.getTeamRoleId().getName());
			task14Schema.setTask_role_value_facet(task14.getTeamRoleId().getName());
			task14Schema.setTask_start_date(task14.getStartDate().toString().concat("T00:00:00Z"));
			task14Schema.setTask_status_id(task14.getStatusId());
			task14Schema.setTask_active_user_cn(!util.isNullOrEmpty(task14.getOwnerId()) && 
					!util.isNullOrEmpty(task14.getOwnerId().getUserId()) ? task14.getOwnerId().getUserId() : "");
			task14Schema.setTask_active_user_id(!util.isNullOrEmpty(task14.getOwnerId()) && 
					!util.isNullOrEmpty(task14.getOwnerId().getId()) ? task14.getOwnerId().getId() : 0);
			task14Schema.setTask_active_user_name(!util.isNullOrEmpty(task14.getOwnerId()) && 
					!util.isNullOrEmpty(task14.getOwnerId().getIdentityDisplayName()) ? 
					task14.getOwnerId().getIdentityDisplayName() : "");
			task14Schema.setTask_owner_cn(!util.isNullOrEmpty(task14.getOwnerId()) && 
					!util.isNullOrEmpty(task14.getOwnerId().getUserId()) ? task14.getOwnerId().getUserId() : "");
			task14Schema.setTask_owner_id(!util.isNullOrEmpty(task14.getOwnerId()) && 
					!util.isNullOrEmpty(task14.getOwnerId().getId()) ? task14.getOwnerId().getId() : 0);
			task14Schema.setTask_owner_name(!util.isNullOrEmpty(task14.getOwnerId()) && 
					!util.isNullOrEmpty(task14.getOwnerId().getIdentityDisplayName()) ? 
					task14.getOwnerId().getIdentityDisplayName() : "");
			task14Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task14.getOwnerId()) && 
					!util.isNullOrEmpty(task14.getOwnerId().getIdentityDisplayName()) ? 
					task14.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task14Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task14.getStatusId()))).findAny().orElse(null);
					
			task14Schema.setTask_status_type(task14Status.getStatusType());
			task14Schema.setTask_status_value(task14Status.getName());
			task14Schema.setTask_status_value_facet(task14Status.getName());
			task14Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task14Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task14Schema.setNPD_TASK_CAN_START(String.valueOf(task14.getIsActive()));;
			task14Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask14ActualStartDate());
			task14Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask14ActualDueDate());
			taskSchemaList.add(task14Schema);
			
			
			NPDTask npdTask14 = new NPDTask();
//			npdTask14.setId(NPDTaskRepository.getCurrentVal());
			npdTask14.setCanstart(task14Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask14.setActualduedate(taskDetails.getTask14ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask14ActualDueDate().split("T")[0]) : null);
			npdTask14.setActualstartdate(taskDetails.getTask14ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask14ActualStartDate().split("T")[0]) : null);
			npdTask14.setTask_id(Integer.parseInt(task14Schema.getId()));
			npdTask14.setTask_item_id(task14Schema.getItem_id());
			npdTask14.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask14);
			entityManager.flush();
			
		}
		
		
		MPMTask task15 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 15")).findAny().orElse(null);
		if(task15 != null && !util.isNullOrEmpty(task15.getId())) {
			MPMTaskSchema task15Schema = new MPMTaskSchema();
			task15Schema.setContent_type("MPM_TASK");
			task15Schema.setId(String.valueOf(task15.getId()));
			task15Schema.setIs_active_task(String.valueOf(task15.getIsActive()));
			task15Schema.setIs_approval_task("false");
			task15Schema.setIs_deleted_task("false");
			task15Schema.setIs_milestone("false");
			task15Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task15.getId())));
			task15Schema.setOriginal_task_due_date(task15.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task15Schema.setOriginal_task_start_date(task15.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task15Schema.setProject_item_id(projectItemId);
			task15Schema.setTask_description(task15.getDescription());
			task15Schema.setTask_description_facet(task15.getDescription());
			task15Schema.setTask_due_date(task15.getDueDate().toString().concat("T00:00:00Z"));
			task15Schema.setTask_duration(task15.getTaskDuration());
			task15Schema.setTask_duration_type("weeks");
			task15Schema.setTask_milestone_progress("0");
			task15Schema.setTask_name(task15.getName());
			task15Schema.setTask_priority_id(task15.getPriorityId());
			task15Schema.setTask_priority_value("HIGH");
			task15Schema.setTask_priority_value_facet("HIGH");
			task15Schema.setTask_role_id(task15.getTeamRoleId().getId());
			task15Schema.setTask_role_value(task15.getTeamRoleId().getName());
			task15Schema.setTask_role_value_facet(task15.getTeamRoleId().getName());
			task15Schema.setTask_start_date(task15.getStartDate().toString().concat("T00:00:00Z"));
			task15Schema.setTask_status_id(task15.getStatusId());
			task15Schema.setTask_active_user_cn(!util.isNullOrEmpty(task15.getOwnerId()) && 
					!util.isNullOrEmpty(task15.getOwnerId().getUserId()) ? task15.getOwnerId().getUserId() : "");
			task15Schema.setTask_active_user_id(!util.isNullOrEmpty(task15.getOwnerId()) && 
					!util.isNullOrEmpty(task15.getOwnerId().getId()) ? task15.getOwnerId().getId() : 0);
			task15Schema.setTask_active_user_name(!util.isNullOrEmpty(task15.getOwnerId()) && 
					!util.isNullOrEmpty(task15.getOwnerId().getIdentityDisplayName()) ? 
					task15.getOwnerId().getIdentityDisplayName() : "");
			task15Schema.setTask_owner_cn(!util.isNullOrEmpty(task15.getOwnerId()) && 
					!util.isNullOrEmpty(task15.getOwnerId().getUserId()) ? task15.getOwnerId().getUserId() : "");
			task15Schema.setTask_owner_id(!util.isNullOrEmpty(task15.getOwnerId()) && 
					!util.isNullOrEmpty(task15.getOwnerId().getId()) ? task15.getOwnerId().getId() : 0);
			task15Schema.setTask_owner_name(!util.isNullOrEmpty(task15.getOwnerId()) && 
					!util.isNullOrEmpty(task15.getOwnerId().getIdentityDisplayName()) ? 
					task15.getOwnerId().getIdentityDisplayName() : "");
			task15Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task15.getOwnerId()) && 
					!util.isNullOrEmpty(task15.getOwnerId().getIdentityDisplayName()) ? 
					task15.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task15Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task15.getStatusId()))).findAny().orElse(null);
					
			task15Schema.setTask_status_type(task15Status.getStatusType());
			task15Schema.setTask_status_value(task15Status.getName());
			task15Schema.setTask_status_value_facet(task15Status.getName());
			task15Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task15Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task15Schema.setNPD_TASK_CAN_START(String.valueOf(task15.getIsActive()));;
			task15Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask15ActualStartDate());
			task15Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask15ActualDueDate());
			taskSchemaList.add(task15Schema);
			
			
			NPDTask npdTask15 = new NPDTask();
//			npdTask15.setId(NPDTaskRepository.getCurrentVal());
			npdTask15.setCanstart(task15Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask15.setActualduedate(taskDetails.getTask15ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask15ActualDueDate().split("T")[0]) : null);
			npdTask15.setActualstartdate(taskDetails.getTask15ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask15ActualStartDate().split("T")[0]) : null);
			npdTask15.setTask_id(Integer.parseInt(task15Schema.getId()));
			npdTask15.setTask_item_id(task15Schema.getItem_id());
			npdTask15.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask15);
			entityManager.flush();
			
		}
		
		MPMTask task16 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 16")).findAny().orElse(null);
		if(task16 != null && !util.isNullOrEmpty(task16.getId())) {
			MPMTaskSchema task16Schema = new MPMTaskSchema();
			task16Schema.setContent_type("MPM_TASK");
			task16Schema.setId(String.valueOf(task16.getId()));
			task16Schema.setIs_active_task(String.valueOf(task16.getIsActive()));
			task16Schema.setIs_approval_task("false");
			task16Schema.setIs_deleted_task("false");
			task16Schema.setIs_milestone("false");
			task16Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task16.getId())));
			task16Schema.setOriginal_task_due_date(task16.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task16Schema.setOriginal_task_start_date(task16.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task16Schema.setProject_item_id(projectItemId);
			task16Schema.setTask_description(task16.getDescription());
			task16Schema.setTask_description_facet(task16.getDescription());
			task16Schema.setTask_due_date(task16.getDueDate().toString().concat("T00:00:00Z"));
			task16Schema.setTask_duration(task16.getTaskDuration());
			task16Schema.setTask_duration_type("weeks");
			task16Schema.setTask_milestone_progress("0");
			task16Schema.setTask_name(task16.getName());
			task16Schema.setTask_priority_id(task16.getPriorityId());
			task16Schema.setTask_priority_value("HIGH");
			task16Schema.setTask_priority_value_facet("HIGH");
			task16Schema.setTask_role_id(task16.getTeamRoleId().getId());
			task16Schema.setTask_role_value(task16.getTeamRoleId().getName());
			task16Schema.setTask_role_value_facet(task16.getTeamRoleId().getName());
			task16Schema.setTask_start_date(task16.getStartDate().toString().concat("T00:00:00Z"));
			task16Schema.setTask_status_id(task16.getStatusId());
			task16Schema.setTask_active_user_cn(!util.isNullOrEmpty(task16.getOwnerId()) && 
					!util.isNullOrEmpty(task16.getOwnerId().getUserId()) ? task16.getOwnerId().getUserId() : "");
			task16Schema.setTask_active_user_id(!util.isNullOrEmpty(task16.getOwnerId()) && 
					!util.isNullOrEmpty(task16.getOwnerId().getId()) ? task16.getOwnerId().getId() : 0);
			task16Schema.setTask_active_user_name(!util.isNullOrEmpty(task16.getOwnerId()) && 
					!util.isNullOrEmpty(task16.getOwnerId().getIdentityDisplayName()) ? 
					task16.getOwnerId().getIdentityDisplayName() : "");
			task16Schema.setTask_owner_cn(!util.isNullOrEmpty(task16.getOwnerId()) && 
					!util.isNullOrEmpty(task16.getOwnerId().getUserId()) ? task16.getOwnerId().getUserId() : "");
			task16Schema.setTask_owner_id(!util.isNullOrEmpty(task16.getOwnerId()) && 
					!util.isNullOrEmpty(task16.getOwnerId().getId()) ? task16.getOwnerId().getId() : 0);
			task16Schema.setTask_owner_name(!util.isNullOrEmpty(task16.getOwnerId()) && 
					!util.isNullOrEmpty(task16.getOwnerId().getIdentityDisplayName()) ? 
					task16.getOwnerId().getIdentityDisplayName() : "");
			task16Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task16.getOwnerId()) && 
					!util.isNullOrEmpty(task16.getOwnerId().getIdentityDisplayName()) ? 
					task16.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task16Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task16.getStatusId()))).findAny().orElse(null);
					
			task16Schema.setTask_status_type(task16Status.getStatusType());
			task16Schema.setTask_status_value(task16Status.getName());
			task16Schema.setTask_status_value_facet(task16Status.getName());
			task16Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task16Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task16Schema.setNPD_TASK_ISSUANCE_TYPE(taskDetails.getTask16IssuanceType());
			task16Schema.setNPD_TASK_ISSUANCE_TYPE_facet(taskDetails.getTask16IssuanceType());
			task16Schema.setNPD_TASK_CAN_START(String.valueOf(task16.getIsActive()));;
			task16Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask16ActualStartDate());
			task16Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask16ActualDueDate());
			taskSchemaList.add(task16Schema);
			
			
			NPDTask npdTask16 = new NPDTask();
//			npdTask16.setId(NPDTaskRepository.getCurrentVal());
			npdTask16.setCanstart(task16Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask16.setActualduedate(taskDetails.getTask16ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask16ActualDueDate().split("T")[0]) : null);
			npdTask16.setActualstartdate(taskDetails.getTask16ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask16ActualStartDate().split("T")[0]) : null);
			npdTask16.setTask_id(Integer.parseInt(task16Schema.getId()));
			npdTask16.setTask_item_id(task16Schema.getItem_id());
			npdTask16.setIssuancetype(taskDetails.getTask16IssuanceType());
			npdTask16.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask16);
			entityManager.flush();
			
		}
		
		MPMTask task17 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 17")).findAny().orElse(null);
		if(task17 != null && !util.isNullOrEmpty(task17.getId())) {
			MPMTaskSchema task17Schema = new MPMTaskSchema();
			task17Schema.setContent_type("MPM_TASK");
			task17Schema.setId(String.valueOf(task17.getId()));
			task17Schema.setIs_active_task(String.valueOf(task17.getIsActive()));
			task17Schema.setIs_approval_task("false");
			task17Schema.setIs_deleted_task("false");
			task17Schema.setIs_milestone("false");
			task17Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task17.getId())));
			task17Schema.setOriginal_task_due_date(task17.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task17Schema.setOriginal_task_start_date(task17.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task17Schema.setProject_item_id(projectItemId);
			task17Schema.setTask_description(task17.getDescription());
			task17Schema.setTask_description_facet(task17.getDescription());
			task17Schema.setTask_due_date(task17.getDueDate().toString().concat("T00:00:00Z"));
			task17Schema.setTask_duration(task17.getTaskDuration());
			task17Schema.setTask_duration_type("weeks");
			task17Schema.setTask_milestone_progress("0");
			task17Schema.setTask_name(task17.getName());
			task17Schema.setTask_priority_id(task17.getPriorityId());
			task17Schema.setTask_priority_value("HIGH");
			task17Schema.setTask_priority_value_facet("HIGH");
			task17Schema.setTask_role_id(task17.getTeamRoleId().getId());
			task17Schema.setTask_role_value(task17.getTeamRoleId().getName());
			task17Schema.setTask_role_value_facet(task17.getTeamRoleId().getName());
			task17Schema.setTask_start_date(task17.getStartDate().toString().concat("T00:00:00Z"));
			task17Schema.setTask_status_id(task17.getStatusId());
			task17Schema.setTask_active_user_cn(!util.isNullOrEmpty(task17.getOwnerId()) && 
					!util.isNullOrEmpty(task17.getOwnerId().getUserId()) ? task17.getOwnerId().getUserId() : "");
			task17Schema.setTask_active_user_id(!util.isNullOrEmpty(task17.getOwnerId()) && 
					!util.isNullOrEmpty(task17.getOwnerId().getId()) ? task17.getOwnerId().getId() : 0);
			task17Schema.setTask_active_user_name(!util.isNullOrEmpty(task17.getOwnerId()) && 
					!util.isNullOrEmpty(task17.getOwnerId().getIdentityDisplayName()) ? 
					task17.getOwnerId().getIdentityDisplayName() : "");
			task17Schema.setTask_owner_cn(!util.isNullOrEmpty(task17.getOwnerId()) && 
					!util.isNullOrEmpty(task17.getOwnerId().getUserId()) ? task17.getOwnerId().getUserId() : "");
			task17Schema.setTask_owner_id(!util.isNullOrEmpty(task17.getOwnerId()) && 
					!util.isNullOrEmpty(task17.getOwnerId().getId()) ? task17.getOwnerId().getId() : 0);
			task17Schema.setTask_owner_name(!util.isNullOrEmpty(task17.getOwnerId()) && 
					!util.isNullOrEmpty(task17.getOwnerId().getIdentityDisplayName()) ? 
					task17.getOwnerId().getIdentityDisplayName() : "");
			task17Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task17.getOwnerId()) && 
					!util.isNullOrEmpty(task17.getOwnerId().getIdentityDisplayName()) ? 
					task17.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task17Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task17.getStatusId()))).findAny().orElse(null);
					
			task17Schema.setTask_status_type(task17Status.getStatusType());
			task17Schema.setTask_status_value(task17Status.getName());
			task17Schema.setTask_status_value_facet(task17Status.getName());
			task17Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task17Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task17Schema.setNPD_TASK_CAN_START(String.valueOf(task17.getIsActive()));;
			task17Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask17ActualStartDate());
			task17Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask17ActualDueDate());
			taskSchemaList.add(task17Schema);
			
			
			NPDTask npdTask17 = new NPDTask();
//			npdTask17.setId(NPDTaskRepository.getCurrentVal());
			npdTask17.setCanstart(task17Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask17.setActualduedate(taskDetails.getTask17ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask17ActualDueDate().split("T")[0]) : null);
			npdTask17.setActualstartdate(taskDetails.getTask17ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask17ActualStartDate().split("T")[0]) : null);
			npdTask17.setTask_id(Integer.parseInt(task17Schema.getId()));
			npdTask17.setTask_item_id(task17Schema.getItem_id());
			npdTask17.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask17);
			entityManager.flush();
			
		}
		MPMTask task18 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 18")).findAny().orElse(null);
		if(task18 != null && !util.isNullOrEmpty(task18.getId())) {
			MPMTaskSchema task18Schema = new MPMTaskSchema();
			task18Schema.setContent_type("MPM_TASK");
			task18Schema.setId(String.valueOf(task18.getId()));
			task18Schema.setIs_active_task(String.valueOf(task18.getIsActive()));
			task18Schema.setIs_approval_task("false");
			task18Schema.setIs_deleted_task("false");
			task18Schema.setIs_milestone("false");
			task18Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task18.getId())));
			task18Schema.setOriginal_task_due_date(task18.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task18Schema.setOriginal_task_start_date(task18.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task18Schema.setProject_item_id(projectItemId);
			task18Schema.setTask_description(task18.getDescription());
			task18Schema.setTask_description_facet(task18.getDescription());
			task18Schema.setTask_due_date(task18.getDueDate().toString().concat("T00:00:00Z"));
			task18Schema.setTask_duration(task18.getTaskDuration());
			task18Schema.setTask_duration_type("weeks");
			task18Schema.setTask_milestone_progress("0");
			task18Schema.setTask_name(task18.getName());
			task18Schema.setTask_priority_id(task18.getPriorityId());
			task18Schema.setTask_priority_value("HIGH");
			task18Schema.setTask_priority_value_facet("HIGH");
			task18Schema.setTask_role_id(task18.getTeamRoleId().getId());
			task18Schema.setTask_role_value(task18.getTeamRoleId().getName());
			task18Schema.setTask_role_value_facet(task18.getTeamRoleId().getName());
			task18Schema.setTask_start_date(task18.getStartDate().toString().concat("T00:00:00Z"));
			task18Schema.setTask_status_id(task18.getStatusId());
			task18Schema.setTask_active_user_cn(!util.isNullOrEmpty(task18.getOwnerId()) && 
					!util.isNullOrEmpty(task18.getOwnerId().getUserId()) ? task18.getOwnerId().getUserId() : "");
			task18Schema.setTask_active_user_id(!util.isNullOrEmpty(task18.getOwnerId()) && 
					!util.isNullOrEmpty(task18.getOwnerId().getId()) ? task18.getOwnerId().getId() : 0);
			task18Schema.setTask_active_user_name(!util.isNullOrEmpty(task18.getOwnerId()) && 
					!util.isNullOrEmpty(task18.getOwnerId().getIdentityDisplayName()) ? 
					task18.getOwnerId().getIdentityDisplayName() : "");
			task18Schema.setTask_owner_cn(!util.isNullOrEmpty(task18.getOwnerId()) && 
					!util.isNullOrEmpty(task18.getOwnerId().getUserId()) ? task18.getOwnerId().getUserId() : "");
			task18Schema.setTask_owner_id(!util.isNullOrEmpty(task18.getOwnerId()) && 
					!util.isNullOrEmpty(task18.getOwnerId().getId()) ? task18.getOwnerId().getId() : 0);
			task18Schema.setTask_owner_name(!util.isNullOrEmpty(task18.getOwnerId()) && 
					!util.isNullOrEmpty(task18.getOwnerId().getIdentityDisplayName()) ? 
					task18.getOwnerId().getIdentityDisplayName() : "");
			task18Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task18.getOwnerId()) && 
					!util.isNullOrEmpty(task18.getOwnerId().getIdentityDisplayName()) ? 
					task18.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task18Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task18.getStatusId()))).findAny().orElse(null);
					
			task18Schema.setTask_status_type(task18Status.getStatusType());
			task18Schema.setTask_status_value(task18Status.getName());
			task18Schema.setTask_status_value_facet(task18Status.getName());
			task18Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task18Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task18Schema.setNPD_TASK_CAN_START(String.valueOf(task18.getIsActive()));;
			task18Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask18ActualStartDate());
			task18Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask18ActualDueDate());
			taskSchemaList.add(task18Schema);
			
			
			NPDTask npdTask18 = new NPDTask();
//			npdTask18.setId(NPDTaskRepository.getCurrentVal());
			npdTask18.setCanstart(task18Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask18.setActualduedate(taskDetails.getTask18ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask18ActualDueDate().split("T")[0]) : null);
			npdTask18.setActualstartdate(taskDetails.getTask18ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask18ActualStartDate().split("T")[0]) : null);
			npdTask18.setTask_id(Integer.parseInt(task18Schema.getId()));
			npdTask18.setTask_item_id(task18Schema.getItem_id());
			npdTask18.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			NPDTaskRepository.save(npdTask18);
			entityManager.flush();
			
		}
		
		MPMTask task19 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 19")).findAny().orElse(null);
		if(task19 != null && !util.isNullOrEmpty(task19.getId())) {
			MPMTaskSchema task19Schema = new MPMTaskSchema();
			task19Schema.setContent_type("MPM_TASK");
			task19Schema.setId(String.valueOf(task19.getId()));
			task19Schema.setIs_active_task(String.valueOf(task19.getIsActive()));
			task19Schema.setIs_approval_task("false");
			task19Schema.setIs_deleted_task("false");
			task19Schema.setIs_milestone("false");
			task19Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task19.getId())));
			task19Schema.setOriginal_task_due_date(task19.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task19Schema.setOriginal_task_start_date(task19.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task19Schema.setProject_item_id(projectItemId);
			task19Schema.setTask_description(task19.getDescription());
			task19Schema.setTask_description_facet(task19.getDescription());
			task19Schema.setTask_due_date(task19.getDueDate().toString().concat("T00:00:00Z"));
			task19Schema.setTask_duration(task19.getTaskDuration());
			task19Schema.setTask_duration_type("weeks");
			task19Schema.setTask_milestone_progress("0");
			task19Schema.setTask_name(task19.getName());
			task19Schema.setTask_priority_id(task19.getPriorityId());
			task19Schema.setTask_priority_value("HIGH");
			task19Schema.setTask_priority_value_facet("HIGH");
			task19Schema.setTask_role_id(task19.getTeamRoleId().getId());
			task19Schema.setTask_role_value(task19.getTeamRoleId().getName());
			task19Schema.setTask_role_value_facet(task19.getTeamRoleId().getName());
			task19Schema.setTask_start_date(task19.getStartDate().toString().concat("T00:00:00Z"));
			task19Schema.setTask_status_id(task19.getStatusId());
			task19Schema.setTask_active_user_cn(!util.isNullOrEmpty(task19.getOwnerId()) && 
					!util.isNullOrEmpty(task19.getOwnerId().getUserId()) ? task19.getOwnerId().getUserId() : "");
			task19Schema.setTask_active_user_id(!util.isNullOrEmpty(task19.getOwnerId()) && 
					!util.isNullOrEmpty(task19.getOwnerId().getId()) ? task19.getOwnerId().getId() : 0);
			task19Schema.setTask_active_user_name(!util.isNullOrEmpty(task19.getOwnerId()) && 
					!util.isNullOrEmpty(task19.getOwnerId().getIdentityDisplayName()) ? 
					task19.getOwnerId().getIdentityDisplayName() : "");
			task19Schema.setTask_owner_cn(!util.isNullOrEmpty(task19.getOwnerId()) && 
					!util.isNullOrEmpty(task19.getOwnerId().getUserId()) ? task19.getOwnerId().getUserId() : "");
			task19Schema.setTask_owner_id(!util.isNullOrEmpty(task19.getOwnerId()) && 
					!util.isNullOrEmpty(task19.getOwnerId().getId()) ? task19.getOwnerId().getId() : 0);
			task19Schema.setTask_owner_name(!util.isNullOrEmpty(task19.getOwnerId()) && 
					!util.isNullOrEmpty(task19.getOwnerId().getIdentityDisplayName()) ? 
					task19.getOwnerId().getIdentityDisplayName() : "");
			task19Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task19.getOwnerId()) && 
					!util.isNullOrEmpty(task19.getOwnerId().getIdentityDisplayName()) ? 
					task19.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task19Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task19.getStatusId()))).findAny().orElse(null);
					
			task19Schema.setTask_status_type(task19Status.getStatusType());
			task19Schema.setTask_status_value(task19Status.getName());
			task19Schema.setTask_status_value_facet(task19Status.getName());
			task19Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task19Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task19Schema.setNPD_TASK_CAN_START(String.valueOf(task19.getIsActive()));;
			task19Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask19ActualStartDate());
			task19Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask19ActualDueDate());
			taskSchemaList.add(task19Schema);
			
			NPDTask npdTask19 = new NPDTask();
//			npdTask19.setId(NPDTaskRepository.getCurrentVal());
			npdTask19.setCanstart(task19Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask19.setActualduedate(taskDetails.getTask19ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask19ActualDueDate().split("T")[0]) : null);
			npdTask19.setActualstartdate(taskDetails.getTask19ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask19ActualStartDate().split("T")[0]) : null);
			npdTask19.setTask_id(Integer.parseInt(task19Schema.getId()));
			npdTask19.setTask_item_id(task19Schema.getItem_id());
			npdTask19.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask19);
			entityManager.flush();
			
		}
		
		MPMTask task20 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 20")).findAny().orElse(null);
		if(task20 != null && !util.isNullOrEmpty(task20.getId())) {
			MPMTaskSchema task20Schema = new MPMTaskSchema();
			task20Schema.setContent_type("MPM_TASK");
			task20Schema.setId(String.valueOf(task20.getId()));
			task20Schema.setIs_active_task(String.valueOf(task20.getIsActive()));
			task20Schema.setIs_approval_task("false");
			task20Schema.setIs_deleted_task("false");
			task20Schema.setIs_milestone("false");
			task20Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task20.getId())));
			task20Schema.setOriginal_task_due_date(task20.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task20Schema.setOriginal_task_start_date(task20.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task20Schema.setProject_item_id(projectItemId);
			task20Schema.setTask_description(task20.getDescription());
			task20Schema.setTask_description_facet(task20.getDescription());
			task20Schema.setTask_due_date(task20.getDueDate().toString().concat("T00:00:00Z"));
			task20Schema.setTask_duration(task20.getTaskDuration());
			task20Schema.setTask_duration_type("weeks");
			task20Schema.setTask_milestone_progress("0");
			task20Schema.setTask_name(task20.getName());
			task20Schema.setTask_priority_id(task20.getPriorityId());
			task20Schema.setTask_priority_value("HIGH");
			task20Schema.setTask_priority_value_facet("HIGH");
			task20Schema.setTask_role_id(task20.getTeamRoleId().getId());
			task20Schema.setTask_role_value(task20.getTeamRoleId().getName());
			task20Schema.setTask_role_value_facet(task20.getTeamRoleId().getName());
			task20Schema.setTask_start_date(task20.getStartDate().toString().concat("T00:00:00Z"));
			task20Schema.setTask_status_id(task20.getStatusId());
			task20Schema.setTask_active_user_cn(!util.isNullOrEmpty(task20.getOwnerId()) && 
					!util.isNullOrEmpty(task20.getOwnerId().getUserId()) ? task20.getOwnerId().getUserId() : "");
			task20Schema.setTask_active_user_id(!util.isNullOrEmpty(task20.getOwnerId()) && 
					!util.isNullOrEmpty(task20.getOwnerId().getId()) ? task20.getOwnerId().getId() : 0);
			task20Schema.setTask_active_user_name(!util.isNullOrEmpty(task20.getOwnerId()) && 
					!util.isNullOrEmpty(task20.getOwnerId().getIdentityDisplayName()) ? 
					task20.getOwnerId().getIdentityDisplayName() : "");
			task20Schema.setTask_owner_cn(!util.isNullOrEmpty(task20.getOwnerId()) && 
					!util.isNullOrEmpty(task20.getOwnerId().getUserId()) ? task20.getOwnerId().getUserId() : "");
			task20Schema.setTask_owner_id(!util.isNullOrEmpty(task20.getOwnerId()) && 
					!util.isNullOrEmpty(task20.getOwnerId().getId()) ? task20.getOwnerId().getId() : 0);
			task20Schema.setTask_owner_name(!util.isNullOrEmpty(task20.getOwnerId()) && 
					!util.isNullOrEmpty(task20.getOwnerId().getIdentityDisplayName()) ? 
					task20.getOwnerId().getIdentityDisplayName() : "");
			task20Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task20.getOwnerId()) && 
					!util.isNullOrEmpty(task20.getOwnerId().getIdentityDisplayName()) ? 
					task20.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task20Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task20.getStatusId()))).findAny().orElse(null);
					
			task20Schema.setTask_status_type(task20Status.getStatusType());
			task20Schema.setTask_status_value(task20Status.getName());
			task20Schema.setTask_status_value_facet(task20Status.getName());
			task20Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task20Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task20Schema.setNPD_TASK_CAN_START(String.valueOf(task20.getIsActive()));;
			task20Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask20ActualStartDate());
			task20Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask20ActualDueDate());
			taskSchemaList.add(task20Schema);
			
			
			NPDTask npdTask20 = new NPDTask();
//			npdTask20.setId(NPDTaskRepository.getCurrentVal());
			npdTask20.setCanstart(task20Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask20.setActualduedate(taskDetails.getTask20ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask20ActualDueDate().split("T")[0]) : null);
			npdTask20.setActualstartdate(taskDetails.getTask20ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask20ActualStartDate().split("T")[0]) : null);
			npdTask20.setTask_id(Integer.parseInt(task20Schema.getId()));
			npdTask20.setTask_item_id(task20Schema.getItem_id());
			npdTask20.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask20);
			entityManager.flush();
			
		}
		
		MPMTask task21 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 21")).findAny().orElse(null);
		if(task21 != null && !util.isNullOrEmpty(task21.getId())) {
			MPMTaskSchema task21Schema = new MPMTaskSchema();
			task21Schema.setContent_type("MPM_TASK");
			task21Schema.setId(String.valueOf(task21.getId()));
			task21Schema.setIs_active_task(String.valueOf(task21.getIsActive()));
			task21Schema.setIs_approval_task("false");
			task21Schema.setIs_deleted_task("false");
			task21Schema.setIs_milestone("false");
			task21Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task21.getId())));
			task21Schema.setOriginal_task_due_date(task21.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task21Schema.setOriginal_task_start_date(task21.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task21Schema.setProject_item_id(projectItemId);
			task21Schema.setTask_description(task21.getDescription());
			task21Schema.setTask_description_facet(task21.getDescription());
			task21Schema.setTask_due_date(task21.getDueDate().toString().concat("T00:00:00Z"));
			task21Schema.setTask_duration(task21.getTaskDuration());
			task21Schema.setTask_duration_type("weeks");
			task21Schema.setTask_milestone_progress("0");
			task21Schema.setTask_name(task21.getName());
			task21Schema.setTask_priority_id(task21.getPriorityId());
			task21Schema.setTask_priority_value("HIGH");
			task21Schema.setTask_priority_value_facet("HIGH");
			task21Schema.setTask_role_id(task21.getTeamRoleId().getId());
			task21Schema.setTask_role_value(task21.getTeamRoleId().getName());
			task21Schema.setTask_role_value_facet(task21.getTeamRoleId().getName());
			task21Schema.setTask_start_date(task21.getStartDate().toString().concat("T00:00:00Z"));
			task21Schema.setTask_status_id(task21.getStatusId());
			task21Schema.setTask_active_user_cn(!util.isNullOrEmpty(task21.getOwnerId()) && 
					!util.isNullOrEmpty(task21.getOwnerId().getUserId()) ? task21.getOwnerId().getUserId() : "");
			task21Schema.setTask_active_user_id(!util.isNullOrEmpty(task21.getOwnerId()) && 
					!util.isNullOrEmpty(task21.getOwnerId().getId()) ? task21.getOwnerId().getId() : 0);
			task21Schema.setTask_active_user_name(!util.isNullOrEmpty(task21.getOwnerId()) && 
					!util.isNullOrEmpty(task21.getOwnerId().getIdentityDisplayName()) ? 
					task21.getOwnerId().getIdentityDisplayName() : "");
			task21Schema.setTask_owner_cn(!util.isNullOrEmpty(task21.getOwnerId()) && 
					!util.isNullOrEmpty(task21.getOwnerId().getUserId()) ? task21.getOwnerId().getUserId() : "");
			task21Schema.setTask_owner_id(!util.isNullOrEmpty(task21.getOwnerId()) && 
					!util.isNullOrEmpty(task21.getOwnerId().getId()) ? task21.getOwnerId().getId() : 0);
			task21Schema.setTask_owner_name(!util.isNullOrEmpty(task21.getOwnerId()) && 
					!util.isNullOrEmpty(task21.getOwnerId().getIdentityDisplayName()) ? 
					task21.getOwnerId().getIdentityDisplayName() : "");
			task21Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task21.getOwnerId()) && 
					!util.isNullOrEmpty(task21.getOwnerId().getIdentityDisplayName()) ? 
					task21.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task21Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task21.getStatusId()))).findAny().orElse(null);
					
			task21Schema.setTask_status_type(task21Status.getStatusType());
			task21Schema.setTask_status_value(task21Status.getName());
			task21Schema.setTask_status_value_facet(task21Status.getName());
			task21Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task21Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task21Schema.setNPD_TASK_CAN_START(String.valueOf(task21.getIsActive()));;
			task21Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask21ActualStartDate());
			task21Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask21ActualDueDate());
			taskSchemaList.add(task21Schema);
			
			
			NPDTask npdTask21 = new NPDTask();
//			npdTask21.setId(NPDTaskRepository.getCurrentVal());
			npdTask21.setCanstart(task21Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask21.setActualduedate(taskDetails.getTask21ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask21ActualDueDate().split("T")[0]) : null);
			npdTask21.setActualstartdate(taskDetails.getTask21ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask21ActualStartDate().split("T")[0]) : null);
			npdTask21.setTask_id(Integer.parseInt(task21Schema.getId()));
			npdTask21.setTask_item_id(task21Schema.getItem_id());
			npdTask21.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask21);
			entityManager.flush();
			
		}
		
		MPMTask task22 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 22")).findAny().orElse(null);
		if(task22 != null && !util.isNullOrEmpty(task22.getId())) {
			MPMTaskSchema task22Schema = new MPMTaskSchema();
			task22Schema.setContent_type("MPM_TASK");
			task22Schema.setId(String.valueOf(task22.getId()));
			task22Schema.setIs_active_task(String.valueOf(task22.getIsActive()));
			task22Schema.setIs_approval_task("false");
			task22Schema.setIs_deleted_task("false");
			task22Schema.setIs_milestone("false");
			task22Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task22.getId())));
			task22Schema.setOriginal_task_due_date(task22.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task22Schema.setOriginal_task_start_date(task22.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task22Schema.setProject_item_id(projectItemId);
			task22Schema.setTask_description(task22.getDescription());
			task22Schema.setTask_description_facet(task22.getDescription());
			task22Schema.setTask_due_date(task22.getDueDate().toString().concat("T00:00:00Z"));
			task22Schema.setTask_duration(task22.getTaskDuration());
			task22Schema.setTask_duration_type("weeks");
			task22Schema.setTask_milestone_progress("0");
			task22Schema.setTask_name(task22.getName());
			task22Schema.setTask_priority_id(task22.getPriorityId());
			task22Schema.setTask_priority_value("HIGH");
			task22Schema.setTask_priority_value_facet("HIGH");
			task22Schema.setTask_role_id(task22.getTeamRoleId().getId());
			task22Schema.setTask_role_value(task22.getTeamRoleId().getName());
			task22Schema.setTask_role_value_facet(task22.getTeamRoleId().getName());
			task22Schema.setTask_start_date(task22.getStartDate().toString().concat("T00:00:00Z"));
			task22Schema.setTask_status_id(task22.getStatusId());
			task22Schema.setTask_active_user_cn(!util.isNullOrEmpty(task22.getOwnerId()) && 
					!util.isNullOrEmpty(task22.getOwnerId().getUserId()) ? task22.getOwnerId().getUserId() : "");
			task22Schema.setTask_active_user_id(!util.isNullOrEmpty(task22.getOwnerId()) && 
					!util.isNullOrEmpty(task22.getOwnerId().getId()) ? task22.getOwnerId().getId() : 0);
			task22Schema.setTask_active_user_name(!util.isNullOrEmpty(task22.getOwnerId()) && 
					!util.isNullOrEmpty(task22.getOwnerId().getIdentityDisplayName()) ? 
					task22.getOwnerId().getIdentityDisplayName() : "");
			task22Schema.setTask_owner_cn(!util.isNullOrEmpty(task22.getOwnerId()) && 
					!util.isNullOrEmpty(task22.getOwnerId().getUserId()) ? task22.getOwnerId().getUserId() : "");
			task22Schema.setTask_owner_id(!util.isNullOrEmpty(task22.getOwnerId()) && 
					!util.isNullOrEmpty(task22.getOwnerId().getId()) ? task22.getOwnerId().getId() : 0);
			task22Schema.setTask_owner_name(!util.isNullOrEmpty(task22.getOwnerId()) && 
					!util.isNullOrEmpty(task22.getOwnerId().getIdentityDisplayName()) ? 
					task22.getOwnerId().getIdentityDisplayName() : "");
			task22Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task22.getOwnerId()) && 
					!util.isNullOrEmpty(task22.getOwnerId().getIdentityDisplayName()) ? 
					task22.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task22Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task22.getStatusId()))).findAny().orElse(null);
					
			task22Schema.setTask_status_type(task22Status.getStatusType());
			task22Schema.setTask_status_value(task22Status.getName());
			task22Schema.setTask_status_value_facet(task22Status.getName());
			task22Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task22Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task22Schema.setNPD_TASK_CAN_START(String.valueOf(task22.getIsActive()));;
			task22Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask22ActualStartDate());
			task22Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask22ActualDueDate());
			taskSchemaList.add(task22Schema);
			
			
			NPDTask npdTask22 = new NPDTask();
//			npdTask22.setId(NPDTaskRepository.getCurrentVal());
			npdTask22.setCanstart(task22Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask22.setActualduedate(taskDetails.getTask22ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask22ActualDueDate().split("T")[0]) : null);
			npdTask22.setActualstartdate(taskDetails.getTask22ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask22ActualStartDate().split("T")[0]) : null);
			npdTask22.setTask_id(Integer.parseInt(task22Schema.getId()));
			npdTask22.setTask_item_id(task22Schema.getItem_id());
			npdTask22.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask22);
			entityManager.flush();
			
		}
		
		MPMTask task23 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 23")).findAny().orElse(null);
		if(task23 != null && !util.isNullOrEmpty(task23.getId())) {
			MPMTaskSchema task23Schema = new MPMTaskSchema();
			task23Schema.setContent_type("MPM_TASK");
			task23Schema.setId(String.valueOf(task23.getId()));
			task23Schema.setIs_active_task(String.valueOf(task23.getIsActive()));
			task23Schema.setIs_approval_task("false");
			task23Schema.setIs_deleted_task("false");
			task23Schema.setIs_milestone("false");
			task23Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task23.getId())));
			task23Schema.setOriginal_task_due_date(task23.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task23Schema.setOriginal_task_start_date(task23.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task23Schema.setProject_item_id(projectItemId);
			task23Schema.setTask_description(task23.getDescription());
			task23Schema.setTask_description_facet(task23.getDescription());
			task23Schema.setTask_due_date(task23.getDueDate().toString().concat("T00:00:00Z"));
			task23Schema.setTask_duration(task23.getTaskDuration());
			task23Schema.setTask_duration_type("weeks");
			task23Schema.setTask_milestone_progress("0");
			task23Schema.setTask_name(task23.getName());
			task23Schema.setTask_priority_id(task23.getPriorityId());
			task23Schema.setTask_priority_value("HIGH");
			task23Schema.setTask_priority_value_facet("HIGH");
			task23Schema.setTask_role_id(task23.getTeamRoleId().getId());
			task23Schema.setTask_role_value(task23.getTeamRoleId().getName());
			task23Schema.setTask_role_value_facet(task23.getTeamRoleId().getName());
			task23Schema.setTask_start_date(task23.getStartDate().toString().concat("T00:00:00Z"));
			task23Schema.setTask_status_id(task23.getStatusId());
			task23Schema.setTask_active_user_cn(!util.isNullOrEmpty(task23.getOwnerId()) && 
					!util.isNullOrEmpty(task23.getOwnerId().getUserId()) ? task23.getOwnerId().getUserId() : "");
			task23Schema.setTask_active_user_id(!util.isNullOrEmpty(task23.getOwnerId()) && 
					!util.isNullOrEmpty(task23.getOwnerId().getId()) ? task23.getOwnerId().getId() : 0);
			task23Schema.setTask_active_user_name(!util.isNullOrEmpty(task23.getOwnerId()) && 
					!util.isNullOrEmpty(task23.getOwnerId().getIdentityDisplayName()) ? 
					task23.getOwnerId().getIdentityDisplayName() : "");
			task23Schema.setTask_owner_cn(!util.isNullOrEmpty(task23.getOwnerId()) && 
					!util.isNullOrEmpty(task23.getOwnerId().getUserId()) ? task23.getOwnerId().getUserId() : "");
			task23Schema.setTask_owner_id(!util.isNullOrEmpty(task23.getOwnerId()) && 
					!util.isNullOrEmpty(task23.getOwnerId().getId()) ? task23.getOwnerId().getId() : 0);
			task23Schema.setTask_owner_name(!util.isNullOrEmpty(task23.getOwnerId()) && 
					!util.isNullOrEmpty(task23.getOwnerId().getIdentityDisplayName()) ? 
					task23.getOwnerId().getIdentityDisplayName() : "");
			task23Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task23.getOwnerId()) && 
					!util.isNullOrEmpty(task23.getOwnerId().getIdentityDisplayName()) ? 
					task23.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task23Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task23.getStatusId()))).findAny().orElse(null);
					
			task23Schema.setTask_status_type(task23Status.getStatusType());
			task23Schema.setTask_status_value(task23Status.getName());
			task23Schema.setTask_status_value_facet(task23Status.getName());
			task23Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task23Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task23Schema.setNPD_TASK_CAN_START(String.valueOf(task23.getIsActive()));;
			task23Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask23ActualStartDate());
			task23Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask23ActualDueDate());
			taskSchemaList.add(task23Schema);
			
			
			NPDTask npdTask23 = new NPDTask();
//			npdTask23.setId(NPDTaskRepository.getCurrentVal());
			npdTask23.setCanstart(task23Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask23.setActualduedate(taskDetails.getTask23ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask23ActualDueDate().split("T")[0]) : null);
			npdTask23.setActualstartdate(taskDetails.getTask23ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask23ActualStartDate().split("T")[0]) : null);
			npdTask23.setTask_id(Integer.parseInt(task23Schema.getId()));
			npdTask23.setTask_item_id(task23Schema.getItem_id());
			npdTask23.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask23);
			entityManager.flush();
			
		}
		
		MPMTask task24 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 24")).findAny().orElse(null);
		if(task24 != null && !util.isNullOrEmpty(task24.getId())) {
			MPMTaskSchema task24Schema = new MPMTaskSchema();
			task24Schema.setContent_type("MPM_TASK");
			task24Schema.setId(String.valueOf(task24.getId()));
			task24Schema.setIs_active_task(String.valueOf(task24.getIsActive()));
			task24Schema.setIs_approval_task("false");
			task24Schema.setIs_deleted_task("false");
			task24Schema.setIs_milestone("false");
			task24Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task24.getId())));
			task24Schema.setOriginal_task_due_date(task24.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task24Schema.setOriginal_task_start_date(task24.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task24Schema.setProject_item_id(projectItemId);
			task24Schema.setTask_description(task24.getDescription());
			task24Schema.setTask_description_facet(task24.getDescription());
			task24Schema.setTask_due_date(task24.getDueDate().toString().concat("T00:00:00Z"));
			task24Schema.setTask_duration(task24.getTaskDuration());
			task24Schema.setTask_duration_type("weeks");
			task24Schema.setTask_milestone_progress("0");
			task24Schema.setTask_name(task24.getName());
			task24Schema.setTask_priority_id(task24.getPriorityId());
			task24Schema.setTask_priority_value("HIGH");
			task24Schema.setTask_priority_value_facet("HIGH");
			task24Schema.setTask_role_id(task24.getTeamRoleId().getId());
			task24Schema.setTask_role_value(task24.getTeamRoleId().getName());
			task24Schema.setTask_role_value_facet(task24.getTeamRoleId().getName());
			task24Schema.setTask_start_date(task24.getStartDate().toString().concat("T00:00:00Z"));
			task24Schema.setTask_status_id(task24.getStatusId());
			task24Schema.setTask_active_user_cn(!util.isNullOrEmpty(task24.getOwnerId()) && 
					!util.isNullOrEmpty(task24.getOwnerId().getUserId()) ? task24.getOwnerId().getUserId() : "");
			task24Schema.setTask_active_user_id(!util.isNullOrEmpty(task24.getOwnerId()) && 
					!util.isNullOrEmpty(task24.getOwnerId().getId()) ? task24.getOwnerId().getId() : 0);
			task24Schema.setTask_active_user_name(!util.isNullOrEmpty(task24.getOwnerId()) && 
					!util.isNullOrEmpty(task24.getOwnerId().getIdentityDisplayName()) ? 
					task24.getOwnerId().getIdentityDisplayName() : "");
			task24Schema.setTask_owner_cn(!util.isNullOrEmpty(task24.getOwnerId()) && 
					!util.isNullOrEmpty(task24.getOwnerId().getUserId()) ? task24.getOwnerId().getUserId() : "");
			task24Schema.setTask_owner_id(!util.isNullOrEmpty(task24.getOwnerId()) && 
					!util.isNullOrEmpty(task24.getOwnerId().getId()) ? task24.getOwnerId().getId() : 0);
			task24Schema.setTask_owner_name(!util.isNullOrEmpty(task24.getOwnerId()) && 
					!util.isNullOrEmpty(task24.getOwnerId().getIdentityDisplayName()) ? 
					task24.getOwnerId().getIdentityDisplayName() : "");
			task24Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task24.getOwnerId()) && 
					!util.isNullOrEmpty(task24.getOwnerId().getIdentityDisplayName()) ? 
					task24.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task24Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task24.getStatusId()))).findAny().orElse(null);
					
			task24Schema.setTask_status_type(task24Status.getStatusType());
			task24Schema.setTask_status_value(task24Status.getName());
			task24Schema.setTask_status_value_facet(task24Status.getName());
			task24Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task24Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task24Schema.setNPD_TASK_CAN_START(String.valueOf(task24.getIsActive()));;
			task24Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask24ActualStartDate());
			task24Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask24ActualDueDate());
			taskSchemaList.add(task24Schema);
			
			
			NPDTask npdTask24 = new NPDTask();
//			npdTask24.setId(NPDTaskRepository.getCurrentVal());
			npdTask24.setCanstart(task24Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask24.setActualduedate(taskDetails.getTask24ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask24ActualDueDate().split("T")[0]) : null);
			npdTask24.setActualstartdate(taskDetails.getTask24ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask24ActualStartDate().split("T")[0]) : null);
			npdTask24.setTask_id(Integer.parseInt(task24Schema.getId()));
			npdTask24.setTask_item_id(task24Schema.getItem_id());
			npdTask24.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask24);
			entityManager.flush();
			
		}
		
		MPMTask task25 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 25")).findAny().orElse(null);
		if(task25 != null && !util.isNullOrEmpty(task25.getId())) {
			MPMTaskSchema task25Schema = new MPMTaskSchema();
			task25Schema.setContent_type("MPM_TASK");
			task25Schema.setId(String.valueOf(task25.getId()));
			task25Schema.setIs_active_task(String.valueOf(task25.getIsActive()));
			task25Schema.setIs_approval_task("false");
			task25Schema.setIs_deleted_task("false");
			task25Schema.setIs_milestone("false");
			task25Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task25.getId())));
			task25Schema.setOriginal_task_due_date(task25.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task25Schema.setOriginal_task_start_date(task25.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task25Schema.setProject_item_id(projectItemId);
			task25Schema.setTask_description(task25.getDescription());
			task25Schema.setTask_description_facet(task25.getDescription());
			task25Schema.setTask_due_date(task25.getDueDate().toString().concat("T00:00:00Z"));
			task25Schema.setTask_duration(task25.getTaskDuration());
			task25Schema.setTask_duration_type("weeks");
			task25Schema.setTask_milestone_progress("0");
			task25Schema.setTask_name(task25.getName());
			task25Schema.setTask_priority_id(task25.getPriorityId());
			task25Schema.setTask_priority_value("HIGH");
			task25Schema.setTask_priority_value_facet("HIGH");
			task25Schema.setTask_role_id(task25.getTeamRoleId().getId());
			task25Schema.setTask_role_value(task25.getTeamRoleId().getName());
			task25Schema.setTask_role_value_facet(task25.getTeamRoleId().getName());
			task25Schema.setTask_start_date(task25.getStartDate().toString().concat("T00:00:00Z"));
			task25Schema.setTask_status_id(task25.getStatusId());
			task25Schema.setTask_active_user_cn(!util.isNullOrEmpty(task25.getOwnerId()) && 
					!util.isNullOrEmpty(task25.getOwnerId().getUserId()) ? task25.getOwnerId().getUserId() : "");
			task25Schema.setTask_active_user_id(!util.isNullOrEmpty(task25.getOwnerId()) && 
					!util.isNullOrEmpty(task25.getOwnerId().getId()) ? task25.getOwnerId().getId() : 0);
			task25Schema.setTask_active_user_name(!util.isNullOrEmpty(task25.getOwnerId()) && 
					!util.isNullOrEmpty(task25.getOwnerId().getIdentityDisplayName()) ? 
					task25.getOwnerId().getIdentityDisplayName() : "");
			task25Schema.setTask_owner_cn(!util.isNullOrEmpty(task25.getOwnerId()) && 
					!util.isNullOrEmpty(task25.getOwnerId().getUserId()) ? task25.getOwnerId().getUserId() : "");
			task25Schema.setTask_owner_id(!util.isNullOrEmpty(task25.getOwnerId()) && 
					!util.isNullOrEmpty(task25.getOwnerId().getId()) ? task25.getOwnerId().getId() : 0);
			task25Schema.setTask_owner_name(!util.isNullOrEmpty(task25.getOwnerId()) && 
					!util.isNullOrEmpty(task25.getOwnerId().getIdentityDisplayName()) ? 
					task25.getOwnerId().getIdentityDisplayName() : "");
			task25Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task25.getOwnerId()) && 
					!util.isNullOrEmpty(task25.getOwnerId().getIdentityDisplayName()) ? 
					task25.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task25Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task25.getStatusId()))).findAny().orElse(null);
					
			task25Schema.setTask_status_type(task25Status.getStatusType());
			task25Schema.setTask_status_value(task25Status.getName());
			task25Schema.setTask_status_value_facet(task25Status.getName());
			task25Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task25Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task25Schema.setNPD_TASK_CAN_START(String.valueOf(task25.getIsActive()));;
			task25Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask25ActualStartDate());
			task25Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask25ActualDueDate());
			taskSchemaList.add(task25Schema);
			
			
			NPDTask npdTask25 = new NPDTask();
//			npdTask25.setId(NPDTaskRepository.getCurrentVal());
			npdTask25.setCanstart(task25Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask25.setActualduedate(taskDetails.getTask25ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask25ActualDueDate().split("T")[0]) : null);
			npdTask25.setActualstartdate(taskDetails.getTask25ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask25ActualStartDate().split("T")[0]) : null);
			npdTask25.setTask_id(Integer.parseInt(task25Schema.getId()));
			npdTask25.setTask_item_id(task25Schema.getItem_id());
			npdTask25.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask25);
			entityManager.flush();
			
		}
		
		MPMTask task26 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 26")).findAny().orElse(null);
		if(task26 != null && !util.isNullOrEmpty(task26.getId())) {
			MPMTaskSchema task26Schema = new MPMTaskSchema();
			task26Schema.setContent_type("MPM_TASK");
			task26Schema.setId(String.valueOf(task26.getId()));
			task26Schema.setIs_active_task(String.valueOf(task26.getIsActive()));
			task26Schema.setIs_approval_task("false");
			task26Schema.setIs_deleted_task("false");
			task26Schema.setIs_milestone("false");
			task26Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task26.getId())));
			task26Schema.setOriginal_task_due_date(task26.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task26Schema.setOriginal_task_start_date(task26.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task26Schema.setProject_item_id(projectItemId);
			task26Schema.setTask_description(task26.getDescription());
			task26Schema.setTask_description_facet(task26.getDescription());
			task26Schema.setTask_due_date(task26.getDueDate().toString().concat("T00:00:00Z"));
			task26Schema.setTask_duration(task26.getTaskDuration());
			task26Schema.setTask_duration_type("weeks");
			task26Schema.setTask_milestone_progress("0");
			task26Schema.setTask_name(task26.getName());
			task26Schema.setTask_priority_id(task26.getPriorityId());
			task26Schema.setTask_priority_value("HIGH");
			task26Schema.setTask_priority_value_facet("HIGH");
			task26Schema.setTask_role_id(task26.getTeamRoleId().getId());
			task26Schema.setTask_role_value(task26.getTeamRoleId().getName());
			task26Schema.setTask_role_value_facet(task26.getTeamRoleId().getName());
			task26Schema.setTask_start_date(task26.getStartDate().toString().concat("T00:00:00Z"));
			task26Schema.setTask_status_id(task26.getStatusId());
			task26Schema.setTask_active_user_cn(!util.isNullOrEmpty(task26.getOwnerId()) && 
					!util.isNullOrEmpty(task26.getOwnerId().getUserId()) ? task26.getOwnerId().getUserId() : "");
			task26Schema.setTask_active_user_id(!util.isNullOrEmpty(task26.getOwnerId()) && 
					!util.isNullOrEmpty(task26.getOwnerId().getId()) ? task26.getOwnerId().getId() : 0);
			task26Schema.setTask_active_user_name(!util.isNullOrEmpty(task26.getOwnerId()) && 
					!util.isNullOrEmpty(task26.getOwnerId().getIdentityDisplayName()) ? 
					task26.getOwnerId().getIdentityDisplayName() : "");
			task26Schema.setTask_owner_cn(!util.isNullOrEmpty(task26.getOwnerId()) && 
					!util.isNullOrEmpty(task26.getOwnerId().getUserId()) ? task26.getOwnerId().getUserId() : "");
			task26Schema.setTask_owner_id(!util.isNullOrEmpty(task26.getOwnerId()) && 
					!util.isNullOrEmpty(task26.getOwnerId().getId()) ? task26.getOwnerId().getId() : 0);
			task26Schema.setTask_owner_name(!util.isNullOrEmpty(task26.getOwnerId()) && 
					!util.isNullOrEmpty(task26.getOwnerId().getIdentityDisplayName()) ? 
					task26.getOwnerId().getIdentityDisplayName() : "");
			task26Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task26.getOwnerId()) && 
					!util.isNullOrEmpty(task26.getOwnerId().getIdentityDisplayName()) ? 
					task26.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task26Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task26.getStatusId()))).findAny().orElse(null);
					
			task26Schema.setTask_status_type(task26Status.getStatusType());
			task26Schema.setTask_status_value(task26Status.getName());
			task26Schema.setTask_status_value_facet(task26Status.getName());
			task26Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task26Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task26Schema.setNPD_TASK_CAN_START(String.valueOf(task26.getIsActive()));;
			task26Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask26ActualStartDate());
			task26Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask26ActualDueDate());
			taskSchemaList.add(task26Schema);
			
			
			NPDTask npdTask26 = new NPDTask();
//			npdTask26.setId(NPDTaskRepository.getCurrentVal());
			npdTask26.setCanstart(task26Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask26.setActualduedate(taskDetails.getTask26ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask26ActualDueDate().split("T")[0]) : null);
			npdTask26.setActualstartdate(taskDetails.getTask26ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask26ActualStartDate().split("T")[0]) : null);
			npdTask26.setTask_id(Integer.parseInt(task26Schema.getId()));
			npdTask26.setTask_item_id(task26Schema.getItem_id());
			npdTask26.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask26);
			entityManager.flush();
			
		}
		
		
		MPMTask task27 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 27")).findAny().orElse(null);
		if(task27 != null && !util.isNullOrEmpty(task27.getId())) {
			MPMTaskSchema task27Schema = new MPMTaskSchema();
			task27Schema.setContent_type("MPM_TASK");
			task27Schema.setId(String.valueOf(task27.getId()));
			task27Schema.setIs_active_task(String.valueOf(task27.getIsActive()));
			task27Schema.setIs_approval_task("false");
			task27Schema.setIs_deleted_task("false");
			task27Schema.setIs_milestone("false");
			task27Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task27.getId())));
			task27Schema.setOriginal_task_due_date(task27.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task27Schema.setOriginal_task_start_date(task27.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task27Schema.setProject_item_id(projectItemId);
			task27Schema.setTask_description(task27.getDescription());
			task27Schema.setTask_description_facet(task27.getDescription());
			task27Schema.setTask_due_date(task27.getDueDate().toString().concat("T00:00:00Z"));
			task27Schema.setTask_duration(task27.getTaskDuration());
			task27Schema.setTask_duration_type("weeks");
			task27Schema.setTask_milestone_progress("0");
			task27Schema.setTask_name(task27.getName());
			task27Schema.setTask_priority_id(task27.getPriorityId());
			task27Schema.setTask_priority_value("HIGH");
			task27Schema.setTask_priority_value_facet("HIGH");
			task27Schema.setTask_role_id(task27.getTeamRoleId().getId());
			task27Schema.setTask_role_value(task27.getTeamRoleId().getName());
			task27Schema.setTask_role_value_facet(task27.getTeamRoleId().getName());
			task27Schema.setTask_start_date(task27.getStartDate().toString().concat("T00:00:00Z"));
			task27Schema.setTask_status_id(task27.getStatusId());
			task27Schema.setTask_active_user_cn(!util.isNullOrEmpty(task27.getOwnerId()) && 
					!util.isNullOrEmpty(task27.getOwnerId().getUserId()) ? task27.getOwnerId().getUserId() : "");
			task27Schema.setTask_active_user_id(!util.isNullOrEmpty(task27.getOwnerId()) && 
					!util.isNullOrEmpty(task27.getOwnerId().getId()) ? task27.getOwnerId().getId() : 0);
			task27Schema.setTask_active_user_name(!util.isNullOrEmpty(task27.getOwnerId()) && 
					!util.isNullOrEmpty(task27.getOwnerId().getIdentityDisplayName()) ? 
					task27.getOwnerId().getIdentityDisplayName() : "");
			task27Schema.setTask_owner_cn(!util.isNullOrEmpty(task27.getOwnerId()) && 
					!util.isNullOrEmpty(task27.getOwnerId().getUserId()) ? task27.getOwnerId().getUserId() : "");
			task27Schema.setTask_owner_id(task27.getOwnerId().getId());
			task27Schema.setTask_owner_name(!util.isNullOrEmpty(task27.getOwnerId()) && 
					!util.isNullOrEmpty(task27.getOwnerId().getIdentityDisplayName()) ? 
					task27.getOwnerId().getIdentityDisplayName() : "");
			task27Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task27.getOwnerId()) && 
					!util.isNullOrEmpty(task27.getOwnerId().getIdentityDisplayName()) ? 
					task27.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task27Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task27.getStatusId()))).findAny().orElse(null);
					
			task27Schema.setTask_status_type(task27Status.getStatusType());
			task27Schema.setTask_status_value(task27Status.getName());
			task27Schema.setTask_status_value_facet(task27Status.getName());
			task27Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task27Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task27Schema.setNPD_TASK_CAN_START(String.valueOf(task27.getIsActive()));;
			task27Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask27ActualStartDate());
			task27Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask27ActualDueDate());
			taskSchemaList.add(task27Schema);
			
			
			NPDTask npdTask27 = new NPDTask();
//			npdTask27.setId(NPDTaskRepository.getCurrentVal());
			npdTask27.setCanstart(task27Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask27.setActualduedate(taskDetails.getTask27ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask27ActualDueDate().split("T")[0]) : null);
			npdTask27.setActualstartdate(taskDetails.getTask27ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask27ActualStartDate().split("T")[0]) : null);
			npdTask27.setTask_id(Integer.parseInt(task27Schema.getId()));
			npdTask27.setTask_item_id(task27Schema.getItem_id());
			npdTask27.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask27);
			entityManager.flush();
			
		}
		
		MPMTask task28 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 28")).findAny().orElse(null);
		if(task28 != null && !util.isNullOrEmpty(task28.getId())) {
			MPMTaskSchema task28Schema = new MPMTaskSchema();
			task28Schema.setContent_type("MPM_TASK");
			task28Schema.setId(String.valueOf(task28.getId()));
			task28Schema.setIs_active_task(String.valueOf(task28.getIsActive()));
			task28Schema.setIs_approval_task("false");
			task28Schema.setIs_deleted_task("false");
			task28Schema.setIs_milestone("false");
			task28Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task28.getId())));
			task28Schema.setOriginal_task_due_date(task28.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task28Schema.setOriginal_task_start_date(task28.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task28Schema.setProject_item_id(projectItemId);
			task28Schema.setTask_description(task28.getDescription());
			task28Schema.setTask_description_facet(task28.getDescription());
			task28Schema.setTask_due_date(task28.getDueDate().toString().concat("T00:00:00Z"));
			task28Schema.setTask_duration(task28.getTaskDuration());
			task28Schema.setTask_duration_type("weeks");
			task28Schema.setTask_milestone_progress("0");
			task28Schema.setTask_name(task28.getName());
			task28Schema.setTask_priority_id(task28.getPriorityId());
			task28Schema.setTask_priority_value("HIGH");
			task28Schema.setTask_priority_value_facet("HIGH");
			task28Schema.setTask_role_id(task28.getTeamRoleId().getId());
			task28Schema.setTask_role_value(task28.getTeamRoleId().getName());
			task28Schema.setTask_role_value_facet(task28.getTeamRoleId().getName());
			task28Schema.setTask_start_date(task28.getStartDate().toString().concat("T00:00:00Z"));
			task28Schema.setTask_status_id(task28.getStatusId());
			task28Schema.setTask_active_user_cn(!util.isNullOrEmpty(task28.getOwnerId()) && 
					!util.isNullOrEmpty(task28.getOwnerId().getUserId()) ? task28.getOwnerId().getUserId() : "");
			task28Schema.setTask_active_user_id(!util.isNullOrEmpty(task28.getOwnerId()) && 
					!util.isNullOrEmpty(task28.getOwnerId().getId()) ? task28.getOwnerId().getId() : 0);
			task28Schema.setTask_active_user_name(!util.isNullOrEmpty(task28.getOwnerId()) && 
					!util.isNullOrEmpty(task28.getOwnerId().getIdentityDisplayName()) ? 
					task28.getOwnerId().getIdentityDisplayName() : "");
			task28Schema.setTask_owner_cn(!util.isNullOrEmpty(task28.getOwnerId()) && 
					!util.isNullOrEmpty(task28.getOwnerId().getUserId()) ? task28.getOwnerId().getUserId() : "");
			task28Schema.setTask_owner_id(!util.isNullOrEmpty(task28.getOwnerId()) && 
					!util.isNullOrEmpty(task28.getOwnerId().getId()) ? task28.getOwnerId().getId() : 0);
			task28Schema.setTask_owner_name(!util.isNullOrEmpty(task28.getOwnerId()) && 
					!util.isNullOrEmpty(task28.getOwnerId().getIdentityDisplayName()) ? 
					task28.getOwnerId().getIdentityDisplayName() : "");
			task28Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task28.getOwnerId()) && 
					!util.isNullOrEmpty(task28.getOwnerId().getIdentityDisplayName()) ? 
					task28.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task28Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task28.getStatusId()))).findAny().orElse(null);
					
			task28Schema.setTask_status_type(task28Status.getStatusType());
			task28Schema.setTask_status_value(task28Status.getName());
			task28Schema.setTask_status_value_facet(task28Status.getName());
			task28Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task28Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task28Schema.setNPD_TASK_CAN_START(String.valueOf(task28.getIsActive()));;
			task28Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask28ActualStartDate());
			task28Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask28ActualDueDate());
			taskSchemaList.add(task28Schema);
			
			
			NPDTask npdTask28 = new NPDTask();
//			npdTask28.setId(NPDTaskRepository.getCurrentVal());
			npdTask28.setCanstart(task28Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask28.setActualduedate(taskDetails.getTask28ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask28ActualDueDate().split("T")[0]) : null);
			npdTask28.setActualstartdate(taskDetails.getTask28ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask28ActualStartDate().split("T")[0]) : null);
			npdTask28.setTask_id(Integer.parseInt(task28Schema.getId()));
			npdTask28.setTask_item_id(task28Schema.getItem_id());
			npdTask28.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask28);
			entityManager.flush();
			
		}
		
		MPMTask task29 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 29")).findAny().orElse(null);
		if(task29 != null && !util.isNullOrEmpty(task29.getId())) {
			MPMTaskSchema task29Schema = new MPMTaskSchema();
			task29Schema.setContent_type("MPM_TASK");
			task29Schema.setId(String.valueOf(task29.getId()));
			task29Schema.setIs_active_task(String.valueOf(task29.getIsActive()));
			task29Schema.setIs_approval_task("false");
			task29Schema.setIs_deleted_task("false");
			task29Schema.setIs_milestone("false");
			task29Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task29.getId())));
			task29Schema.setOriginal_task_due_date(task29.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task29Schema.setOriginal_task_start_date(task29.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task29Schema.setProject_item_id(projectItemId);
			task29Schema.setTask_description(task29.getDescription());
			task29Schema.setTask_description_facet(task29.getDescription());
			task29Schema.setTask_due_date(task29.getDueDate().toString().concat("T00:00:00Z"));
			task29Schema.setTask_duration(task29.getTaskDuration());
			task29Schema.setTask_duration_type("weeks");
			task29Schema.setTask_milestone_progress("0");
			task29Schema.setTask_name(task29.getName());
			task29Schema.setTask_priority_id(task29.getPriorityId());
			task29Schema.setTask_priority_value("HIGH");
			task29Schema.setTask_priority_value_facet("HIGH");
			task29Schema.setTask_role_id(task29.getTeamRoleId().getId());
			task29Schema.setTask_role_value(task29.getTeamRoleId().getName());
			task29Schema.setTask_role_value_facet(task29.getTeamRoleId().getName());
			task29Schema.setTask_start_date(task29.getStartDate().toString().concat("T00:00:00Z"));
			task29Schema.setTask_status_id(task29.getStatusId());
			task29Schema.setTask_active_user_cn(!util.isNullOrEmpty(task29.getOwnerId()) && 
					!util.isNullOrEmpty(task29.getOwnerId().getUserId()) ? task29.getOwnerId().getUserId() : "");
			task29Schema.setTask_active_user_id(!util.isNullOrEmpty(task29.getOwnerId()) && 
					!util.isNullOrEmpty(task29.getOwnerId().getId()) ? task29.getOwnerId().getId() : 0);
			task29Schema.setTask_active_user_name(!util.isNullOrEmpty(task29.getOwnerId()) && 
					!util.isNullOrEmpty(task29.getOwnerId().getIdentityDisplayName()) ? 
					task29.getOwnerId().getIdentityDisplayName() : "");
			task29Schema.setTask_owner_cn(!util.isNullOrEmpty(task29.getOwnerId()) && 
					!util.isNullOrEmpty(task29.getOwnerId().getUserId()) ? task29.getOwnerId().getUserId() : "");
			task29Schema.setTask_owner_id(!util.isNullOrEmpty(task29.getOwnerId()) && 
					!util.isNullOrEmpty(task29.getOwnerId().getId()) ? task29.getOwnerId().getId() : 0);
			task29Schema.setTask_owner_name(!util.isNullOrEmpty(task29.getOwnerId()) && 
					!util.isNullOrEmpty(task29.getOwnerId().getIdentityDisplayName()) ? 
					task29.getOwnerId().getIdentityDisplayName() : "");
			task29Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task29.getOwnerId()) && 
					!util.isNullOrEmpty(task29.getOwnerId().getIdentityDisplayName()) ? 
					task29.getOwnerId().getIdentityDisplayName() : "");
			
			
			MPMStatus task29Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task29.getStatusId()))).findAny().orElse(null);
					
			task29Schema.setTask_status_type(task29Status.getStatusType());
			task29Schema.setTask_status_value(task29Status.getName());
			task29Schema.setTask_status_value_facet(task29Status.getName());
			task29Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task29Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task29Schema.setNPD_TASK_CAN_START(String.valueOf(task29.getIsActive()));;
			task29Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask29ActualStartDate());
			task29Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask29ActualDueDate());
			taskSchemaList.add(task29Schema);
			
			
			NPDTask npdTask29 = new NPDTask();
//			npdTask29.setId(NPDTaskRepository.getCurrentVal());
			npdTask29.setCanstart(task29Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask29.setActualduedate(taskDetails.getTask29ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask29ActualDueDate().split("T")[0]) : null);
			npdTask29.setActualstartdate(taskDetails.getTask29ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask29ActualStartDate().split("T")[0]) : null);
			npdTask29.setTask_id(Integer.parseInt(task29Schema.getId()));
			npdTask29.setTask_item_id(task29Schema.getItem_id());
			npdTask29.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask29);
			entityManager.flush();
			
		}
		
		MPMTask task30 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 30")).findAny().orElse(null);
		if(task30 != null && !util.isNullOrEmpty(task30.getId())) {
			MPMTaskSchema task30Schema = new MPMTaskSchema();
			task30Schema.setContent_type("MPM_TASK");
			task30Schema.setId(String.valueOf(task30.getId()));
			task30Schema.setIs_active_task(String.valueOf(task30.getIsActive()));
			task30Schema.setIs_approval_task("false");
			task30Schema.setIs_deleted_task("false");
			task30Schema.setIs_milestone("false");
			task30Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task30.getId())));
			task30Schema.setOriginal_task_due_date(task30.getDueDate().toString().concat("T00:00:00Z"));
			task30Schema.setOriginal_task_start_date(task30.getDueDate().toString().concat("T00:00:00Z"));
			task30Schema.setProject_item_id(projectItemId);
			task30Schema.setTask_description(task30.getDescription());
			task30Schema.setTask_description_facet(task30.getDescription());
			task30Schema.setTask_due_date(task30.getDueDate().toString().concat("T00:00:00Z"));
			task30Schema.setTask_duration(task30.getTaskDuration());
			task30Schema.setTask_duration_type("weeks");
			task30Schema.setTask_milestone_progress("0");
			task30Schema.setTask_name(task30.getName());
			task30Schema.setTask_priority_id(task30.getPriorityId());
			task30Schema.setTask_priority_value("HIGH");
			task30Schema.setTask_priority_value_facet("HIGH");
			task30Schema.setTask_role_id(task30.getTeamRoleId().getId());
			task30Schema.setTask_role_value(task30.getTeamRoleId().getName());
			task30Schema.setTask_role_value_facet(task30.getTeamRoleId().getName());
			task30Schema.setTask_start_date(task30.getDueDate().toString().concat("T00:00:00Z"));
			task30Schema.setTask_status_id(task30.getStatusId());
			task30Schema.setTask_active_user_cn(!util.isNullOrEmpty(task30.getOwnerId()) && 
					!util.isNullOrEmpty(task30.getOwnerId().getUserId()) ? task30.getOwnerId().getUserId() : "");
			task30Schema.setTask_active_user_id(!util.isNullOrEmpty(task30.getOwnerId()) && 
					!util.isNullOrEmpty(task30.getOwnerId().getId()) ? task30.getOwnerId().getId() : 0);
			task30Schema.setTask_active_user_name(!util.isNullOrEmpty(task30.getOwnerId()) && 
					!util.isNullOrEmpty(task30.getOwnerId().getIdentityDisplayName()) ? 
					task30.getOwnerId().getIdentityDisplayName() : "");
			task30Schema.setTask_owner_cn(!util.isNullOrEmpty(task30.getOwnerId()) && 
					!util.isNullOrEmpty(task30.getOwnerId().getUserId()) ? task30.getOwnerId().getUserId() : "");
			task30Schema.setTask_owner_id(!util.isNullOrEmpty(task30.getOwnerId()) && 
					!util.isNullOrEmpty(task30.getOwnerId().getId()) ? task30.getOwnerId().getId() : 0);
			task30Schema.setTask_owner_name(!util.isNullOrEmpty(task30.getOwnerId()) && 
					!util.isNullOrEmpty(task30.getOwnerId().getIdentityDisplayName()) ? 
					task30.getOwnerId().getIdentityDisplayName() : "");
			task30Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task30.getOwnerId()) && 
					!util.isNullOrEmpty(task30.getOwnerId().getIdentityDisplayName()) ? 
					task30.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task30Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task30.getStatusId()))).findAny().orElse(null);
					
			task30Schema.setTask_status_type(task30Status.getStatusType());
			task30Schema.setTask_status_value(task30Status.getName());
			task30Schema.setTask_status_value_facet(task30Status.getName());
			task30Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task30Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task30Schema.setNPD_TASK_CAN_START(String.valueOf(task30.getIsActive()));
//			task30Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask30ActualStartDate());
			task30Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask30ActualDueDate());
			taskSchemaList.add(task30Schema);
			
			
			NPDTask npdTask30 = new NPDTask();
			npdTask30.setId(NPDTaskRepository.getCurrentVal());
			npdTask30.setCanstart(task30Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask30.setActualduedate(taskDetails.getTask30ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask30ActualDueDate().split("T")[0]) : null);
//			npdTask30.setActualstartdate(taskDetails.getTask30ActualStartDate().indexOf("T") > 0 ? 
//					Date.valueOf(taskDetails.getTask30ActualStartDate().split("T")[0]) : null);
			npdTask30.setTask_id(Integer.parseInt(task30Schema.getId()));
			npdTask30.setTask_item_id(task30Schema.getItem_id());
		npdTask30.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask30);
			entityManager.flush();
			
		}
		
		MPMTask task31 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 31")).findAny().orElse(null);
		if(task31 != null && !util.isNullOrEmpty(task31.getId())) {
			MPMTaskSchema task31Schema = new MPMTaskSchema();
			task31Schema.setContent_type("MPM_TASK");
			task31Schema.setId(String.valueOf(task31.getId()));
			task31Schema.setIs_active_task(String.valueOf(task31.getIsActive()));
			task31Schema.setIs_approval_task("false");
			task31Schema.setIs_deleted_task("false");
			task31Schema.setIs_milestone("false");
			task31Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task31.getId())));
			task31Schema.setOriginal_task_due_date(task31.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task31Schema.setOriginal_task_start_date(task31.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task31Schema.setProject_item_id(projectItemId);
			task31Schema.setTask_description(task31.getDescription());
			task31Schema.setTask_description_facet(task31.getDescription());
			task31Schema.setTask_due_date(task31.getDueDate().toString().concat("T00:00:00Z"));
			task31Schema.setTask_duration(task31.getTaskDuration());
			task31Schema.setTask_duration_type("weeks");
			task31Schema.setTask_milestone_progress("0");
			task31Schema.setTask_name(task31.getName());
			task31Schema.setTask_priority_id(task31.getPriorityId());
			task31Schema.setTask_priority_value("HIGH");
			task31Schema.setTask_priority_value_facet("HIGH");
			task31Schema.setTask_role_id(task31.getTeamRoleId().getId());
			task31Schema.setTask_role_value(task31.getTeamRoleId().getName());
			task31Schema.setTask_role_value_facet(task31.getTeamRoleId().getName());
			task31Schema.setTask_start_date(task31.getStartDate().toString().concat("T00:00:00Z"));
			task31Schema.setTask_status_id(task31.getStatusId());
			task31Schema.setTask_active_user_cn(!util.isNullOrEmpty(task31.getOwnerId()) && 
					!util.isNullOrEmpty(task31.getOwnerId().getUserId()) ? task31.getOwnerId().getUserId() : "");
			task31Schema.setTask_active_user_id(!util.isNullOrEmpty(task31.getOwnerId()) && 
					!util.isNullOrEmpty(task31.getOwnerId().getId()) ? task31.getOwnerId().getId() : 0);
			task31Schema.setTask_active_user_name(!util.isNullOrEmpty(task31.getOwnerId()) && 
					!util.isNullOrEmpty(task31.getOwnerId().getIdentityDisplayName()) ? 
					task31.getOwnerId().getIdentityDisplayName() : "");
			task31Schema.setTask_owner_cn(!util.isNullOrEmpty(task31.getOwnerId()) && 
					!util.isNullOrEmpty(task31.getOwnerId().getUserId()) ? task31.getOwnerId().getUserId() : "");
			task31Schema.setTask_owner_id(!util.isNullOrEmpty(task31.getOwnerId()) && 
					!util.isNullOrEmpty(task31.getOwnerId().getId()) ? task31.getOwnerId().getId() : 0);
			task31Schema.setTask_owner_name(!util.isNullOrEmpty(task31.getOwnerId()) && 
					!util.isNullOrEmpty(task31.getOwnerId().getIdentityDisplayName()) ? 
					task31.getOwnerId().getIdentityDisplayName() : "");
			task31Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task31.getOwnerId()) && 
					!util.isNullOrEmpty(task31.getOwnerId().getIdentityDisplayName()) ? 
					task31.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task31Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task31.getStatusId()))).findAny().orElse(null);
					
			task31Schema.setTask_status_type(task31Status.getStatusType());
			task31Schema.setTask_status_value(task31Status.getName());
			task31Schema.setTask_status_value_facet(task31Status.getName());
			task31Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task31Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task31Schema.setNPD_TASK_PALLET(taskDetails.getTask31Pallet());
			task31Schema.setNPD_TASK_PALLET_facet(taskDetails.getTask31Pallet());
			task31Schema.setNPD_TASK_TRAY(taskDetails.getTask31Tray());
			task31Schema.setNPD_TASK_TRAY_facet(taskDetails.getTask31Tray());
			task31Schema.setNPD_TASK_CAN_START(String.valueOf(task31.getIsActive()));;
			task31Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask31ActualStartDate());
			task31Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask31ActualDueDate());
			taskSchemaList.add(task31Schema);
			
			
			NPDTask npdTask31 = new NPDTask();
//			npdTask31.setId(NPDTaskRepository.getCurrentVal());
			npdTask31.setCanstart(task31Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask31.setActualduedate(taskDetails.getTask31ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask31ActualDueDate().split("T")[0]) : null);
			npdTask31.setActualstartdate(taskDetails.getTask31ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask31ActualStartDate().split("T")[0]) : null);
			npdTask31.setTask_id(Integer.parseInt(task31Schema.getId()));
			npdTask31.setTask_item_id(task31Schema.getItem_id());
			npdTask31.setTray(taskDetails.getTask31Tray());
			npdTask31.setPallet(taskDetails.getTask31Pallet());
			npdTask31.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask31);
			entityManager.flush();
			
		}
		
		MPMTask task32 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 32")).findAny().orElse(null);
		if(task32 != null && !util.isNullOrEmpty(task32.getId())) {
			MPMTaskSchema task32Schema = new MPMTaskSchema();
			task32Schema.setContent_type("MPM_TASK");
			task32Schema.setId(String.valueOf(task32.getId()));
			task32Schema.setIs_active_task(String.valueOf(task32.getIsActive()));
			task32Schema.setIs_approval_task("false");
			task32Schema.setIs_deleted_task("false");
			task32Schema.setIs_milestone("false");
			task32Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task32.getId())));
			task32Schema.setOriginal_task_due_date(task32.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task32Schema.setOriginal_task_start_date(task32.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task32Schema.setProject_item_id(projectItemId);
			task32Schema.setTask_description(task32.getDescription());
			task32Schema.setTask_description_facet(task32.getDescription());
			task32Schema.setTask_due_date(task32.getDueDate().toString().concat("T00:00:00Z"));
			task32Schema.setTask_duration(task32.getTaskDuration());
			task32Schema.setTask_duration_type("weeks");
			task32Schema.setTask_milestone_progress("0");
			task32Schema.setTask_name(task32.getName());
			task32Schema.setTask_priority_id(task32.getPriorityId());
			task32Schema.setTask_priority_value("HIGH");
			task32Schema.setTask_priority_value_facet("HIGH");
			task32Schema.setTask_role_id(task32.getTeamRoleId().getId());
			task32Schema.setTask_role_value(task32.getTeamRoleId().getName());
			task32Schema.setTask_role_value_facet(task32.getTeamRoleId().getName());
			task32Schema.setTask_start_date(task32.getStartDate().toString().concat("T00:00:00Z"));
			task32Schema.setTask_status_id(task32.getStatusId());
			task32Schema.setTask_active_user_cn(!util.isNullOrEmpty(task32.getOwnerId()) && 
					!util.isNullOrEmpty(task32.getOwnerId().getUserId()) ? task32.getOwnerId().getUserId() : "");
			task32Schema.setTask_active_user_id(!util.isNullOrEmpty(task32.getOwnerId()) && 
					!util.isNullOrEmpty(task32.getOwnerId().getId()) ? task32.getOwnerId().getId() : 0);
			task32Schema.setTask_active_user_name(!util.isNullOrEmpty(task32.getOwnerId()) && 
					!util.isNullOrEmpty(task32.getOwnerId().getIdentityDisplayName()) ? 
					task32.getOwnerId().getIdentityDisplayName() : "");
			task32Schema.setTask_owner_cn(!util.isNullOrEmpty(task32.getOwnerId()) && 
					!util.isNullOrEmpty(task32.getOwnerId().getUserId()) ? task32.getOwnerId().getUserId() : "");
			task32Schema.setTask_owner_id(!util.isNullOrEmpty(task32.getOwnerId()) && 
					!util.isNullOrEmpty(task32.getOwnerId().getId()) ? task32.getOwnerId().getId() : 0);
			task32Schema.setTask_owner_name(!util.isNullOrEmpty(task32.getOwnerId()) && 
					!util.isNullOrEmpty(task32.getOwnerId().getIdentityDisplayName()) ? 
					task32.getOwnerId().getIdentityDisplayName() : "");
			task32Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task32.getOwnerId()) && 
					!util.isNullOrEmpty(task32.getOwnerId().getIdentityDisplayName()) ? 
					task32.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task32Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task32.getStatusId()))).findAny().orElse(null);
					
			task32Schema.setTask_status_type(task32Status.getStatusType());
			task32Schema.setTask_status_value(task32Status.getName());
			task32Schema.setTask_status_value_facet(task32Status.getName());
			task32Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task32Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task32Schema.setNPD_TASK_CAN_START(String.valueOf(task32.getIsActive()));;
			task32Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask32ActualStartDate());
			task32Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask32ActualDueDate());
			taskSchemaList.add(task32Schema);
			
			
			NPDTask npdTask32 = new NPDTask();
//			npdTask32.setId(NPDTaskRepository.getCurrentVal());
			npdTask32.setCanstart(task32Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask32.setActualduedate(taskDetails.getTask32ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask32ActualDueDate().split("T")[0]) : null);
			npdTask32.setActualstartdate(taskDetails.getTask32ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask32ActualStartDate().split("T")[0]) : null);
			npdTask32.setTask_id(Integer.parseInt(task32Schema.getId()));
			npdTask32.setTask_item_id(task32Schema.getItem_id());
			npdTask32.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask32);
			entityManager.flush();
			
		}
		
		MPMTask task33 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 33")).findAny().orElse(null);
		if(task33 != null && !util.isNullOrEmpty(task33.getId())) {
			MPMTaskSchema task33Schema = new MPMTaskSchema();
			task33Schema.setContent_type("MPM_TASK");
			task33Schema.setId(String.valueOf(task33.getId()));
			task33Schema.setIs_active_task(String.valueOf(task33.getIsActive()));
			task33Schema.setIs_approval_task("false");
			task33Schema.setIs_deleted_task("false");
			task33Schema.setIs_milestone("false");
			task33Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task33.getId())));
			task33Schema.setOriginal_task_due_date(task33.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task33Schema.setOriginal_task_start_date(task33.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task33Schema.setProject_item_id(projectItemId);
			task33Schema.setTask_description(task33.getDescription());
			task33Schema.setTask_description_facet(task33.getDescription());
			task33Schema.setTask_due_date(task33.getDueDate().toString().concat("T00:00:00Z"));
			task33Schema.setTask_duration(task33.getTaskDuration());
			task33Schema.setTask_duration_type("weeks");
			task33Schema.setTask_milestone_progress("0");
			task33Schema.setTask_name(task33.getName());
			task33Schema.setTask_priority_id(task33.getPriorityId());
			task33Schema.setTask_priority_value("HIGH");
			task33Schema.setTask_priority_value_facet("HIGH");
			task33Schema.setTask_role_id(task33.getTeamRoleId().getId());
			task33Schema.setTask_role_value(task33.getTeamRoleId().getName());
			task33Schema.setTask_role_value_facet(task33.getTeamRoleId().getName());
			task33Schema.setTask_start_date(task33.getStartDate().toString().concat("T00:00:00Z"));
			task33Schema.setTask_status_id(task33.getStatusId());
			task33Schema.setTask_active_user_cn(!util.isNullOrEmpty(task33.getOwnerId()) && 
					!util.isNullOrEmpty(task33.getOwnerId().getUserId()) ? task33.getOwnerId().getUserId() : "");
			task33Schema.setTask_active_user_id(!util.isNullOrEmpty(task33.getOwnerId()) && 
					!util.isNullOrEmpty(task33.getOwnerId().getId()) ? task33.getOwnerId().getId() : 0);
			task33Schema.setTask_active_user_name(!util.isNullOrEmpty(task33.getOwnerId()) && 
					!util.isNullOrEmpty(task33.getOwnerId().getIdentityDisplayName()) ? 
					task33.getOwnerId().getIdentityDisplayName() : "");
			task33Schema.setTask_owner_cn(!util.isNullOrEmpty(task33.getOwnerId()) && 
					!util.isNullOrEmpty(task33.getOwnerId().getUserId()) ? task33.getOwnerId().getUserId() : "");
			task33Schema.setTask_owner_id(!util.isNullOrEmpty(task33.getOwnerId()) && 
					!util.isNullOrEmpty(task33.getOwnerId().getId()) ? task33.getOwnerId().getId() : 0);
			task33Schema.setTask_owner_name(!util.isNullOrEmpty(task33.getOwnerId()) && 
					!util.isNullOrEmpty(task33.getOwnerId().getIdentityDisplayName()) ? 
					task33.getOwnerId().getIdentityDisplayName() : "");
			task33Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task33.getOwnerId()) && 
					!util.isNullOrEmpty(task33.getOwnerId().getIdentityDisplayName()) ? 
					task33.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task33Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task33.getStatusId()))).findAny().orElse(null);
					
			task33Schema.setTask_status_type(task33Status.getStatusType());
			task33Schema.setTask_status_value(task33Status.getName());
			task33Schema.setTask_status_value_facet(task33Status.getName());
			task33Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task33Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			task33Schema.setNPD_TASK_CAN_START(String.valueOf(task33.getIsActive()));;
			task33Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask33ActualStartDate());
			task33Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask33ActualDueDate());
			taskSchemaList.add(task33Schema);
			
			
			NPDTask npdTask33 = new NPDTask();
//			npdTask33.setId(NPDTaskRepository.getCurrentVal());
			npdTask33.setCanstart(task33Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask33.setActualduedate(taskDetails.getTask33ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask33ActualDueDate().split("T")[0]) : null);
			npdTask33.setActualstartdate(taskDetails.getTask33ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask33ActualStartDate().split("T")[0]) : null);
			npdTask33.setTask_id(Integer.parseInt(task33Schema.getId()));
			npdTask33.setTask_item_id(task33Schema.getItem_id());
			npdTask33.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask33);
			entityManager.flush();
			
		}
		
		MPMTask task34 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 34")).findAny().orElse(null);
		if(task34 != null && !util.isNullOrEmpty(task34.getId())) {
			MPMTaskSchema task34Schema = new MPMTaskSchema();
			task34Schema.setContent_type("MPM_TASK");
			task34Schema.setId(String.valueOf(task34.getId()));
			task34Schema.setIs_active_task(String.valueOf(task34.getIsActive()));
			task34Schema.setIs_approval_task("false");
			task34Schema.setIs_deleted_task("false");
			task34Schema.setIs_milestone("false");
			task34Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task34.getId())));
			task34Schema.setOriginal_task_due_date(task34.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task34Schema.setOriginal_task_start_date(task34.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task34Schema.setProject_item_id(projectItemId);
			task34Schema.setTask_description(task34.getDescription());
			task34Schema.setTask_description_facet(task34.getDescription());
			task34Schema.setTask_due_date(task34.getDueDate().toString().concat("T00:00:00Z"));
			task34Schema.setTask_duration(task34.getTaskDuration());
			task34Schema.setTask_duration_type("weeks");
			task34Schema.setTask_milestone_progress("0");
			task34Schema.setTask_name(task34.getName());
			task34Schema.setTask_priority_id(task34.getPriorityId());
			task34Schema.setTask_priority_value("HIGH");
			task34Schema.setTask_priority_value_facet("HIGH");
			task34Schema.setTask_role_id(task34.getTeamRoleId().getId());
			task34Schema.setTask_role_value(task34.getTeamRoleId().getName());
			task34Schema.setTask_role_value_facet(task34.getTeamRoleId().getName());
			task34Schema.setTask_start_date(task34.getStartDate().toString().concat("T00:00:00Z"));
			task34Schema.setTask_status_id(task34.getStatusId());
			task34Schema.setTask_active_user_cn(!util.isNullOrEmpty(task34.getOwnerId()) && 
					!util.isNullOrEmpty(task34.getOwnerId().getUserId()) ? task34.getOwnerId().getUserId() : "");
			task34Schema.setTask_active_user_id(!util.isNullOrEmpty(task34.getOwnerId()) && 
					!util.isNullOrEmpty(task34.getOwnerId().getId()) ? task34.getOwnerId().getId() : 0);
			task34Schema.setTask_active_user_name(!util.isNullOrEmpty(task34.getOwnerId()) && 
					!util.isNullOrEmpty(task34.getOwnerId().getIdentityDisplayName()) ? 
					task34.getOwnerId().getIdentityDisplayName() : "");
			task34Schema.setTask_owner_cn(!util.isNullOrEmpty(task34.getOwnerId()) && 
					!util.isNullOrEmpty(task34.getOwnerId().getUserId()) ? task34.getOwnerId().getUserId() : "");
			task34Schema.setTask_owner_id(!util.isNullOrEmpty(task34.getOwnerId()) && 
					!util.isNullOrEmpty(task34.getOwnerId().getId()) ? task34.getOwnerId().getId() : 0);
			task34Schema.setTask_owner_name(!util.isNullOrEmpty(task34.getOwnerId()) && 
					!util.isNullOrEmpty(task34.getOwnerId().getIdentityDisplayName()) ? 
					task34.getOwnerId().getIdentityDisplayName() : "");
			task34Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task34.getOwnerId()) && 
					!util.isNullOrEmpty(task34.getOwnerId().getIdentityDisplayName()) ? 
					task34.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task34Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task34.getStatusId()))).findAny().orElse(null);
					
			task34Schema.setTask_status_type(task34Status.getStatusType());
			task34Schema.setTask_status_value(task34Status.getName());
			task34Schema.setTask_status_value_facet(task34Status.getName());
			task34Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task34Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task34Schema.setNPD_TASK_CAN_START(String.valueOf(task34.getIsActive()));;
			task34Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask34ActualStartDate());
			task34Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask34ActualDueDate());
			taskSchemaList.add(task34Schema);
			
			
			NPDTask npdTask34 = new NPDTask();
//			npdTask34.setId(NPDTaskRepository.getCurrentVal());
			npdTask34.setCanstart(task34Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask34.setActualduedate(taskDetails.getTask34ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask34ActualDueDate().split("T")[0]) : null);
			npdTask34.setActualstartdate(taskDetails.getTask34ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask34ActualStartDate().split("T")[0]) : null);
			npdTask34.setTask_id(Integer.parseInt(task34Schema.getId()));
			npdTask34.setTask_item_id(task34Schema.getItem_id());
			npdTask34.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask34);
			entityManager.flush();
			
		}
		
		MPMTask task35 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 35")).findAny().orElse(null);
		if(task35 != null && !util.isNullOrEmpty(task35.getId())) {
			MPMTaskSchema task35Schema = new MPMTaskSchema();
			task35Schema.setContent_type("MPM_TASK");
			task35Schema.setId(String.valueOf(task35.getId()));
			task35Schema.setIs_active_task(String.valueOf(task35.getIsActive()));
			task35Schema.setIs_approval_task("false");
			task35Schema.setIs_deleted_task("false");
			task35Schema.setIs_milestone("false");
			task35Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task35.getId())));
			task35Schema.setOriginal_task_due_date(task35.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task35Schema.setOriginal_task_start_date(task35.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task35Schema.setProject_item_id(projectItemId);
			task35Schema.setTask_description(task35.getDescription());
			task35Schema.setTask_description_facet(task35.getDescription());
			task35Schema.setTask_due_date(task35.getDueDate().toString().concat("T00:00:00Z"));
			task35Schema.setTask_duration(task35.getTaskDuration());
			task35Schema.setTask_duration_type("weeks");
			task35Schema.setTask_milestone_progress("0");
			task35Schema.setTask_name(task35.getName());
			task35Schema.setTask_priority_id(task35.getPriorityId());
			task35Schema.setTask_priority_value("HIGH");
			task35Schema.setTask_priority_value_facet("HIGH");
			task35Schema.setTask_role_id(task35.getTeamRoleId().getId());
			task35Schema.setTask_role_value(task35.getTeamRoleId().getName());
			task35Schema.setTask_role_value_facet(task35.getTeamRoleId().getName());
			task35Schema.setTask_start_date(task35.getStartDate().toString().concat("T00:00:00Z"));
			task35Schema.setTask_status_id(task35.getStatusId());
			task35Schema.setTask_active_user_cn(!util.isNullOrEmpty(task35.getOwnerId()) && 
					!util.isNullOrEmpty(task35.getOwnerId().getUserId()) ? task35.getOwnerId().getUserId() : "");
			task35Schema.setTask_active_user_id(!util.isNullOrEmpty(task35.getOwnerId()) && 
					!util.isNullOrEmpty(task35.getOwnerId().getId()) ? task35.getOwnerId().getId() : 0);
			task35Schema.setTask_active_user_name(!util.isNullOrEmpty(task35.getOwnerId()) && 
					!util.isNullOrEmpty(task35.getOwnerId().getIdentityDisplayName()) ? 
					task35.getOwnerId().getIdentityDisplayName() : "");
			task35Schema.setTask_owner_cn(!util.isNullOrEmpty(task35.getOwnerId()) && 
					!util.isNullOrEmpty(task35.getOwnerId().getUserId()) ? task35.getOwnerId().getUserId() : "");
			task35Schema.setTask_owner_id(!util.isNullOrEmpty(task35.getOwnerId()) && 
					!util.isNullOrEmpty(task35.getOwnerId().getId()) ? task35.getOwnerId().getId() : 0);
			task35Schema.setTask_owner_name(!util.isNullOrEmpty(task35.getOwnerId()) && 
					!util.isNullOrEmpty(task35.getOwnerId().getIdentityDisplayName()) ? 
					task35.getOwnerId().getIdentityDisplayName() : "");
			task35Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task35.getOwnerId()) && 
					!util.isNullOrEmpty(task35.getOwnerId().getIdentityDisplayName()) ? 
					task35.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task35Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task35.getStatusId()))).findAny().orElse(null);
					
			task35Schema.setTask_status_type(task35Status.getStatusType());
			task35Schema.setTask_status_value(task35Status.getName());
			task35Schema.setTask_status_value_facet(task35Status.getName());
			task35Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task35Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task35Schema.setNPD_TASK_CAN_START(String.valueOf(task35.getIsActive()));;
			task35Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask35ActualStartDate());
			task35Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask35ActualDueDate());
			taskSchemaList.add(task35Schema);
			
			
			NPDTask npdTask35 = new NPDTask();
//			npdTask35.setId(NPDTaskRepository.getCurrentVal());
			npdTask35.setCanstart(task35Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask35.setActualduedate(taskDetails.getTask35ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask35ActualDueDate().split("T")[0]) : null);
			npdTask35.setActualstartdate(taskDetails.getTask35ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask35ActualStartDate().split("T")[0]) : null);
			npdTask35.setTask_id(Integer.parseInt(task35Schema.getId()));
			npdTask35.setTask_item_id(task35Schema.getItem_id());
			npdTask35.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask35);
			entityManager.flush();
			
		}
		
		MPMTask task36 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 36")).findAny().orElse(null);
		if(task36 != null && !util.isNullOrEmpty(task36.getId())) {
			MPMTaskSchema task36Schema = new MPMTaskSchema();
			task36Schema.setContent_type("MPM_TASK");
			task36Schema.setId(String.valueOf(task36.getId()));
			task36Schema.setIs_active_task(String.valueOf(task36.getIsActive()));
			task36Schema.setIs_approval_task("false");
			task36Schema.setIs_deleted_task("false");
			task36Schema.setIs_milestone("false");
			task36Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task36.getId())));
			task36Schema.setOriginal_task_due_date(task36.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task36Schema.setOriginal_task_start_date(task36.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task36Schema.setProject_item_id(projectItemId);
			task36Schema.setTask_description(task36.getDescription());
			task36Schema.setTask_description_facet(task36.getDescription());
			task36Schema.setTask_due_date(task36.getDueDate().toString().concat("T00:00:00Z"));
			task36Schema.setTask_duration(task36.getTaskDuration());
			task36Schema.setTask_duration_type("weeks");
			task36Schema.setTask_milestone_progress("0");
			task36Schema.setTask_name(task36.getName());
			task36Schema.setTask_priority_id(task36.getPriorityId());
			task36Schema.setTask_priority_value("HIGH");
			task36Schema.setTask_priority_value_facet("HIGH");
			task36Schema.setTask_role_id(task36.getTeamRoleId().getId());
			task36Schema.setTask_role_value(task36.getTeamRoleId().getName());
			task36Schema.setTask_role_value_facet(task36.getTeamRoleId().getName());
			task36Schema.setTask_start_date(task36.getStartDate().toString().concat("T00:00:00Z"));
			task36Schema.setTask_status_id(task36.getStatusId());
			task36Schema.setTask_active_user_cn(!util.isNullOrEmpty(task36.getOwnerId()) && 
					!util.isNullOrEmpty(task36.getOwnerId().getUserId()) ? task36.getOwnerId().getUserId() : "");
			task36Schema.setTask_active_user_id(!util.isNullOrEmpty(task36.getOwnerId()) && 
					!util.isNullOrEmpty(task36.getOwnerId().getId()) ? task36.getOwnerId().getId() : 0);
			task36Schema.setTask_active_user_name(!util.isNullOrEmpty(task36.getOwnerId()) && 
					!util.isNullOrEmpty(task36.getOwnerId().getIdentityDisplayName()) ? 
					task36.getOwnerId().getIdentityDisplayName() : "");
			task36Schema.setTask_owner_cn(!util.isNullOrEmpty(task36.getOwnerId()) && 
					!util.isNullOrEmpty(task36.getOwnerId().getUserId()) ? task36.getOwnerId().getUserId() : "");
			task36Schema.setTask_owner_id(!util.isNullOrEmpty(task36.getOwnerId()) && 
					!util.isNullOrEmpty(task36.getOwnerId().getId()) ? task36.getOwnerId().getId() : 0);
			task36Schema.setTask_owner_name(!util.isNullOrEmpty(task36.getOwnerId()) && 
					!util.isNullOrEmpty(task36.getOwnerId().getIdentityDisplayName()) ? 
					task36.getOwnerId().getIdentityDisplayName() : "");
			task36Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task36.getOwnerId()) && 
					!util.isNullOrEmpty(task36.getOwnerId().getIdentityDisplayName()) ? 
					task36.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task36Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task36.getStatusId()))).findAny().orElse(null);
					
			task36Schema.setTask_status_type(task36Status.getStatusType());
			task36Schema.setTask_status_value(task36Status.getName());
			task36Schema.setTask_status_value_facet(task36Status.getName());
			task36Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task36Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task36Schema.setNPD_TASK_CAN_START(String.valueOf(task36.getIsActive()));;
			task36Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask36ActualStartDate());
			task36Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask36ActualDueDate());
			
			taskSchemaList.add(task36Schema);
			
			
			NPDTask npdTask36 = new NPDTask();
//			npdTask36.setId(NPDTaskRepository.getCurrentVal());
			npdTask36.setCanstart(task36Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask36.setActualduedate(taskDetails.getTask36ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask36ActualDueDate().split("T")[0]) : null);
			npdTask36.setActualstartdate(taskDetails.getTask36ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask36ActualStartDate().split("T")[0]) : null);
			npdTask36.setTask_id(Integer.parseInt(task36Schema.getId()));
			npdTask36.setTask_item_id(task36Schema.getItem_id());
			npdTask36.setAllowproductionschedulelessthan8weeks(taskDetails.getTask36AllowProdSchedule().equalsIgnoreCase("true") 
					? true : false);
			npdTask36.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask36);
			entityManager.flush();
			
		}
		
		MPMTask task37 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 37")).findAny().orElse(null);
		if(task37 != null && !util.isNullOrEmpty(task37.getId())) {
			MPMTaskSchema task37Schema = new MPMTaskSchema();
			task37Schema.setContent_type("MPM_TASK");
			task37Schema.setId(String.valueOf(task37.getId()));
			task37Schema.setIs_active_task(String.valueOf(task37.getIsActive()));
			task37Schema.setIs_approval_task("false");
			task37Schema.setIs_deleted_task("false");
			task37Schema.setIs_milestone("false");
			task37Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task37.getId())));
			task37Schema.setOriginal_task_due_date(task37.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task37Schema.setOriginal_task_start_date(task37.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task37Schema.setProject_item_id(projectItemId);
			task37Schema.setTask_description(task37.getDescription());
			task37Schema.setTask_description_facet(task37.getDescription());
			task37Schema.setTask_due_date(task37.getDueDate().toString().concat("T00:00:00Z"));
			task37Schema.setTask_duration(task37.getTaskDuration());
			task37Schema.setTask_duration_type("weeks");
			task37Schema.setTask_milestone_progress("0");
			task37Schema.setTask_name(task37.getName());
			task37Schema.setTask_priority_id(task37.getPriorityId());
			task37Schema.setTask_priority_value("HIGH");
			task37Schema.setTask_priority_value_facet("HIGH");
			task37Schema.setTask_role_id(task37.getTeamRoleId().getId());
			task37Schema.setTask_role_value(task37.getTeamRoleId().getName());
			task37Schema.setTask_role_value_facet(task37.getTeamRoleId().getName());
			task37Schema.setTask_start_date(task37.getStartDate().toString().concat("T00:00:00Z"));
			task37Schema.setTask_status_id(task37.getStatusId());
			task37Schema.setTask_active_user_cn(!util.isNullOrEmpty(task37.getOwnerId()) && 
					!util.isNullOrEmpty(task37.getOwnerId().getUserId()) ? task37.getOwnerId().getUserId() : "");
			task37Schema.setTask_active_user_id(!util.isNullOrEmpty(task37.getOwnerId()) && 
					!util.isNullOrEmpty(task37.getOwnerId().getId()) ? task37.getOwnerId().getId() : 0);
			task37Schema.setTask_active_user_name(!util.isNullOrEmpty(task37.getOwnerId()) && 
					!util.isNullOrEmpty(task37.getOwnerId().getIdentityDisplayName()) ? 
					task37.getOwnerId().getIdentityDisplayName() : "");
			task37Schema.setTask_owner_cn(!util.isNullOrEmpty(task37.getOwnerId()) && 
					!util.isNullOrEmpty(task37.getOwnerId().getUserId()) ? task37.getOwnerId().getUserId() : "");
			task37Schema.setTask_owner_id(!util.isNullOrEmpty(task37.getOwnerId()) && 
					!util.isNullOrEmpty(task37.getOwnerId().getId()) ? task37.getOwnerId().getId() : 0);
			task37Schema.setTask_owner_name(!util.isNullOrEmpty(task37.getOwnerId()) && 
					!util.isNullOrEmpty(task37.getOwnerId().getIdentityDisplayName()) ? 
					task37.getOwnerId().getIdentityDisplayName() : "");
			task37Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task37.getOwnerId()) && 
					!util.isNullOrEmpty(task37.getOwnerId().getIdentityDisplayName()) ? 
					task37.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task37Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task37.getStatusId()))).findAny().orElse(null);
					
			task37Schema.setTask_status_type(task37Status.getStatusType());
			task37Schema.setTask_status_value(task37Status.getName());
			task37Schema.setTask_status_value_facet(task37Status.getName());
			task37Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task37Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task37Schema.setNPD_TASK_CAN_START(String.valueOf(task37.getIsActive()));;
			task37Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask37ActualStartDate());
			task37Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask37ActualDueDate());
			taskSchemaList.add(task37Schema);
			
			
			NPDTask npdTask37 = new NPDTask();
//			npdTask37.setId(NPDTaskRepository.getCurrentVal());
			npdTask37.setCanstart(task37Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask37.setActualduedate(taskDetails.getTask37ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask37ActualDueDate().split("T")[0]) : null);
			npdTask37.setActualstartdate(taskDetails.getTask37ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask37ActualStartDate().split("T")[0]) : null);
			npdTask37.setTask_id(Integer.parseInt(task37Schema.getId()));
			npdTask37.setTask_item_id(task37Schema.getItem_id());
			npdTask37.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask37);
			entityManager.flush();
			
		}
		
		MPMTask task38 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 38")).findAny().orElse(null);
		if(task38 != null && !util.isNullOrEmpty(task38.getId())) {
			MPMTaskSchema task38Schema = new MPMTaskSchema();
			task38Schema.setContent_type("MPM_TASK");
			task38Schema.setId(String.valueOf(task38.getId()));
			task38Schema.setIs_active_task(String.valueOf(task38.getIsActive()));
			task38Schema.setIs_approval_task("false");
			task38Schema.setIs_deleted_task("false");
			task38Schema.setIs_milestone("false");
			task38Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task38.getId())));
			task38Schema.setOriginal_task_due_date(task38.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task38Schema.setOriginal_task_start_date(task38.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task38Schema.setProject_item_id(projectItemId);
			task38Schema.setTask_description(task38.getDescription());
			task38Schema.setTask_description_facet(task38.getDescription());
			task38Schema.setTask_due_date(task38.getDueDate().toString().concat("T00:00:00Z"));
			task38Schema.setTask_duration(task38.getTaskDuration());
			task38Schema.setTask_duration_type("weeks");
			task38Schema.setTask_milestone_progress("0");
			task38Schema.setTask_name(task38.getName());
			task38Schema.setTask_priority_id(task38.getPriorityId());
			task38Schema.setTask_priority_value("HIGH");
			task38Schema.setTask_priority_value_facet("HIGH");
			task38Schema.setTask_role_id(task38.getTeamRoleId().getId());
			task38Schema.setTask_role_value(task38.getTeamRoleId().getName());
			task38Schema.setTask_role_value_facet(task38.getTeamRoleId().getName());
			task38Schema.setTask_start_date(task38.getStartDate().toString().concat("T00:00:00Z"));
			task38Schema.setTask_status_id(task38.getStatusId());
			task38Schema.setTask_active_user_cn(!util.isNullOrEmpty(task38.getOwnerId()) && 
					!util.isNullOrEmpty(task38.getOwnerId().getUserId()) ? task38.getOwnerId().getUserId() : "");
			task38Schema.setTask_active_user_id(!util.isNullOrEmpty(task38.getOwnerId()) && 
					!util.isNullOrEmpty(task38.getOwnerId().getId()) ? task38.getOwnerId().getId() : 0);
			task38Schema.setTask_active_user_name(!util.isNullOrEmpty(task38.getOwnerId()) && 
					!util.isNullOrEmpty(task38.getOwnerId().getIdentityDisplayName()) ? 
					task38.getOwnerId().getIdentityDisplayName() : "");
			task38Schema.setTask_owner_cn(!util.isNullOrEmpty(task38.getOwnerId()) && 
					!util.isNullOrEmpty(task38.getOwnerId().getUserId()) ? task38.getOwnerId().getUserId() : "");
			task38Schema.setTask_owner_id(!util.isNullOrEmpty(task38.getOwnerId()) && 
					!util.isNullOrEmpty(task38.getOwnerId().getId()) ? task38.getOwnerId().getId() : 0);
			task38Schema.setTask_owner_name(!util.isNullOrEmpty(task38.getOwnerId()) && 
					!util.isNullOrEmpty(task38.getOwnerId().getIdentityDisplayName()) ? 
					task38.getOwnerId().getIdentityDisplayName() : "");
			task38Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task38.getOwnerId()) && 
					!util.isNullOrEmpty(task38.getOwnerId().getIdentityDisplayName()) ? 
					task38.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task38Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task38.getStatusId()))).findAny().orElse(null);
					
			task38Schema.setTask_status_type(task38Status.getStatusType());
			task38Schema.setTask_status_value(task38Status.getName());
			task38Schema.setTask_status_value_facet(task38Status.getName());
			task38Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task38Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task38Schema.setNPD_TASK_CAN_START(String.valueOf(task38.getIsActive()));;
			task38Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask38ActualStartDate());
			task38Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask38ActualDueDate());
			taskSchemaList.add(task38Schema);
			
			
			NPDTask npdTask38 = new NPDTask();
//			npdTask38.setId(NPDTaskRepository.getCurrentVal());
			npdTask38.setCanstart(task38Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask38.setActualduedate(taskDetails.getTask38ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask38ActualDueDate().split("T")[0]) : null);
			npdTask38.setActualstartdate(taskDetails.getTask38ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask38ActualStartDate().split("T")[0]) : null);
			npdTask38.setTask_id(Integer.parseInt(task38Schema.getId()));
			npdTask38.setTask_item_id(task38Schema.getItem_id());
			npdTask38.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask38);
			entityManager.flush();
			
		}
		
		MPMTask task39 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 39")).findAny().orElse(null);
		if(task39 != null && !util.isNullOrEmpty(task39.getId())) {
			MPMTaskSchema task39Schema = new MPMTaskSchema();
			task39Schema.setContent_type("MPM_TASK");
			task39Schema.setId(String.valueOf(task39.getId()));
			task39Schema.setIs_active_task(String.valueOf(task39.getIsActive()));
			task39Schema.setIs_approval_task("false");
			task39Schema.setIs_deleted_task("false");
			task39Schema.setIs_milestone("false");
			task39Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task39.getId())));
			task39Schema.setOriginal_task_due_date(task39.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task39Schema.setOriginal_task_start_date(task39.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task39Schema.setProject_item_id(projectItemId);
			task39Schema.setTask_description(task39.getDescription());
			task39Schema.setTask_description_facet(task39.getDescription());
			task39Schema.setTask_due_date(task39.getDueDate().toString().concat("T00:00:00Z"));
			task39Schema.setTask_duration(task39.getTaskDuration());
			task39Schema.setTask_duration_type("weeks");
			task39Schema.setTask_milestone_progress("0");
			task39Schema.setTask_name(task39.getName());
			task39Schema.setTask_priority_id(task39.getPriorityId());
			task39Schema.setTask_priority_value("HIGH");
			task39Schema.setTask_priority_value_facet("HIGH");
			task39Schema.setTask_role_id(task39.getTeamRoleId().getId());
			task39Schema.setTask_role_value(task39.getTeamRoleId().getName());
			task39Schema.setTask_role_value_facet(task39.getTeamRoleId().getName());
			task39Schema.setTask_start_date(task39.getStartDate().toString().concat("T00:00:00Z"));
			task39Schema.setTask_status_id(task39.getStatusId());
			task39Schema.setTask_active_user_cn(!util.isNullOrEmpty(task39.getOwnerId()) && 
					!util.isNullOrEmpty(task39.getOwnerId().getUserId()) ? task39.getOwnerId().getUserId() : "");
			task39Schema.setTask_active_user_id(!util.isNullOrEmpty(task39.getOwnerId()) && 
					!util.isNullOrEmpty(task39.getOwnerId().getId()) ? task39.getOwnerId().getId() : 0);
			task39Schema.setTask_active_user_name(!util.isNullOrEmpty(task39.getOwnerId()) && 
					!util.isNullOrEmpty(task39.getOwnerId().getIdentityDisplayName()) ? 
					task39.getOwnerId().getIdentityDisplayName() : "");
			task39Schema.setTask_owner_cn(!util.isNullOrEmpty(task39.getOwnerId()) && 
					!util.isNullOrEmpty(task39.getOwnerId().getUserId()) ? task39.getOwnerId().getUserId() : "");
			task39Schema.setTask_owner_id(!util.isNullOrEmpty(task39.getOwnerId()) && 
					!util.isNullOrEmpty(task39.getOwnerId().getId()) ? task39.getOwnerId().getId() : 0);
			task39Schema.setTask_owner_name(!util.isNullOrEmpty(task39.getOwnerId()) && 
					!util.isNullOrEmpty(task39.getOwnerId().getIdentityDisplayName()) ? 
					task39.getOwnerId().getIdentityDisplayName() : "");
			task39Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task39.getOwnerId()) && 
					!util.isNullOrEmpty(task39.getOwnerId().getIdentityDisplayName()) ? 
					task39.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task39Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task39.getStatusId()))).findAny().orElse(null);
					
			task39Schema.setTask_status_type(task39Status.getStatusType());
			task39Schema.setTask_status_value(task39Status.getName());
			task39Schema.setTask_status_value_facet(task39Status.getName());
			task39Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task39Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task39Schema.setNPD_TASK_CAN_START(String.valueOf(task39.getIsActive()));;
			task39Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask39ActualStartDate());
			task39Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask39ActualDueDate());
			taskSchemaList.add(task39Schema);
			
			
			NPDTask npdTask39 = new NPDTask();
//			npdTask39.setId(NPDTaskRepository.getCurrentVal());
			npdTask39.setCanstart(task39Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask39.setActualduedate(taskDetails.getTask39ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask39ActualDueDate().split("T")[0]) : null);
			npdTask39.setActualstartdate(taskDetails.getTask39ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask39ActualStartDate().split("T")[0]) : null);
			npdTask39.setTask_id(Integer.parseInt(task39Schema.getId()));
			npdTask39.setTask_item_id(task39Schema.getItem_id());
			npdTask39.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask39);
			entityManager.flush();
			
		}
		
		MPMTask task40 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 40")).findAny().orElse(null);
		if(task40 != null && !util.isNullOrEmpty(task40.getId())) {
			MPMTaskSchema task40Schema = new MPMTaskSchema();
			task40Schema.setContent_type("MPM_TASK");
			task40Schema.setId(String.valueOf(task40.getId()));
			task40Schema.setIs_active_task(String.valueOf(task40.getIsActive()));
			task40Schema.setIs_approval_task("false");
			task40Schema.setIs_deleted_task("false");
			task40Schema.setIs_milestone("false");
			task40Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task40.getId())));
			task40Schema.setOriginal_task_due_date(task40.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task40Schema.setOriginal_task_start_date(task40.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task40Schema.setProject_item_id(projectItemId);
			task40Schema.setTask_description(task40.getDescription());
			task40Schema.setTask_description_facet(task40.getDescription());
			task40Schema.setTask_due_date(task40.getDueDate().toString().concat("T00:00:00Z"));
			task40Schema.setTask_duration(task40.getTaskDuration());
			task40Schema.setTask_duration_type("weeks");
			task40Schema.setTask_milestone_progress("0");
			task40Schema.setTask_name(task40.getName());
			task40Schema.setTask_priority_id(task40.getPriorityId());
			task40Schema.setTask_priority_value("HIGH");
			task40Schema.setTask_priority_value_facet("HIGH");
			task40Schema.setTask_role_id(task40.getTeamRoleId().getId());
			task40Schema.setTask_role_value(task40.getTeamRoleId().getName());
			task40Schema.setTask_role_value_facet(task40.getTeamRoleId().getName());
			task40Schema.setTask_start_date(task40.getStartDate().toString().concat("T00:00:00Z"));
			task40Schema.setTask_status_id(task40.getStatusId());
			task40Schema.setTask_active_user_cn(!util.isNullOrEmpty(task40.getOwnerId()) && 
					!util.isNullOrEmpty(task40.getOwnerId().getUserId()) ? task40.getOwnerId().getUserId() : "");
			task40Schema.setTask_active_user_id(!util.isNullOrEmpty(task40.getOwnerId()) && 
					!util.isNullOrEmpty(task40.getOwnerId().getId()) ? task40.getOwnerId().getId() : 0);
			task40Schema.setTask_active_user_name(!util.isNullOrEmpty(task40.getOwnerId()) && 
					!util.isNullOrEmpty(task40.getOwnerId().getIdentityDisplayName()) ? 
					task40.getOwnerId().getIdentityDisplayName() : "");
			task40Schema.setTask_owner_cn(!util.isNullOrEmpty(task40.getOwnerId()) && 
					!util.isNullOrEmpty(task40.getOwnerId().getUserId()) ? task40.getOwnerId().getUserId() : "");
			task40Schema.setTask_owner_id(!util.isNullOrEmpty(task40.getOwnerId()) && 
					!util.isNullOrEmpty(task40.getOwnerId().getId()) ? task40.getOwnerId().getId() : 0);
			task40Schema.setTask_owner_name(!util.isNullOrEmpty(task40.getOwnerId()) && 
					!util.isNullOrEmpty(task40.getOwnerId().getIdentityDisplayName()) ? 
					task40.getOwnerId().getIdentityDisplayName() : "");
			task40Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task40.getOwnerId()) && 
					!util.isNullOrEmpty(task40.getOwnerId().getIdentityDisplayName()) ? 
					task40.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task40Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task40.getStatusId()))).findAny().orElse(null);
					
			task40Schema.setTask_status_type(task40Status.getStatusType());
			task40Schema.setTask_status_value(task40Status.getName());
			task40Schema.setTask_status_value_facet(task40Status.getName());
			task40Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task40Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task40Schema.setNPD_TASK_CAN_START(String.valueOf(task40.getIsActive()));;
			task40Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask40ActualStartDate());
			task40Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask40ActualDueDate());
			taskSchemaList.add(task40Schema);
			
			
			NPDTask npdTask40 = new NPDTask();
//			npdTask40.setId(NPDTaskRepository.getCurrentVal());
			npdTask40.setCanstart(task40Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask40.setActualduedate(taskDetails.getTask40ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask40ActualDueDate().split("T")[0]) : null);
			npdTask40.setActualstartdate(taskDetails.getTask40ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask40ActualStartDate().split("T")[0]) : null);
			npdTask40.setTask_id(Integer.parseInt(task40Schema.getId()));
			npdTask40.setTask_item_id(task40Schema.getItem_id());
			npdTask40.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask40);
			entityManager.flush();
			
		}
		
		MPMTask task41 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 41")).findAny().orElse(null);
		if(task41 != null && !util.isNullOrEmpty(task41.getId())) {
			MPMTaskSchema task41Schema = new MPMTaskSchema();
			task41Schema.setContent_type("MPM_TASK");
			task41Schema.setId(String.valueOf(task41.getId()));
			task41Schema.setIs_active_task(String.valueOf(task41.getIsActive()));
			task41Schema.setIs_approval_task("false");
			task41Schema.setIs_deleted_task("false");
			task41Schema.setIs_milestone("false");
			task41Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task41.getId())));
			task41Schema.setOriginal_task_due_date(task41.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task41Schema.setOriginal_task_start_date(task41.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task41Schema.setProject_item_id(projectItemId);
			task41Schema.setTask_description(task41.getDescription());
			task41Schema.setTask_description_facet(task41.getDescription());
			task41Schema.setTask_due_date(task41.getDueDate().toString().concat("T00:00:00Z"));
			task41Schema.setTask_duration(task41.getTaskDuration());
			task41Schema.setTask_duration_type("weeks");
			task41Schema.setTask_milestone_progress("0");
			task41Schema.setTask_name(task41.getName());
			task41Schema.setTask_priority_id(task41.getPriorityId());
			task41Schema.setTask_priority_value("HIGH");
			task41Schema.setTask_priority_value_facet("HIGH");
			task41Schema.setTask_role_id(task41.getTeamRoleId().getId());
			task41Schema.setTask_role_value(task41.getTeamRoleId().getName());
			task41Schema.setTask_role_value_facet(task41.getTeamRoleId().getName());
			task41Schema.setTask_start_date(task41.getStartDate().toString().concat("T00:00:00Z"));
			task41Schema.setTask_status_id(task41.getStatusId());
			task41Schema.setTask_active_user_cn(!util.isNullOrEmpty(task41.getOwnerId()) && 
					!util.isNullOrEmpty(task41.getOwnerId().getUserId()) ? task41.getOwnerId().getUserId() : "");
			task41Schema.setTask_active_user_id(!util.isNullOrEmpty(task41.getOwnerId()) && 
					!util.isNullOrEmpty(task41.getOwnerId().getId()) ? task41.getOwnerId().getId() : 0);
			task41Schema.setTask_active_user_name(!util.isNullOrEmpty(task41.getOwnerId()) && 
					!util.isNullOrEmpty(task41.getOwnerId().getIdentityDisplayName()) ? 
					task41.getOwnerId().getIdentityDisplayName() : "");
			task41Schema.setTask_owner_cn(!util.isNullOrEmpty(task41.getOwnerId()) && 
					!util.isNullOrEmpty(task41.getOwnerId().getUserId()) ? task41.getOwnerId().getUserId() : "");
			task41Schema.setTask_owner_id(!util.isNullOrEmpty(task41.getOwnerId()) && 
					!util.isNullOrEmpty(task41.getOwnerId().getId()) ? task41.getOwnerId().getId() : 0);
			task41Schema.setTask_owner_name(!util.isNullOrEmpty(task41.getOwnerId()) && 
					!util.isNullOrEmpty(task41.getOwnerId().getIdentityDisplayName()) ? 
					task41.getOwnerId().getIdentityDisplayName() : "");
			task41Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task41.getOwnerId()) && 
					!util.isNullOrEmpty(task41.getOwnerId().getIdentityDisplayName()) ? 
					task41.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task41Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task41.getStatusId()))).findAny().orElse(null);
					
			task41Schema.setTask_status_type(task41Status.getStatusType());
			task41Schema.setTask_status_value(task41Status.getName());
			task41Schema.setTask_status_value_facet(task41Status.getName());
			task41Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task41Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task41Schema.setNPD_TASK_CAN_START(String.valueOf(task41.getIsActive()));;
			task41Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask41ActualStartDate());
			task41Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask41ActualDueDate());
			taskSchemaList.add(task41Schema);
			
			
			NPDTask npdTask41 = new NPDTask();
//			npdTask41.setId(NPDTaskRepository.getCurrentVal());
			npdTask41.setCanstart(task41Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask41.setActualduedate(taskDetails.getTask41ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask41ActualDueDate().split("T")[0]) : null);
			npdTask41.setActualstartdate(taskDetails.getTask41ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask41ActualStartDate().split("T")[0]) : null);
			npdTask41.setTask_id(Integer.parseInt(task41Schema.getId()));
			npdTask41.setTask_item_id(task41Schema.getItem_id());
			npdTask41.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask41);
			entityManager.flush();
			
		}
		
		MPMTask task42 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("TASK 42")).findAny().orElse(null);
		if(task42 != null && !util.isNullOrEmpty(task42.getId())) {
			MPMTaskSchema task42Schema = new MPMTaskSchema();
			task42Schema.setContent_type("MPM_TASK");
			task42Schema.setId(String.valueOf(task42.getId()));
			task42Schema.setIs_active_task(String.valueOf(task42.getIsActive()));
			task42Schema.setIs_approval_task("false");
			task42Schema.setIs_deleted_task("false");
			task42Schema.setIs_milestone("false");
			task42Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(task42.getId())));
			task42Schema.setOriginal_task_due_date(task42.getOriginalDueDate().toString().concat("T00:00:00Z"));
			task42Schema.setOriginal_task_start_date(task42.getOriginalStartDate().toString().concat("T00:00:00Z"));
			task42Schema.setProject_item_id(projectItemId);
			task42Schema.setTask_description(task42.getDescription());
			task42Schema.setTask_description_facet(task42.getDescription());
			task42Schema.setTask_due_date(task42.getDueDate().toString().concat("T00:00:00Z"));
			task42Schema.setTask_duration(task42.getTaskDuration());
			task42Schema.setTask_duration_type("weeks");
			task42Schema.setTask_milestone_progress("0");
			task42Schema.setTask_name(task42.getName());
			task42Schema.setTask_priority_id(task42.getPriorityId());
			task42Schema.setTask_priority_value("HIGH");
			task42Schema.setTask_priority_value_facet("HIGH");
			task42Schema.setTask_role_id(task42.getTeamRoleId().getId());
			task42Schema.setTask_role_value(task42.getTeamRoleId().getName());
			task42Schema.setTask_role_value_facet(task42.getTeamRoleId().getName());
			task42Schema.setTask_start_date(task42.getStartDate().toString().concat("T00:00:00Z"));
			task42Schema.setTask_status_id(task42.getStatusId());
			task42Schema.setTask_active_user_cn(!util.isNullOrEmpty(task42.getOwnerId()) && 
					!util.isNullOrEmpty(task42.getOwnerId().getUserId()) ? task42.getOwnerId().getUserId() : "");
			task42Schema.setTask_active_user_id(!util.isNullOrEmpty(task42.getOwnerId()) && 
					!util.isNullOrEmpty(task42.getOwnerId().getId()) ? task42.getOwnerId().getId() : 0);
			task42Schema.setTask_active_user_name(!util.isNullOrEmpty(task42.getOwnerId()) && 
					!util.isNullOrEmpty(task42.getOwnerId().getIdentityDisplayName()) ? 
					task42.getOwnerId().getIdentityDisplayName() : "");
			task42Schema.setTask_owner_cn(!util.isNullOrEmpty(task42.getOwnerId()) && 
					!util.isNullOrEmpty(task42.getOwnerId().getUserId()) ? task42.getOwnerId().getUserId() : "");
			task42Schema.setTask_owner_id(!util.isNullOrEmpty(task42.getOwnerId()) && 
					!util.isNullOrEmpty(task42.getOwnerId().getId()) ? task42.getOwnerId().getId() : 0);
			task42Schema.setTask_owner_name(!util.isNullOrEmpty(task42.getOwnerId()) && 
					!util.isNullOrEmpty(task42.getOwnerId().getIdentityDisplayName()) ? 
					task42.getOwnerId().getIdentityDisplayName() : "");
			task42Schema.setTask_owner_name_facet(!util.isNullOrEmpty(task42.getOwnerId()) && 
					!util.isNullOrEmpty(task42.getOwnerId().getIdentityDisplayName()) ? 
					task42.getOwnerId().getIdentityDisplayName() : "");
			
			MPMStatus task42Status = ConfigService.getMPMStatusList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(task42.getStatusId()))).findAny().orElse(null);
					
			task42Schema.setTask_status_type(task42Status.getStatusType());
			task42Schema.setTask_status_value(task42Status.getName());
			task42Schema.setTask_status_value_facet(task42Status.getName());
			task42Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
			task42Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
			
			task42Schema.setNPD_TASK_CAN_START(String.valueOf(task42.getIsActive()));;
			task42Schema.setNPD_TASK_ACTUAL_START_DATE(taskDetails.getTask42ActualStartDate());
			task42Schema.setNPD_TASK_ACTUAL_DUE_DATE(taskDetails.getTask42ActualDueDate());
			taskSchemaList.add(task42Schema);
			
			
			NPDTask npdTask42 = new NPDTask();
//			npdTask42.setId(NPDTaskRepository.getCurrentVal());
			npdTask42.setCanstart(task42Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
			npdTask42.setActualduedate(taskDetails.getTask42ActualDueDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask42ActualDueDate().split("T")[0]) : null);
			npdTask42.setActualstartdate(taskDetails.getTask42ActualStartDate().indexOf("T") > 0 ? 
					Date.valueOf(taskDetails.getTask42ActualStartDate().split("T")[0]) : null);
			npdTask42.setTask_id(Integer.parseInt(task42Schema.getId()));
			npdTask42.setTask_item_id(task42Schema.getItem_id());
			npdTask42.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			
			NPDTaskRepository.save(npdTask42);
			entityManager.flush();
			
		}
		
		MPMTask taskCP1 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("CP1")).findAny().orElse(null);
		if(taskCP1 != null && !util.isNullOrEmpty(taskCP1.getId())) {
		MPMTaskSchema taskCP1Schema = new MPMTaskSchema();
		taskCP1Schema.setContent_type("MPM_TASK");
		taskCP1Schema.setId(String.valueOf(taskCP1.getId()));
		taskCP1Schema.setIs_active_task(String.valueOf(taskCP1.getIsActive()));
		taskCP1Schema.setIs_approval_task("false");
		taskCP1Schema.setIs_deleted_task("false");
		taskCP1Schema.setIs_milestone("false");
		taskCP1Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(taskCP1.getId())));
		
		taskCP1Schema.setOriginal_task_due_date(!util.isNullOrEmpty(taskCP1.getOriginalDueDate()) ?
				taskCP1.getOriginalDueDate().toString().concat("T00:00:00Z") : "");
		taskCP1Schema.setOriginal_task_start_date(!util.isNullOrEmpty(taskCP1.getOriginalStartDate()) ?
				taskCP1.getOriginalStartDate().toString().concat("T00:00:00Z") : "");
		taskCP1Schema.setTask_start_date(taskCP1.getStartDate().toString().concat("T00:00:00Z"));
		taskCP1Schema.setTask_due_date(taskCP1.getDueDate().toString().concat("T00:00:00Z"));
		
		taskCP1Schema.setProject_item_id(projectItemId);
		taskCP1Schema.setTask_description(taskCP1.getDescription());
		taskCP1Schema.setTask_description_facet(taskCP1.getDescription());
		taskCP1Schema.setTask_duration(taskCP1.getTaskDuration());
		taskCP1Schema.setTask_duration_type("weeks");
		taskCP1Schema.setTask_milestone_progress("0");
		taskCP1Schema.setTask_name(taskCP1.getName());
		taskCP1Schema.setTask_priority_id(taskCP1.getPriorityId());
		taskCP1Schema.setTask_priority_value("HIGH");
		taskCP1Schema.setTask_priority_value_facet("HIGH");
		taskCP1Schema.setTask_role_id(taskCP1.getTeamRoleId().getId());
		taskCP1Schema.setTask_role_value(taskCP1.getTeamRoleId().getName());
		taskCP1Schema.setTask_role_value_facet(taskCP1.getTeamRoleId().getName());
		
		taskCP1Schema.setTask_status_id(taskCP1.getStatusId());
		taskCP1Schema.setTask_active_user_cn(!util.isNullOrEmpty(taskCP1.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP1.getOwnerId().getUserId()) ? taskCP1.getOwnerId().getUserId() : "");
		taskCP1Schema.setTask_active_user_id(!util.isNullOrEmpty(taskCP1.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP1.getOwnerId().getId()) ? taskCP1.getOwnerId().getId() : 0);
		taskCP1Schema.setTask_active_user_name(!util.isNullOrEmpty(taskCP1.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP1.getOwnerId().getIdentityDisplayName()) ? 
				taskCP1.getOwnerId().getIdentityDisplayName() : "");
		taskCP1Schema.setTask_owner_cn(!util.isNullOrEmpty(taskCP1.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP1.getOwnerId().getUserId()) ? taskCP1.getOwnerId().getUserId() : "");
		taskCP1Schema.setTask_owner_id(!util.isNullOrEmpty(taskCP1.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP1.getOwnerId().getId()) ? taskCP1.getOwnerId().getId() : 0);
		taskCP1Schema.setTask_owner_name(!util.isNullOrEmpty(taskCP1.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP1.getOwnerId().getIdentityDisplayName()) ? 
				taskCP1.getOwnerId().getIdentityDisplayName() : "");
		taskCP1Schema.setTask_owner_name_facet(!util.isNullOrEmpty(taskCP1.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP1.getOwnerId().getIdentityDisplayName()) ? 
				taskCP1.getOwnerId().getIdentityDisplayName() : "");
		
		MPMStatus taskCP1Status = ConfigService.getMPMStatusList().stream()
				.filter(obj -> Objects.nonNull(obj.getId()))
				.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(taskCP1.getStatusId()))).findAny().orElse(null);
				
		taskCP1Schema.setTask_status_type(taskCP1Status.getStatusType());
		taskCP1Schema.setTask_status_value(taskCP1Status.getName());
		taskCP1Schema.setTask_status_value_facet(taskCP1Status.getName());
		taskCP1Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
		taskCP1Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
		
		taskCP1Schema.setNPD_TASK_CAN_START(String.valueOf(taskCP1.getIsActive()));
		taskCP1Schema.setNPD_TASK_ACTUAL_START_DATE("");
		taskCP1Schema.setNPD_TASK_ACTUAL_DUE_DATE("");
		
		taskSchemaList.add(taskCP1Schema);
		logger.info("Task CP1 Indexed Successfully..");
		
		NPDTask npdTaskCP1 = new NPDTask();
//		npdTaskCP1.setId(NPDTaskRepository.getCurrentVal());
		npdTaskCP1.setCanstart(taskCP1Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
		npdTaskCP1.setTask_id(Integer.parseInt(taskCP1Schema.getId()));
		npdTaskCP1.setTask_item_id(taskCP1Schema.getItem_id());
		npdTaskCP1.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
		
		NPDTaskRepository.save(npdTaskCP1);
		entityManager.flush();
		}
		
		MPMTask taskCP2 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("CP2")).findAny().orElse(null);
		if(taskCP2 != null && !util.isNullOrEmpty(taskCP2.getId())) {
		MPMTaskSchema taskCP2Schema = new MPMTaskSchema();
		taskCP2Schema.setContent_type("MPM_TASK");
		taskCP2Schema.setId(String.valueOf(taskCP2.getId()));
		taskCP2Schema.setIs_active_task(String.valueOf(taskCP2.getIsActive()));
		taskCP2Schema.setIs_approval_task("false");
		taskCP2Schema.setIs_deleted_task("false");
		taskCP2Schema.setIs_milestone("false");
		taskCP2Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(taskCP2.getId())));
		
		taskCP2Schema.setOriginal_task_due_date(!util.isNullOrEmpty(taskCP2.getOriginalDueDate()) ?
				taskCP2.getOriginalDueDate().toString().concat("T00:00:00Z") : "");
		taskCP2Schema.setOriginal_task_start_date(!util.isNullOrEmpty(taskCP2.getOriginalStartDate()) ?
				taskCP2.getOriginalStartDate().toString().concat("T00:00:00Z") : "");
		taskCP2Schema.setTask_start_date(taskCP2.getStartDate().toString().concat("T00:00:00Z"));
		taskCP2Schema.setTask_due_date(taskCP2.getDueDate().toString().concat("T00:00:00Z"));
		
		taskCP2Schema.setProject_item_id(projectItemId);

		taskCP2Schema.setTask_description(taskCP2.getDescription());
		taskCP2Schema.setTask_description_facet(taskCP2.getDescription());
		taskCP2Schema.setTask_duration(taskCP2.getTaskDuration());
		taskCP2Schema.setTask_duration_type("weeks");
		taskCP2Schema.setTask_milestone_progress("0");
		taskCP2Schema.setTask_name(taskCP2.getName());
		taskCP2Schema.setTask_priority_id(taskCP2.getPriorityId());
		taskCP2Schema.setTask_priority_value("HIGH");
		taskCP2Schema.setTask_priority_value_facet("HIGH");
		taskCP2Schema.setTask_role_id(taskCP2.getTeamRoleId().getId());
		taskCP2Schema.setTask_role_value(taskCP2.getTeamRoleId().getName());
		taskCP2Schema.setTask_role_value_facet(taskCP2.getTeamRoleId().getName());
		taskCP2Schema.setTask_status_id(taskCP2.getStatusId());
		taskCP2Schema.setTask_active_user_cn(!util.isNullOrEmpty(taskCP2.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP2.getOwnerId().getUserId()) ? taskCP2.getOwnerId().getUserId() : "");
		taskCP2Schema.setTask_active_user_id(!util.isNullOrEmpty(taskCP2.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP2.getOwnerId().getId()) ? taskCP2.getOwnerId().getId() : 0);
		taskCP2Schema.setTask_active_user_name(!util.isNullOrEmpty(taskCP2.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP2.getOwnerId().getIdentityDisplayName()) ? 
				taskCP2.getOwnerId().getIdentityDisplayName() : "");
		taskCP2Schema.setTask_owner_cn(!util.isNullOrEmpty(taskCP2.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP2.getOwnerId().getUserId()) ? taskCP2.getOwnerId().getUserId() : "");
		taskCP2Schema.setTask_owner_id(!util.isNullOrEmpty(taskCP2.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP2.getOwnerId().getId()) ? taskCP2.getOwnerId().getId() : 0);
		taskCP2Schema.setTask_owner_name(!util.isNullOrEmpty(taskCP2.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP2.getOwnerId().getIdentityDisplayName()) ? 
				taskCP2.getOwnerId().getIdentityDisplayName() : "");
		taskCP2Schema.setTask_owner_name_facet(!util.isNullOrEmpty(taskCP2.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP2.getOwnerId().getIdentityDisplayName()) ? 
				taskCP2.getOwnerId().getIdentityDisplayName() : "");
		
		MPMStatus taskCP2Status = ConfigService.getMPMStatusList().stream()
				.filter(obj -> Objects.nonNull(obj.getId()))
				.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(taskCP2.getStatusId()))).findAny().orElse(null);
				
		taskCP2Schema.setTask_status_type(taskCP2Status.getStatusType());
		taskCP2Schema.setTask_status_value(taskCP2Status.getName());
		taskCP2Schema.setTask_status_value_facet(taskCP2Status.getName());
		taskCP2Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
		taskCP2Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
		
		taskCP2Schema.setNPD_TASK_CAN_START(String.valueOf(taskCP2.getIsActive()));
		taskCP2Schema.setNPD_TASK_ACTUAL_START_DATE("");
		taskCP2Schema.setNPD_TASK_ACTUAL_DUE_DATE("");
		
		taskSchemaList.add(taskCP2Schema);
		logger.info("Task CP2 Indexed Successfully..");
		
		NPDTask npdTaskCP2 = new NPDTask();
//		npdTaskCP2.setId(NPDTaskRepository.getCurrentVal());
		npdTaskCP2.setCanstart(taskCP2Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
		npdTaskCP2.setTask_id(Integer.parseInt(taskCP2Schema.getId()));
		npdTaskCP2.setTask_item_id(taskCP2Schema.getItem_id());
		npdTaskCP2.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
		
		NPDTaskRepository.save(npdTaskCP2);
		entityManager.flush();
		}
		
		MPMTask taskCP3 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("CP3")).findAny().orElse(null);
		if(taskCP3 != null && !util.isNullOrEmpty(taskCP3.getId())) {
		MPMTaskSchema taskCP3Schema = new MPMTaskSchema();
		taskCP3Schema.setContent_type("MPM_TASK");
		taskCP3Schema.setId(String.valueOf(taskCP3.getId()));
		taskCP3Schema.setIs_active_task(String.valueOf(taskCP3.getIsActive()));
		taskCP3Schema.setIs_approval_task("false");
		taskCP3Schema.setIs_deleted_task("false");
		taskCP3Schema.setIs_milestone("false");
		taskCP3Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(taskCP3.getId())));
		
		taskCP3Schema.setOriginal_task_due_date(!util.isNullOrEmpty(taskCP3.getOriginalDueDate()) ?
				taskCP3.getOriginalDueDate().toString().concat("T00:00:00Z") : "");
		taskCP3Schema.setOriginal_task_start_date(!util.isNullOrEmpty(taskCP3.getOriginalStartDate()) ?
				taskCP3.getOriginalStartDate().toString().concat("T00:00:00Z") : "");
		taskCP3Schema.setTask_start_date(taskCP3.getStartDate().toString().concat("T00:00:00Z"));
		taskCP3Schema.setTask_due_date(taskCP3.getDueDate().toString().concat("T00:00:00Z"));

		taskCP3Schema.setProject_item_id(projectItemId);
		taskCP3Schema.setTask_description(taskCP3.getDescription());
		taskCP3Schema.setTask_description_facet(taskCP3.getDescription());
		taskCP3Schema.setTask_duration(taskCP3.getTaskDuration());
		taskCP3Schema.setTask_duration_type("weeks");
		taskCP3Schema.setTask_milestone_progress("0");
		taskCP3Schema.setTask_name(taskCP3.getName());
		taskCP3Schema.setTask_priority_id(taskCP3.getPriorityId());
		taskCP3Schema.setTask_priority_value("HIGH");
		taskCP3Schema.setTask_priority_value_facet("HIGH");
		taskCP3Schema.setTask_role_id(taskCP3.getTeamRoleId().getId());
		taskCP3Schema.setTask_role_value(taskCP3.getTeamRoleId().getName());
		taskCP3Schema.setTask_role_value_facet(taskCP3.getTeamRoleId().getName());
		taskCP3Schema.setTask_status_id(taskCP3.getStatusId());
		taskCP3Schema.setTask_active_user_cn(!util.isNullOrEmpty(taskCP3.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP3.getOwnerId().getUserId()) ? taskCP3.getOwnerId().getUserId() : "");
		taskCP3Schema.setTask_active_user_id(!util.isNullOrEmpty(taskCP3.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP3.getOwnerId().getId()) ? taskCP3.getOwnerId().getId() : 0);
		taskCP3Schema.setTask_active_user_name(!util.isNullOrEmpty(taskCP3.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP3.getOwnerId().getIdentityDisplayName()) ? 
				taskCP3.getOwnerId().getIdentityDisplayName() : "");
		taskCP3Schema.setTask_owner_cn(!util.isNullOrEmpty(taskCP3.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP3.getOwnerId().getUserId()) ? taskCP3.getOwnerId().getUserId() : "");
		taskCP3Schema.setTask_owner_id(!util.isNullOrEmpty(taskCP3.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP3.getOwnerId().getId()) ? taskCP3.getOwnerId().getId() : 0);
		taskCP3Schema.setTask_owner_name(!util.isNullOrEmpty(taskCP3.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP3.getOwnerId().getIdentityDisplayName()) ? 
				taskCP3.getOwnerId().getIdentityDisplayName() : "");
		taskCP3Schema.setTask_owner_name_facet(!util.isNullOrEmpty(taskCP3.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP3.getOwnerId().getIdentityDisplayName()) ? 
				taskCP3.getOwnerId().getIdentityDisplayName() : "");
		
		MPMStatus taskCP3Status = ConfigService.getMPMStatusList().stream()
				.filter(obj -> Objects.nonNull(obj.getId()))
				.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(taskCP3.getStatusId()))).findAny().orElse(null);
				
		taskCP3Schema.setTask_status_type(taskCP3Status.getStatusType());
		taskCP3Schema.setTask_status_value(taskCP3Status.getName());
		taskCP3Schema.setTask_status_value_facet(taskCP3Status.getName());
		taskCP3Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
		taskCP3Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
		
		taskCP3Schema.setNPD_TASK_CAN_START(String.valueOf(taskCP3.getIsActive()));
		taskCP3Schema.setNPD_TASK_ACTUAL_START_DATE("");
		taskCP3Schema.setNPD_TASK_ACTUAL_DUE_DATE("");
		
		taskSchemaList.add(taskCP3Schema);
		logger.info("Task CP3 Indexed Successfully..");
		
		NPDTask npdTaskCP3 = new NPDTask();
//		npdTaskCP3.setId(NPDTaskRepository.getCurrentVal());
		npdTaskCP3.setCanstart(taskCP3Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
		npdTaskCP3.setTask_id(Integer.parseInt(taskCP3Schema.getId()));
		npdTaskCP3.setTask_item_id(taskCP3Schema.getItem_id());
		npdTaskCP3.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
		
		NPDTaskRepository.save(npdTaskCP3);
		entityManager.flush();
		}
		
		MPMTask taskCP4 = taskResponse.stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase("CP4")).findAny().orElse(null);
		if(taskCP4 != null && !util.isNullOrEmpty(taskCP4.getId())) {
		MPMTaskSchema taskCP4Schema = new MPMTaskSchema();
		taskCP4Schema.setContent_type("MPM_TASK");
		taskCP4Schema.setId(String.valueOf(taskCP4.getId()));
		taskCP4Schema.setIs_active_task(String.valueOf(taskCP4.getIsActive()));
		taskCP4Schema.setIs_approval_task("false");
		taskCP4Schema.setIs_deleted_task("false");
		taskCP4Schema.setIs_milestone("false");
		taskCP4Schema.setItem_id(taskItemIdPrefix.concat(".").concat(String.valueOf(taskCP4.getId())));
		
		taskCP4Schema.setOriginal_task_due_date(!util.isNullOrEmpty(taskCP4.getOriginalDueDate()) ?
				taskCP4.getOriginalDueDate().toString().concat("T00:00:00Z") : "");
		taskCP4Schema.setOriginal_task_start_date(!util.isNullOrEmpty(taskCP4.getOriginalStartDate()) ?
				taskCP4.getOriginalStartDate().toString().concat("T00:00:00Z") : "");
		taskCP4Schema.setTask_start_date(taskCP4.getStartDate().toString().concat("T00:00:00Z"));
		taskCP4Schema.setTask_due_date(taskCP4.getDueDate().toString().concat("T00:00:00Z"));

		taskCP4Schema.setProject_item_id(projectItemId);
		taskCP4Schema.setTask_description(taskCP4.getDescription());
		taskCP4Schema.setTask_description_facet(taskCP4.getDescription());
		taskCP4Schema.setTask_duration(taskCP4.getTaskDuration());
		taskCP4Schema.setTask_duration_type("weeks");
		taskCP4Schema.setTask_milestone_progress("0");
		taskCP4Schema.setTask_name(taskCP4.getName());
		taskCP4Schema.setTask_priority_id(taskCP4.getPriorityId());
		taskCP4Schema.setTask_priority_value("HIGH");
		taskCP4Schema.setTask_priority_value_facet("HIGH");
		taskCP4Schema.setTask_role_id(taskCP4.getTeamRoleId().getId());
		taskCP4Schema.setTask_role_value(taskCP4.getTeamRoleId().getName());
		taskCP4Schema.setTask_role_value_facet(taskCP4.getTeamRoleId().getName());
		taskCP4Schema.setTask_status_id(taskCP4.getStatusId());
		taskCP4Schema.setTask_active_user_cn(!util.isNullOrEmpty(taskCP4.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP4.getOwnerId().getUserId()) ? taskCP4.getOwnerId().getUserId() : "");
		taskCP4Schema.setTask_active_user_id(!util.isNullOrEmpty(taskCP4.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP4.getOwnerId().getId()) ? taskCP4.getOwnerId().getId() : 0);
		taskCP4Schema.setTask_active_user_name(!util.isNullOrEmpty(taskCP4.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP4.getOwnerId().getIdentityDisplayName()) ? 
				taskCP4.getOwnerId().getIdentityDisplayName() : "");
		taskCP4Schema.setTask_owner_cn(!util.isNullOrEmpty(taskCP4.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP4.getOwnerId().getUserId()) ? taskCP4.getOwnerId().getUserId() : "");
		taskCP4Schema.setTask_owner_id(!util.isNullOrEmpty(taskCP4.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP4.getOwnerId().getId()) ? taskCP4.getOwnerId().getId() : 0);
		taskCP4Schema.setTask_owner_name(!util.isNullOrEmpty(taskCP4.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP4.getOwnerId().getIdentityDisplayName()) ? 
				taskCP4.getOwnerId().getIdentityDisplayName() : "");
		taskCP4Schema.setTask_owner_name_facet(!util.isNullOrEmpty(taskCP4.getOwnerId()) && 
				!util.isNullOrEmpty(taskCP4.getOwnerId().getIdentityDisplayName()) ? 
				taskCP4.getOwnerId().getIdentityDisplayName() : "");
		
		MPMStatus taskCP4Status = ConfigService.getMPMStatusList().stream()
				.filter(obj -> Objects.nonNull(obj.getId()))
				.filter(obj -> obj.getId() == Integer.parseInt(String.valueOf(taskCP4.getStatusId()))).findAny().orElse(null);
				
		taskCP4Schema.setTask_status_type(taskCP4Status.getStatusType());
		taskCP4Schema.setTask_status_value(taskCP4Status.getName());
		taskCP4Schema.setTask_status_value_facet(taskCP4Status.getName());
		taskCP4Schema.setTask_type("TASK_WITHOUT_DELIVERABLE");
		taskCP4Schema.setTask_type_facet("TASK_WITHOUT_DELIVERABLE");
		
		taskCP4Schema.setNPD_TASK_CAN_START(String.valueOf(taskCP4.getIsActive()));
		taskCP4Schema.setNPD_TASK_ACTUAL_START_DATE("");
		taskCP4Schema.setNPD_TASK_ACTUAL_DUE_DATE("");
		
		taskSchemaList.add(taskCP4Schema);
		logger.info("Task CP4 Indexed Successfully..");
		
		NPDTask npdTaskCP4 = new NPDTask();
//		npdTaskCP4.setId(NPDTaskRepository.getCurrentVal());
		npdTaskCP4.setCanstart(taskCP4Schema.getIs_active_task().equalsIgnoreCase("true") ? true : false);
		npdTaskCP4.setTask_id(Integer.parseInt(taskCP4Schema.getId()));
		npdTaskCP4.setTask_item_id(taskCP4Schema.getItem_id());
		npdTaskCP4.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
		
		NPDTaskRepository.save(npdTaskCP4);
		entityManager.flush();
		}
		
		
//		logger.info("input indexer " + taskSchemaList);
		
		Iterable<MPMTaskSchema> taskSchemaReponseList = taskSolrRepository.saveAll(taskSchemaList);
		
//		taskSchemaReponseList.forEach(ad -> {
//			logger.info("content type " + ad.getContent_type() + " : " + ad.getId() + " : " + ad.getTask_status_value());
//		});
//		
		taskSchemaList.forEach(ad -> {
			logger.info("content type 2 " + ad.getContent_type() + " : " + ad.getId()  + " : " + ad.getTask_status_value());
		});

		
		return taskSchemaList;
		
	}


}
