package com.monster.npd.migration.model;

public enum PmOwner {
	Christoffer("49154"), Will("49154"), Antonios("49154"), Donald("49154"), Hans("49154"), Tiffany("49154"),
	Mike("49154"), Martin("49154"), Jenny("49154"), Ivonne("49154"), Mark("147460"), Toan("49154"), Rashad("49154"),
	ChrisH("147459"), Josh("49154"), Steven("49154"), Doug("49154"), Ed("49154"), Kammie("49154"), Peichi("49154"),
	Leila("49154"), Marcus("49154"), Emmanuel("49154"), Carly("49154"), Lamiss("49154"), Shilpi("49154"),
	Regional("49154"), Niamh("147462"), Albert("49154"), Susan("20"), Ben("49154"), Alice("49154"), Christine("49154"),
	Corp("49154"), CorpWest("49154"),

	Fatima("49154"), Payal("49154"), Charlie("49154"), Natalie("49154"), Dom("147461"), DomSmith("49154"),
	DominicSmith("49154"), Silva("49154"), alex("49154"), HaydenRawson("49154"), PaulGoolde("49154"),
	DanFratila("49154"), VladislavSivkov("49154"), Louise("49154"), Damianos("49154"), Sonay("49154"),
	ChrisWallace("49154"), ANTON("49154"), OmarAlsamhoury("49154"), AntonioClementi("49154"), Azamat("49154"),
	AzamatKabdrakhman("49154"), ChristofGemke("49154"), Christoff("49154"), DimitrisSmailosLouise("49154"),
	SergioBravo("49154"), NeilThompson("49154"), Joerg("49154"), TomGroothuijse("49154"), Anton("49154"), Alex("49154"),
	Mirek("49154"), SaraFernandez("49154"), Juan("49154"), PatrickWiklund("49154"), Thomas("49154"), Shumyl("49154"),
	HamadSherazi("49154"), Patrick("15"), Csaba("49154"), ErOLtiHA("49154"), Jorg("49154"), Sara("49154"),
	Dimitris("49154"), louise("49154"), Fred("49154"), Hayden("49154"), Hisham("49154"), Ivan("49154"), Jorge("49154"),
	JuanAbajo("49154"), Julie("49154"), Juiia("49154"), Luc("49154"), Matt("49154"), MattHoward("49154"),
	Mutlu("49154"), Neil("49154"), Niels("49154"), Paul("14"), PaulGoold("49154"), Philippe("49154"), Roman("49154"),
	Rusian("49154"), SaraFernadez("49154"), SonayNustekin("49154"), Tatjana("49154"), Tom("49154"),
	TomGroothijuse("49154"), Tony("49154"), Uche("49154"), Ushmi("49154"), Cartrice("49154"), CorpEast("49154"),
	Harpreet("49154"), Justyna("49154"), Kari("49154"), Komal("49154"), Naz("49154"), Peta("49154"), SharedCU("49154"),
	TBD("49154"), USKari("49154"), USMarilyn("49154"), USPS("49154"), Bre("49154"), Britney("49154"), Jade("49154"),
	KOREY("49154"), Patricia("49154"), RJ("49154"), Robert("49154"), wo("49154"), CorpGFG("49154"), Damian("49154"),
	Hilda("49154"), Justine("49154"), Samantha("49154"), Thu("49154"),

	;

	public final String id;

	public String getId() {
		return id;
	}

	PmOwner(String id) {
		this.id = id;
	}
}
