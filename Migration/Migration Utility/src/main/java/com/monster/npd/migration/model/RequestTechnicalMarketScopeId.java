package com.monster.npd.migration.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestTechnicalMarketScopeId implements Serializable {
	private Integer requestTech;
    private Integer technicalMarketScope;
    
    public int hashCode() {
    	  return (int)(requestTech + technicalMarketScope);
    	 }

    	 public boolean equals(Object object) {
    	  if (object instanceof RequestTechnicalMarketScopeId) {
    		  RequestTechnicalMarketScopeId otherId = (RequestTechnicalMarketScopeId) object;
    	   return (otherId.requestTech == this.requestTech) && (otherId.technicalMarketScope == this.technicalMarketScope);
    	  }
    	  return false;
    	 }
}
