package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.PrimaryPackaging;

@Repository
public interface PrimaryPackagingTypeRepository extends JpaRepository<PrimaryPackaging, Integer> {
	
	PrimaryPackaging findByDisplayName(String name);

}