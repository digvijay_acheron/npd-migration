package com.monster.npd.migration.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@Table(name = "shrdnpdsubmissioncommercial_market_scope")
@Entity
public class CommercialMarketScope  {

	  	@Id
	  	@GeneratedValue(generator = "comm_market_seq", strategy = GenerationType.SEQUENCE)
		@SequenceGenerator(name = "comm_market_seq", sequenceName = "comm_market_seq",allocationSize=1)
	    @Column(name = "Id")
	    private Integer id;

	    @Column(name = "leadmarket")
	    private Boolean leadMarket;
	    
	    @Column(name = "targetdpinwarehouse")
	    private String targetDPInWarehouse;

	    
	    @Column(name = "three_month_launchvolume")
	    private String threeMonthLaunchVolume;
	    
	    @Column(name = "annualisedyear1volume")
	    private String annualisedYear1Volume;
	    
	    @Column(name = "nsvcase")
	    private String NSVCase;
	       
	    @OneToOne
	    @JoinColumn(name = "r_po_markets_id", referencedColumnName = "Id")
	    private Market market;
	    
//	    @ManyToMany(mappedBy = "commericialMarketScope", fetch = FetchType.LAZY)
//	    private Set<Request> requests = new HashSet<Request>();
	    
	    @Column(name = "s_organizationid")
	    private Integer organizationid; 
	    
	    @OneToMany(mappedBy = "commercialMarketScope", cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	    private Set<RequestCommercialMarketScope> requestCommercialMarketScope = new HashSet<RequestCommercialMarketScope>();
}
