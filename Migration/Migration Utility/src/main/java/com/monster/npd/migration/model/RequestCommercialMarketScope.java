package com.monster.npd.migration.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "shrdnpdsubmissionrequestcommercial_market_scope")
@Entity
@IdClass(RequestCommercialMarketScopeId.class) 
public class RequestCommercialMarketScope {
	
	
	@Id
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RequestidBD344B9E7CD35447")
    private Request request;

	 @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "commercial_market_scope_id")
    private CommercialMarketScope commercialMarketScope;

    
    @Column(name = "s_organizationid")
    private int organizationid;

}
