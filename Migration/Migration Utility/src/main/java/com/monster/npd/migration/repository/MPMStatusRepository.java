package com.monster.npd.migration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.MPMStatus;

@Repository
public interface MPMStatusRepository extends JpaRepository<MPMStatus, Integer> {
  
    List<MPMStatus> findByOrganizationid(int organizationid);
}