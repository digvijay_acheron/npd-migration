package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.monster.npd.migration.model.MPMCategoryLevel;

public interface MPMCategoryLevelRepository extends JpaRepository<MPMCategoryLevel, Integer>{

}
