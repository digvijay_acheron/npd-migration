package com.monster.npd.migration.repository;

import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.MPMProjectSchema;

@Repository
public interface ProjectSolrRepository extends SolrCrudRepository<MPMProjectSchema, String> {

}
