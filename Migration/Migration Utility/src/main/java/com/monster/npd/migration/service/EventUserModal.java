package com.monster.npd.migration.service;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.util.XMLHelper;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import com.monster.npd.migration.model.ProjectDetails;
import com.monster.npd.migration.model.RequestData;
import com.monster.npd.migration.model.TaskDetails;

/**
 * 
 * @author JeevaR
 *
 */
@Service
public class EventUserModal {

	private static final Logger logger = LogManager.getLogger(EventUserModal.class);
	private static List<RequestData> requestList = new ArrayList<>();
	private static List<TaskDetails> tasksList = new ArrayList<>();
	private static List<ProjectDetails> projectDetailsList = new ArrayList<>();
	
	@Autowired
	static
	UtilService util;

	static int count = 1;
	@Autowired
	MigrationUtilityService migrationUtilityService;

//	private static final Logger logger = LogManager.getLogger(EventUserModal.class);

	/**
	 * method to return static field requestList which has all the request data
	 * except task details
	 * 
	 * @return requestList which has all the excel sheet data
	 */
	public static List<RequestData> getRequestList() {
		return requestList;
	}

	/**
	 * method to return static field tasksList which has all the task details
	 * 
	 * @return the tasksList which has all the task details
	 */
	public static List<TaskDetails> getTasksList() {
		return tasksList;
	}

	/**
	 * method to return static field projectDetailsList which has all the project
	 * details
	 * 
	 * @return the projectDetailsList which has all the project details
	 */
	public static List<ProjectDetails> getProjectDetailsList() {
		return projectDetailsList;
	}

	/**
	 * method to process to all the sheets in the excel
	 * 
	 * @param filename - path of the excel sheet
	 * @throws Exception
	 */
	public void processAllSheets(String filename, String samlart) throws Exception {

		OPCPackage pkg = OPCPackage.open(filename);

		XSSFReader r = new XSSFReader(pkg);

		SharedStringsTable sst = r.getSharedStringsTable();

		XMLReader parser = fetchSheetParser(sst, samlart);

		Iterator<InputStream> sheets = r.getSheetsData();
		while (sheets.hasNext()) {
			logger.info("Processing new sheet:\n");
			InputStream sheet = sheets.next();
			InputSource sheetSource = new InputSource(sheet);
			parser.parse(sheetSource);
			sheet.close();
			System.out.println("");
		}

	}

	/**
	 * method to fetch sheet
	 * 
	 * @param sst
	 * @return parsed sheet
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	public XMLReader fetchSheetParser(SharedStringsTable sst, String samlart)
			throws SAXException, ParserConfigurationException {
		XMLReader parser = XMLHelper.newXMLReader();
		ContentHandler handler = new SheetHandler(sst, migrationUtilityService, samlart);
		parser.setContentHandler(handler);
		return parser;
	}

	/**
	 * See org.xml.sax.helpers.DefaultHandler javadocs
	 */

	private static class SheetHandler extends DefaultHandler {

		private SharedStringsTable sst;
		private String lastContents;
		private boolean nextIsString;
		private String attributeValue;

		RequestData data = null;
		TaskDetails taskDetail = null;
		ProjectDetails projectDetail = null;
//		List<String> cellList = new ArrayList<>(List.of("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "L", "M", "N",
//				"O", "R", "U", "V", "AT", "AU", "BM", "BS", "BT", "BU", "BV", "BY", "BZ", "CA", "CD", "CJ", "CK", "CL",
//				"CM", "CP", "CQ", "CR", "CU", "DA", "DB", "DC", "DD", "DG", "DH", "DI", "DL", "DN", "DS", "DT", "DU",
//				"DV", "DY", "DZ", "EA", "ED", "EF", "EG", "EH", "EL", "EM", "EN", "EO", "ER", "ES", "ET", "EW", "FC",
//				"FD", "FE", "FF", "FI", "FJ", "FK", "FN", "GB", "GC", "GD", "GE", "GH", "GI", "GJ", "GM", "GS", "GT",
//				"GU", "GV", "GY", "GZ", "HA", "HD", "HJ", "HK", "HL", "HM", "HN", "HQ", "HR", "HS", "HV", "IB", "IC",
//				"ID", "IE", "IH", "II", "IJ", "IM", "IS", "IT", "IU", "IV", "IY", "IZ", "JA", "JD", "JF", "JG", "JH",
//				"JM", "JN", "JO", "JP", "JS", "JT", "JU", "JX", "KD", "KE", "KF", "KG", "KJ", "KK", "KL", "KO", "KU",
//				"KV", "KW", "KX", "LA", "LB", "LC", "LF", "LL", "LM", "LN", "LO", "LR", "LS", "LT", "LW", "MC", "MD",
//				"ME", "MF", "MI", "MJ", "MK", "MN", "MT", "MU", "MV", "MW", "MZ", "NA", "NB", "NE", "NK", "NL", "NM",
//				"NN", "NQ", "NR", "NS", "NV", "OB", "OC", "OD", "OE", "OH", "OI", "OJ", "OM", "OU", "OV", "OX", "OY",
//				"PC", "PD", "PE", "PG", "PM", "PN", "PO", "PP", "PS", "PT", "PU", "PY", "QE", "QF", "QG", "QH", "QK",
//				"QL", "QM", "QQ", "QW", "QX", "QY", "QZ", "RC", "RD", "RE", "RH", "RN", "RO", "RP", "RQ", "RT", "RU",
//				"RV", "RZ", "SF", "SG", "SH", "SI", "SL", "SM", "SN", "SQ", "SW", "SX", "SZ", "TA", "TD", "TE", "TF",
//				"TI", "TL", "TM", "TP", "TY", "TZ", "UA", "UB", "UE", "UF", "UG", "UJ", "UP", "UQ", "US", "UV", "UY",
//				"UZ", "VC", "VI", "VJ", "VK", "VL", "VO", "VP", "VQ", "VU", "WA", "WB", "WC", "WD", "WG", "WH", "WI",
//				"WL", "WR", "WS", "WT", "WU", "WX", "WY", "WZ", "XC", "XI", "XJ", "XK", "XL", "XO", "XP", "XQ", "XT",
//				"YC", "YD", "YE", "YF", "YI", "YJ", "YK", "YN", "YT", "YU", "YV", "YW", "YZ", "ZA", "ZB", "ZE", "ZK",
//				"ZL", "ZM", "ZN", "ZQ", "ZR", "ZS", "ZV", "AAB", "AAC", "AAD", "AAE", "AAH", "AAI", "AAJ", "AAM", "AAS",
//				"AAT", "AAU", "AAV", "AAY", "AAZ", "ABA", "ABD", "ABJ", "ABK", "ABM", "ABN", "ABQ", "ABR", "ABS", "ABV",
//				"ACE", "ACF", "ACG", "ACI", "ACN", "ACO", "ACR", "ACY", "ADE", "ADF", "ADG", "ADH", "ADI", "ADJ", "ADK",
//				"ADL", "AEF", "AEG", "AEH", "AEI", "AEJ", "AEK",
//
//				"AES", "ALE", "AMR", "AOB", "AOX", "AOY", "AOZ", "APA", "APB"));

		/**
		 * constructor of sheet handler
		 * 
		 * @param migrationUtilityService
		 */
		private SheetHandler(SharedStringsTable sst, MigrationUtilityService migrationUtilityService, String samlart) {
			this.sst = sst;
		}

		/**
		 * method to find the cell reference
		 * 
		 * @param uri
		 * @param localName
		 * @param name
		 * @param attributes
		 */
		public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
			// c => cell
			if (name.equals("c")) {
				// Print the cell reference
				attributeValue = attributes.getValue("r");
				// System.out.print(attributes.getValue("r")+"***" + " - ");
				// Figure out if the value is an index in the SST
				String cellType = attributes.getValue("t");
				if (cellType != null && cellType.equals("s")) {
					nextIsString = true;
				} else {
					nextIsString = false;

				}
			}
			// Clear contents cache
			lastContents = "";
		}

		/**
		 * method to get the cell content and set it to RequestData object field
		 * 
		 * @param uri
		 * @param localName
		 * @param name
		 */
		public void endElement(String uri, String localName, String name) throws SAXException {
			// Process the last contents as required.
			// Do now, as characters() may be called more than once
			if (nextIsString) {
				int idx = Integer.parseInt(lastContents);
				lastContents = sst.getItemAt(idx).getString();
				nextIsString = false;
			}
			// v => contents of a cell
			// Output after we've seen the string contents
			if (name.equals("v")) {
				if (isCellExist(attributeValue, "A")) {
					data = new RequestData();
					taskDetail = new TaskDetails();
					projectDetail = new ProjectDetails();
					data.setReferenceId(lastContents);
					data.setRequestID(lastContents);

				} else if (isCellExist(attributeValue, "B")

				) {
					if (isNotValidValue()) {
						data.setProjectName("");
						data.setLeadMarketId("");
					} else {
						data.setProjectName(lastContents);
						data.setLeadMarketId(lastContents);
					}

				} else if (isCellExist(attributeValue, "C")

				) {
					if (isNotValidValue()) {
						data.setOtherLinkedMarketsId("");
					} else
						data.setOtherLinkedMarketsId(lastContents);

				} else if (isCellExist(attributeValue, "D")

				) {
					if (isNotValidValue()) {
						data.setR_PO_BUSINESS_UNIT("");

					} else
						data.setR_PO_BUSINESS_UNIT(lastContents);

				} else if (isCellExist(attributeValue, "E")

				) {
					if (isNotValidValue()) {
						data.setR_PO_BRANDS("");
					} else
						data.setR_PO_BRANDS(lastContents);
				}

				else if (isCellExist(attributeValue, "F")

				) {
					if (isNotValidValue()) {
						data.setR_PO_PLATFORMS("");
						data.setProjectName(data.getProjectName() + " " + "");
					} else {
						data.setR_PO_PLATFORMS(lastContents);
						data.setProjectName(data.getProjectName() + " " + lastContents);
					}

				} else if (isCellExist(attributeValue, "G")

				) {
					if (isNotValidValue()) {
						data.setProjectName(data.getProjectName() + " " + "");
						data.setR_PO_VARIANT_SKU("");
					} else {
						data.setProjectName(data.getProjectName() + " " + lastContents);

						data.setR_PO_VARIANT_SKU(lastContents);
					}
				} else if (isCellExist(attributeValue, "H")

				) {
					if (isNotValidValue()) {
						data.setR_PO_CONSUMER_UNIT_SIZE("");

					} else {
						data.setR_PO_CONSUMER_UNIT_SIZE(lastContents);
					}

				}

				else if (isCellExist(attributeValue, "I")

				) {
					if (isNotValidValue()) {
						data.setR_PO_CASE_PACK_SIZE("");
					} else
						data.setR_PO_CASE_PACK_SIZE(lastContents);

				}

				else if (isCellExist(attributeValue, "J")

				) {
					if (isNotValidValue()) {
						data.setR_PO_PACKAGING_TYPE("");
					} else
						data.setR_PO_PACKAGING_TYPE(lastContents);

				} else if (isCellExist(attributeValue, "L")

				) {
					if (isNotValidValue()) {
						data.setR_PO_BOTTLER("");
					} else
						data.setR_PO_BOTTLER(lastContents);

				} else if (isCellExist(attributeValue, "M")

				) {
					if (isNotValidValue()) {
						data.setRequestStatus("");
					} else
						data.setRequestStatus(lastContents);

				} else if (isCellExist(attributeValue, "O")

				) {
					if (isNotValidValue()) {
						data.setR_PO_PROJECT_CLASSIFICATION("");
					} else
						data.setR_PO_PROJECT_CLASSIFICATION(lastContents);

				} else if (isCellExist(attributeValue, "P")) { /**/
					if (isNotValidValue()) {
						projectDetail.setFGOrConcModel("");
					} else {
						projectDetail.setFGOrConcModel(lastContents.equalsIgnoreCase("FG") ? "true" : "false");
					}
						

				} else if (isCellExist(attributeValue, "R")

				) {
					if (isNotValidValue()) {
						data.setCP0DecisionDate("");
						data.setRequestedDate("");
						data.setGovMilestoneCP0DecisionDate("");

					} else {
						data.setCP0DecisionDate(UtilService.getValidDate(lastContents));
			
						data.setRequestedDate(UtilService.getValidDate(lastContents));
						data.setGovMilestoneCP0DecisionDate(UtilService.getValidDate(lastContents));
						}

				} else if (isCellExist(attributeValue, "U")

				) {
					if (isNotValidValue()) {
						data.setR_PO_E2E_PM("");
					} else
						data.setR_PO_E2E_PM(lastContents);

				} else if (isCellExist(attributeValue, "V")

				) {
					if (isNotValidValue()) {
						data.setTechnicalDescription("");
					} else {
						data.setTechnicalDescription(lastContents);
						projectDetail.setProjectDescription(lastContents);
					}

				} else if (isCellExist(attributeValue, "W")

						) {
							if (isNotValidValue()) {
								data.setNSV("");
							} else {
								data.setNSV(lastContents);
				
							}

				} 
				else if (isCellExist(attributeValue, "X")

				) {
					if (isNotValidValue()) {
						data.setVolume1Year("");
					} else {
						data.setVolume1Year(lastContents);
					}

				}

				else if (isCellExist(attributeValue, "Y")

				) {
					if (isNotValidValue()) {
						data.setVolume3Months("");
					} else {
						data.setVolume3Months(lastContents);
					}

				}

				else if (isCellExist(attributeValue, "AC")) {
					if (isNotValidValue()) {
						projectDetail.setCP1RevisedDate("");
					} else
						projectDetail.setCP1RevisedDate(lastContents);

				} else if (isCellExist(attributeValue, "AD")) {
					if (isNotValidValue()) {
						projectDetail.setCP1RevisedDate("");
					} else {
						String weekValue = projectDetail.getCP1RevisedDate() + "/" + lastContents;
						if(weekValue.equals("/0") || weekValue.equals("52/1899") ||  weekValue.equals("0/0")) {
							projectDetail.setCP1RevisedDate("");
						}else {
							projectDetail.setCP1RevisedDate(weekValue);
						}
						
					}
						

				}

				else if (isCellExist(attributeValue, "AE")) {
					if (isNotValidValue()) {
						projectDetail.setTimelineRisk("");
					} else
						projectDetail.setTimelineRisk(lastContents);

				} else if (isCellExist(attributeValue, "AI")) {
					if (isNotValidValue()) {
						projectDetail.setTechOpsAgreedLatestView(" ");
					} else
						projectDetail.setTechOpsAgreedLatestView(lastContents);

				} else if (isCellExist(attributeValue, "AJ")) {
					if (isNotValidValue()) {
						projectDetail.setTechOpsAgreedLatestView(" ");
					} else
						projectDetail.setTechOpsAgreedLatestView(
								projectDetail.getTechOpsAgreedLatestView() + "/" + lastContents);

				} else if (isCellExist(attributeValue, "AL")) {
					if (isNotValidValue() || lastContents.equalsIgnoreCase("TBC")) {
						projectDetail.setComTechOpsAlignedDate(" ");
					} else
						projectDetail.setComTechOpsAlignedDate(lastContents);

				} else if (isCellExist(attributeValue, "AM")) {
					if (isNotValidValue()) {
						projectDetail.setComTechOpsAlignedDate(" ");
					} else {
					
						if(projectDetail.getComTechOpsAlignedDate().equalsIgnoreCase(" ") ) {
							projectDetail.setComTechOpsAlignedDate(" ");
						}else {
							projectDetail.setComTechOpsAlignedDate(projectDetail.getComTechOpsAlignedDate() + "/" + lastContents);
						}
					}
						

				}

				else if (isCellExist(attributeValue, "AO")) {
					if (isNotValidValue()) {
						projectDetail.setRiskSummary("");
					} else
						projectDetail.setRiskSummary(lastContents);
					
				} else if (isCellExist(attributeValue, "AP")) { 
					if (isNotValidValue()) {
						projectDetail.setStatusAndCriticalItems("");
					} else
						projectDetail.setStatusAndCriticalItems(lastContents);

				}

				else if (isCellExist(attributeValue, "AT")

				) {
					if (isNotValidValue()) {
						data.setR_PO_CORP_PM("");
					} else
						data.setR_PO_CORP_PM(lastContents);

				} else if (isCellExist(attributeValue, "AU")

				) {
					if (isNotValidValue()) {
						data.setR_PO_OPS_PM("");
					} else
						data.setR_PO_OPS_PM(lastContents);

				} else if (isCellExist(attributeValue, "AW")

				) {
					if (isNotValidValue()) {
						projectDetail.setRegionTechManager("");
					} else
						projectDetail.setRegionTechManager(lastContents);

				} else if (isCellExist(attributeValue, "AX")) {
					if (isNotValidValue()) {
						projectDetail.setCommercialLead("");
					} else
						projectDetail.setCommercialLead(lastContents);

				}

				else if (isCellExist(attributeValue, "AY")) {/**/
					if (isNotValidValue()) {
						projectDetail.setRegulatoryLead("");
					} else
						projectDetail.setRegulatoryLead(lastContents);

				} else if (isCellExist(attributeValue, "AZ")

						) {
							if (isNotValidValue()) {
								data.setGovMilestoneCP0TargetStartDate("");

							} else {
								String date = UtilService.getNextMonday(lastContents);
								data.setGovMilestoneCP0TargetStartDate(date);

							}

				} else if (isCellExist(attributeValue, "BA")

						) {
							if (isNotValidValue()) {
								data.setGovMilestoneCP1TargetStartDate("");

							} else {
								String date = UtilService.getNextMonday(lastContents);
								data.setGovMilestoneCP1TargetStartDate(date);

							}

				} else if (isCellExist(attributeValue, "BB")

						) {
							if (isNotValidValue()) {
								data.setGovMilestoneCP2TargetStartDate("");

							} else {
								String date = UtilService.getNextMonday(lastContents);
								data.setGovMilestoneCP2TargetStartDate(date);

							}

				} else if (isCellExist(attributeValue, "BC")

						) {
							if (isNotValidValue()) {
								data.setGovMilestoneCP3TargetStartDate("");

							} else {
								String date = UtilService.getNextMonday(lastContents);
								data.setGovMilestoneCP3TargetStartDate(date);

							}

				} else if (isCellExist(attributeValue, "BD")

						) {
							if (isNotValidValue()) {
								data.setGovMilestoneCP4TargetStartDate("");

							} else {
								String date = UtilService.getNextMonday(lastContents);
								data.setGovMilestoneCP4TargetStartDate(date);

							}

				}
				
				
				else if (isCellExist(attributeValue, "BM")

				) {
					if (isNotValidValue()) {
						data.setR_PO_SKU_DETAILS(lastContents);
					} else
						data.setR_PO_SKU_DETAILS(lastContents);
				}

				// task 2

				else if (isCellExist(attributeValue, "BS")

				) {

					if (isNotValidValue()) {
						taskDetail.setTask2IsCompleted("");
					} else
						taskDetail.setTask2IsCompleted(lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "BT")

				) {

					if (isNotValidValue()) {
						taskDetail.setTask2IsActive("");

					} else {
						taskDetail.setTask2IsActive(lastContents.equals("1") ? "true" : "false");
					}
				} else if (isCellExist(attributeValue, "BU")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask2Duration("0");

					} else {
						taskDetail.setTask2Duration(lastContents);
					}

				} else if (isCellExist(attributeValue, "BV")

				) {
					if (isNotValidValue() || taskDetail.getTask2Duration().equals("0")) {
						taskDetail.setTask2StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask2StartDate(date);

					}

				} else if (isCellExist(attributeValue, "BY")

				) {
					if (isNotValidValue() || taskDetail.getTask2Duration().equals("0")) {
						taskDetail.setTask2ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask2ActualStartDate(date);
					}

				} else if (isCellExist(attributeValue, "BZ") && !attributeValue.startsWith("BZ1")

				) {
					if (isNotValidValue() || taskDetail.getTask2Duration().equals("0")) {
						taskDetail.setTask2Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask2Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask2Duration(duration.toString());
					}

				} else if (isCellExist(attributeValue, "CA")

				) {
					if (isNotValidValue() || taskDetail.getTask2Duration().equals("0")) {
						taskDetail.setTask2DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask2DueDate(date);
					}

				} else if (isCellExist(attributeValue, "CD")

				) {
					if (isNotValidValue() || taskDetail.getTask2Duration().equals("0")) {
						taskDetail.setTask2ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask2ActualDueDate(date);
					}
				}

				// task 3
				else if (isCellExist(attributeValue, "CJ")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask3IsCompleted("");

					} else {
						taskDetail.setTask3IsCompleted(lastContents.equals("1") ? "true" : "false");
					}
				} else if (isCellExist(attributeValue, "CK")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask3IsActive("");

					} else {
						taskDetail.setTask3IsActive(lastContents.equals("1") ? "true" : "false");
					}
				} else if (isCellExist(attributeValue, "CL")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask3Duration("0");

					} else {
						taskDetail.setTask3Duration(lastContents);
					}
				} else if (isCellExist(attributeValue, "CM")

				) {
					if (isNotValidValue() || taskDetail.getTask3Duration().equals("0")) {
						taskDetail.setTask3StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask3StartDate(date);
					}
				} else if (isCellExist(attributeValue, "CP")

				) {
					if (isNotValidValue() || taskDetail.getTask3Duration().equals("0")) {
						taskDetail.setTask3ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask3ActualStartDate(date);
					}
				} else if (isCellExist(attributeValue, "CQ") && !attributeValue.startsWith("CQ1")

				) {
					if (isNotValidValue() || taskDetail.getTask3Duration().equals("0")) {
						taskDetail.setTask3Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask3Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask3Duration(duration.toString());
					}
				} else if (isCellExist(attributeValue, "CR")

				) {
					if (isNotValidValue() || taskDetail.getTask3Duration().equals("0")) {
						taskDetail.setTask3DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask3DueDate(date);
					}
				} else if (isCellExist(attributeValue, "CU")

				) {
					if (isNotValidValue() || taskDetail.getTask3Duration().equals("0")) {
						taskDetail.setTask3ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);

						taskDetail.setTask3ActualDueDate(date);
					}
				}

				// task 4
				else if (isCellExist(attributeValue, "DA")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask4IsCompleted("");

					} else
						taskDetail.setTask4IsCompleted(lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "DB")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask4IsActive("");

					} else
						taskDetail.setTask4IsActive(lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "DC")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask4Duration("0");

					} else
						taskDetail.setTask4Duration(lastContents);
				} else if (isCellExist(attributeValue, "DD")

				) {
					if (isNotValidValue() || taskDetail.getTask4Duration().equals("0")) {
						taskDetail.setTask4StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask4StartDate(date);
					}
				} else if (isCellExist(attributeValue, "DG")

				) {
					if (isNotValidValue() || taskDetail.getTask4Duration().equals("0")) {
						taskDetail.setTask4ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask4ActualStartDate(date);
					}
				} else if (isCellExist(attributeValue, "DH")

				) {
					if (isNotValidValue() || taskDetail.getTask4Duration().equals("0")) {
						taskDetail.setTask4Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask4Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask4Duration(duration.toString());
					}
				} else if (isCellExist(attributeValue, "DI")

				) {
					if (isNotValidValue() || taskDetail.getTask4Duration().equals("0")) {
						taskDetail.setTask4DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask4DueDate(date);
					}
				} else if (isCellExist(attributeValue, "DL")

				) {
					if (isNotValidValue() || taskDetail.getTask4Duration().equals("0")) {
						taskDetail.setTask4ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask4ActualDueDate(date);
					}
				}

				else if (isCellExist(attributeValue, "DM")) {
					if (isNotValidValue()) {
						projectDetail.setFinalProjectClassification("");
						taskDetail.setTask3FinalClassification("");
					} else {
						if (lastContents.contains("Awaiting")) {
							projectDetail.setFinalProjectClassification("");
							taskDetail.setTask3FinalClassification("");
						} else {
							projectDetail.setFinalProjectClassification(lastContents);
							taskDetail.setTask3FinalClassification(lastContents);
						}
					}

				}

				// task 5
				else if (isCellExist(attributeValue, "DS")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask5IsCompleted("");

					} else
						taskDetail.setTask5IsCompleted(lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "DT")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask5IsActive("");

					} else
						taskDetail.setTask5IsActive(lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "DU")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask5Duration("0");

					} else
						taskDetail.setTask5Duration(lastContents);
				} else if (isCellExist(attributeValue, "DV")

				) {
					if (isNotValidValue() || taskDetail.getTask5Duration().equals("0")) {
						taskDetail.setTask5StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask5StartDate(date);
					}
				} else if (isCellExist(attributeValue, "DY")

				) {
					if (isNotValidValue() || taskDetail.getTask5Duration().equals("0")) {
						taskDetail.setTask5ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask5ActualStartDate(date);
					}
				} else if (isCellExist(attributeValue, "DZ") && !attributeValue.startsWith("DZ1")

				) {
					if (isNotValidValue() || taskDetail.getTask5Duration().equals("0")) {
						taskDetail.setTask5Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask5Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask5Duration(duration.toString());
					}
				} else if (isCellExist(attributeValue, "EA")

				) {
					if (isNotValidValue() || taskDetail.getTask5Duration().equals("0")) {
						taskDetail.setTask5DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask5DueDate(date);
					}
				} else if (isCellExist(attributeValue, "ED")

				) {
					if (isNotValidValue() || taskDetail.getTask5Duration().equals("0")) {
						taskDetail.setTask5ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask5ActualDueDate(date);
					}
				}

				else if (isCellExist(attributeValue, "EF")

				) {
					if (isNotValidValue()) {
						data.setR_PO_EARLY_MANUFACTURING_SITE("");
						projectDetail.setFinalManufacturingSite("");

					} else {
						data.setR_PO_EARLY_MANUFACTURING_SITE(lastContents);
						projectDetail.setFinalManufacturingSite(lastContents);
					}

				}

				else if (isCellExist(attributeValue, "EG")

				) {
					if (isNotValidValue()) {
						data.setR_PO_DRAFT_MANUFACTURING_LOCATION("");
						taskDetail.setTask3FinalCountryOfManufacture("");
						projectDetail.setFinalCountryOfManufacture("");

					} else {
						data.setR_PO_DRAFT_MANUFACTURING_LOCATION(lastContents);
						taskDetail.setTask3FinalCountryOfManufacture(lastContents);
						projectDetail.setFinalCountryOfManufacture(lastContents);

					}
				} else if (isCellExist(attributeValue, "EH")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask3FinalSite(" ");

					} else
						taskDetail.setTask3FinalSite(lastContents);

				}

				// task 6
				else if (isCellExist(attributeValue, "EL")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask6IsCompleted("");

					} else
						taskDetail.setTask6IsCompleted(lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "EM")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask6IsActive("");

					} else
						taskDetail.setTask6IsActive(lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "EN")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask6Duration("0");

					} else
						taskDetail.setTask6Duration(lastContents);
				} else if (isCellExist(attributeValue, "EO")

				) {
					if (isNotValidValue() || taskDetail.getTask6Duration().equals("0")) {
						taskDetail.setTask6StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask6StartDate(date);
					}
				} else if (isCellExist(attributeValue, "ER")

				) {
					if (isNotValidValue() || taskDetail.getTask6Duration().equals("0")) {
						taskDetail.setTask6ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask6ActualStartDate(date);
					}
				} else if (isCellExist(attributeValue, "ES") && !attributeValue.startsWith("ES1")

				) {
					if (isNotValidValue() || taskDetail.getTask6Duration().equals("0")) {
						taskDetail.setTask6Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask6Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask6Duration(duration.toString());
					}
				} else if (isCellExist(attributeValue, "ET")

				) {
					if (isNotValidValue() || taskDetail.getTask6Duration().equals("0")) {
						taskDetail.setTask6DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask6DueDate(date);
					}
				} else if (isCellExist(attributeValue, "EW")

				) {
					if (isNotValidValue() || taskDetail.getTask6Duration().equals("0")) {
						taskDetail.setTask6ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask6ActualDueDate(date);
					}
				}

				// task 7
				else if (isCellExist(attributeValue, "FC")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask7IsCompleted("");

					} else
						taskDetail.setTask7IsCompleted(lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "FD")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask7IsActive("");

					} else
						taskDetail.setTask7IsActive(lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "FE")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask7Duration("0");

					} else
						taskDetail.setTask7Duration(lastContents);
				} else if (isCellExist(attributeValue, "FF")

				) {
					if (isNotValidValue() || taskDetail.getTask7Duration().equals("0")) {
						taskDetail.setTask7StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask7StartDate(date);
					}
				} else if (isCellExist(attributeValue, "FI")

				) {
					if (isNotValidValue() || taskDetail.getTask7Duration().equals("0")) {
						taskDetail.setTask7ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask7ActualStartDate(date);
					}
				} else if (isCellExist(attributeValue, "FJ")) {
					if (isNotValidValue() || taskDetail.getTask7Duration().equals("0")) {
						taskDetail.setTask7Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask7Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask7Duration(duration.toString());
					}
				} else if (isCellExist(attributeValue, "FK")

				) {
					if (isNotValidValue() || taskDetail.getTask7Duration().equals("0")) {
						taskDetail.setTask7DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask7DueDate(date);
					}
				} else if (isCellExist(attributeValue, "FN")

				) {
					if (isNotValidValue() || taskDetail.getTask7Duration().equals("0")) {
						taskDetail.setTask7ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask7ActualDueDate(date);
					}
					
					
				} else if (isCellExist(attributeValue, "FQ")

						) {
							if (isNotValidValue()) {
								projectDetail.setTrialOrFirstProductionIncludedInThePlan("");

							} else {
								
								projectDetail.setTrialOrFirstProductionIncludedInThePlan(lastContents.equals("Y") ||
										  lastContents.equals("1") ? "true" : "false");
							}
							
							
				} else if (isCellExist(attributeValue, "FR")) {
					if (isNotValidValue()) {
						projectDetail.setProgramManagerSlackTime("");
					} else
						projectDetail.setProgramManagerSlackTime(lastContents);

				} else if (isCellExist(attributeValue, "FS")) {
					if (isNotValidValue()) {
						projectDetail.setProgramManagerCP1PhaseDelay("");
					} else
						projectDetail.setProgramManagerCP1PhaseDelay(lastContents);

				} else if (isCellExist(attributeValue, "FV")

						) {
							if (isNotValidValue()) {
								data.setGovMilestoneCP1DecisionDate("");

							} else {
								String date = UtilService.getNextSunday(lastContents);
								data.setGovMilestoneCP1DecisionDate(date);

							}

				}

				// task 9
				else if (isCellExist(attributeValue, "GB")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask9IsCompleted("");

					} else
						taskDetail.setTask9IsCompleted(lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "GC")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask9IsActive("");

					} else
						taskDetail.setTask9IsActive(lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "GD")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask9Duration("0");

					} else
						taskDetail.setTask9Duration(lastContents);
				} else if (isCellExist(attributeValue, "GE")

				) {
					if (isNotValidValue() || taskDetail.getTask9Duration() == null
							|| taskDetail.getTask9Duration().equals("0")) {
						taskDetail.setTask9StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask9StartDate(date);

					}
				} else if (isCellExist(attributeValue, "GH")

				) {
					if (isNotValidValue() || taskDetail.getTask9Duration() == null
							|| taskDetail.getTask9Duration().equals("0")) {
						taskDetail.setTask9ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask9ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "GI") && !attributeValue.startsWith("GI1")

				) {
					if (isNotValidValue() || taskDetail.getTask9Duration() == null
							|| taskDetail.getTask9Duration().equals("0")) {
						taskDetail.setTask9Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask9Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask9Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "GJ")

				) {
					if (isNotValidValue() || taskDetail.getTask9Duration() == null
							|| taskDetail.getTask9Duration().equals("0")) {
						taskDetail.setTask9DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask9DueDate(date);

					}
				} else if (isCellExist(attributeValue, "GM")

				) {
					if (isNotValidValue() || taskDetail.getTask9Duration() == null
							|| taskDetail.getTask9Duration().equals("0")) {
						taskDetail.setTask9ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask9ActualDueDate(date);
					}

				}

				// task 11
				else if (isCellExist(attributeValue, "GS")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask11IsCompleted("");

					} else
						taskDetail.setTask11IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "GT")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask11IsActive("");

					} else
						taskDetail.setTask11IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "GU")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask11Duration("0");

					} else
						taskDetail.setTask11Duration(lastContents);

				} else if (isCellExist(attributeValue, "GV")

				) {
					if (isNotValidValue() || taskDetail.getTask11Duration().equals("0")) {
						taskDetail.setTask11StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask11StartDate(date);

					}
				} else if (isCellExist(attributeValue, "GY")

				) {
					if (isNotValidValue() || taskDetail.getTask11Duration().equals("0")) {
						taskDetail.setTask11ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask11ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "GZ") && !attributeValue.startsWith("GZ1")

				) {
					if (isNotValidValue() || taskDetail.getTask11Duration().equals("0")) {
						taskDetail.setTask11Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask11Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask11Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "HA")

				) {
					if (isNotValidValue() || taskDetail.getTask11Duration().equals("0")) {
						taskDetail.setTask11DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask11DueDate(date);

					}
				} else if (isCellExist(attributeValue, "HD")

				) {
					if (isNotValidValue() || taskDetail.getTask11Duration().equals("0")) {
						taskDetail.setTask11ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask11ActualDueDate(date);
					}

				}

				// task 10
				else if (isCellExist(attributeValue, "HJ")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask10IsCompleted("");

					} else
						taskDetail.setTask10IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "HK")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask10IsActive("");

					} else
						taskDetail.setTask10IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "HL")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask10Duration("0");

					} else
						taskDetail.setTask10Duration(lastContents);

				} else if (isCellExist(attributeValue, "HM")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask10RequiredComplete("");

					} else
						taskDetail.setTask10RequiredComplete(lastContents);

				} else if (isCellExist(attributeValue, "HN")

				) {
					if (isNotValidValue() || taskDetail.getTask10Duration().equals("0")) {
						taskDetail.setTask10StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask10StartDate(date);

					}
				} else if (isCellExist(attributeValue, "HQ")

				) {
					if (isNotValidValue() || taskDetail.getTask10Duration().equals("0")) {
						taskDetail.setTask10ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask10ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "HR")) {
					if (isNotValidValue() || taskDetail.getTask10Duration().equals("0")) {
						taskDetail.setTask10Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask10Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask10Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "HS")

				) {
					if (isNotValidValue() || taskDetail.getTask10Duration().equals("0")) {
						taskDetail.setTask10DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask10DueDate(date);

					}
				} else if (isCellExist(attributeValue, "HV")

				) {
					if (isNotValidValue() || taskDetail.getTask10Duration().equals("0")) {
						taskDetail.setTask10ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask10ActualDueDate(date);
					}

				}

				// task 12
				else if (isCellExist(attributeValue, "IB")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask12IsCompleted("");

					} else
						taskDetail.setTask12IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "IC")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask12IsActive("");

					} else
						taskDetail.setTask12IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ID")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask12Duration("0");

					} else
						taskDetail.setTask12Duration(lastContents);
				} else if (isCellExist(attributeValue, "IE")

				) {
					if (isNotValidValue() || taskDetail.getTask12Duration().equals("0")) {
						taskDetail.setTask12StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask12StartDate(date);

					}
				} else if (isCellExist(attributeValue, "IH")

				) {
					if (isNotValidValue() || taskDetail.getTask12Duration().equals("0")) {
						taskDetail.setTask12ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask12ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "II") && !attributeValue.startsWith("II1")

				) {
					if (isNotValidValue() || taskDetail.getTask12Duration().equals("0")) {
						taskDetail.setTask12Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask12Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask12Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "IJ")

				) {
					if (isNotValidValue() || taskDetail.getTask12Duration().equals("0")) {
						taskDetail.setTask12DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask12DueDate(date);

					}
				} else if (isCellExist(attributeValue, "IM")

				) {
					if (isNotValidValue() || taskDetail.getTask12Duration().equals("0")) {
						taskDetail.setTask12ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask12ActualDueDate(date);
					}

				}

				// task 13
				else if (isCellExist(attributeValue, "IS")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask13IsCompleted("");

					} else
						taskDetail.setTask13IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "IT")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask13IsActive("");

					} else
						taskDetail.setTask13IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "IU")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask13Duration("0");

					} else
						taskDetail.setTask13Duration(lastContents);

				} else if (isCellExist(attributeValue, "IV")

				) {
					if (isNotValidValue() || taskDetail.getTask13Duration().equals("0")) {
						taskDetail.setTask13StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask13StartDate(date);

					}
				} else if (isCellExist(attributeValue, "IY")

				) {
					if (isNotValidValue() || taskDetail.getTask13Duration().equals("0")) {
						taskDetail.setTask13ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask13ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "IZ") && !attributeValue.startsWith("IZ1")

				) {
					if (isNotValidValue() || taskDetail.getTask13Duration().equals("0")) {
						taskDetail.setTask13Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask13Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask13Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "JA")

				) {
					if (isNotValidValue() || taskDetail.getTask13Duration().equals("0")) {
						taskDetail.setTask13DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask13DueDate(date);

					}
				} else if (isCellExist(attributeValue, "JD")

				) {
					if (isNotValidValue() || taskDetail.getTask13Duration().equals("0")) {
						taskDetail.setTask13ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask13ActualDueDate(date);

					}
				} else if (isCellExist(attributeValue, "JF")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask13IsNewRMCosting("");

					} else
						taskDetail.setTask13IsNewRMCosting(lastContents);

				} else if (isCellExist(attributeValue, "JG")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask13IsTransactionSchemeAnalysisRequired("");

					} else
						taskDetail.setTask13IsTransactionSchemeAnalysisRequired(lastContents);

				}

				else if (isCellExist(attributeValue, "JH")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask7IsProcessAuthorityApproval("");

					} else
						taskDetail.setTask7IsProcessAuthorityApproval(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				}

				// task 15
				else if (isCellExist(attributeValue, "JM")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask15IsCompleted("");

					} else
						taskDetail.setTask15IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "JN")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask15IsActive("");

					} else
						taskDetail.setTask15IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "JO")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask15Duration("0");

					} else
						taskDetail.setTask15Duration(lastContents);

				} else if (isCellExist(attributeValue, "JP")

				) {
					if (isNotValidValue() || taskDetail.getTask15Duration().equals("0")) {
						taskDetail.setTask15StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask15StartDate(date);

					}
				} else if (isCellExist(attributeValue, "JS")

				) {
					if (isNotValidValue() || taskDetail.getTask15Duration().equals("0")) {
						taskDetail.setTask15ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask15ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "JT") && !attributeValue.startsWith("JT1")

				) {
					if (isNotValidValue() || taskDetail.getTask15Duration().equals("0")) {
						taskDetail.setTask15Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask15Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask15Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "JU")

				) {
					if (isNotValidValue() || taskDetail.getTask15Duration().equals("0")) {
						taskDetail.setTask15DueDate("");
						

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask15DueDate(date);
						

					}
				}  else if (isCellExist(attributeValue, "JX")

				) {
					if (isNotValidValue() || taskDetail.getTask15Duration().equals("0")) {
						taskDetail.setTask15ActualDueDate("");
						projectDetail.setSiteSetupCompleted("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						if(date.equals("1899-12-31T00:00:00Z")) {
							taskDetail.setTask15ActualDueDate("");
							projectDetail.setSiteSetupCompleted("");
						}else {
							taskDetail.setTask15ActualDueDate(date);
							projectDetail.setSiteSetupCompleted(date);
						}
						
					}

				}

				// task 25
				else if (isCellExist(attributeValue, "KD")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask25IsCompleted("");

					} else
						taskDetail.setTask25IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "KE")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask25IsActive("");

					} else
						taskDetail.setTask25IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "KF")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask25Duration("0");

					} else
						taskDetail.setTask25Duration(lastContents);

				} else if (isCellExist(attributeValue, "KG")

				) {
					if (isNotValidValue() || taskDetail.getTask25Duration().equals("0")) {
						taskDetail.setTask25StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask25StartDate(date);

					}
				} else if (isCellExist(attributeValue, "KJ")

				) {
					if (isNotValidValue() || taskDetail.getTask25Duration().equals("0")) {
						taskDetail.setTask25ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask25ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "KK") && !attributeValue.startsWith("KK1")

				) {
					if (isNotValidValue() || taskDetail.getTask25Duration().equals("0")) {
						taskDetail.setTask25Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask25Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask25Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "KL")

				) {
					if (isNotValidValue() || taskDetail.getTask25Duration().equals("0")) {
						taskDetail.setTask25DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask25DueDate(date);

					}
				} else if (isCellExist(attributeValue, "KO")

				) {
					if (isNotValidValue() || taskDetail.getTask25Duration().equals("0")) {
						taskDetail.setTask25ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask25ActualDueDate(date);
					}

				}

				// task 16
				else if (isCellExist(attributeValue, "KU")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask16IsCompleted("");

					} else
						taskDetail.setTask16IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "KV")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask16IsActive("");

					} else
						taskDetail.setTask16IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "KW")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask16Duration("0");

					} else
						taskDetail.setTask16Duration(lastContents);

				} else if (isCellExist(attributeValue, "KX")

				) {
					if (isNotValidValue() || taskDetail.getTask16Duration().equals("0")) {
						taskDetail.setTask16StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask16StartDate(date);

					}
				} else if (isCellExist(attributeValue, "LA")

				) {
					if (isNotValidValue() || taskDetail.getTask16Duration().equals("0")) {
						taskDetail.setTask16ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask16ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "LB") && !attributeValue.startsWith("LB1")

				) {
					if (isNotValidValue() || taskDetail.getTask16Duration().equals("0")) {
						taskDetail.setTask16Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask16Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask16Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "LC")

				) {
					if (isNotValidValue() || taskDetail.getTask16Duration().equals("0")) {
						taskDetail.setTask16DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask16DueDate(date);

					}
				} else if (isCellExist(attributeValue, "LF")

				) {
					if (isNotValidValue() || taskDetail.getTask16Duration().equals("0")) {
						taskDetail.setTask16ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask16ActualDueDate(date);
					}

				}

				// task 23
				else if (isCellExist(attributeValue, "LL")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask23IsCompleted("");

					} else
						taskDetail.setTask23IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "LM")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask23IsActive("");

					} else
						taskDetail.setTask23IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "LN")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask23Duration("0");

					} else
						taskDetail.setTask23Duration(lastContents);

				} else if (isCellExist(attributeValue, "LO")

				) {
					if (isNotValidValue() || taskDetail.getTask23Duration().equals("0")) {
						taskDetail.setTask23StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask23StartDate(date);

					}
				} else if (isCellExist(attributeValue, "LR")

				) {
					if (isNotValidValue() || taskDetail.getTask23Duration().equals("0")) {
						taskDetail.setTask23ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask23ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "LS") && !attributeValue.startsWith("LS1")

				) {
					if (isNotValidValue() || taskDetail.getTask23Duration().equals("0")) {
						taskDetail.setTask23Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask23Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask23Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "LT")

				) {
					if (isNotValidValue() || taskDetail.getTask23Duration().equals("0")) {
						taskDetail.setTask23DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask23DueDate(date);

					}
				} else if (isCellExist(attributeValue, "LW")

				) {
					if (isNotValidValue() || taskDetail.getTask23Duration().equals("0")) {
						taskDetail.setTask23ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask23ActualDueDate(date);
					}

				}

				// task 14

				else if (isCellExist(attributeValue, "MC")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask14IsCompleted("");

					} else
						taskDetail.setTask14IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "MD")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask14IsActive("");

					} else
						taskDetail.setTask14IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ME")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask14Duration("0");

					} else
						taskDetail.setTask14Duration(lastContents);

				} else if (isCellExist(attributeValue, "MF")

				) {
					if (isNotValidValue() || taskDetail.getTask14Duration().equals("0")) {
						taskDetail.setTask14StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask14StartDate(date);

					}
				} else if (isCellExist(attributeValue, "MI")

				) {
					if (isNotValidValue() || taskDetail.getTask14Duration().equals("0")) {
						taskDetail.setTask14ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask14ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "MJ") && !attributeValue.startsWith("MJ1")

				) {
					if (isNotValidValue() || taskDetail.getTask14Duration().equals("0")) {
						taskDetail.setTask14Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask14Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask14Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "MK")

				) {
					if (isNotValidValue() || taskDetail.getTask14Duration().equals("0")) {
						taskDetail.setTask14DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask14DueDate(date);

					}
				} else if (isCellExist(attributeValue, "MN")

				) {
					if (isNotValidValue() || taskDetail.getTask14Duration().equals("0")) {
						taskDetail.setTask14ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask14ActualDueDate(date);

					}
				}

				// task 27
				else if (isCellExist(attributeValue, "MT")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask27IsCompleted("");

					} else
						taskDetail.setTask27IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "MU")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask27IsActive("");

					} else
						taskDetail.setTask27IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "MV")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask27Duration("0");

					} else
						taskDetail.setTask27Duration(lastContents);

				} else if (isCellExist(attributeValue, "MW")

				) {
					if (isNotValidValue() || taskDetail.getTask27Duration().equals("0")) {
						taskDetail.setTask27StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask27StartDate(date);

					}
				} else if (isCellExist(attributeValue, "MZ")

				) {
					if (isNotValidValue() || taskDetail.getTask27Duration().equals("0")) {
						taskDetail.setTask27ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask27ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "NA") && !attributeValue.startsWith("NA1")

				) {
					if (isNotValidValue() || taskDetail.getTask27Duration().equals("0")) {
						taskDetail.setTask27Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask27Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask27Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "NB")

				) {
					if (isNotValidValue() || taskDetail.getTask27Duration().equals("0")) {
						taskDetail.setTask27DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask27DueDate(date);

					}
				} else if (isCellExist(attributeValue, "NE")

				) {
					if (isNotValidValue() || taskDetail.getTask27Duration().equals("0")) {
						taskDetail.setTask27ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask27ActualDueDate(date);

					}
				}

				// task 26
				else if (isCellExist(attributeValue, "NK")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask26IsCompleted("");

					} else
						taskDetail.setTask26IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "NL")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask26IsActive("");

					} else
						taskDetail.setTask26IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "NM")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask26Duration("0");

					} else
						taskDetail.setTask26Duration(lastContents);

				} else if (isCellExist(attributeValue, "NN")

				) {
					if (isNotValidValue() || taskDetail.getTask26Duration().equals("0")) {
						taskDetail.setTask26StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask26StartDate(date);

					}
				} else if (isCellExist(attributeValue, "NQ")

				) {
					if (isNotValidValue() || taskDetail.getTask26Duration().equals("0")) {
						taskDetail.setTask26ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask26ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "NR") && !attributeValue.startsWith("NR1")

				) {
					if (isNotValidValue() || taskDetail.getTask26Duration().equals("0")) {
						taskDetail.setTask26Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask26Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask26Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "NS")

				) {
					if (isNotValidValue() || taskDetail.getTask26Duration().equals("0")) {
						taskDetail.setTask26DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask26DueDate(date);

					}
				} else if (isCellExist(attributeValue, "NV")

				) {
					if (isNotValidValue() || taskDetail.getTask26Duration().equals("0")) {
						taskDetail.setTask26ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask26ActualDueDate(date);

					}
				}

				// task 19

				else if (isCellExist(attributeValue, "OB")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask19IsCompleted("");

					} else
						taskDetail.setTask19IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "OC")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask19IsActive("");

					} else
						taskDetail.setTask19IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "OD")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask19Duration("0");

					} else
						taskDetail.setTask19Duration(lastContents);

				} else if (isCellExist(attributeValue, "OE")

				) {
					if (isNotValidValue() || taskDetail.getTask19Duration().equals("0")) {
						taskDetail.setTask19StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask19StartDate(date);

					}
				} else if (isCellExist(attributeValue, "OH")

				) {
					if (isNotValidValue() || taskDetail.getTask19Duration().equals("0")) {
						taskDetail.setTask19ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask19ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "OI") && !attributeValue.startsWith("OI1")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask19Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask19Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask19Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "OJ")

				) {
					if (isNotValidValue() || taskDetail.getTask19Duration().equals("0")) {
						taskDetail.setTask19DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask19DueDate(date);

					}
				} else if (isCellExist(attributeValue, "OM")

				) {
					if (isNotValidValue() || taskDetail.getTask19Duration().equals("0")) {
						taskDetail.setTask19ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask19ActualDueDate(date);

					}
				}

				// task 21
				else if (isCellExist(attributeValue, "OU")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask21IsCompleted("");

					} else
						taskDetail.setTask21IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "OV")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask21IsActive("");

					} else
						taskDetail.setTask21IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "OX")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask21Duration("0");

					} else
						taskDetail.setTask21Duration(lastContents);

				} else if (isCellExist(attributeValue, "OY")

				) {
					if (isNotValidValue() || taskDetail.getTask21Duration().equals("0")) {
						taskDetail.setTask21StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask21StartDate(date);

					}
				} else if (isCellExist(attributeValue, "PB")

				) {
					if (isNotValidValue() || taskDetail.getTask21Duration().equals("0")) {
						taskDetail.setTask21ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask21ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "PC")) {
					if (isNotValidValue() || taskDetail.getTask21Duration().equals("0")) {
						taskDetail.setTask21Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask21Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask21Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "PD")

				) {
					if (isNotValidValue() || taskDetail.getTask21Duration().equals("0")) {
						taskDetail.setTask21DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask21DueDate(date);

					}
				} else if (isCellExist(attributeValue, "PG")

				) {
					if (isNotValidValue() || taskDetail.getTask21Duration().equals("0")) {
						taskDetail.setTask21ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask21ActualDueDate(date);

					}
				}

				// task 17

				else if (isCellExist(attributeValue, "PM")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask17IsCompleted("");

					} else
						taskDetail.setTask17IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "PN")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask17IsActive("");

					} else
						taskDetail.setTask17IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "PO")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask17Duration("0");

					} else
						taskDetail.setTask17Duration(lastContents);

				} else if (isCellExist(attributeValue, "PP")

				) {
					if (isNotValidValue() || taskDetail.getTask17Duration().equals("0")) {
						taskDetail.setTask17StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask17StartDate(date);

					}
				} else if (isCellExist(attributeValue, "PS")

				) {
					if (isNotValidValue() || taskDetail.getTask17Duration().equals("0")) {
						taskDetail.setTask17ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask17ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "PT") && !attributeValue.startsWith("PT1")

				) {
					if (isNotValidValue() || taskDetail.getTask17Duration().equals("0")) {
						taskDetail.setTask17Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask17Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask17Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "PU")

				) {
					if (isNotValidValue() || taskDetail.getTask17Duration().equals("0")) {
						taskDetail.setTask17DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask17DueDate(date);

					}
				} else if (isCellExist(attributeValue, "PY")

				) {
					if (isNotValidValue() || taskDetail.getTask17Duration().equals("0")) {
						taskDetail.setTask17ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask17ActualDueDate(date);

					}
				}

				// task 18

				else if (isCellExist(attributeValue, "QE")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask18IsCompleted("");

					} else
						taskDetail.setTask18IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "QF")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask18IsActive("");

					} else
						taskDetail.setTask18IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "QG")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask18Duration("0");

					} else
						taskDetail.setTask18Duration(lastContents);

				} else if (isCellExist(attributeValue, "QH")

				) {
					if (isNotValidValue() || taskDetail.getTask18Duration().equals("0")) {
						taskDetail.setTask18StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask18StartDate(date);

					}
				} else if (isCellExist(attributeValue, "QK")

				) {
					if (isNotValidValue() || taskDetail.getTask18Duration().equals("0")) {
						taskDetail.setTask18ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask18ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "QL") && !attributeValue.startsWith("QL1")

				) {
					if (isNotValidValue() || taskDetail.getTask18Duration().equals("0")) {
						taskDetail.setTask18Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask18Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask18Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "QM")

				) {
					if (isNotValidValue() || taskDetail.getTask18Duration().equals("0")) {
						taskDetail.setTask18DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask18DueDate(date);

					}
				} else if (isCellExist(attributeValue, "QQ")

				) {
					if (isNotValidValue() || taskDetail.getTask18Duration().equals("0")) {
						taskDetail.setTask18ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask18ActualDueDate(date);

					}
				}

				// task 28
				else if (isCellExist(attributeValue, "QW")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask28IsCompleted("");

					} else
						taskDetail.setTask28IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "QX")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask28IsActive("");

					} else
						taskDetail.setTask28IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "QY")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask28Duration("0");

					} else
						taskDetail.setTask28Duration(lastContents);

				} else if (isCellExist(attributeValue, "QZ")

				) {
					if (isNotValidValue() || taskDetail.getTask28Duration().equals("0")) {
						taskDetail.setTask28StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask28StartDate(date);

					}
				} else if (isCellExist(attributeValue, "RC")

				) {
					if (isNotValidValue() || taskDetail.getTask28Duration().equals("0")) {
						taskDetail.setTask28ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask28ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "RD") && !attributeValue.startsWith("RD1")

				) {
					if (isNotValidValue() || taskDetail.getTask28Duration().equals("0")) {
						taskDetail.setTask28Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask28Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask28Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "RE")

				) {
					if (isNotValidValue() || taskDetail.getTask28Duration().equals("0")) {
						taskDetail.setTask28DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask28DueDate(date);

					}
				} else if (isCellExist(attributeValue, "RH")

				) {
					if (isNotValidValue() || taskDetail.getTask28Duration().equals("0")) {
						taskDetail.setTask28ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask28ActualDueDate(date);

					}
				}

				// task 20
				else if (isCellExist(attributeValue, "RN")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask20IsCompleted("");

					} else
						taskDetail.setTask20IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "RO")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask20IsActive("");

					} else
						taskDetail.setTask20IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "RP")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask20Duration("0");

					} else
						taskDetail.setTask20Duration(lastContents);

				} else if (isCellExist(attributeValue, "RQ")

				) {
					if (isNotValidValue() || taskDetail.getTask20Duration().equals("0")) {
						taskDetail.setTask20StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask20StartDate(date);

					}
				} else if (isCellExist(attributeValue, "RT")

				) {
					if (isNotValidValue() || taskDetail.getTask20Duration().equals("0")) {
						taskDetail.setTask20ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask20ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "RU") && !attributeValue.startsWith("RU1")

				) {
					if (isNotValidValue() || taskDetail.getTask20Duration().equals("0")) {
						taskDetail.setTask20Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask20Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask20Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "RV")

				) {
					if (isNotValidValue() || taskDetail.getTask20Duration().equals("0")) {
						taskDetail.setTask20DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask20DueDate(date);

					}
				} else if (isCellExist(attributeValue, "RZ")

				) {
					if (isNotValidValue() || taskDetail.getTask20Duration().equals("0")) {
						taskDetail.setTask20ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask20ActualDueDate(date);

					}
				}

				// task 29
				else if (isCellExist(attributeValue, "SF")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask29IsCompleted("");

					} else
						taskDetail.setTask29IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "SG")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask29IsActive("");

					} else
						taskDetail.setTask29IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "SH")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask29Duration("0");

					} else
						taskDetail.setTask29Duration(lastContents);

				} else if (isCellExist(attributeValue, "SI")

				) {
					if (isNotValidValue() || taskDetail.getTask29Duration().equals("0")) {
						taskDetail.setTask29StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask29StartDate(date);

					}
				} else if (isCellExist(attributeValue, "SL")

				) {
					if (isNotValidValue() || taskDetail.getTask29Duration().equals("0")) {
						taskDetail.setTask29ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask29ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "SM") && !attributeValue.startsWith("SM1")

				) {
					if (isNotValidValue() || taskDetail.getTask29Duration().equals("0")) {
						taskDetail.setTask29Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask29Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask29Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "SN")

				) {
					if (isNotValidValue() || taskDetail.getTask29Duration().equals("0")) {
						taskDetail.setTask29DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask29DueDate(date);

					}
				} else if (isCellExist(attributeValue, "SQ")

				) {
					if (isNotValidValue() || taskDetail.getTask29Duration().equals("0")) {
						taskDetail.setTask29ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask29ActualDueDate(date);

					}
				}

				// task 22
				else if (isCellExist(attributeValue, "SW")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask22IsCompleted("");

					} else
						taskDetail.setTask22IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "SX")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask22IsActive("");

					} else
						taskDetail.setTask22IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "SZ")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask22Duration("0");

					} else
						taskDetail.setTask22Duration(lastContents);

				} else if (isCellExist(attributeValue, "TA")

				) {
					if (isNotValidValue() || taskDetail.getTask22Duration().equals("0")) {
						taskDetail.setTask22StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask22StartDate(date);

					}
				} else if (isCellExist(attributeValue, "TD")

				) {
					if (isNotValidValue() || taskDetail.getTask22Duration().equals("0")) {
						taskDetail.setTask22ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask22ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "TE") && !attributeValue.startsWith("TE1")

				) {
					if (isNotValidValue() || taskDetail.getTask22Duration().equals("0")) {
						taskDetail.setTask22Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask22Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask22Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "TF")

				) {
					if (isNotValidValue() || taskDetail.getTask22Duration().equals("0")) {
						taskDetail.setTask22DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask22DueDate(date);

					}
				} else if (isCellExist(attributeValue, "TI")

				) {
					if (isNotValidValue() || taskDetail.getTask22Duration().equals("0")) {
						taskDetail.setTask22ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask22ActualDueDate(date);

					}
				}

				// task 30
//					else if ( RN isCellExist(attributeValue,cell,"RN")
//							
//							) {
//						if (lastContents.equals("#N/A")||lastContents.equals("#VALUE!")||lastContents.equals("#############")) {
//							taskDetail.setTask30IsCompleted("");
//							
//						}
//						taskDetail.setTask30IsCompleted(lastContents.equals("1") ? "true" : "false");
//					} 

				else if (isCellExist(attributeValue, "TL")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask30IsActive("");

					} else
						taskDetail.setTask30IsActive(lastContents.equals("1") ? "true" : "false");

				}

//						else if ( RP isCellExist(attributeValue,cell,"RP")
//							
//							) {
//						if (lastContents.equals("#N/A")||lastContents.equals("#VALUE!")||lastContents.equals("#############")) {
//							taskDetail.setTask30Duration("0");
//							
//						}
//						taskDetail.setTask30Duration(lastContents);
//					} 
//						else if ( RQ isCellExist(attributeValue,cell,"RQ")
//							
//							) {
//						if (lastContents.equals("#N/A")||lastContents.equals("#VALUE!")||lastContents.equals("#############")||taskDetail.getTask30Duration().equals("0")) {
//							taskDetail.setTask30StartDate("");
//							
//						}
//						String date = UtilService.getNextMonday(lastContents,attributeValue);
//						taskDetail.setTask30StartDate(date);
//					} else if ( RT isCellExist(attributeValue,cell,"RT")
//							
//							) {
//						if (lastContents.equals("#N/A")||lastContents.equals("#VALUE!")||lastContents.equals("#############")||taskDetail.getTask30Duration().equals("0")) {
//							taskDetail.setTask30ActualStartDate("");
//							
//						}
//						String date = UtilService.getNextMonday(lastContents,attributeValue);
//						taskDetail.setTask30ActualStartDate(date);
//					}
//						else if ( RU isCellExist(attributeValue,cell,"RU")
//							
//							) {
//						if (lastContents.equals("#N/A")||lastContents.equals("#VALUE!")||lastContents.equals("#############")||taskDetail.getTask30Duration().equals("0")) {
//							taskDetail.setTask30Duration("0");
//							
//						}
//						else {Integer duration = (Integer.parseInt(taskDetail.getTask30Duration())
//								+ Integer.parseInt(lastContents)) / 7;
//						taskDetail.setTask30Duration(duration.toString());
//					}

				else if (isCellExist(attributeValue, "TM")

				) {
					if (isNotValidValue() || taskDetail.getTask30Duration() == null) {
						taskDetail.setTask30DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask30DueDate(date);

					}
				} else if (isCellExist(attributeValue, "TP")

				) {
					if (isNotValidValue() || taskDetail.getTask30Duration() == null) {
						taskDetail.setTask30ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask30ActualDueDate(date);

					}
				}
				else if (isCellExist(attributeValue, "TS")

						) {
							if (isNotValidValue()) {
								data.setGovMilestoneCP2DecisionDate("");

							} else {
								String date = UtilService.getNextSunday(lastContents);
								data.setGovMilestoneCP2DecisionDate(date);

							}

				}
				
				

				// task 31
				else if (isCellExist(attributeValue, "TY")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask31IsCompleted("");

					} else
						taskDetail.setTask31IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "TZ")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask31IsActive("");

					} else
						taskDetail.setTask31IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "UA")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask31Duration("0");

					} else
						taskDetail.setTask31Duration(lastContents);

				} else if (isCellExist(attributeValue, "UB")

				) {
					if (isNotValidValue() || taskDetail.getTask31Duration().equals("0")) {
						taskDetail.setTask31StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask31StartDate(date);

					}
				} else if (isCellExist(attributeValue, "UE")

				) {
					if (isNotValidValue() || taskDetail.getTask31Duration().equals("0")) {
						taskDetail.setTask31ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask31ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "UF") && !attributeValue.startsWith("UF1")

				) {
					if (isNotValidValue() || taskDetail.getTask31Duration().equals("0")) {
						taskDetail.setTask31Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask31Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask31Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "UG")

				) {
					if (isNotValidValue() || taskDetail.getTask31Duration().equals("0")) {
						taskDetail.setTask31DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask31DueDate(date);

					}
				} else if (isCellExist(attributeValue, "UJ")

				) {
					if (isNotValidValue() || taskDetail.getTask31Duration().equals("0")) {
						taskDetail.setTask31ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask31ActualDueDate(date);

					}
				}

				// task 32
				else if (isCellExist(attributeValue, "UP")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask32IsCompleted("");

					} else
						taskDetail.setTask32IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "UQ")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask32IsActive("");

					} else
						taskDetail.setTask32IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "US")) {
					if (isNotValidValue()) {
						taskDetail.setTask32Duration("0");
					} else
						taskDetail.setTask32Duration(((Integer) (Integer.parseInt(lastContents) / 7)).toString());

				} else if (isCellExist(attributeValue, "UV")

				) {
					if (isNotValidValue() || taskDetail.getTask32Duration() == null
							|| taskDetail.getTask32Duration().equals("0")) {
						taskDetail.setTask32StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask32StartDate(date);

					}
				} else if (isCellExist(attributeValue, "UY")

				) {
					if (isNotValidValue() || taskDetail.getTask32Duration() == null
							|| taskDetail.getTask32Duration().equals("0")) {
						taskDetail.setTask32ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask32ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "UZ")

				) {
					if (isNotValidValue() || taskDetail.getTask32Duration() == null
							|| taskDetail.getTask32Duration().equals("0")) {
						taskDetail.setTask32DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask32DueDate(date);

					}
				} else if (isCellExist(attributeValue, "VC")

				) {
					if (isNotValidValue() || taskDetail.getTask32Duration() == null
							|| taskDetail.getTask32Duration().equals("0")) {
						taskDetail.setTask32ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask32ActualDueDate(date);

					}
				}

				// task 33
				else if (isCellExist(attributeValue, "VI")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask33IsCompleted("");

					} else
						taskDetail.setTask33IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "VJ")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask33IsActive("");

					} else
						taskDetail.setTask33IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "VK")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask33Duration("0");

					} else
						taskDetail.setTask33Duration(lastContents);

				} else if (isCellExist(attributeValue, "VL")

				) {
					if (isNotValidValue() || taskDetail.getTask33Duration().equals("0")) {
						taskDetail.setTask33StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask33StartDate(date);

					}
				} else if (isCellExist(attributeValue, "VO")

				) {
					if (isNotValidValue() || taskDetail.getTask33Duration().equals("0")) {
						taskDetail.setTask33ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask33ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "VP") && !attributeValue.startsWith("VP1")

				) {
					if (isNotValidValue() || taskDetail.getTask33Duration().equals("0")) {
						taskDetail.setTask33Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask33Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask33Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "VQ")

				) {
					if (isNotValidValue() || taskDetail.getTask33Duration().equals("0")) {
						taskDetail.setTask33DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask33DueDate(date);

					}
				} else if (isCellExist(attributeValue, "VU")

				) {
					if (isNotValidValue() || taskDetail.getTask33Duration().equals("0")) {
						taskDetail.setTask33ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask33ActualDueDate(date);

					}
				}

				// task 34
				else if (isCellExist(attributeValue, "WA")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask34IsCompleted("");

					} else
						taskDetail.setTask34IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "WB")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask34IsActive("");

					} else
						taskDetail.setTask34IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "WC")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask34Duration("0");

					} else
						taskDetail.setTask34Duration(lastContents);

				} else if (isCellExist(attributeValue, "WD")

				) {
					if (isNotValidValue() || taskDetail.getTask34Duration().equals("0")) {
						taskDetail.setTask34StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask34StartDate(date);

					}
				} else if (isCellExist(attributeValue, "WG")

				) {
					if (isNotValidValue() || taskDetail.getTask34Duration().equals("0")) {
						taskDetail.setTask34ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask34ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "WH") && !attributeValue.startsWith("WH1")

				) {
					if (isNotValidValue() || taskDetail.getTask34Duration().equals("0")) {
						taskDetail.setTask34Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask34Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask34Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "WI")

				) {
					if (isNotValidValue() || taskDetail.getTask34Duration().equals("0")) {
						taskDetail.setTask34DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask34DueDate(date);

					}
				} else if (isCellExist(attributeValue, "WL")

				) {
					if (isNotValidValue() || taskDetail.getTask34Duration().equals("0")) {
						taskDetail.setTask34ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask34ActualDueDate(date);

					}
				}

				// task 24
				else if (isCellExist(attributeValue, "WR")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask24IsCompleted("");

					} else
						taskDetail.setTask24IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "WS")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask24IsActive("");

					} else
						taskDetail.setTask24IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "WT")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask24Duration("0");

					} else
						taskDetail.setTask24Duration(lastContents);

				} else if (isCellExist(attributeValue, "WU")

				) {
					if (isNotValidValue() || taskDetail.getTask24Duration().equals("0")) {
						taskDetail.setTask24StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask24StartDate(date);

					}
				} else if (isCellExist(attributeValue, "WX")

				) {
					if (isNotValidValue() || taskDetail.getTask24Duration().equals("0")) {
						taskDetail.setTask24ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask24ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "WY") && !attributeValue.startsWith("WY")

				) {
					if (isNotValidValue() || taskDetail.getTask24Duration().equals("0")) {
						taskDetail.setTask24Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask24Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask24Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "WZ")

				) {
					if (isNotValidValue() || taskDetail.getTask24Duration().equals("0")) {
						taskDetail.setTask24DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask24DueDate(date);

					}
				} else if (isCellExist(attributeValue, "XC")

				) {
					if (isNotValidValue() || taskDetail.getTask24Duration().equals("0")) {
						taskDetail.setTask24ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask24ActualDueDate(date);

					}
				}

				// task 35
				else if (isCellExist(attributeValue, "XI")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask35IsCompleted("");

					} else
						taskDetail.setTask35IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "XJ")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask35IsActive("");

					} else
						taskDetail.setTask35IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "XK")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask35Duration("0");

					} else
						taskDetail.setTask35Duration(lastContents);

				} else if (isCellExist(attributeValue, "XL")

				) {
					if (isNotValidValue() || taskDetail.getTask35Duration().equals("0")) {
						taskDetail.setTask35StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask35StartDate(date);

					}
				} else if (isCellExist(attributeValue, "XO")

				) {
					if (isNotValidValue() || taskDetail.getTask35Duration().equals("0")) {
						taskDetail.setTask35ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask35ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "XP") && !attributeValue.startsWith("XP1")

				) {
					if (isNotValidValue() || taskDetail.getTask35Duration().equals("0")) {
						taskDetail.setTask35Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask35Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask35Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "XQ")

				) {
					if (isNotValidValue() || taskDetail.getTask35Duration().equals("0")) {
						taskDetail.setTask35DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask35DueDate(date);

					}
				} else if (isCellExist(attributeValue, "XT")

				) {
					if (isNotValidValue() || taskDetail.getTask35Duration().equals("0")) {
						taskDetail.setTask35ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask35ActualDueDate(date);

					}
				}
				
				else if (isCellExist(attributeValue, "XW")

						) {
							if (isNotValidValue()) {
								data.setGovMilestoneCP3DecisionDate("");

							} else {
								String date = UtilService.getNextSunday(lastContents);
								data.setGovMilestoneCP3DecisionDate(date);

							}

				}

				// task 36
				else if (isCellExist(attributeValue, "YC")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask36IsCompleted("");

					} else
						taskDetail.setTask36IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "YD")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask36IsActive("");

					} else
						taskDetail.setTask36IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "YE")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask36Duration("0");

					} else
						taskDetail.setTask36Duration(lastContents);

				} else if (isCellExist(attributeValue, "YF")

				) {
					if (isNotValidValue() || taskDetail.getTask36Duration().equals("0")) {
						taskDetail.setTask36StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask36StartDate(date);

					}
				} else if (isCellExist(attributeValue, "YI")

				) {
					if (isNotValidValue() || taskDetail.getTask36Duration().equals("0")) {
						taskDetail.setTask36ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask36ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "YJ") && !attributeValue.startsWith("YJ1")

				) {
					if (isNotValidValue() || taskDetail.getTask36Duration().equals("0")) {
						taskDetail.setTask36Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask36Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask36Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "YK")

				) {
					if (isNotValidValue() || taskDetail.getTask36Duration().equals("0")) {
						taskDetail.setTask36DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask36DueDate(date);

					}
				} else if (isCellExist(attributeValue, "YN")

				) {
					if (isNotValidValue() || taskDetail.getTask36Duration().equals("0")) {
						taskDetail.setTask36ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask36ActualDueDate(date);

					}
				}

				// task 37
				else if (isCellExist(attributeValue, "YT")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask37IsCompleted("");

					} else
						taskDetail.setTask37IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "YU")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask37IsActive("");

					} else
						taskDetail.setTask37IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "YV")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask37Duration("0");

					} else
						taskDetail.setTask37Duration(lastContents);

				} else if (isCellExist(attributeValue, "YW")

				) {
					if (isNotValidValue() || taskDetail.getTask37Duration().equals("0")) {
						taskDetail.setTask37StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask37StartDate(date);

					}
				} else if (isCellExist(attributeValue, "YZ")

				) {
					if (isNotValidValue() || taskDetail.getTask37Duration().equals("0")) {
						taskDetail.setTask37ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask37ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "ZB")

				) {
					if (isNotValidValue() || taskDetail.getTask37Duration().equals("0")) {
						taskDetail.setTask37DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask37DueDate(date);

					}
				} else if (isCellExist(attributeValue, "ZE")

				) {
					if (isNotValidValue() || taskDetail.getTask37Duration().equals("0")) {
						taskDetail.setTask37ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask37ActualDueDate(date);

					}
				}

				// task 38
				else if (isCellExist(attributeValue, "ZK")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask38IsCompleted("");

					} else
						taskDetail.setTask38IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ZL")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask38IsActive("");

					} else
						taskDetail.setTask38IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ZM")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask38Duration("0");

					} else
						taskDetail.setTask38Duration(lastContents);

				} else if (isCellExist(attributeValue, "ZN")

				) {
					if (isNotValidValue() || taskDetail.getTask38Duration().equals("0")) {
						taskDetail.setTask38StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask38StartDate(date);

					}
				} else if (isCellExist(attributeValue, "ZQ")

				) {
					if (isNotValidValue() || taskDetail.getTask38Duration().equals("0")) {
						taskDetail.setTask38ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask38ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "ZR") && !attributeValue.startsWith("ZR1")

				) {
					if (isNotValidValue() || taskDetail.getTask38Duration().equals("0")) {
						taskDetail.setTask38Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask38Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask38Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "ZS")

				) {
					if (isNotValidValue() || taskDetail.getTask38Duration().equals("0")) {
						taskDetail.setTask38DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask38DueDate(date);

					}
				} else if (isCellExist(attributeValue, "ZV")

				) {
					if (isNotValidValue() || taskDetail.getTask38Duration().equals("0")) {
						taskDetail.setTask38ActualDueDate("");
						projectDetail.setTrialMaterialsConfirmedDeliveryWeek("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask38ActualDueDate(date);
						String weekdate = UtilService.getValidDate(lastContents);
						String[] dateArray = weekdate.split("-");
						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
								Integer.parseInt(dateArray[1]),
								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);						
						if(dateArray[0].equals("1899")) {
							projectDetail.setTrialMaterialsConfirmedDeliveryWeek("");
						}else {
							projectDetail.setTrialMaterialsConfirmedDeliveryWeek(weekOfYear + "/" + dateArray[0] + "");
						}

					}
				}

				// task 39
				else if (isCellExist(attributeValue, "AAB")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask39IsCompleted("");

					} else
						taskDetail.setTask39IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "AAC")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask39IsActive("");

					} else
						taskDetail.setTask39IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "AAD")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask39Duration("0");

					} else
						taskDetail.setTask39Duration(lastContents);

				} else if (isCellExist(attributeValue, "AAE")

				) {
					if (isNotValidValue() || taskDetail.getTask39Duration().equals("0")) {
						taskDetail.setTask39StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask39StartDate(date);

					}
				} else if (isCellExist(attributeValue, "AAH")

				) {
					if (isNotValidValue() || taskDetail.getTask39Duration().equals("0")) {
						taskDetail.setTask39ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask39ActualStartDate(date);
					}
				} else if (isCellExist(attributeValue, "AAI") && !attributeValue.startsWith("AAI1")) {
					if (isNotValidValue() || taskDetail.getTask39Duration().equals("0")) {
						taskDetail.setTask39Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask39Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask39Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "AAJ")

				) {
					if (isNotValidValue() || taskDetail.getTask39Duration().equals("0")) {
						taskDetail.setTask39DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask39DueDate(date);

					}
				} else if (isCellExist(attributeValue, "AAM")

				) {
					if (isNotValidValue() || taskDetail.getTask39Duration().equals("0")) {
						taskDetail.setTask39ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask39ActualDueDate(date);

					}
				}

				// task 40
				else if (isCellExist(attributeValue, "AAS")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask40IsCompleted("");

					} else
						taskDetail.setTask40IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "AAT")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask40IsActive("");

					} else
						taskDetail.setTask40IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "AAU")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask40Duration("0");

					} else
						taskDetail.setTask40Duration(lastContents);

				} else if (isCellExist(attributeValue, "AAV")

				) {
					if (isNotValidValue() || taskDetail.getTask40Duration().equals("0")) {
						taskDetail.setTask40StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask40StartDate(date);

					}
				} else if (isCellExist(attributeValue, "AAY")

				) {
					if (isNotValidValue() || taskDetail.getTask40Duration().equals("0")) {
						taskDetail.setTask40ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask40ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "AAZ") && !attributeValue.startsWith("AAZ")) {
					if (isNotValidValue() || taskDetail.getTask40Duration().equals("0")) {
						taskDetail.setTask40Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask40Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask40Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "ABA")

				) {
					if (isNotValidValue() || taskDetail.getTask40Duration().equals("0")) {
						taskDetail.setTask40DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask40DueDate(date);

					}
				} else if (isCellExist(attributeValue, "ABD")

				) {
					if (isNotValidValue() || taskDetail.getTask40Duration().equals("0")) {
						taskDetail.setTask40ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask40ActualDueDate(date);

					}
				}

				// task 41
				else if (isCellExist(attributeValue, "ABJ")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask41IsCompleted("");

					} else
						taskDetail.setTask41IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ABK")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask41IsActive("");

					} else
						taskDetail.setTask41IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ABM")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask41Duration("0");

					} else
						taskDetail.setTask41Duration(lastContents);

				} else if (isCellExist(attributeValue, "ABN")

				) {
					if (isNotValidValue() || taskDetail.getTask41Duration().equals("0")) {
						taskDetail.setTask41StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask41StartDate(date);

					}
				} else if (isCellExist(attributeValue, "ABQ")

				) {
					if (isNotValidValue() || taskDetail.getTask41Duration().equals("0")) {
						taskDetail.setTask41ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask41ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "ABR") && !attributeValue.startsWith("ABR1")) {
					if (isNotValidValue() || taskDetail.getTask41Duration().equals("0")) {
						taskDetail.setTask41Duration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask41Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask41Duration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "ABS")

				) {
					if (isNotValidValue() || taskDetail.getTask41Duration().equals("0")) {
						taskDetail.setTask41DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask41DueDate(date);

					}
				} else if (isCellExist(attributeValue, "ABV")

				) {
					if (isNotValidValue() || taskDetail.getTask41Duration().equals("0")) {
						taskDetail.setTask41ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask41ActualDueDate(date);
					}

				}
				
				else if (isCellExist(attributeValue, "ABY")

						) {
							if (isNotValidValue()) {
								data.setGovMilestoneCP4DecisionDate("");

							} else {
								String date = UtilService.getNextSunday(lastContents);
								data.setGovMilestoneCP4DecisionDate(date);

							}

				}

				// task 42
				else if (isCellExist(attributeValue, "ACE")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask42IsCompleted("");

					} else
						taskDetail.setTask42IsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ACF")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask42IsActive("");

					} else
						taskDetail.setTask42IsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ACG")) {
					if (isNotValidValue()) {
						taskDetail.setTask42Duration("0");

					} else
						taskDetail.setTask42Duration(((Integer) (Integer.parseInt(lastContents) / 7)).toString());

				} else if (isCellExist(attributeValue, "ACI")

				) {
					if (isNotValidValue() || taskDetail.getTask42Duration() == null
							|| taskDetail.getTask42Duration().equals("0")) {
						taskDetail.setTask42StartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask42StartDate(date);

					}
				} else if (isCellExist(attributeValue, "ACN")

				) {
					if (isNotValidValue() || taskDetail.getTask42Duration() == null
							|| taskDetail.getTask42Duration().equals("0")) {
						taskDetail.setTask42ActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask42ActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "ACO")

				) {
					if (isNotValidValue() || taskDetail.getTask42Duration() == null
							|| taskDetail.getTask42Duration().equals("0")) {
						taskDetail.setTask42DueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask42DueDate(date);

					}
				} else if (isCellExist(attributeValue, "ACR")

				) {
					if (isNotValidValue() || taskDetail.getTask42Duration() == null
							|| taskDetail.getTask42Duration().equals("0")) {
						taskDetail.setTask42ActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask42ActualDueDate(date);

					}

				}

				else if (isCellExist(attributeValue, "ACS")) {

					if (isNotValidValue()) {
						projectDetail.setProjectEndDate("");
					} else
						projectDetail.setProjectEndDate(UtilService.getValidDate(lastContents));

				} else if (isCellExist(attributeValue, "ACU")) {
					if (isNotValidValue()) {
						projectDetail.setGFG("");
					} else
						projectDetail.setGFG(lastContents);

				} else if (isCellExist(attributeValue, "ACV")) {
					if (isNotValidValue()) {
						projectDetail.setProjectSpecialist("");
					} else
						projectDetail.setProjectSpecialist(lastContents);

				}

				else if (isCellExist(attributeValue, "ACY")

				) {
					if (isNotValidValue()) {
						data.setR_PO_PROJECT_TYPE("");

					} else
						data.setR_PO_PROJECT_TYPE(lastContents);
				} else if (isCellExist(attributeValue, "ADE")

				) {
					if (isNotValidValue()) {
						data.setNewFGSAP("");

					} else
						data.setNewFGSAP(
								lastContents.equals("Y") || lastContents.equals("1") || lastContents.equals("1")
										? "true"
										: "false");
				} else if (isCellExist(attributeValue, "ADF")

				) {
					if (isNotValidValue()) {
						data.setNewRDFormula("");

					} else
						data.setNewRDFormula(lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "ADG")

				) {
					if (isNotValidValue()) {
						data.setNewHBCFormula("");

					} else
						data.setNewHBCFormula(lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "ADH")

				) {
					if (isNotValidValue()) {
						data.setNewPrimaryPackaging("");

					} else
						data.setNewPrimaryPackaging(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "ADI")

				) {
					if (isNotValidValue()) {
						data.setIsSecondaryPackaging("");

					} else
						data.setIsSecondaryPackaging(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "ADJ")

				) {
					if (isNotValidValue()) {
						data.setRegistrationDossier("");
						projectDetail.setFinalRegistrationDossier("");

					} else
						data.setRegistrationDossier(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
						projectDetail.setFinalRegistrationDossier(
							lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
				} else if (isCellExist(attributeValue, "ADK")

				) {
					if (isNotValidValue()) {
						data.setPreProductionRegistartion("");
						projectDetail.setFinalPreProductionRegistartion("");

					} else {
						projectDetail.setFinalPreProductionRegistartion(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
						data.setPreProductionRegistartion(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
					}
						
				} else if (isCellExist(attributeValue, "ADL")

				) {
					if (isNotValidValue()) {
						data.setPostProductionRegistration("");
						projectDetail.setFinalPostProductionRegistration("");
					} else
						data.setPostProductionRegistration(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
					projectDetail.setFinalPostProductionRegistration(
							lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ADX")) {
					if (isNotValidValue()) {
						projectDetail.setFinalProjectClassificationDesc("");
					} else
						projectDetail.setFinalProjectClassificationDesc(lastContents);

				}

				else if (isCellExist(attributeValue, "ADZ")

				) {
					if (isNotValidValue()) {
						data.setR_PO_REQUEST_TYPE("");
					} else
						data.setR_PO_REQUEST_TYPE(lastContents);

				} else if (isCellExist(attributeValue, "AEF")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask3FinalProjectType("");
						projectDetail.setFinalProjectType("");
//						logger.info("Project type not valid" + lastContents);
					} else {
//						logger.info("Project type " + lastContents);
						taskDetail.setTask3FinalProjectType(lastContents);
						projectDetail.setFinalProjectType(lastContents);
					}

				}

				else if (isCellExist(attributeValue, "AEG")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask3NewFG("");
						projectDetail.setFinalNewFG("");

					} else {
						taskDetail.setTask3NewFG(lastContents.equals("1") ? "true" : "false");
						projectDetail
								.setFinalNewFG(lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
					}

				} else if (isCellExist(attributeValue, "AEH")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask3NewRDFormula("");
						projectDetail.setFinalnewRDFormula("");

					} else {
						taskDetail.setTask3NewRDFormula(lastContents.equals("1") ? "true" : "false");
						projectDetail.setFinalnewRDFormula(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
					}

				}

				else if (isCellExist(attributeValue, "AEI")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask3NewHBCFormula("");
						projectDetail.setFinalNewHBCFormula("");

					} else {
						taskDetail.setTask3NewHBCFormula(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
						projectDetail.setFinalNewHBCFormula(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
					}

				}

				else if (isCellExist(attributeValue, "AEJ")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask3NewPrimaryPackaging("");
						projectDetail.setFinalNewPrimaryPackaging("");
						;

					} else {
						taskDetail.setTask3NewPrimaryPackaging(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
						projectDetail.setFinalNewPrimaryPackaging(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
					}

				}

				else if (isCellExist(attributeValue, "AEK")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask3NewSecondaryPackaging("");
						projectDetail.setFinalSecondaryPackaging("");

					} else {
						taskDetail.setTask3NewSecondaryPackaging(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
						projectDetail.setFinalSecondaryPackaging(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
					}

				} else if (isCellExist(attributeValue, "AEU")) {
					if (isNotValidValue()) {
						projectDetail.setClassificationRational("");
					} else
						projectDetail.setClassificationRational(lastContents);

				} else if (isCellExist(attributeValue, "AFR")) {
					if (isNotValidValue()) {
						projectDetail.setProjectSpecialist("");
					} else
						projectDetail.setProjectSpecialist(lastContents);

				} else if (isCellExist(attributeValue, "AFX")) {
					if (isNotValidValue()) {
						projectDetail.setEANCodeSharedWithBottlerTarget("");
					} else {
						String date = UtilService.getValidDate(lastContents);
						String[] dateArray = date.split("-");
						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
								Integer.parseInt(dateArray[1]),
								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
						if(dateArray[0].equals("1899")) {
							projectDetail.setEANCodeSharedWithBottlerTarget("");
						}else {
							projectDetail.setEANCodeSharedWithBottlerTarget(weekOfYear + "/" + dateArray[0]+ "");
						}
					}

				} else if (isCellExist(attributeValue, "AGA")) {
					if (isNotValidValue()) {
						projectDetail.setEANCodesSharedWithBottlerActual("");
					} else {
						String date = UtilService.getValidDate(lastContents);
						String[] dateArray = date.split("-");
						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
								Integer.parseInt(dateArray[1]),
								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
						if(dateArray[0].equals("1899")) {
							projectDetail.setEANCodesSharedWithBottlerActual("");
						}else {
							projectDetail.setEANCodesSharedWithBottlerActual(weekOfYear + "/" + dateArray[0]+ "");
						}
					}
					

				} else if (isCellExist(attributeValue, "AGG")) {
					if (isNotValidValue() || lastContents.equalsIgnoreCase("0")) {
						projectDetail.setProdInfoSheetSharedActual("");
					} else {
						String value = lastContents.replaceAll("-", "/");
						projectDetail.setProdInfoSheetSharedActual(value);
					}
						

				} else if (isCellExist(attributeValue, "AGL")) {
					if (isNotValidValue() || lastContents.equalsIgnoreCase("0")) {
						projectDetail.setBottlerActualWeek("");
					} else {
						String value = lastContents.replaceAll("-", "/");
						projectDetail.setBottlerActualWeek(value);
					}
						

				} else if (isCellExist(attributeValue, "AGM")) {
					if (isNotValidValue()) {
						projectDetail.setBottlerSpecificItemNumber("");
					} else
						projectDetail.setBottlerSpecificItemNumber(lastContents);

				} else if (isCellExist(attributeValue, "AGO")) {
					if (isNotValidValue()) {
						projectDetail.setNoOfCanCompaniesReq("");
					} else
						projectDetail.setNoOfCanCompaniesReq(lastContents);

				} else if (isCellExist(attributeValue, "AGP")) {
					if (isNotValidValue()) {
						projectDetail.setNameOfCanCompanys("");
					} else
						projectDetail.setNameOfCanCompanys(lastContents);

				} else if (isCellExist(attributeValue, "AGT")) {
					if (isNotValidValue()) {
						projectDetail.setLabelUplaodedToCanCompanyDate("");

					} else {
						String date = UtilService.getValidDate(lastContents);
						String[] dateArray = date.split("-");
						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
								Integer.parseInt(dateArray[1]),
								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
						if(dateArray[0].equals("1899")) {
							projectDetail.setLabelUplaodedToCanCompanyDate("");
						}else {
							projectDetail.setLabelUplaodedToCanCompanyDate(weekOfYear + "/" + dateArray[0]);
						}
						
					}

				} else if (isCellExist(attributeValue, "AGY")) {
					if (isNotValidValue()) {
						projectDetail.setPOReceivedDate("");
					} else {
						projectDetail.setPOReceivedDate("");
//						String date = UtilService.getValidDate(lastContents);
//						String[] dateArray = date.split("-");
//						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
//								Integer.parseInt(dateArray[1]),
//								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
//						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
//						projectDetail.setPOReceivedDate(weekOfYear + "/" + dateArray[0]);
					}
				} else if (isCellExist(attributeValue, "AHK")) {
					if (isNotValidValue()) {
						projectDetail.setPalletLabelReceivedActual("");
					} else {
						String date = UtilService.getValidDate(lastContents);
						String[] dateArray = date.split("-");
						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
								Integer.parseInt(dateArray[1]),
								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
						projectDetail.setPalletLabelReceivedActual(weekOfYear + "/" + dateArray[0]);
					}

				} else if (isCellExist(attributeValue, "AHN")) {
					if (isNotValidValue()) {
						projectDetail.setPalletLabelSentTarget("");
					} else
						projectDetail.setPalletLabelSentTarget(lastContents);

				} else if (isCellExist(attributeValue, "AHQ")) {
					if (isNotValidValue()) {
						projectDetail.setPalletLabelSentActual("");
					} else {
						String date = UtilService.getValidDate(lastContents);
						String[] dateArray = date.split("-");
						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
								Integer.parseInt(dateArray[1]),
								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
						projectDetail.setPalletLabelSentActual(weekOfYear + "/" + dateArray[0]);
					}
				} else if (isCellExist(attributeValue, "AHV")) {
					if (isNotValidValue()) {
						projectDetail.setBottlerSpecificComments("");
					} else
						projectDetail.setBottlerSpecificComments(lastContents);

				} else if (isCellExist(attributeValue, "AHX")) {
					if (isNotValidValue()) {
						projectDetail.setPalletLabelFormat("");
					} else
						projectDetail.setPalletLabelFormat(lastContents);

				} else if (isCellExist(attributeValue, "AHY")) {
					if (isNotValidValue()) {
						projectDetail.setTrayLabelFormat("");
					} else
						projectDetail.setTrayLabelFormat(lastContents);

				} else if (isCellExist(attributeValue, "AIC")) {
					if (isNotValidValue()) {
						projectDetail.setNoOfPiecesRequired("");
					} else
						projectDetail.setNoOfPiecesRequired(lastContents);

				} else if (isCellExist(attributeValue, "AIE")) {
					if (isNotValidValue()) {
						projectDetail.setRequire1("");
					} else
						projectDetail
								.setRequire1(lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "AIF")) {
					if (isNotValidValue()) {
						projectDetail.setRegsNeeded1("");
					} else
						projectDetail.setRegsNeeded1(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "AIG")) {
					if (isNotValidValue()) {
						projectDetail.setPackFormat1("");
					} else
						projectDetail.setPackFormat1(lastContents);

				} else if (isCellExist(attributeValue, "AIH")) {
					if (isNotValidValue()) {
						projectDetail.setMPMJobNo1("");
					} else
						projectDetail.setMPMJobNo1(lastContents);

				} else if (isCellExist(attributeValue, "AII")) {
					if (isNotValidValue()) {
						projectDetail.setPrintSupplier1("");
					} else
						projectDetail.setPrintSupplier1(lastContents);

				} else if (isCellExist(attributeValue, "AIJ")) {
					if (isNotValidValue()) {
						projectDetail.setApprovedDielineAvailable1("");
					} else
						projectDetail.setApprovedDielineAvailable1(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "AIK")) {
					if (isNotValidValue()) {
						projectDetail.setSecPackItemNumber1("");
					} else
						projectDetail.setSecPackItemNumber1(lastContents);

				} else if (isCellExist(attributeValue, "AIL")) {
					if (isNotValidValue()) {
						projectDetail.setColourApproval1("");
					} else
						projectDetail.setColourApproval1(lastContents);

				} else if (isCellExist(attributeValue, "AIM")) {
					if (isNotValidValue()) {
						projectDetail.setInvoiceApproval1("");
					} else
						projectDetail.setInvoiceApproval1(lastContents);

				} else if (isCellExist(attributeValue, "AIN")) {
					if (isNotValidValue()) {
						projectDetail.setStatusComments1("");
					} else
						projectDetail.setStatusComments1(lastContents);

				} else if (isCellExist(attributeValue, "AIO")) {
					if (isNotValidValue()) {
						projectDetail.setDateDevelopmentCompleted1("");
					} else {
						String date = UtilService.getValidDate(lastContents);
						String[] dateArray = date.split("-");
						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
								Integer.parseInt(dateArray[1]),
								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
						projectDetail.setDateDevelopmentCompleted1(weekOfYear + "/" + dateArray[0] + "");
					}
				} else if (isCellExist(attributeValue, "AIR")) {
					if (isNotValidValue()) {
						projectDetail.setDateProofingCompletedSecondaryPackaging1("");
					} else {
						String date = UtilService.getValidDate(lastContents);
						String[] dateArray = date.split("-");
						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
								Integer.parseInt(dateArray[1]),
								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
						projectDetail.setDateProofingCompletedSecondaryPackaging1(weekOfYear + "/" + dateArray[0] + "");
					}
				}

				else if (isCellExist(attributeValue, "AIT")) {
					if (isNotValidValue()) {
						projectDetail.setRequire2("");
					} else
						projectDetail
								.setRequire2(lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "AIU")) {
					if (isNotValidValue()) {
						projectDetail.setRegsNeeded2("");
					} else
						projectDetail.setRegsNeeded2(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "AIV")) {
					if (isNotValidValue()) {
						projectDetail.setPackFormat2("");
					} else
						projectDetail.setPackFormat2(lastContents);

				} else if (isCellExist(attributeValue, "AIW")) {
					if (isNotValidValue()) {
						projectDetail.setMPMJobNo2("");
					} else
						projectDetail.setMPMJobNo2(lastContents);

				} else if (isCellExist(attributeValue, "AIX")) {
					if (isNotValidValue()) {
						projectDetail.setPrintSupplier2("");
					} else
						projectDetail.setPrintSupplier2(lastContents);

				} else if (isCellExist(attributeValue, "AIY")) {
					if (isNotValidValue()) {
						projectDetail.setApprovedDielineAvailable2("");
					} else
						projectDetail.setApprovedDielineAvailable2(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "AIZ")) {
					if (isNotValidValue()) {
						projectDetail.setSecPackItemNumber2("");
					} else
						projectDetail.setSecPackItemNumber2(lastContents);

				} else if (isCellExist(attributeValue, "AJA")) {
					if (isNotValidValue()) {
						projectDetail.setColourApproval2("");
					} else
						projectDetail.setColourApproval2(lastContents);

				} else if (isCellExist(attributeValue, "AJB")) {
					if (isNotValidValue()) {
						projectDetail.setInvoiceApproval2("");
					} else
						projectDetail.setInvoiceApproval2(lastContents);

				} else if (isCellExist(attributeValue, "AJC")) {
					if (isNotValidValue()) {
						projectDetail.setStatusComments2("");
					} else
						projectDetail.setStatusComments2(lastContents);

				} else if (isCellExist(attributeValue, "AJD")) {
					if (isNotValidValue()) {
						projectDetail.setDateDevelopmentCompleted2("");
					} else {
						String date = UtilService.getValidDate(lastContents);
						String[] dateArray = date.split("-");
						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
								Integer.parseInt(dateArray[1]),
								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);

						projectDetail.setDateDevelopmentCompleted2(weekOfYear + "/" + dateArray[0] + "");
					}
				} else if (isCellExist(attributeValue, "AJG")) {
					if (isNotValidValue()) {
						projectDetail.setDateProofingCompletedSecondaryPackaging2("");
					} else {
						String date = UtilService.getValidDate(lastContents);
						String[] dateArray = date.split("-");
						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
								Integer.parseInt(dateArray[1]),
								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);

						projectDetail.setDateProofingCompletedSecondaryPackaging2(weekOfYear + "/" + dateArray[0] + "");
					}

				}

				else if (isCellExist(attributeValue, "AJI")) {
					if (isNotValidValue()) {
						projectDetail.setRequire3("");
					} else
						projectDetail
								.setRequire3(lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "AJJ")) {
					if (isNotValidValue()) {
						projectDetail.setRegsNeeded3("");
					} else
						projectDetail.setRegsNeeded3(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "AJK")) {
					if (isNotValidValue()) {
						projectDetail.setPackFormat3("");
					} else
						projectDetail.setPackFormat3(lastContents);

				} else if (isCellExist(attributeValue, "AJL")) {
					if (isNotValidValue()) {
						projectDetail.setMPMJobNo3("");
					} else
						projectDetail.setMPMJobNo3(lastContents);

				} else if (isCellExist(attributeValue, "AJM")) {
					if (isNotValidValue()) {
						projectDetail.setPrintSupplier3("");
					} else
						projectDetail.setPrintSupplier3(lastContents);

				} else if (isCellExist(attributeValue, "AJN")) {
					if (isNotValidValue()) {
						projectDetail.setApprovedDielineAvailable3("");
					} else
						projectDetail.setApprovedDielineAvailable3(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "AJO")) {
					if (isNotValidValue()) {
						projectDetail.setSecPackItemNumber3("");
					} else
						projectDetail.setSecPackItemNumber3(lastContents);

				} else if (isCellExist(attributeValue, "AJP")) {
					if (isNotValidValue()) {
						projectDetail.setColourApproval3("");
					} else
						projectDetail.setColourApproval3(lastContents);

				} else if (isCellExist(attributeValue, "AJQ")) {
					if (isNotValidValue()) {
						projectDetail.setInvoiceApproval3("");
					} else
						projectDetail.setInvoiceApproval3(lastContents);

				} else if (isCellExist(attributeValue, "AJR")) {
					if (isNotValidValue()) {
						projectDetail.setStatusComments3("");
					} else
						projectDetail.setStatusComments3(lastContents);

				} else if (isCellExist(attributeValue, "AJS")) {
					if (isNotValidValue()) {
						projectDetail.setDateDevelopmentCompleted3("");
					} else {
						String date = UtilService.getValidDate(lastContents);
						String[] dateArray = date.split("-");
						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
								Integer.parseInt(dateArray[1]),
								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);

						projectDetail.setDateDevelopmentCompleted3(weekOfYear + "/" + dateArray[0] + "");
					}

				} else if (isCellExist(attributeValue, "AJV")) {
					if (isNotValidValue()) {

						projectDetail.setDateProofingCompletedSecondaryPackaging3("");
					} else {
						String date = UtilService.getValidDate(lastContents);
						String[] dateArray = date.split("-");
						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
								Integer.parseInt(dateArray[1]),
								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);

						projectDetail.setDateProofingCompletedSecondaryPackaging3(weekOfYear + "/" + dateArray[0] + "");
					}

				}

				else if (isCellExist(attributeValue, "ALF")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask9WinshuttleProjectNo("");
						projectDetail.setWinshuttleProjectNumber("");

					} else {
//						taskDetail.setTask9WinshuttleProjectNo(lastContents);
						projectDetail.setWinshuttleProjectNumber(lastContents);
					}
				} else if (isCellExist(attributeValue, "ALG")) {
					if (isNotValidValue()) {
						projectDetail.setFGMaterialCode("");
					} else
						projectDetail.setFGMaterialCode(lastContents);

				} else if (isCellExist(attributeValue, "ALH")) {
					if (isNotValidValue()) {
						projectDetail.setFormulaHBC("");
					} else
						projectDetail.setFormulaHBC(lastContents);

				} else if (isCellExist(attributeValue, "ALI")) {
					if (isNotValidValue()) {
						projectDetail.setCanSleeveOrPrimaryBottler("");
					} else
						projectDetail.setCanSleeveOrPrimaryBottler(lastContents);

				} else if (isCellExist(attributeValue, "ALP")) {
					if (isNotValidValue()) {
						projectDetail.setNoOfWeeksIncreaseVsPrior("");
					} else
						projectDetail.setNoOfWeeksIncreaseVsPrior(lastContents);

				} else if (isCellExist(attributeValue, "ALQ")) {
					if (isNotValidValue()) {
						projectDetail.setNPDWasOnTrackForOriginalCp1Date("");
					} else
						projectDetail.setNPDWasOnTrackForOriginalCp1Date(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ALR")) {
					if (isNotValidValue()) {
						projectDetail.setBottlerPushedOutProductionWithoutLaunchImpact("");
					} else
						projectDetail.setBottlerPushedOutProductionWithoutLaunchImpact(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ALS")) {
					if (isNotValidValue()) {
						projectDetail.setMECCommercialBottlerRequestedDelayToLaunch(lastContents);
					} else
						projectDetail.setMECCommercialBottlerRequestedDelayToLaunch(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ALT")) {
					if (isNotValidValue()) {
						projectDetail.setOtherTbc("");
					} else
						projectDetail
								.setOtherTbc(lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ALU")) {
					if (isNotValidValue()) {
						projectDetail.setOther2Tbc("");
					} else
						projectDetail
								.setOther2Tbc(lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ALV")) {
					if (isNotValidValue()) {
						projectDetail.setHasTheChangeBeenAlignedViaCPMtg("");
					} else
						projectDetail.setHasTheChangeBeenAlignedViaCPMtg(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ALW")) {
					if (isNotValidValue()) {
						projectDetail.setBriefDescOfDelay("");
					} else
						projectDetail.setBriefDescOfDelay(lastContents);

				} else if (isCellExist(attributeValue, "ALY")) {
					if (isNotValidValue()) {
						projectDetail.setCP1DeliveryWeek("");
					} else
						projectDetail.setCP1DeliveryWeek(lastContents);

				} else if (isCellExist(attributeValue, "ALZ")) {
					if (isNotValidValue()) {
						projectDetail.setCP1DeliveryWeek("");
					} else {
						String weekValue = projectDetail.getCP1DeliveryWeek() + "/" + lastContents;
						if(weekValue.equals("/0") || weekValue.equals("52/1899") ||  weekValue.equals("0/0")) {
							projectDetail.setCP1DeliveryWeek("");
						}else {
							projectDetail.setCP1DeliveryWeek(weekValue);
						}
						
					}

				}

				else if (isCellExist(attributeValue, "AMA")

				) {
					if (isNotValidValue()) {
						data.setLeadMarketTargetDPInWarehouse("");
						data.setOtherLinkedMarketsTargetDPInWarehouse("");

					} else {
						String date = UtilService.getValidDate(lastContents);
						String[] dateArray = date.split("-");
						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
								Integer.parseInt(dateArray[1]),
								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
						data.setLeadMarketTargetDPInWarehouse(weekOfYear + "/" + dateArray[0] + "");
						data.setOtherLinkedMarketsTargetDPInWarehouse(weekOfYear + "/" + dateArray[0] + "");
					}

				}
				
				else if (isCellExist(attributeValue, "AMO")

						) {
							if (isNotValidValue()) {
								projectDetail.setFinalAgreed1stProd("");

							} else {
								String date = UtilService.getValidDate(lastContents);
								String[] dateArray = date.split("-");
								LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
										Integer.parseInt(dateArray[1]),
										Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
								int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
								if(dateArray[0].equals("1899")) {
									projectDetail.setFinalAgreed1stProd("");
								}else {
									projectDetail.setFinalAgreed1stProd(weekOfYear + "/" + dateArray[0] + "");
								}
							}

						}

//				else if (isCellExist(attributeValue, "AMR")
//
//				) {
//					if (isNotValidValue()) {
//						taskDetail.setTask39TrialDateConfirmedWithBottler("");
//
//					} else {
//						String date = UtilService.getValidDate(lastContents);
//						String[] dateArray = date.split("-");
//						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
//								Integer.parseInt(dateArray[1]),
//								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
//						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
//						if(dateArray[0].equals("1899")) {
//							taskDetail.setTask39TrialDateConfirmedWithBottler("");
//						}else {
//							taskDetail.setTask39TrialDateConfirmedWithBottler(weekOfYear + "/" + dateArray[0] + "");
//						}
//					
//					}
//
//				} 
				else if (isCellExist(attributeValue, "AMU")) {
					if (isNotValidValue()) {
						projectDetail.setEarliestTrialOr1stProd("");
					} else{
						String date = UtilService.getValidDate(lastContents);
						String[] dateArray = date.split("-");
						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
							Integer.parseInt(dateArray[1]),
							Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
						if(dateArray[0].equals("1899")) {
							projectDetail.setEarliestTrialOr1stProd("");
						}else {
							projectDetail.setEarliestTrialOr1stProd(weekOfYear + "/" + dateArray[0] + "");
						}
					}

				}
				// task 13 a
				else if (isCellExist(attributeValue, "ANB")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask13aIsCompleted("");

					} else
						taskDetail.setTask13aIsCompleted(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "ANC")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask13aIsActive("");

					} else
						taskDetail.setTask13aIsActive(lastContents.equals("1") ? "true" : "false");

				} else if (isCellExist(attributeValue, "AND")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask13aDuration("0");

					} else
						taskDetail.setTask13aDuration(lastContents);

				} else if (isCellExist(attributeValue, "ANE")

				) {
					if (isNotValidValue() || taskDetail.getTask13Duration().equals("0")) {
						taskDetail.setTask13aStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask13aStartDate(date);

					}
				} else if (isCellExist(attributeValue, "ANH")

				) {
					if (isNotValidValue() || taskDetail.getTask13Duration().equals("0")) {
						taskDetail.setTask13aActualStartDate("");

					} else {
						String date = UtilService.getNextMonday(lastContents);
						taskDetail.setTask13aActualStartDate(date);

					}
				} else if (isCellExist(attributeValue, "ANI")

				) {
					if (isNotValidValue() || taskDetail.getTask13Duration().equals("0")) {
						taskDetail.setTask13aDuration("0");

					} else {
						Integer duration = (Integer.parseInt(taskDetail.getTask13Duration())
								+ Integer.parseInt(lastContents)) / 7;
						taskDetail.setTask13aDuration(duration.toString());

					}
				} else if (isCellExist(attributeValue, "ANJ")

				) {
					if (isNotValidValue() || taskDetail.getTask13Duration().equals("0")) {
						taskDetail.setTask13aDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask13aDueDate(date);

					}
				} else if (isCellExist(attributeValue, "ANM")

				) {
					if (isNotValidValue() || taskDetail.getTask13Duration().equals("0")) {
						taskDetail.setTask13aActualDueDate("");

					} else {
						String date = UtilService.getNextSunday(lastContents);
						taskDetail.setTask13aActualDueDate(date);

					}
				} else if (isCellExist(attributeValue, "ANP")) {
					if (isNotValidValue()) {
						projectDetail.setBottlerCommunicationStatus("NONE");
					} else
						if(lastContents.equalsIgnoreCase("NO COMS TO BOTTLER"))
						projectDetail.setBottlerCommunicationStatus("NO_COMS_TO_BOTTLER");

				} else if (isCellExist(attributeValue, "ANQ")) {
					if (isNotValidValue()) {
						projectDetail.setHBCIssuanceType("");
					} else
						projectDetail.setHBCIssuanceType(lastContents);

				}
				
				else if (isCellExist(attributeValue, "ANW")) {
					if (isNotValidValue()) {
						projectDetail.setLocalBOMInSAp("");
					} else
						projectDetail.setLocalBOMInSAp(lastContents);

				}

				else if (isCellExist(attributeValue, "AOB")

				) {
					if (isNotValidValue()) {
						taskDetail.setTask36AllowProdSchedule("");
						projectDetail.setAllowProdSchedule("");

					} else {
						taskDetail.setTask36AllowProdSchedule(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
						projectDetail.setAllowProdSchedule(
								lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
					}

				} else if (isCellExist(attributeValue, "AOC")

					) {
						if (isNotValidValue()) {
							projectDetail.setBomSentActualDate("");

						} else {
							String date = UtilService.getValidDate(lastContents);
							String[] dateArray = date.split("-");
							LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
								Integer.parseInt(dateArray[1]),
								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
							int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
							projectDetail.setBomSentActualDate(weekOfYear + "/" + dateArray[0] + "");
						}

					} else if (isCellExist(attributeValue, "AOV")) {
					if (isNotValidValue()) {
						projectDetail.setOPSPlanner("");
					} else
						projectDetail.setOPSPlanner(lastContents);

				} else if (isCellExist(attributeValue, "AOX")

				) {
					if (isNotValidValue()) {
						data.setR_PO_CP_PROJECT_TYPE("");

					} else
						data.setR_PO_CP_PROJECT_TYPE(lastContents);

				} else if (isCellExist(attributeValue, "AOY")

				) {
					if (isNotValidValue()) {
						data.setR_PO_PROJECT_SUB_TYPE("");

					} else
						data.setR_PO_PROJECT_SUB_TYPE(lastContents);

				}

				else if (isCellExist(attributeValue, "AOZ")

				) {
					if (isNotValidValue()) {
						data.setR_PO_PROJECT_DETAIL("");

					} else
						data.setR_PO_PROJECT_DETAIL(lastContents);

				} else if (isCellExist(attributeValue, "APA")

				) {
					if (isNotValidValue()) {
						data.setR_PO_PM_DELIVERY_QUARTER("");
					} else
						data.setR_PO_PM_DELIVERY_QUARTER(lastContents);

				} else if (isCellExist(attributeValue, "APB")

				) {
					if (isNotValidValue()) {
						data.setNumberOfLinkedMarkets("");
					} else {
						data.setNumberOfLinkedMarkets(lastContents);
					}

				} else if (isCellExist(attributeValue, "APC")) {
					if (isNotValidValue()) {
						projectDetail.setOther1("");
					} else
						projectDetail.setOther1(lastContents);

				} else if (isCellExist(attributeValue, "APD")) {
					if (isNotValidValue()) {
						projectDetail.setOther2("");
					} else
						projectDetail.setOther2(lastContents);

				} else if (isCellExist(attributeValue, "APE")) {
					if (isNotValidValue()) {
						projectDetail.setComments("");
					} else {
						projectDetail.setComments(lastContents);
					}
					

				}
				
				else if (isCellExist(attributeValue, "APR")) {
					if (isNotValidValue()) {
						projectDetail.setPosiibleNotificationRequirement("");
					} else {
						projectDetail.setPosiibleNotificationRequirement(lastContents.equals("Y") ||
								  lastContents.equals("1") ? "true" : "false");
					}
						

				}
				else if (isCellExist(attributeValue, "AQE")) { 
					if (isNotValidValue()) {
						projectDetail.setRegulatoryComments("");
					} else
						projectDetail.setRegulatoryComments(lastContents); 
				}
				
				else if (isCellExist(attributeValue, "AQG")) { 
					if (isNotValidValue()) {
						projectDetail.setTrialQuality(""); 
					  } else 
						projectDetail.setTrialQuality(lastContents);
					  
				} 
				
				else if (isCellExist(attributeValue, "AQH")) { 
					if (isNotValidValue()) {
						projectDetail.setTrialNPD(""); 
					} else 
						projectDetail.setTrialNPD(lastContents);
				} 
				else if (isCellExist(attributeValue, "AQI")) { 
					if (isNotValidValue()) {
					  projectDetail.setOnsiteRemoteNotRequired(""); } 
					else
					  projectDetail.setOnsiteRemoteNotRequired(lastContents);
					  
				} else if (isCellExist(attributeValue, "AQJ")) { 
					if (isNotValidValue()) {
					  projectDetail.setTrialProtocolWrittenBy(""); } 
					else
					  projectDetail.setTrialProtocolWrittenBy(lastContents);
					  
				} else if (isCellExist(attributeValue, "AQK")) { 
					if (isNotValidValue()) {
					  projectDetail.setProtocolIssued(""); } 
					else
					  projectDetail.setProtocolIssued(lastContents.equals("Y") ||
					  lastContents.equals("1") ? "true" : "false");
					  
				} else if (isCellExist(attributeValue, "AQL")) { 
					if (isNotValidValue()) {
					  projectDetail.setTastingRequirement(""); } 
					else
					  projectDetail.setTastingRequirement(lastContents);
					  
				} else if (isCellExist(attributeValue, "AQM")) { 
					if (isNotValidValue()) {
					  projectDetail.setQAQAReleaseDate(""); } 
					else {
						String date = util.getValidDate(lastContents);
						projectDetail.setQAQAReleaseDate(date);
					}
					
					  
		  		} else if (isCellExist(attributeValue, "AQN")) { 
		  			if (isNotValidValue()) {
					  projectDetail.setTrialOverallComments(""); } 
		  			else
					  projectDetail.setTrialOverallComments(lastContents);	  
				 }
		  		else if (isCellExist(attributeValue, "AQO")) { 
		  			if (isNotValidValue()) {
					  projectDetail.setSiteAuditRequired(""); } 
		  			else
					  projectDetail.setSiteAuditRequired(lastContents.equals("Y") ||
					  lastContents.equals("1") ? "true" : "false");
					  
					  }
		  		else if (isCellExist(attributeValue, "AQP")) { 
		  			if (isNotValidValue()) {
					  projectDetail.setWhoWillAudit(""); } 
		  			else
					  projectDetail.setWhoWillAudit(lastContents);
					  
					  }
		  		else if (isCellExist(attributeValue, "AQQ")) { 
		  			if (isNotValidValue()) {
					  projectDetail.setAuditDate(""); } 
		  			else {
		  				String date = UtilService.getValidDate(lastContents);
//						String[] dateArray = date.split("-");
//						LocalDate weekDate = LocalDate.of(Integer.parseInt(dateArray[0]),
//								Integer.parseInt(dateArray[1]),
//								Integer.parseInt(dateArray[2].substring(0, 2).replace("T", "")));
//						int weekOfYear = weekDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
		  				
						projectDetail.setAuditDate(date.equals("1899-12-31T00:00:00Z") ? "" : date);
		  			}				  
				 }
		  		else if (isCellExist(attributeValue, "AQR")) { 
		  			if (isNotValidValue()) {
					  projectDetail.setSiteApproved(""); } 
		  			else
					  projectDetail.setSiteApproved(lastContents.equals("Y") ||
					  lastContents.equals("1") ? "true" : "false");
					  
					  }
		  		else if (isCellExist(attributeValue, "AQS")) {
		  			if (isNotValidValue()) {
					  projectDetail.setPlantSpecificBatchingInstructionsReapplicationOnly(""); }
					else projectDetail.setPlantSpecificBatchingInstructionsReapplicationOnly(
					  lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
					  
					  }
		  		else if (isCellExist(attributeValue, "AQT")) {
		  			if (isNotValidValue()) {
					  projectDetail.setWhoWillComplete(""); }
					else 
						projectDetail.setWhoWillComplete(lastContents);
					  
					  }
		  		 else if (isCellExist(attributeValue, "AQU")) { 
		  			 if (isNotValidValue()) {
					  projectDetail.setPlantSpecificBatchingInstructionsComments(""); }
		  			 else
					  projectDetail.setPlantSpecificBatchingInstructionsComments(lastContents);
					  
					  }
		  		 else if (isCellExist(attributeValue, "AQW")) { 
		  			 if (isNotValidValue() || lastContents.equalsIgnoreCase("0") ) {
					  projectDetail.setTrialDateConfirmedWithBottler(""); }
		  			 else {
		  				String value = lastContents.replaceAll("-", "/");
						projectDetail.setTrialDateConfirmedWithBottler(value);
		  			 }
					  
					  }
		  		 else if (isCellExist(attributeValue, "AQY")) { 
		  			 if (isNotValidValue() || lastContents.equalsIgnoreCase("0")) {
		  				 projectDetail.setCanCompany1("");
		  				projectDetail.setNameOfCanCompanys("");
//		  				logger.info("can company empty " + lastContents);
					  }	 else {
//						  logger.info("can company 1 " + lastContents);
						  
		  				 if(lastContents != null || !lastContents.isEmpty() 
//		  						 && lastContents.length() > 1
		  						) {
		  					projectDetail.setNameOfCanCompanys(lastContents);
		  					projectDetail.setCanCompany1(lastContents);
		  				 }
		  			 }
					  
					  
					  }
		  		 else if (isCellExist(attributeValue, "AQZ")) { 
		  			if (isNotValidValue() || lastContents.equalsIgnoreCase("0")) {
		  				 projectDetail.setCanCompany2("");
//		  				projectDetail.setNameOfCanCompanys("");
					  }	 else {
		  				 if((lastContents != null || !lastContents.isEmpty()) && projectDetail.getNameOfCanCompanys().length() > 1 
//		  						&& lastContents.length() > 1
		  						) {
		  					 String value = projectDetail.getNameOfCanCompanys().concat(",").concat(lastContents);
		  					projectDetail.setNameOfCanCompanys(value);
		  					projectDetail.setCanCompany2(lastContents);
		  				 }
		  			 }
					  
					  }
		  		else if (isCellExist(attributeValue, "ARA")) {
		  			if (isNotValidValue() || lastContents.equalsIgnoreCase("0")) {
		  				 projectDetail.setCanCompany3("");
//		  				projectDetail.setNameOfCanCompanys("");
					  }	 else {
		  				 if((lastContents != null || !lastContents.isEmpty()) && projectDetail.getNameOfCanCompanys().length() > 1 
//		  						&& lastContents.length()> 1
		  						) {
		  					 String value = projectDetail.getNameOfCanCompanys().concat(",").concat(lastContents);
		  					projectDetail.setNameOfCanCompanys(value);
		  					projectDetail.setCanCompany3(lastContents);
		  				 }
		  			 }
						
					requestList.add(data);
					tasksList.add(taskDetail);
					projectDetailsList.add(projectDetail);

					System.out.println(count++);
					System.out.println(data);
					// System.out.println(taskDetail);
					// System.out.println(projectDetailsList);
					data = null;
					taskDetail = null;
					projectDetail = null;
				}
		  	
				

				
//				  else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setFinalRegistrationDossier(""); } else
//				  projectDetail.setFinalRegistrationDossier(lastContents.equals("Y") ||
//				  lastContents.equals("1")?"true": "false");
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setFinalPreProductionRegistartion(""); } else
//				  projectDetail.setFinalPreProductionRegistartion(lastContents.equals("Y") ||
//				  lastContents.equals("1")? "true":"false");
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setFinalPostProductionRegistration(""); } else
//				  projectDetail.setFinalPostProductionRegistration(lastContents.equals("Y") ||
//				  lastContents.equals("1")? "true":"false");
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setProjectHealth(""); } else
//				  projectDetail.setProjectHealth(lastContents);
//				  
//				  }else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setLeadTimeTargetWks(""); } else
//				  projectDetail.setLeadTimeTargetWks(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setRegionTechManager(""); } else
//				  projectDetail.setRegionTechManager(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setProdInfoSheetSharedTarget(""); } else
//				  projectDetail.setProdInfoSheetSharedTarget(lastContents);
//				  //change it to
//				  week/year
//				 
//				
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setProdInfoSheetSharedActual(""); } else
//				  projectDetail.setProdInfoSheetSharedActual(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setPosiibleNotificationRequirement(""); } else
//				  projectDetail.setPosiibleNotificationRequirement(lastContents.equals("Y") ||
//				  lastContents.equals("1") ? "true" : "false");
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) { } else
//				  projectDetail.setJobID(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setRegulatoryComments(""); } else
//				  projectDetail.setRegulatoryComments(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setRequiresPrimaryCanAW(""); } else
//				  projectDetail.setRequiresPrimaryCanAW(lastContents.equals("Y") ||
//				  lastContents.equals("1") ? "true" : "false");
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setPrimaryCanAWAvailableToStarted(""); } else
//				  projectDetail.setPrimaryCanAWAvailableToStarted(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setRequiresSecCanAW(""); } else
//				  projectDetail.setRequiresSecCanAW(lastContents.equals("Y") ||
//				  lastContents.equals("1") ? "true" : "false");
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setSeconCansAWAvailableToStarted(""); } else
//				  projectDetail.setSeconCansAWAvailableToStarted(lastContents.equals("Y") ||
//				  lastContents.equals("1") ? "true" : "false");
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setAreAllAWTasksCompletedDevelopmentRouting(""); } else
//				  projectDetail.setAreAllAWTasksCompletedDevelopmentRouting(lastContents.equals
//				  ("Y") || lastContents.equals("1") ? "true" : "false");
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setHowManyPieces(""); } else
//				  projectDetail.setHowManyPieces(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setWhatItemsDieLineAvai(""); } else
//				  projectDetail.setWhatItemsDieLineAvai(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setTechnicalOrQualitySupervisionRequired(""); } else
//				  projectDetail.setTechnicalOrQualitySupervisionRequired(lastContents.equals(
//				  "Y") || lastContents.equals("1") ? "true" : "false");
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setSiteAuditRequired(""); } else
//				  projectDetail.setSiteAuditRequired(lastContents.equals("Y") ||
//				  lastContents.equals("1") ? "true" : "false");
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setHBCRPEMESBCreated(""); } else
//				  projectDetail.setHBCRPEMESBCreated(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setPlantSpecificBatchingInstructionsReapplicationOnly(""); }
//				  else projectDetail.setPlantSpecificBatchingInstructionsReapplicationOnly(
//				  lastContents.equals("Y") || lastContents.equals("1") ? "true" : "false");
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setWeekDue(""); } else projectDetail.setWeekDue(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setWhoWillComplete(""); } else
//				  projectDetail.setWhoWillComplete(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setCompleted(""); } else
//				  projectDetail.setCompleted(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setPlantSpecificBatchingInstructionsComments(""); } else
//				  projectDetail.setPlantSpecificBatchingInstructionsComments(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setQuality(""); } else projectDetail.setQuality(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setNPD(""); } else projectDetail.setNPD(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setOnsiteRemoteNotRequired(""); } else
//				  projectDetail.setOnsiteRemoteNotRequired(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setTrialProtocolWrittenBy(""); } else
//				  projectDetail.setTrialProtocolWrittenBy(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setProtocolIssued(""); } else
//				  projectDetail.setProtocolIssued(lastContents.equals("Y") ||
//				  lastContents.equals("1") ? "true" : "false");
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setTastingRequirement(""); } else
//				  projectDetail.setTastingRequirement(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setQAQAReleaseDate(""); } else
//				  projectDetail.setQAQAReleaseDate(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setTrialOverallComments(""); } else
//				  projectDetail.setTrialOverallComments(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setTrialMaterialsConfirmedDeliveryWeek(""); } else
//				  projectDetail.setTrialMaterialsConfirmedDeliveryWeek(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setIstProdMaterialsConfirmed(""); } else
//				  projectDetail.setIstProdMaterialsConfirmed(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setFinalAgreed1stProd(""); } else
//				  projectDetail.setFinalAgreed1stProd(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setTrialOrFirstProductionIncludedInThePlan(""); } else
//				  projectDetail.setTrialOrFirstProductionIncludedInThePlan(lastContents.equals(
//				  "Y") || lastContents.equals("1") ? "true" : "false");
//				  
//				  }else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setPalletLabelReceivedTarget(""); } else
//				  projectDetail.setPalletLabelReceivedTarget(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setCanSupplier(""); } else
//				  projectDetail.setCanSupplier(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setTargetWeek(""); } else
//				  projectDetail.setTargetWeek(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setActualWeek(""); } else
//				  projectDetail.setActualWeek(lastContents);
//				  
//				  } else if (isCellExist(attributeValue, "")) { if (isNotValidValue()) {
//				  projectDetail.setBottlerSpecificItemNumber(""); } else
//				  projectDetail.setBottlerSpecificItemNumber(lastContents);
//				  
//				  }
				 
			}

		}

		/**
		 * method to check if cell is exist in the required cell list
		 * 
		 * @param attributeValue
		 * 
		 * @param cell           -cell reference in the loop call
		 * 
		 * @return
		 */
		public boolean isCellExist(String attributeValue, String value) {
			String cell = value;
			if (attributeValue.startsWith(value) && !attributeValue.equals(value + "1")
					&& attributeValue.length() > cell.length()
					&& Character.isDigit(attributeValue.charAt(cell.length()))) {
				return true;
			}
			return false;
		}

		/**
		 * method to check valid value in the excel
		 * 
		 * @return boolean value true or false
		 */
		private boolean isNotValidValue() {
			return lastContents.equals("#N/A") || lastContents.equals("#VALUE!") || lastContents.equals("#############")
					|| lastContents.equals("#REF!") || lastContents.equals("NA") || lastContents.equals("Not Avilable")
					|| lastContents.equals("Not Available") || lastContents.equals("") || lastContents.equals(null) 
					|| lastContents.equals("Data Error") || lastContents.equals("N/A") || lastContents.equals("No Data");
		}

		/**
		 * method to find the content of the cell
		 * 
		 * @param ch[]
		 * @param start
		 * @param length
		 */
		public void characters(char[] ch, int start, int length) {
			lastContents += new String(ch, start, length);
		}

	}

}
