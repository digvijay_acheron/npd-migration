package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "shrdnpdlookupdomainmodelprint_supplier")
@Entity
public class PrintSupplier {
	
	@Id
    @Column(name = "Id", updatable = false)
    private String id;

    @Column(name = "supplier", updatable = false)
    private String supplier;
    

}
