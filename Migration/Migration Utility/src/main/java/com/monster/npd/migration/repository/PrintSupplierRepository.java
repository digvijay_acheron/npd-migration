package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.PrintSupplier;

@Repository
public interface PrintSupplierRepository extends JpaRepository<PrintSupplier, Integer> {
	
	PrintSupplier findBySupplier(String name);

}