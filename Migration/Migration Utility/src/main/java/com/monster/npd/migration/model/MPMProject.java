package com.monster.npd.migration.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@Table(name = "shrdacheronmpmcoreproject")
@Entity
@ToString
public class MPMProject  {

	@Id
	@GeneratedValue(generator = "projectsequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "projectsequence", sequenceName = "projectsequence",allocationSize=1)
    @Column(name = "id")
    private Integer id;

    @Column(name = "actual_due_date")
    private Date actualDueDate;
    
    @Column(name = "actual_start_date")
    private Date actualStartDate;
        
    @Column(name = "description")
    private String description;
    
    @Column(name = "due_date")
    private Date dueDate;

    @Column(name = "end_date")
    private Date endDate;
    
    @Column(name = "expected_duration")
    private Integer expectedDuration;
    
    @Column(name = "is_active")
    private Boolean isActive;
    
    @Column(name = "is_approval_flow")
    private Boolean isApprovalFlow; 
    
    @Column(name = "is_bulk_update")
    private Boolean isBulkUpdate;
    
    @Column(name = "is_custom_workflow")
    private Boolean isCustomWorkflow;
    
    @Column(name = "is_deleted")
    private Boolean isDeleted;
    
    @Column(name = "is_on_hold")
    private Boolean isOnHold;
    
    @Column(name = "is_standard_status")
    private Boolean isStandardStatus;
    
    @Column(name = "max_task_date")
    private Date maxTaskDate;

    @Column(name = "min_task_date")
    private Date minTaskDate;
    
    @Column(name = "original_due_date")
    private Date originalDueDate;

    @Column(name = "original_start_date")
    private Date originalStartDate;
    
    @Column(name = "otmm_folder_id")
    private String otmmFolderId;
    
    @Column(name = "progress")
    private String progress;
    
    @Column(name = "project_group")
    private String projectGroup;
    
    @Column(name = "project_id")
    private String projectId;
       
    @Column(name = "project_name")
    private String projectName;
    
    @Column(name = "project_template_ref_id")
    private String projectTemplateRefId;
    
    @Column(name = "project_type")
    private String projectType;
    
    @Column(name = "reference_id")
    private String referenceId;
    
    @Column(name = "reference_link_id")
    private String referenceLinkId;
    
    @Column(name = "start_date")
    private Date startDate;
    
    @Column(name = "thumbnail_asset_id")
    private String thumbnailAssetId;
    
    @Column(name = "thumbnail_url")
    private String thumbnailUrl;
    
    @Column(name = "time_spent")
    private Integer timeSpent;
    
    @Column(name = "version")
    private Integer version;
    
    //RelationShips
        
    @Column(name = "r_po_campaign_id")
    private Integer campaignId;
    
    @Column(name = "r_po_category_id")
    private Integer categoryId;
    
    @Column(name = "r_po_category_metadata_id")
    private Integer categoryMetadataId;
    
    @Column(name = "r_po_priority_id")
    private Integer priorityId;
    
    @Column(name = "r_po_project_owner_id")
    private Integer projectOwnerId;
    
    @Column(name = "r_po_status_id")
    private Integer statusId;
    
    @Column(name = "r_po_team_id")
    private Integer teamId;
	
	// Tracking Data
    @Column(name = "lastmodifiedby_id")
    private Integer lastModifiedBy;
    
    @Column(name = "createdby_id")
    private Integer createdBy;
	
    @Column(name = "s_createddate")
    private Date createdDate;
    
    @Column(name = "s_lastmodifieddate")
    private Date lastModifiedDate;
    
    @Column(name = "s_item_status")
    private Integer itemStatus;
    
    
    @Column(name = "s_is_temporary_copy")
    private Boolean temporaryCopy;
    
    //@Column(name = "s_temporary_copy_data")
    //private String temporaryCopyData;
    
    @Column(name = "s_organizationid")
    private int organizationid;

}
