package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.RequestCommercialMarketScope;

@Repository
public interface RequestCommercialMarketScopeRepo extends JpaRepository<RequestCommercialMarketScope, Integer> {
	
	

}
