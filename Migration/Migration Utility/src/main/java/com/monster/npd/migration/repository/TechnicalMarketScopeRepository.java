package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.TechnicalMarketScope;

@Repository
public interface TechnicalMarketScopeRepository extends JpaRepository<TechnicalMarketScope, Integer>{
	
	@Query("SELECT MAX(p.id) FROM TechnicalMarketScope p")
    Integer maxTechnicalMarketScopeId();
	
	@Query(value = "SELECT NEXT VALUE FOR dbo.tech_market_seq", nativeQuery = true)
    public Integer getCurrentVal();

}
