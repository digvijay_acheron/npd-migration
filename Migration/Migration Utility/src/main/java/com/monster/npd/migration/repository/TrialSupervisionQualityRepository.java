package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.TrialSupervisionQuality;

@Repository
public interface TrialSupervisionQualityRepository extends JpaRepository<TrialSupervisionQuality, Integer> {
	
	TrialSupervisionQuality findByUsers(String name);

}