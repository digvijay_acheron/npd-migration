package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.DraftManufacturingLocation;

@Repository
public interface DraftManufacturingLocationRepository extends JpaRepository<DraftManufacturingLocation, Integer> {
	
	DraftManufacturingLocation findByMarketDisplayName(String name);

}
