package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.GovernanceMilestone;


@Repository
public interface GovernanceMilestoneRepository extends JpaRepository<GovernanceMilestone, Integer> {
	
	@Query("SELECT MAX(p.id) FROM GovernanceMilestone p")
    Integer maxGovernanceMilestoneId();
	
	@Query(value = "SELECT NEXT VALUE FOR dbo.gov_milestone_seq", nativeQuery = true)
    public Integer getCurrentVal();

}
