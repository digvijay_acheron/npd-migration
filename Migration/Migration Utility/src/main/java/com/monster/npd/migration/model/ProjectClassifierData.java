package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@Table(name = "shrdnpdlookupdomainmodelproject_classifier_data")
@Entity
public class ProjectClassifierData {
	
	@Id
    @Column(name = "Id", updatable = false)
    private String id;

    @Column(name = "projectclassificationnumber", updatable = false)
    private String projectClassificationNumber;
    
    @Column(name = "projectclassificationdescription", updatable = false)
    private String projectClassificationDescription;
    
    @Column(name = "projecttemplateid", updatable = false)
    private Integer projectTemplateid;
    
//    @OneToOne
//    @JoinColumn(name = "r_po_project_type_id", referencedColumnName = "Id", insertable = false, updatable = false)
//    private ProjectType projectTypeId;
    
    @Column(name = "abridgedclassification", updatable = false)
    private String abridgedClassification;
    
    @Column(name = "projectduration", updatable = false)
    private Integer projectDuration;
    
    @Column(name = "projectclassificationtotalscore", updatable = false)
    private String projectClassificationtotalScore;
    
    @Column(name = "artworkdevelopmentpoints", updatable = false)
    private Integer artworkDevelopmentPoints;
    
    @Column(name = "registrationpreparationworkpoints", updatable = false)
    private Integer registrationPreparationWorkPoints;
    
    @Column(name = "secondarypackagingdevpoints", updatable = false)
    private Integer secondaryPackagingDevPoints;

}