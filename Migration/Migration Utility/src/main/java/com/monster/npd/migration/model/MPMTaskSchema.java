package com.monster.npd.migration.model;

import java.sql.Date;

import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@SolrDocument(collection = "NPD")
@NoArgsConstructor
public class MPMTaskSchema {
	
	// Default required fields ---------
    @org.springframework.data.annotation.Id
    @Indexed(name = "ITEM_ID", type = "string")
    private String item_id;
    
    @Indexed(name = "CONTENT_TYPE", type = "string")
    private String content_type;
    
    @Indexed(name = "TASK_PRIORITY_ID", type = "string")
    private Integer task_priority_id;
    
    @Indexed(name = "TASK_NAME", type = "string")
    private String task_name;

    @Indexed(name = "TASK_START_DATE", type = "string")
    private String task_start_date;
    
    @Indexed(name = "IS_ACTIVE_TASK", type = "boolean")
    private String is_active_task; 
    
    @Indexed(name = "TASK_DESCRIPTION", type = "string")
    private String task_description;
    
    @Indexed(name = "TASK_DUE_DATE", type = "string")
    private String task_due_date;
    
    @Indexed(name = "ID", type = "string")
    private String id;
    
    @Indexed(name = "TASK_STATUS_VALUE", type = "string")
    private String task_status_value;
    
    @Indexed(name = "TASK_STATUS_ID", type = "string")
    private Integer task_status_id;
    
    @Indexed(name = "TASK_PRIORITY_VALUE", type = "string")
    private String task_priority_value;
    
    @Indexed(name = "TASK_ROLE_ID", type = "string")
    private Integer task_role_id;
    
    @Indexed(name = "TASK_ROLE_VALUE", type = "string")
    private String task_role_value;
    
    @Indexed(name = "TASK_OWNER_ID", type = "string")
    private Integer task_owner_id;
    
    @Indexed(name = "TASK_OWNER_CN", type = "string")
    private String task_owner_cn;
    
    @Indexed(name = "TASK_OWNER_NAME", type = "string")
    private String task_owner_name;
    
    @Indexed(name = "IS_APPROVAL_TASK", type = "string")
    private String is_approval_task;
    
    @Indexed(name = "IS_DELETED_TASK", type = "string")
    private String is_deleted_task;
    
    @Indexed(name = "TASK_PARENT_ID", type = "string")
    private Integer task_parent_id;
    
    @Indexed(name = "TASK_MILESTONE_PROGRESS", type = "string")
    private String task_milestone_progress;
    
    @Indexed(name = "TASK_TYPE", type = "string")
    private String task_type;
    
    @Indexed(name = "TASK_ACTIVE_USER_ID", type = "string")
    private Integer task_active_user_id;
    
    @Indexed(name = "TASK_ACTIVE_USER_CN", type = "string")
    private String task_active_user_cn;
    
    @Indexed(name = "TASK_ACTIVE_USER_NAME", type = "string")
    private String task_active_user_name;
    
    @Indexed(name = "TASK_STATUS_TYPE", type = "string")
    private String task_status_type;
    
    @Indexed(name = "ORIGINAL_TASK_START_DATE", type = "string")
    private String original_task_start_date;
    
    @Indexed(name = "ORIGINAL_TASK_DUE_DATE", type = "string")
    private String original_task_due_date;
    
    @Indexed(name = "TASK_DURATION", type = "string")
    private Integer task_duration;
    
    @Indexed(name = "TASK_DURATION_TYPE", type = "string")
    private String task_duration_type;
    
    @Indexed(name = "IS_MILESTONE", type = "string")
    private String is_milestone;
    
    @Indexed(name = "PROJECT_ITEM_ID", type = "string")
    private String project_item_id;
    
    @Indexed(name = "TASK_DESCRIPTION_facet", type = "string")
    private String task_description_facet;
    
    @Indexed(name = "TASK_STATUS_VALUE_facet", type = "string")
    private String task_status_value_facet;
    
    @Indexed(name = "TASK_TYPE_facet", type = "string")
    private String task_type_facet;
    
    @Indexed(name = "TASK_ROLE_VALUE_facet", type = "string")
    private String task_role_value_facet;
    
    @Indexed(name = "TASK_OWNER_NAME_facet", type = "string")
    private String task_owner_name_facet;
    
    @Indexed(name = "TASK_PRIORITY_VALUE_facet", type = "string")
    private String task_priority_value_facet;
    
    @Indexed(name = "NPD_TASK_ADJUSTMENT_POINT", type = "string")
    private String NPD_TASK_ADJUSTMENT_POINT;
    
    @Indexed(name = "NPD_TASK_CAN_START", type = "string")
    private String NPD_TASK_CAN_START;
    
    @Indexed(name = "NPD_TASK_ADJUSTMENT_POINT_facet", type = "string")
    private String NPD_TASK_ADJUSTMENT_POINT_facet;
    
    @Indexed(name = "NPD_TASK_CAN_START_facet", type = "string")
    private String NPD_TASK_CAN_START_facet;
    
    @Indexed(name = "NPD_TASK_FINAL_CLASSIFICATION", type = "string")
    private Integer NPD_TASK_FINAL_CLASSIFICATION;
    
    @Indexed(name = "NPD_TASK_DRAFT_CLASSIFICATION", type = "string")
    private Integer NPD_TASK_DRAFT_CLASSIFICATION;
    
    @Indexed(name = "NPD_TASK_WINSHUTTLE_PROJECT", type = "string")
    private Integer NPD_TASK_WINSHUTTLE_PROJECT;
    
    @Indexed(name = "NPD_TASK_REQUIRED_COMPLETE", type = "string")
    private String NPD_TASK_REQUIRED_COMPLETE;
    
    @Indexed(name = "NPD_TASK_ISSUANCE_TYPE", type = "string")
    private String NPD_TASK_ISSUANCE_TYPE;
    
    @Indexed(name = "NPD_TASK_PALLET", type = "string")
    private String NPD_TASK_PALLET;
    
    @Indexed(name = "NPD_TASK_TRAY", type = "string")
    private String NPD_TASK_TRAY;
    
    @Indexed(name = "NPD_TASK_IS_THIS_REQUIRED", type = "string")
    private String NPD_TASK_IS_THIS_REQUIRED;
    
    @Indexed(name = "NPD_TASK_REQUIRED_COMPLETE_facet", type = "string")
    private String NPD_TASK_REQUIRED_COMPLETE_facet;
    
    @Indexed(name = "NPD_TASK_ISSUANCE_TYPE_facet", type = "string")
    private String NPD_TASK_ISSUANCE_TYPE_facet;
    
    @Indexed(name = "NPD_TASK_PALLET_facet", type = "string")
    private String NPD_TASK_PALLET_facet;
    
    @Indexed(name = "NPD_TASK_TRAY_facet", type = "string")
    private String NPD_TASK_TRAY_facet;
    
    @Indexed(name = "NPD_TASK_IS_THIS_REQUIRED_facet", type = "string")
    private String NPD_TASK_IS_THIS_REQUIRED_facet;
    
    @Indexed(name = "NPD_TASK_BOM_SENT_ACTUAL_DATE", type = "string")
    private String NPD_TASK_BOM_SENT_ACTUAL_DATE;
    
    @Indexed(name = "NPD_TASK_TRIAL_DATE", type = "string")
    private String NPD_TASK_TRIAL_DATE;
    
    @Indexed(name = "NPD_TASK_SCOPING_DOCUMENT_REQUIREMENT", type = "string")
    private String NPD_TASK_SCOPING_DOCUMENT_REQUIREMENT;
    
    @Indexed(name = "NPD_TASK_PROCESS_AUTHORITY_APPROVAL", type = "string")
    private String NPD_TASK_PROCESS_AUTHORITY_APPROVAL;
    
    @Indexed(name = "NPD_TASK_TRANSACTION_SCHEME_ANALYSIS_REQUIRED", type = "string")
    private String NPD_TASK_TRANSACTION_SCHEME_ANALYSIS_REQUIRED;
    
    @Indexed(name = "NPD_TASK_NON_PROPRIETARY_MATERIAL_COSTING_REQUIRED", type = "string")
    private String NPD_TASK_NON_PROPRIETARY_MATERIAL_COSTING_REQUIRED;
    
    @Indexed(name = "NPD_TASK_SCOPING_DOCUMENT_REQUIREMENT_facet", type = "string")
    private String NPD_TASK_SCOPING_DOCUMENT_REQUIREMENT_facet;
    
    @Indexed(name = "NPD_TASK_PROCESS_AUTHORITY_APPROVAL_facet", type = "string")
    private String NPD_TASK_PROCESS_AUTHORITY_APPROVAL_facet;
    
    @Indexed(name = "NPD_TASK_TRANSACTION_SCHEME_ANALYSIS_REQUIRED_facet", type = "string")
    private String NPD_TASK_TRANSACTION_SCHEME_ANALYSIS_REQUIRED_facet;
    
    @Indexed(name = "NPD_TASK_NON_PROPRIETARY_MATERIAL_COSTING_REQUIRED_facet", type = "string")
    private String NPD_TASK_NON_PROPRIETARY_MATERIAL_COSTING_REQUIRED_facet;
    
    @Indexed(name = "NPD_TASK_ACTUAL_DUE_DATE", type = "string")
    private String NPD_TASK_ACTUAL_DUE_DATE;
    
    @Indexed(name = "NPD_TASK_ACTUAL_START_DATE", type = "string")
    private String NPD_TASK_ACTUAL_START_DATE;
    
    @Indexed(name = "NPD_TASK_QC_RELEASE_OR_INITIAL_PROD_SCHEDULE_DIFFERENCE", type = "string")
    private Integer NPD_TASK_QC_RELEASE_OR_INITIAL_PROD_SCHEDULE_DIFFERENCE;
    
    @Indexed(name = "NPD_TASK_ALLOW_PROD_SCHEDULE_LESS_THAN_EIGHT_WEEKS", type = "string")
    private String NPD_TASK_ALLOW_PROD_SCHEDULE_LESS_THAN_EIGHT_WEEKS;
    
    @Indexed(name = "NPD_TASK_ALLOW_PROD_SCHEDULE_LESS_THAN_EIGHT_WEEKS_facet", type = "string")
    private String NPD_TASK_ALLOW_PROD_SCHEDULE_LESS_THAN_EIGHT_WEEKS_facet;
    
    @Indexed(name = "NPD_TASK_RM_ROLE_ID", type = "string")
    private Integer NPD_TASK_RM_ROLE_ID;
    
    @Indexed(name = "NPD_TASK_RM_USER_ID", type = "string")
    private Integer NPD_TASK_RM_USER_ID;
    
    @Indexed(name = "NPD_TASK_RM_ROLE", type = "string")
    private String NPD_TASK_RM_ROLE;
    
    @Indexed(name = "NPD_TASK_RM_USER", type = "string")
    private String NPD_TASK_RM_USER;
    
    @Indexed(name = "NPD_TASK_RM_USER_CN", type = "string")
    private String NPD_TASK_RM_USER_CN;
    
    @Indexed(name = "NPD_TASK_RM_ROLE_facet", type = "string")
    private String NPD_TASK_RM_ROLE_facet;
    
    @Indexed(name = "NPD_TASK_RM_USER_facet", type = "string")
    private String NPD_TASK_RM_USER_facet;
    
    @Indexed(name = "NPD_TASK_RM_USER_CN_facet", type = "string")
    private String NPD_TASK_RM_USER_CN_facet; 
    
    

}
