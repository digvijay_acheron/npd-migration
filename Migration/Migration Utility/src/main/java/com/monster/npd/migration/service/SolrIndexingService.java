package com.monster.npd.migration.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.SolrInputDocument;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.monster.npd.migration.model.MPMProjectSchema;
import com.monster.npd.migration.model.MPMTaskSchema;

@Service
public class SolrIndexingService {
	
	@Autowired
	Environment env;
	
	@Autowired
	UtilService utilService;
	
//	private static final Logger logger = LoggerFactory.getLogger(SolrIndexingService.class);
	private static final Logger logger = LogManager.getLogger(SolrIndexingService.class);
	
	public void updateTaskOnProjectEdit(SolrClient client, String copyToItemId, String copyFromItemId,
			List<MPMTaskSchema> taskschemaList ) throws SolrServerException, IOException, SolrException, InterruptedException {

		
		try {
//			String indexerQuery = "ITEM_ID:" + copyFromItemId; 
			
		
			
//			copyFromItemId = "000c2913cb82a1ea987cd6d5131f02f2.1671215";
			
			
			SolrQuery indexerQuery = new SolrQuery();
			indexerQuery.setQuery("ITEM_ID:" + copyFromItemId + " AND CONTENT_TYPE:MPM_PROJECT"  );		
			logger.info("indexerQuery " + "ITEM_ID:" + copyFromItemId + " AND CONTENT_TYPE:MPM_PROJECT"  );		
			QueryResponse response = client.query(indexerQuery);
			
			SolrDocumentList documents = response.getResults();
			
			logger.error("results " + documents.toString());
			
//			List<MPMTaskSchema> t1List = new ArrayList<MPMTaskSchema>();
//			MPMTaskSchema t1 = new MPMTaskSchema();
//			t1.setItem_id("000c2913cb82a1ea98a351a430aa1049.1524125");

//			SolrDocumentList documents = getDocuments(client, indexerQuery);
			for(MPMTaskSchema taskschema : taskschemaList) {
				logger.info("Task schema id " + taskschema.getItem_id());
				SolrInputDocument solrDoc = new SolrInputDocument();
				solrDoc.addField("ITEM_ID", taskschema.getItem_id());
				solrDoc.addField("CONTENT_TYPE", "MPM_TASK");
				if (documents != null) {

					for (SolrDocument solrDocument : documents) {
//						logger.info("solrDocument " + solrDocument.getFieldNames());
						Collection<String> keylist = solrDocument.getFieldNames();
						for (String key : keylist) {
							if (!(key.equalsIgnoreCase("ID") || key.equalsIgnoreCase("ITEM_ID")
									|| key.equalsIgnoreCase("KEYWORD_METADATA") || key.equalsIgnoreCase("CONTENT_TYPE")
									|| key.equalsIgnoreCase("_VERSION_"))) {
//								logger.info("addField " +key + " : " + solrDocument.getFieldValue(key));
								solrDoc.addField(key, Collections.singletonMap("set", solrDocument.getFieldValue(key)));

							}
						}
					}
				}
				client.add(solrDoc);
			}
			
			
//			logger.info("item " + solrDoc.get("ITEM_ID"));
//			logger.info("CONTENT_TYPE " + solrDoc.get("CONTENT_TYPE"));
			
			client.commit();
//			closeIndexerClient(client);
		} catch (SolrException e) {
			logger.error("SolrException " + e.getMessage());
		} catch (SolrServerException e) {
			logger.error("SolrServerException " + e.getMessage());
		} catch (Exception e) {
			logger.error("Exception " + e.getMessage());
		}

	}
	
	public static SolrDocumentList getDocuments(SolrClient client, String query)
			throws SolrException, SolrServerException, IOException, InterruptedException {
		SolrQuery indexerQuery = new SolrQuery();
		indexerQuery.setQuery(query);
		
		QueryResponse response = client.query(indexerQuery);
		
		SolrDocumentList results = response.getResults();
		
		logger.error("results " + results.toString());
		return results;
	}
	
	public SolrClient startIndexerClient() throws SolrException, SolrServerException, Exception {

		String solrURL = env.getProperty("solr.server.url");
		if (utilService.isNullOrEmpty(solrURL)) {
			logger.error("No Solr URL found in application properties.");
			throw new Exception("No Solr URL Configured");
		}
		HttpSolrClient client = null;
		client = new HttpSolrClient.Builder(solrURL).build();
		if (client != null) {
			client.setParser(new XMLResponseParser());
		}
		logger.error("Client Started");
		return (SolrClient) client;
	}

	public void closeIndexerClient(SolrClient client) throws Exception {
		if (client != null) {
			client.close();
			logger.error("Client Closed");
		}
	}

}
