package com.monster.npd.migration.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "shrdnpdsubmissionoperations_market_scope")
@Entity //O2NPDSubmissionTechnical_Market_Scope
public class OperationsMarketScope {
	
	 @Id
	    @Column(name = "Id")
	    private String id;

	    @Column(name = "lead_market")
	    private Boolean leadMarket;
	    
	    @Column(name = "targetdpinwarehouse")
	    private String targetDPInWarehouse;

	    
	    @Column(name = "three_month_launch_volume")
	    private Boolean threeMonthLaunchVolume;
	    
	    @Column(name = "annualisedyear1volume")
	    private Boolean annualisedYear1Volume;
	    
	    
	    @OneToOne
	    @JoinColumn(name = "R_PO_MARKET_Id", referencedColumnName = "Id", insertable = false)
	    private Market market;
	    
	    @ManyToMany(mappedBy = "operationsMarketScope", fetch = FetchType.LAZY)
	    private Set<Request> requests = new HashSet<Request>();
	    
	    @Column(name = "s_organizationid")
	    private int organizationid;

}
