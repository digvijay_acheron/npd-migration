package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@Entity
@Table(name = "shrdnpdlookupdomainmodelaudit_users")
public class AuditUsers {
	@Id
    @Column(name = "Id")
    private Integer id;

    @Column(name = "users")
    private String users;
    
    @Column(name = "s_organizationid")
    private Integer sOrganizationId;
    
   
}