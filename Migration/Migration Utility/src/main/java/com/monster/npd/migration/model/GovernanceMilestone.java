package com.monster.npd.migration.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "shrdnpdsubmissiongovernance_milestone")
@Entity
public class GovernanceMilestone {
		
    @Id
    @GeneratedValue(generator = "gov_milestone_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "gov_milestone_seq", sequenceName = "gov_milestone_seq",allocationSize=1)
    @Column(name = "Id")
    private Integer id;

    @Column(name = "stage")
    private String stage;

    @Column(name = "decision_date")
    private String decisionDate;
    
    @Column(name = "decision")
    private String decision;
    
    @Column(name = "comments")
    private String comments;
    
    @Column(name = "target_start_date")
    private String targetStartDate;
    
//    @ManyToMany(mappedBy = "governanceMilestones", fetch = FetchType.LAZY)
//    private Set<Request> requests = new HashSet<Request>();
    
    @OneToMany(mappedBy = "governanceMilestone", cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    private Set<RequestGovernanceMilestone> requestGovernanceMilestone = new HashSet<RequestGovernanceMilestone>();
    
    @Column(name = "s_organizationid")
    private int organizationid;

	@Override
	public String toString() {
		return "{id=" + id + ", stage=" + stage + ", decisionDate=" + decisionDate + ", decision="
				+ decision + ", comments=" + comments + ", targetStartDate=" + targetStartDate + "}";
	}

}
