package com.monster.npd.migration.model;


import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@Table(name = "shrdnpdsubmissionrequest")
@Entity
@ToString 
@NamedEntityGraph(name = "Request", attributeNodes = {  @NamedAttributeNode("requestType")
														,@NamedAttributeNode("secondaryPackagingType")
														,@NamedAttributeNode("projectType")
														,@NamedAttributeNode(value="platform") //,@NamedSubgraph(name = "brand-subgraph",attributeNodes = { @NamedAttributeNode("brand") })
														,@NamedAttributeNode("primaryPackaging")
//														,@NamedAttributeNode("commercialCategorisation")
														,@NamedAttributeNode(value="businessUnit", subgraph = "region-subgraph")
														,@NamedAttributeNode(value="brand") //,@NamedSubgraph(name = "platform-subgraph",attributeNodes = { @NamedAttributeNode("platformType")})
														,@NamedAttributeNode(value="governanceApproach") 
//														,@NamedAttributeNode(value="technicalMarketScope",subgraph = "marketscope-subgraph")
//														,@NamedAttributeNode(value="commericialMarketScope",subgraph = "commercial-subgraph")
														,@NamedAttributeNode(value="operationsMarketScope",subgraph = "operations-subgraph")
//														,@NamedAttributeNode(value="users")
														,@NamedAttributeNode("variant")
														,@NamedAttributeNode("earlyManufacturingSite")
														,@NamedAttributeNode("draftManufacturingLocation")
														,@NamedAttributeNode("e2ePM")
														,@NamedAttributeNode("opsPM")
														,@NamedAttributeNode("corpPM")
//														,@NamedAttributeNode("governanceMilestones")
														,@NamedAttributeNode("casePackSize")
														,@NamedAttributeNode("consumerUnitSize")
													//	,@NamedAttributeNode("productionSite")
														,@NamedAttributeNode("bottler")
														,@NamedAttributeNode("skuDetails")
														,@NamedAttributeNode("projectClassifierData")			
}
								 , subgraphs = {
										 @NamedSubgraph(name = "region-subgraph",attributeNodes = { @NamedAttributeNode("region") }) 
										 ,@NamedSubgraph(name = "marketscope-subgraph",attributeNodes = { @NamedAttributeNode("market")})
										 ,@NamedSubgraph(name = "commercial-subgraph",attributeNodes = { @NamedAttributeNode("market")})
										 //,@NamedSubgraph(name = "operations-subgraph",attributeNodes = { @NamedAttributeNode("market")})
										 
										 
								 }
					)
public class Request {

	
    @Id
//    @GeneratedValue(generator = "request_seq", strategy = GenerationType.SEQUENCE)
//	@SequenceGenerator(name = "request_seq", sequenceName = "request_seq",allocationSize=1)
    @Column(name = "id")
    private Integer id;

    @Column(name = "RequestID")
    private String requestID;
    
    @Column(name = "businesscasecomments")
    private String businessCaseComments;    

    @Column(name = "Project_Name")
    private String projectName;
    
    @Column(name = "createdby_id")
    private Integer requester;
    
//    @Column(name = "r_po_cp_project_type_id")
//    private Integer CPProjectTypeId;
//    
//    @Column(name = "r_po_project_detail_id")
//    private Integer projectDetailId;
//    
//    @Column(name = "r_po_project_sub_type_id")
//    private Integer projectSubTypeId;
//    
//    @Column(name = "r_po_pm_delivery_quarter_id")
//    private Integer PMDeliveryQuarterId;
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "r_po_cp_project_type_id", referencedColumnName = "Id", insertable = true)
    private CPProjectType cpProjectType;
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "r_po_project_sub_type_id", referencedColumnName = "Id", insertable = true)
    private  ReportingProjectSubType projectSubType;
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "r_po_project_detail_id", referencedColumnName = "Id", insertable = true)
    private ReportingProjectDetail projectDetail;
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "r_po_pm_delivery_quarter_id", referencedColumnName = "Id", insertable = true)
    private ReportingDeliveryQuarter PMDeliveryQuarter;
    
    
    @Column(name = "numberoflinkedmarkets")
    private Integer NumberOfLinkedMarkets;
    
    @Column(name = "mpmprojectid")
    private Integer mpmProjectId;
    
    @Column(name = "R_PO_COMMERCIAL_CATEGORISATION_Id")
    private Integer commercialCategorisationId;
    
 
    
//    @OneToOne
//    @Fetch(FetchMode.JOIN)
//    @JoinColumn(name = "createdby_id", referencedColumnName = "Id", insertable = false,  updatable = false)
//    private Users users;

    //@Column(name = "bottler")
    //private String bottler;
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_BOTTLER_Id", referencedColumnName = "Id", insertable = true)
    private Bottler bottler;
    
//    @Column(name = "case_pack_size")
//    private Integer casePackSize;
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_CASE_PACK_SIZE_Id", referencedColumnName = "Id", insertable = true)
    private CasePackSize casePackSize;
    
    
//    @Column(name = "consumer_unit_size")
//    private Integer consumerUnitSize;
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_CONSUMER_UNIT_SIZE_Id", referencedColumnName = "Id", insertable = true)
    private ConsumerUnitSize consumerUnitSize;
    

    @Column(name = "current_checkpoint")
    private Integer currentCheckpoint;
    
    @Column(name = "is_svp_aligned")
    private Boolean isSvpAligned;
    
//    @Column(name = "production_site")
//    private String productionSite;
//    @OneToOne
//    @Fetch(FetchMode.JOIN)
//    @NotFound(action = NotFoundAction.IGNORE)
//    @JoinColumn(name = "R_PO_PRODUCTION_SITE_Id", referencedColumnName = "Id", insertable = true)
//    private ProductionSite productionSite;
    
    
    @Column(name = "request_rationale")
    private String requestRationale;

    @Column(name = "request_status")
    private String requestStatus;
    
    //@Column(name = "sku_details")
    //private String skuDetails;
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_SKU_Details_Id", referencedColumnName = "Id", insertable = true)
    private SKUDetails skuDetails;
    
//    @Column(name = "s_createddate")
//    private Date sCreateddate;
    
//    @CreationTimestamp
//    @Generated(GenerationTime.ALWAYS)
//    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "s_createddate")
    private Date sCreateddate ;
//    = new java.sql.Date(new java.util.Date().getTime());
    
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_PROJECT_CLASSIFICATION_Id", referencedColumnName = "Id", insertable = true)
    private ProjectClassifierData projectClassifierData;
    
    
    /*@Column(name = "earlyprojectclassification")
    private String earlyProjectClassification;    
    
    @Column(name = "earlyprojectclassificationdescription")
    private String earlyProjectClassificationDescription;    */
    
    
    
    @Column(name = "issecondarypackaging")
    private Boolean IsSecondaryPackaging;    
    
    @Column(name = "newfgsap")
    private Boolean newFGSAP;    
    
    @Column(name = "newhbcformula")
    private Boolean newHBCFormula;    
    
    @Column(name = "newprimarypackaging")
    private Boolean newPrimaryPackaging;    
    
    @Column(name = "newrdformula")
    private Boolean newRDFormula;    
    
    @Column(name = "postlaunchanalysis")
    private Boolean postLaunchAnalysis;    
    
    @Column(name = "postproductionregistartion")
    private Boolean postProductionRegistartion;    
    
    @Column(name = "preproductionregistartion")
    private Boolean preProductionRegistartion;    
    
    @Column(name = "registrationdossier")
    private Boolean registrationDossier;    
    
    @Column(name = "technicaldescription")
    private String technicalDescription;     
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "R_PO_REQUEST_TYPE_Id", referencedColumnName = "Id", insertable = true)
    private RequestType requestType;
    
    //@ManyToOne
    //@Fetch(FetchMode.JOIN)
    //@JoinColumn(name = "R_PO_SECONDARY_PACKAGING_TYPE_Id", referencedColumnName = "Id", insertable = true)
    @Column(name = "secondarypackagingtype")
    private String secondaryPackagingType;
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "R_PO_PROJECT_TYPE_Id", referencedColumnName = "Id", insertable = true)
    private ProjectType projectType;
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_PLATFORMS_Id", referencedColumnName = "Id", insertable = true)
    private Platform platform;
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_PACKAGING_TYPE_Id", referencedColumnName = "Id", insertable = true)
    private PrimaryPackaging primaryPackaging;
    
    
//    @OneToOne
//    @Fetch(FetchMode.JOIN)
//    @NotFound(action = NotFoundAction.IGNORE)
//    @JoinColumn(name = "R_PO_COMMERCIAL_CATEGORISATION_Id", referencedColumnName = "Id", insertable = true)
//    private CommercialCategorisation commercialCategorisation;
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_BUSINESS_UNIT_Id", referencedColumnName = "Id", insertable = true)
    private BusinessUnit businessUnit;
    
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_BRANDS_Id", referencedColumnName = "Id", insertable = true)
    private Brand brand;
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_GOVERNANCE_APPROACH_Id", referencedColumnName = "Id", insertable = true)
    private GovernanceApproach governanceApproach;
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_VARIANT_SKU_Id", referencedColumnName = "Id", insertable = true)
    private VariantSKU variant;
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_EARLY_MANUFACTURING_SITE_Id", referencedColumnName = "Id", insertable = true)
    private EarlyManufacturingSite earlyManufacturingSite;
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_DRAFT_MANUFACTURING_LOCATION_Id", referencedColumnName = "Id", insertable = true)
    private DraftManufacturingLocation draftManufacturingLocation;

    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_E2E_PM_Id", referencedColumnName = "Id", insertable = true)
    private OpenTextIdentity e2ePM;
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_OPS_PM_Id", referencedColumnName = "Id", insertable = true)
    private OpenTextIdentity opsPM;
    
    @OneToOne
    @Fetch(FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "R_PO_CORP_PM_Id", referencedColumnName = "Id", insertable = true)
    private OpenTextIdentity corpPM;
    
    
    //ManyToMany is nothing but OneToMany in APpworks - For ManytoOne, appworks will internally create 3rd table
    //Governance Milestone
//    @ManyToMany(fetch = FetchType.LAZY)
//    @NotFound(action = NotFoundAction.IGNORE)
//    @JoinTable(name = "shrdnpdsubmissionrequestgovernance_milestone",
//            joinColumns = {
//                    @JoinColumn(name = "requestidA8D18C068F996305", referencedColumnName = "Id",
//                            nullable = false)},
//            inverseJoinColumns = {
//                    @JoinColumn(name = "governance_milestone_id",referencedColumnName = "Id",
//                            nullable = false)})
//    private Set<GovernanceMilestone> governanceMilestones = new HashSet<GovernanceMilestone>();
    
    @OneToMany(mappedBy = "requestGovMilestone", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<RequestGovernanceMilestone> requestGovernanceMilestone = new HashSet<RequestGovernanceMilestone>();
    
    
    @ManyToMany(fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinTable(name = "shrdnpdsubmissionrequesttechnical_market_scope",
            joinColumns = {
                    @JoinColumn(name = "requestidA9403B7340C3F66B", referencedColumnName = "Id",
                            nullable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "technical_market_scope_id",referencedColumnName = "Id",
                            nullable = false)})
    private Set<TechnicalMarketScope> technicalMarketScope = new HashSet<TechnicalMarketScope>();

//    @ManyToMany(fetch = FetchType.LAZY)
//    @NotFound(action = NotFoundAction.IGNORE)
//    @JoinTable(name = "shrdnpdsubmissionrequestcommercial_market_scope",
//            joinColumns = {
//                    @JoinColumn(name = "RequestidBD344B9E7CD35447", referencedColumnName = "Id",
//                            nullable = false)},
//            inverseJoinColumns = {
//                    @JoinColumn(name = "commercial_market_scope_id",referencedColumnName = "Id",
//                            nullable = false)})
//    private Set<CommercialMarketScope> commericialMarketScope = new HashSet<CommercialMarketScope>();
    
    @OneToMany(mappedBy = "request", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<RequestCommercialMarketScope> requestCommercialMarketScope = new HashSet<RequestCommercialMarketScope>();
    
    @OneToMany(mappedBy = "requestTech", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<RequestTechnicalMarketScope> requestTechnicalMarketScope = new HashSet<RequestTechnicalMarketScope>();

    @ManyToMany(fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinTable(name = "shrdnpdsubmissionrequestoperations_market_scope",
            joinColumns = {
                    @JoinColumn(name = "RequestidA58035DD40E7201D", referencedColumnName = "Id",
                            nullable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "operations_market_scope_id",referencedColumnName = "Id",
                            nullable = false)})
    private Set<OperationsMarketScope> operationsMarketScope = new HashSet<OperationsMarketScope>();
    
    @Column(name = "s_organizationid")
    private int organizationid;
    
}