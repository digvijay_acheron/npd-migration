package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.CPProjectType;

@Repository
public interface CPProjectTypeRepository extends JpaRepository<CPProjectType, Integer> {
	
	CPProjectType findByDisplayName(String name);

}
