package com.monster.npd.migration.repository;

import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.MPMDeliverableSchema;

@Repository
public interface DeliverableSolrRepository extends SolrCrudRepository<MPMDeliverableSchema, String> {

}
