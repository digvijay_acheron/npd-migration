package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.ConsumerUnitSize;

@Repository
public interface ConsumenrUnitSizeRepository  extends JpaRepository<ConsumerUnitSize, Integer> {
	
	ConsumerUnitSize findByDisplayName(String name);

}
