package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.AuditUsers;

@Repository
public interface AuditUsersRepository extends JpaRepository<AuditUsers, Integer> {

}
