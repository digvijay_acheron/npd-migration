package com.monster.npd.migration.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Table(name = "shrdacheronmpmcoredeliverable")
@Entity
@ToString
public class MPMDeliverable {
	@Id
	@GeneratedValue(generator = "deliverable_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "deliverable_seq", sequenceName = "deliverable_seq",allocationSize=1)
    @Column(name = "id")
    private Integer id;
	
    @Column(name = "action_type")
    private String actionType;

    @Column(name = "actual_due_date")
    private Date actualDueDate;
    
    @Column(name = "actual_start_date")
    private Date actualStartDate;
    
    @Column(name = "approver_assignment_type")
    private String approverAssignmentType;
    
    @Column(name = "bulk_action")
    private Boolean bulkAction;
    
    @Column(name = "deliverable_duration")
    private Integer deliverableDuration;
    
    @Column(name = "deliverable_parent_id")
    private Integer deliverableParentId;
    
    @Column(name = "deliverable_root_parent_id")
    private Integer deliverableRootParentId;
    
    @Column(name = "deliverable_sequence")
    private Integer deliverableSequence;
    
    @Column(name = "deliverable_type")
    private String deliverableType;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "due_date")
    private Date dueDate;

    @Column(name = "end_date")
    private Date endDate;
    
    @Column(name = "expected_duration")
    private Integer expectedDuration;
    
    @Column(name = "is_active")
    private Boolean isActive;
    
    @Column(name = "is_bulk_update")
    private Boolean isBulkUpdate;
    
    @Column(name = "is_deleted")
    private Boolean isDeleted;
    
    @Column(name = "is_on_hold")
    private Boolean isOnHold;
    
    @Column(name = "is_pm_assigned")
    private Boolean isPMAssigned;
    
    @Column(name = "is_uploaded")
    private Boolean isUploaded;
    
    @Column(name = "iteration_count")
    private Integer iterationCount;

    @Column(name = "name")
    private String name;
    
    @Column(name = "original_due_date")
    private Date originalDueDate;

    @Column(name = "original_start_date")
    private Date originalStartDate;
    
    @Column(name = "owner_assignment_type")
    private String ownerAssignmentType;
    
    @Column(name = "reference_id")
    private String referenceId;
       
    @Column(name = "reference_link_id")
    private String referenceLinkId;
    
    @Column(name = "start_date")
    private Date startDate;
    
    @Column(name = "time_spent")
    private Integer timeSpent;
    
    @Column(name = "uploaded_asset_id")
    private String uploadedAssetId;
    
    @Column(name = "custom_data")
    private String customData;
    
    //RelationShips
        
    @Column(name = "r_po_active_role_id_id")
    private Integer activeRoleId;
    
    @Column(name = "r_po_active_user_id_id")
    private Integer activeUserId;
    
	@Column(name = "r_po_approver_role_id_id")
	private Integer approverRoleId;
	
	@Column(name = "r_po_approver_user_id_id")
	private Integer approverUserId;
	
    @Column(name = "r_po_campaign_id")
    private Integer campaignId;
    
    @Column(name = "r_po_owner_role_id_id")
    private Integer ownerRoleId;

    @Column(name = "r_po_owner_user_id_id")
    private Integer ownerUserId;
    
    @Column(name = "r_po_priority_id")
    private Integer priorityId;
    
    @Column(name = "r_po_project_id")
    private Integer projectId;
    
    @Column(name = "r_po_status_id")
    private Integer statusId;
    
    @Column(name = "r_po_task_id")
    private Integer taskId;
    
	// Tracking Data
    @Column(name = "lastmodifiedby_id")
    private Integer lastModifiedBy;
    
    @Column(name = "createdby_id")
    private Integer createdBy;
	
    @Column(name = "s_createddate")
    private Date createdDate;
    
    @Column(name = "s_lastmodifieddate")
    private Date lastModifiedDate;
    
    @Column(name = "s_item_status")
    private Integer itemStatus;
    
    @Column(name = "s_is_temporary_copy")
    private Boolean temporaryCopy;
    
    //@Column(name = "s_temporary_copy_data")
    //private String temporaryCopyData;
    
    @Column(name = "s_organizationid")
    private int organizationid;

}
