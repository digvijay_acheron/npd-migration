package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.Market;

@Repository
public interface MarketRepository extends JpaRepository<Market, Integer> {
	
	Market findByDisplayName(String name);

}
