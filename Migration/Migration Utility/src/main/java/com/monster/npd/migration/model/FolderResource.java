package com.monster.npd.migration.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FolderResource {
	Folder folder;
}
