package com.monster.npd.migration.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Future;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.monster.npd.migration.controller.MigrationController;
import com.monster.npd.migration.model.AuditLog;
import com.monster.npd.migration.model.Bottler;
import com.monster.npd.migration.model.Brand;
import com.monster.npd.migration.model.BusinessUnit;
import com.monster.npd.migration.model.CPProjectType;
import com.monster.npd.migration.model.CasePackSize;
import com.monster.npd.migration.model.CommercialMarketScope;
import com.monster.npd.migration.model.ConsumerUnitSize;
import com.monster.npd.migration.model.DraftManufacturingLocation;
import com.monster.npd.migration.model.EarlyManufacturingSite;
import com.monster.npd.migration.model.GovernanceApproach;
import com.monster.npd.migration.model.GovernanceMilestone;
import com.monster.npd.migration.model.MPMTask;
import com.monster.npd.migration.model.MPMTaskSchema;
import com.monster.npd.migration.model.Market;
import com.monster.npd.migration.model.MaxId;
import com.monster.npd.migration.model.OpenTextIdentity;
import com.monster.npd.migration.model.Platform;
import com.monster.npd.migration.model.PrimaryPackaging;
import com.monster.npd.migration.model.ProjectClassifierData;
import com.monster.npd.migration.model.ProjectDetails;
import com.monster.npd.migration.model.ProjectOutputDetails;
import com.monster.npd.migration.model.ProjectType;
import com.monster.npd.migration.model.ReportingDeliveryQuarter;
import com.monster.npd.migration.model.ReportingProjectDetail;
import com.monster.npd.migration.model.ReportingProjectSubType;
import com.monster.npd.migration.model.Request;
import com.monster.npd.migration.model.RequestCommercialMarketScope;
import com.monster.npd.migration.model.RequestData;
import com.monster.npd.migration.model.RequestGovernanceMilestone;
import com.monster.npd.migration.model.RequestTechnicalMarketScope;
import com.monster.npd.migration.model.RequestType;
import com.monster.npd.migration.model.SKUDetails;
import com.monster.npd.migration.model.TaskDetails;
import com.monster.npd.migration.model.TechnicalMarketScope;
import com.monster.npd.migration.model.VariantSKU;
import com.monster.npd.migration.repository.AuditLogRepository;
import com.monster.npd.migration.repository.CommercialMarketScopeRepository;
import com.monster.npd.migration.repository.GovernanceMilestoneRepository;
import com.monster.npd.migration.repository.MPMProjectRepository;
import com.monster.npd.migration.repository.MPMTaskRepository;
import com.monster.npd.migration.repository.MarketRepository;
import com.monster.npd.migration.repository.OpenTextIdentityRepository;
import com.monster.npd.migration.repository.PrintSupplierRepository;
import com.monster.npd.migration.repository.RequestCommercialMarketScopeRepo;
import com.monster.npd.migration.repository.RequestGovernanceMilestoneRepo;
import com.monster.npd.migration.repository.RequestRepository;
import com.monster.npd.migration.repository.RequestTechnicalMarketScopeRepo;
import com.monster.npd.migration.repository.SecondaryPackLeadTimeRepository;
import com.monster.npd.migration.repository.TastingRequirementRepository;
import com.monster.npd.migration.repository.TechQualUserRepository;
import com.monster.npd.migration.repository.TechnicalMarketScopeRepository;
import com.monster.npd.migration.repository.TrialProtocolWrittenByRepository;
import com.monster.npd.migration.repository.TrialSupervisionNPDRepository;
import com.monster.npd.migration.repository.TrialSupervisionQualityRepository;

@Service
public class RequestMigrationService {
	
//	private static final Logger logger = LoggerFactory.getLogger(RequestMigrationService.class);
	private static final Logger logger = LogManager.getLogger(RequestMigrationService.class);
	
	@Autowired
	private MPMTaskRepository MPMTaskRepository;
	
	@Autowired
	private RequestRepository requestRepository;
	
	@Autowired
	AuditLogRepository auditLogRepository;
	
	@Autowired
	private MarketRepository marketRepository;
	
	@Autowired
	private SecondaryPackLeadTimeRepository secondaryPackLeadTimeRepository;
	
	@Autowired
	private PrintSupplierRepository printSupplierRepository;
	
	@Autowired
	private TechQualUserRepository techQualUserRepository;
	
	@Autowired
	private TrialSupervisionNPDRepository trialSupervisionNPDRepository;
	
	@Autowired
	private TrialSupervisionQualityRepository trialSupervisionQualityRepository;
	
	@Autowired
	private TrialProtocolWrittenByRepository trialSupervisionWrittenByRepository;
	
	@Autowired
	private TastingRequirementRepository tastingRequirementRepository;
	
	@Autowired
	OpenTextIdentityRepository openTextIdentityRepository;
	
	@Autowired
	TechnicalMarketScopeRepository technicalMarketScopeRepository;
	
	@Autowired
	CommercialMarketScopeRepository commercialMarketScopeRepository;
	
	@Autowired
	RequestGovernanceMilestoneRepo RequestGovernanceMilestoneRepo;
	
	@Autowired
	UtilService util;
	
	@Autowired
	ProjectMigrationService projectMigrationService;
	
	@Autowired
	TaskMigrationService taskMigrationService;
	
	@Autowired
	DeliverableMigrationService deliverableMigrationService;
	
	@Autowired
	WorkflowRulesService workflowRulesService;
	
	@Autowired
	SolrIndexingService solrIndexingService;
	
	@Autowired
	MPMProjectRepository MPMProjectRepository;
	
	@Autowired
	GovernanceMilestoneRepository governanceMilestoneRepository;
	
	@Autowired
	RequestCommercialMarketScopeRepo requestCommercialMarketScopeRepo;
	
	@Autowired
	RequestTechnicalMarketScopeRepo requestTechnicalMarketScopeRepo;
	
	@Autowired
	ProjectIndex projectIndex;
	
//	@PersistenceContext
//	EntityManager entityManager;
	
	@Autowired
	Environment env;
	
//	public void testrequest() {
//		MPMProject MPMProject = new MPMProject();
//		MPMProject.setProjectName("Success 1");
//		MPMProject.setId(ConfigService.getMaxMPMProjectId());
//		MPMProject response = MPMProjectRepository.save(MPMProject);
//		logger.info("Id " + response.getId());
//	}
	
	

	
//	@Transactional
	public MaxId createRequest(RequestData requestData, TaskDetails taskData, ProjectDetails projectInputDetail, 
			String UserName, String Password) throws SolrException, SolrServerException, Exception {
		logger.info("Inside Create Request.." + requestData);
		String message = "";
		String inputRequestType = requestData.getR_PO_REQUEST_TYPE();
		String requestTypeValue = new String();
		logger.info("Reference Id  " + requestData.getReferenceId() + " Input Request Type : " + requestData.getR_PO_REQUEST_TYPE());
//		String requestTypeValue = "";
		if (inputRequestType.equals("Lift & Shift SKU") || inputRequestType.equals("On-Pack Promo")
				|| inputRequestType.equals("Market Extension") || inputRequestType.equals("Innovation / Adaption")
				|| inputRequestType.equals("Manufacturing Extension")
				|| inputRequestType.equals("Innovation / Adaption + Reg.")
				|| inputRequestType.equals("Market Extension + Reg.") 
				|| inputRequestType.equals("Manufacturing Extension + Reg.")
				|| inputRequestType.equals("On-Pack Promo + Reg.")) {
			requestTypeValue = requestTypeValue.concat("Commercial");
			logger.info("Type value commercial " + requestTypeValue);

		} else if (inputRequestType.equals("Technical Change") ||
				inputRequestType.equals("Technical Change + Reg.")) {

			requestTypeValue = requestTypeValue.concat("Technical");
			logger.info("Type value technical " + requestTypeValue);
			}else {
				requestTypeValue = requestTypeValue.concat("");
				logger.info("No value  " + requestTypeValue);
			}
		
		RequestData newRequestData = new RequestData();
		newRequestData.setR_PO_REQUEST_TYPE(requestTypeValue);
		if(util.isNullOrEmpty(newRequestData.getR_PO_REQUEST_TYPE())) {
			message = "Unable to get the Request Type " + requestData.getR_PO_REQUEST_TYPE();
			throw new Exception(message);
		}
		
		
		Request request = new Request();
		
		PrimaryPackaging primaryPackaging = ConfigService.getPrimaryPackagingList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(requestData.getR_PO_PACKAGING_TYPE())).findAny()
				.orElse(ConfigService.getPrimaryPackagingList().stream()
						.filter(obj -> Objects.nonNull(obj.getName()))
						.filter(obj -> obj.getName().equalsIgnoreCase("NA")).findAny()
				.orElse(null));
		
//		if(util.isNullOrEmpty(primaryPackaging)) {
//			message = "Unable to get the Primary Packaging Type " + requestData.getR_PO_PACKAGING_TYPE();
//			throw new Exception(message);
//		}
		
		
		Bottler bottler = ConfigService.getBottlerList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(requestData.getR_PO_BOTTLER())).findAny()
				.orElse(ConfigService.getBottlerList().stream()
						.filter(obj -> Objects.nonNull(obj.getName()))
						.filter(obj -> obj.getName().equalsIgnoreCase("NA")).findAny().orElse(null));
//		if(util.isNullOrEmpty(bottler)) {
//			message = "Unable to get the Bottler " + requestData.getR_PO_BOTTLER();
//			throw new Exception(message);
//		}
		
		RequestType requestType = ConfigService.getRequestTypeList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(newRequestData.getR_PO_REQUEST_TYPE())).findAny().orElse(null);
		if(util.isNullOrEmpty(requestType)) {
			message = "Unable to get the requestType " + requestData.getR_PO_REQUEST_TYPE();
			throw new Exception(message);
		}
		
		VariantSKU variantSKU = ConfigService.getVariantSKUList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(requestData.getR_PO_VARIANT_SKU()))
				.findAny().orElse(ConfigService.getVariantSKUList().stream()
						.filter(obj -> Objects.nonNull(obj.getName()))
						.filter(obj -> obj.getName().equalsIgnoreCase("NA"))
						.findAny().orElse(null));
//		if(util.isNullOrEmpty(variantSKU)) {
//			message = "Unable to get the Variant SKU " + requestData.getR_PO_VARIANT_SKU();
//			throw new Exception(message);
//		}
		
		DraftManufacturingLocation draftManufacturingLocation = ConfigService.getDraftManufacturingLocationList().stream()
				.filter(obj -> Objects.nonNull(obj.getMarket()))
				.filter(obj -> Objects.nonNull(obj.getMarket().getName()))
				.filter(obj -> obj.getMarket().getName().equalsIgnoreCase(requestData.getR_PO_DRAFT_MANUFACTURING_LOCATION()))
				.findAny().orElse(null);
		if(util.isNullOrEmpty(draftManufacturingLocation)) {
			message = "Unable to get the Draft Manufacturing Location " + requestData.getR_PO_DRAFT_MANUFACTURING_LOCATION();
			throw new Exception(message);
		}

		EarlyManufacturingSite earlyManufacturingSite = ConfigService.getEarlyManufacturingSiteList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(requestData.getR_PO_EARLY_MANUFACTURING_SITE()))
				.findAny().orElse(null);
		if(util.isNullOrEmpty(earlyManufacturingSite)) {
			message = "Unable to get the Early Manufacturing Site " + requestData.getR_PO_EARLY_MANUFACTURING_SITE();
			throw new Exception(message);
		}
		
		ProjectType projectType = ConfigService.getProjectTypeList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(requestData.getR_PO_PROJECT_TYPE())).findAny().orElse(null);
		if(util.isNullOrEmpty(projectType)) {
			message = "Unable to get the Project Type " + requestData.getR_PO_PROJECT_TYPE();
			throw new Exception(message);
		}
		
		
		Platform platform = ConfigService.getPlatformList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(requestData.getR_PO_PLATFORMS())).findAny()
				.orElse(ConfigService.getPlatformList().stream()
						.filter(obj -> Objects.nonNull(obj.getName()))
						.filter(obj -> obj.getName().equalsIgnoreCase("NA")).findAny()
						.orElse(null));
//		if(util.isNullOrEmpty(platform)) {
//			message = "Unable to get the Platform " + requestData.getR_PO_PLATFORMS();
//			throw new Exception(message);
//		}
		
		Brand brand = ConfigService.getBrandList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(requestData.getR_PO_BRANDS())).findAny()
				.orElse(ConfigService.getBrandList().stream()
						.filter(obj -> Objects.nonNull(obj.getName()))
						.filter(obj -> obj.getName().equalsIgnoreCase("NA")).findAny().orElse(null));
//		if(util.isNullOrEmpty(brand)) {
//			message = "Unable to get the rand " + requestData.getR_PO_BRANDS();
//			throw new Exception(message);
//		}
		
		Market market = ConfigService.getMarketList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(requestData.getLeadMarketId())).findAny().orElse(null);
		if(util.isNullOrEmpty(market)) {
			message = "Unable to get the Lead Market " + requestData.getLeadMarketId();
			throw new Exception(message);
		}

		BusinessUnit businessUnit = ConfigService.getBusinessUnitList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(market.getBusinessUnit().getDisplayName()))
				.findAny().orElse(null);
		
		CasePackSize casePackSize = ConfigService.getCasePackSizeList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(requestData.getR_PO_CASE_PACK_SIZE()))
				.findAny().orElse(ConfigService.getCasePackSizeList().stream()
						.filter(obj -> Objects.nonNull(obj.getName()))
						.filter(obj -> obj.getName().equalsIgnoreCase("NA"))
						.findAny().orElse(null));
//		if(util.isNullOrEmpty(casePackSize)) {
//			message = "Unable to get the Case Pack Size " + requestData.getR_PO_CASE_PACK_SIZE();
//			throw new Exception(message);
//		}
		
		ConsumerUnitSize consumerUnitSize = ConfigService.getConsumerUnitSizeList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(requestData.getR_PO_CONSUMER_UNIT_SIZE()))
				.findAny().orElse(ConfigService.getConsumerUnitSizeList().stream()
						.filter(obj -> Objects.nonNull(obj.getName()))
						.filter(obj -> obj.getName().equalsIgnoreCase("0"))
						.findAny().orElse(null));
//		if(util.isNullOrEmpty(consumerUnitSize)) {
//			message = "Unable to get the Consumer Unit Size " + requestData.getR_PO_CONSUMER_UNIT_SIZE();
//			throw new Exception(message);
//		}
		
		SKUDetails SKUDetails = ConfigService.getSKUDetailsList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(requestData.getR_PO_SKU_DETAILS())).findAny()
				.orElse(ConfigService.getSKUDetailsList().stream()
						.filter(obj -> Objects.nonNull(obj.getName()))
						.filter(obj -> obj.getName().equalsIgnoreCase("NA")).findAny().orElse(null));
//		if(util.isNullOrEmpty(SKUDetails)) {
//			message = "Unable to get the SKUDetails " + requestData.getR_PO_SKU_DETAILS();
//			throw new Exception(message);
//		}
		
		ProjectClassifierData projectClassifier = ConfigService.getProjectClassifierList().stream()
				.filter(obj -> Objects.nonNull(obj.getProjectClassificationNumber()))
				.filter(obj -> obj.getProjectClassificationNumber().equalsIgnoreCase(requestData.getR_PO_PROJECT_CLASSIFICATION()))
				.findAny().orElse(null);
		if(util.isNullOrEmpty(projectClassifier)) {
			message = "Unable to get the Project Classification " + requestData.getR_PO_PROJECT_CLASSIFICATION();
			throw new Exception(message);
		}
		
		
		CPProjectType CPProjectType = ConfigService.getCPProjectTypeList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(requestData.getR_PO_CP_PROJECT_TYPE())).findAny()
				.orElse(ConfigService.getCPProjectTypeList().stream()
						.filter(obj -> Objects.nonNull(obj.getName()))
						.filter(obj -> obj.getName().equalsIgnoreCase("NA")).findAny().orElse(null));
//		if(util.isNullOrEmpty(CPProjectType)) {
//			message = "Unable to get the CP Project type " + requestData.getR_PO_CP_PROJECT_TYPE();
//			throw new Exception(message);
//		}
		
		ReportingProjectSubType projectSubType = ConfigService.getProjectSubTypeList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(requestData.getR_PO_PROJECT_SUB_TYPE())).findAny()
				.orElse(ConfigService.getProjectSubTypeList().stream()
						.filter(obj -> Objects.nonNull(obj.getName()))
						.filter(obj -> obj.getName().equalsIgnoreCase("NA")).findAny().orElse(null));
//		if(util.isNullOrEmpty(projectSubType)) {
//			message = "Unable to get the Project Sub Type " + requestData.getR_PO_PROJECT_SUB_TYPE();
//			throw new Exception(message);
//		}
		
		ReportingProjectDetail projectDetail = ConfigService.getProjectDetailList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(requestData.getR_PO_PROJECT_DETAIL())).findAny()
				.orElse(ConfigService.getProjectDetailList().stream()
						.filter(obj -> Objects.nonNull(obj.getName()))
						.filter(obj -> obj.getName().equalsIgnoreCase("NA")).findAny().orElse(null));
//		if(util.isNullOrEmpty(projectDetail)) {
//			message = "Unable to get the Project Detail " + requestData.getR_PO_PROJECT_DETAIL();
//			throw new Exception(message);
//		}
		
		ReportingDeliveryQuarter deliveryQuarter = ConfigService.getDeliveryQuarterList().stream()
				.filter(obj -> Objects.nonNull(obj.getValue()))
				.filter(obj -> obj.getValue().equalsIgnoreCase(requestData.getR_PO_PM_DELIVERY_QUARTER())).findAny()
				.orElse(ConfigService.getDeliveryQuarterList().stream()
						.filter(obj -> Objects.nonNull(obj.getValue()))
						.filter(obj -> obj.getValue().equalsIgnoreCase("NA")).findAny().orElse(null));
//		if(util.isNullOrEmpty(deliveryQuarter)) {
//			message = "Unable to get the Delivery Quarter " + requestData.getR_PO_PM_DELIVERY_QUARTER();
//			throw new Exception(message);
//		}
		
		OpenTextIdentity e2ePM = ConfigService.getUsersList().stream()
				.filter(obj -> Objects.nonNull(obj.getId()))
				.filter(obj -> obj.getId() == Integer.parseInt(util.getPmOwnerIdentityId("Samantha"))).findAny().orElse(null);
		
		OpenTextIdentity corpPM = ConfigService.getUsersList().stream()
				.filter(obj -> Objects.nonNull(obj.getId()))
				.filter(obj -> obj.getId() == Integer.parseInt(util.getPmOwnerIdentityId("Samantha"))).findAny().orElse(null);
		
		OpenTextIdentity opsPM = ConfigService.getUsersList().stream()
				.filter(obj -> Objects.nonNull(obj.getId()))
				.filter(obj -> obj.getId() == Integer.parseInt(util.getPmOwnerIdentityId("Samantha"))).findAny().orElse(null);
		
		GovernanceApproach governanceApproach = ConfigService.getgovernanceApproachList().stream()
		.filter(obj -> Objects.nonNull(obj.getDisplayName()))
		.filter(obj -> obj.getDisplayName().equalsIgnoreCase(requestData.getGovernanceApproach())).findAny()
		.orElse(ConfigService.getgovernanceApproachList().stream()
				.filter(obj -> Objects.nonNull(obj.getDisplayName()))
				.filter(obj -> obj.getDisplayName().equalsIgnoreCase("NA")).findAny().orElse(null));
				
		
//		Integer requestId = Integer.parseInt(requestData.getReferenceId().replaceAll("EM", "").replaceAll("R", ""));
				
		
		
		
		  List<GovernanceMilestone> governanceMilestoneList = new
		  ArrayList<GovernanceMilestone>(); 
		  GovernanceMilestone governanceMilestone0 = new GovernanceMilestone();
//		  governanceMilestone0.setId(governanceMilestoneRepository.getCurrentVal());
		  governanceMilestone0.setStage("CP0");
		  governanceMilestone0.setTargetStartDate(!util.isNullOrEmpty(requestData.
		  getGovMilestoneCP0TargetStartDate()) &&
		  requestData.getGovMilestoneCP0TargetStartDate().indexOf("T") > 0 ?
		  requestData.getGovMilestoneCP0TargetStartDate().split("T")[0] : null);
		  governanceMilestone0.setDecisionDate(!util.isNullOrEmpty(requestData.
		  getGovMilestoneCP0DecisionDate()) &&
		  requestData.getGovMilestoneCP0DecisionDate().indexOf("T") > 0 ?
		  requestData.getGovMilestoneCP0DecisionDate().split("T")[0] : null);
		  governanceMilestone0.setDecision("Approved");
		  governanceMilestone0.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id"))); 
		  governanceMilestoneList.add(governanceMilestone0);
//		  GovernanceMilestone governanceMilestone0Response = governanceMilestoneRepository.save(governanceMilestone0); 
//		  governanceMilestoneList.add(governanceMilestone0Response);
//		  entityManager.flush();
		  
		  
		  GovernanceMilestone governanceMilestone1 = new GovernanceMilestone();
//		  governanceMilestone1.setId(governanceMilestoneRepository.getCurrentVal());
		  governanceMilestone1.setStage("CP1");
		  governanceMilestone1.setTargetStartDate(!util.isNullOrEmpty(requestData.
		  getGovMilestoneCP1TargetStartDate()) &&
		  requestData.getGovMilestoneCP1TargetStartDate().indexOf("T") > 0 ?
		  requestData.getGovMilestoneCP1TargetStartDate().split("T")[0] : null);
		  governanceMilestone1.setDecisionDate(!util.isNullOrEmpty(requestData.
		  getGovMilestoneCP1DecisionDate()) &&
		  requestData.getGovMilestoneCP1DecisionDate().indexOf("T") > 0 ?
		  requestData.getGovMilestoneCP1DecisionDate().split("T")[0] : null);
		  governanceMilestone1.setDecision("Approved");
		  governanceMilestone1.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id"))); 
		  governanceMilestoneList.add(governanceMilestone1);
//		  governanceMilestoneRepository.save(governanceMilestone1); 

//		  GovernanceMilestone governanceMilestone1Response = governanceMilestoneRepository.save(governanceMilestone1); 
//		  governanceMilestoneList.add(governanceMilestone1Response);
		  
		  GovernanceMilestone governanceMilestone2 = new GovernanceMilestone();
//		  governanceMilestone2.setId(governanceMilestoneRepository.getCurrentVal());
		  governanceMilestone2.setStage("CP2");
		  governanceMilestone2.setTargetStartDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP2TargetStartDate()) &&
		  requestData.getGovMilestoneCP2TargetStartDate().indexOf("T") > 0 ?
		  requestData.getGovMilestoneCP2TargetStartDate().split("T")[0] : null);
		  governanceMilestone2.setDecisionDate(!util.isNullOrEmpty(requestData.
		  getGovMilestoneCP2DecisionDate()) &&
		  requestData.getGovMilestoneCP2DecisionDate().indexOf("T") > 0 ?
		  requestData.getGovMilestoneCP2DecisionDate().split("T")[0] : null);
		  governanceMilestone2.setDecision("Approved");
		  governanceMilestone2.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id"))); 
		  governanceMilestoneList.add(governanceMilestone2);
//		  governanceMilestoneRepository.save(governanceMilestone2); 
//		  GovernanceMilestone governanceMilestone2Response = governanceMilestoneRepository.save(governanceMilestone2); 
//		  governanceMilestoneList.add(governanceMilestone2Response);

		  
		  GovernanceMilestone governanceMilestone3 = new GovernanceMilestone();
//		  governanceMilestone3.setId(governanceMilestoneRepository.getCurrentVal());
		  governanceMilestone3.setStage("CP3");
		  governanceMilestone3.setTargetStartDate(!util.isNullOrEmpty(requestData.
		  getGovMilestoneCP3TargetStartDate()) &&
		  requestData.getGovMilestoneCP3TargetStartDate().indexOf("T") > 0 ?
		  requestData.getGovMilestoneCP3TargetStartDate().split("T")[0] : null);
		  governanceMilestone3.setDecisionDate(!util.isNullOrEmpty(requestData.
		  getGovMilestoneCP3DecisionDate()) &&
		  requestData.getGovMilestoneCP3DecisionDate().indexOf("T") > 0 ?
		  requestData.getGovMilestoneCP3DecisionDate().split("T")[0] : null);
		  governanceMilestone3.setDecision("Approved");
		  governanceMilestone3.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id"))); 
		  governanceMilestoneList.add(governanceMilestone3);
//		  governanceMilestoneRepository.save(governanceMilestone3);
//		  GovernanceMilestone governanceMilestone3Response = governanceMilestoneRepository.save(governanceMilestone3); 
//		  governanceMilestoneList.add(governanceMilestone3Response);//

		  
		  GovernanceMilestone governanceMilestone4 = new GovernanceMilestone();
//		  governanceMilestone4.setId(governanceMilestoneRepository.getCurrentVal());
		  governanceMilestone4.setStage("CP4");
		  governanceMilestone4.setTargetStartDate(!util.isNullOrEmpty(requestData.
		  getGovMilestoneCP4TargetStartDate()) &&
		  requestData.getGovMilestoneCP4TargetStartDate().indexOf("T") > 0 ?
		  requestData.getGovMilestoneCP4TargetStartDate().split("T")[0] : null);
		  governanceMilestone4.setDecisionDate(!util.isNullOrEmpty(requestData.
		  getGovMilestoneCP4DecisionDate()) &&
		  requestData.getGovMilestoneCP4DecisionDate().indexOf("T") > 0 ?
		  requestData.getGovMilestoneCP4DecisionDate().split("T")[0] : null);
		  governanceMilestone4.setDecision("Approved");
		  governanceMilestone4.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id"))); 
		  governanceMilestoneList.add(governanceMilestone4);
//		  governanceMilestoneRepository.save(governanceMilestone4); 
//		  GovernanceMilestone governanceMilestone4Response = governanceMilestoneRepository.save(governanceMilestone4); 
//		  governanceMilestoneList.add(governanceMilestone4Response);

		 		
		
//		request.getGovernanceMilestones().addAll(governanceMilestoneList);
		request.setBottler(bottler);
//		Integer requestId = ConfigService.getMaxRequestId();
//		Integer requestId = requestRepository.getCurrentVal();
//		request.setId(requestId);
		
		  String regex = "^0+(?!$)";
		  String reqId = requestData.getReferenceId().replaceAll("EM", "").replaceAll("em", "").replaceAll("Em", "")
				  .replaceAll("eM", "");
		  reqId = reqId.replaceAll(regex, "");
		  
		logger.info("Req Id" + reqId);	
//		request.setRequestID("EM".concat(reqId).concat("R"));
		request.setId(Integer.parseInt(reqId));
//		request.setRequestID("EM5908R");
//		request.setId(5908);
		request.setBrand(brand);
		request.setGovernanceApproach(governanceApproach);
		request.setBusinessCaseComments(requestData.getBusinessCaseComments());
		request.setBusinessUnit(businessUnit);
		request.setCasePackSize(casePackSize);
		request.setConsumerUnitSize(consumerUnitSize);
		request.setCorpPM(corpPM);
		request.setE2ePM(e2ePM);
		request.setOpsPM(opsPM);
		request.setRequester(e2ePM.getId());
		request.setCurrentCheckpoint(1);
		request.setSCreateddate(!util.isNullOrEmpty(requestData.getRequestedDate()) && requestData.getRequestedDate().indexOf("T") > 0
				? Date.valueOf(requestData.getRequestedDate().split("T")[0]) : null);
		
		request.setDraftManufacturingLocation(draftManufacturingLocation);
		request.setEarlyManufacturingSite(earlyManufacturingSite);
		request.setProjectType(projectType);
//		request.setGovernanceApproach(governanceApproach);
		request.setIsSecondaryPackaging(requestData.getIsSecondaryPackaging().equalsIgnoreCase("true") ? true : false);
		request.setNewFGSAP(requestData.getNewFGSAP().equalsIgnoreCase("true") ? true : false);
		request.setNewHBCFormula(requestData.getNewHBCFormula().equalsIgnoreCase("true") ? true : false);
		request.setNewPrimaryPackaging(requestData.getNewPrimaryPackaging().equalsIgnoreCase("true") ? true : false);
		request.setNewRDFormula(requestData.getNewRDFormula().equalsIgnoreCase("true") ? true : false);

		request.setPlatform(platform);
		request.setPostProductionRegistartion(requestData.getPostProductionRegistration().equalsIgnoreCase("true") ? true : false);
		request.setPreProductionRegistartion(requestData.getPreProductionRegistartion().equalsIgnoreCase("true") ? true : false);
		request.setPrimaryPackaging(primaryPackaging);
		request.setProjectClassifierData(projectClassifier);
		request.setProjectName(requestData.getProjectName());
		request.setRegistrationDossier(requestData.getRegistrationDossier().equalsIgnoreCase("true") ? true : false);
		request.setRequestRationale(requestData.getRequestRationale());
		request.setRequestStatus(requestData.getRequestStatus().equalsIgnoreCase("Active") ? "InProgress" : "Completed");
		request.setRequestType(requestType);
		request.setSecondaryPackagingType(requestData.getSecondaryPackagingType());
		request.setSkuDetails(SKUDetails);
		request.setTechnicalDescription(requestData.getTechnicalDescription());
		request.setVariant(variantSKU);
		request.setCpProjectType(CPProjectType);
		request.setProjectDetail(projectDetail);
		request.setProjectSubType(projectSubType);
		request.setPMDeliveryQuarter(deliveryQuarter);
		request.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
		request.setCommercialCategorisationId(2);
		
		
//		logger.info(" id " + Integer.parseInt(env.getProperty("npd.org.id")));
		
		
//		request.setMpmProjectId(projectOutputDetails.getMpmProjectId());
		logger.info(" Request : " + request);
		Request response = requestRepository.save(request);
		logger.info(" Request Created : " + response.getId());
		
		
		
	
		  
		  Integer noOfLinkedMarkets = 0;
		  if(newRequestData.getR_PO_REQUEST_TYPE().equalsIgnoreCase("Technical")) {
			  List<TechnicalMarketScope> techMarketScopeList = new ArrayList<TechnicalMarketScope>(); 
			  TechnicalMarketScope techMarketScope = new TechnicalMarketScope(); 
			  techMarketScope.setLeadMarket(true);
			  techMarketScope.setMarket(market);
			  techMarketScope.setTargetDPInWarehouse(requestData.
			  getLeadMarketTargetDPInWarehouse()); 
			  techMarketScope.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id"))); 
			  techMarketScopeList.add(techMarketScope); 
	
			  
			  List<String> otherLinkedMarkets = new ArrayList<String>();
			  if(requestData.getOtherLinkedMarketsId().indexOf(",") > 0) 
			  {
				  otherLinkedMarkets = Arrays.asList(requestData.getOtherLinkedMarketsId().split(",")); 
			  } else {
				  otherLinkedMarkets.add(requestData.getOtherLinkedMarketsId()); 
			  } 
			  for(String otherLinkedMarket: otherLinkedMarkets) { 
				  Market tempMarket = marketRepository.findByDisplayName(otherLinkedMarket);
				  if(!util.isNullOrEmpty(tempMarket)) { 
					  TechnicalMarketScope tempTechMarketScope = new TechnicalMarketScope();
					  tempTechMarketScope.setLeadMarket(false);
					  tempTechMarketScope.setMarket(tempMarket);
					  tempTechMarketScope.setTargetDPInWarehouse(requestData.
					  getLeadMarketTargetDPInWarehouse()); 
					  tempTechMarketScope.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id"))); 
					  techMarketScopeList.add(tempTechMarketScope);
					  noOfLinkedMarkets++; 
				  }
			  
			  }
		  
			  for(TechnicalMarketScope technicalMarketScope : techMarketScopeList) {
				  TechnicalMarketScope technicalMarketScopeResponse = technicalMarketScopeRepository.save(technicalMarketScope);
				  logger.info("Tech market scope id " + technicalMarketScopeResponse.getId());
				  RequestTechnicalMarketScope requestTechnicalMarketScope = new RequestTechnicalMarketScope(); 
				  Request req =	 requestRepository.findById(response.getId()).orElse(null);
				  logger.info(" inside req id " + req.getProjectName());
				  req.setNumberOfLinkedMarkets(noOfLinkedMarkets);
				  requestTechnicalMarketScope.setTechnicalMarketScope(technicalMarketScopeResponse);
				  requestTechnicalMarketScope.setRequestTech(req);
				  requestTechnicalMarketScope.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
				  
				  technicalMarketScopeResponse.getRequestTechnicalMarketScope().add(requestTechnicalMarketScope);
				  req.getRequestTechnicalMarketScope().add(requestTechnicalMarketScope);
				  
				  requestRepository.save(req); 
				  technicalMarketScopeRepository.save(technicalMarketScopeResponse); 
				  requestTechnicalMarketScopeRepo.save(requestTechnicalMarketScope); 
				  }
		  
		  }
		  
		  if(newRequestData.getR_PO_REQUEST_TYPE().equalsIgnoreCase("Commercial")) {
			  List<CommercialMarketScope> commercialMarketScopeList = new ArrayList<CommercialMarketScope>(); 
			  CommercialMarketScope commercialMarketScope = new CommercialMarketScope();
			  commercialMarketScope.setLeadMarket(true);
			  commercialMarketScope.setMarket(market);
			  commercialMarketScope.setTargetDPInWarehouse(requestData.getLeadMarketTargetDPInWarehouse());
			  commercialMarketScope.setNSVCase(requestData.getNSV());
			  commercialMarketScope.setAnnualisedYear1Volume(requestData.getVolume1Year());
			  commercialMarketScope.setThreeMonthLaunchVolume(requestData.getVolume3Months()); 
			  commercialMarketScope.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));  
			  commercialMarketScopeList.add(commercialMarketScope);
		  
			  List<String> otherLinkedMarkets = new ArrayList<String>();
			  if(requestData.getOtherLinkedMarketsId().indexOf(",") > 0) 
			  {
				  otherLinkedMarkets = Arrays.asList(requestData.getOtherLinkedMarketsId().split(",")); 
			  } else {
				  otherLinkedMarkets.add(requestData.getOtherLinkedMarketsId()); 
			  } 
			  for(String otherLinkedMarket: otherLinkedMarkets) { 
				  Market tempMarket = marketRepository.findByDisplayName(otherLinkedMarket);
				  if(!util.isNullOrEmpty(tempMarket)) 
				  	{ 
					  CommercialMarketScope tempcommercialMarketScope = new CommercialMarketScope();
					  tempcommercialMarketScope.setLeadMarket(false);
					  tempcommercialMarketScope.setMarket(tempMarket); //
					  tempcommercialMarketScope.setTargetDPInWarehouse(requestData.getLeadMarketTargetDPInWarehouse());
					  tempcommercialMarketScope.setNSVCase(requestData.getNSV());
					  tempcommercialMarketScope.setAnnualisedYear1Volume(requestData.getVolume1Year()); 
					  tempcommercialMarketScope.setThreeMonthLaunchVolume(requestData.getVolume3Months());
					  tempcommercialMarketScope.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id"))); 
					  commercialMarketScopeList.add(tempcommercialMarketScope);
					  noOfLinkedMarkets++; 
				  	} 
				  }
			  
				  logger.info("noOfLinkedMarkets " + noOfLinkedMarkets);
				  for(CommercialMarketScope comMarketScope : commercialMarketScopeList) {
					  CommercialMarketScope comMarketScopeResponse = commercialMarketScopeRepository.save(comMarketScope);
					  logger.info("comMarketScope id " + comMarketScopeResponse.getId()); 
					  RequestCommercialMarketScope requestCommercialMarketScope = new RequestCommercialMarketScope(); 
					  Request req = requestRepository.findById(response.getId()).orElse(null);
					  logger.info(" inside comm req id " + req.getProjectName()); 
					  req.setNumberOfLinkedMarkets(noOfLinkedMarkets);
					  requestCommercialMarketScope.setCommercialMarketScope(comMarketScopeResponse);
					  requestCommercialMarketScope.setRequest(req);
					  requestCommercialMarketScope.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
					  
					  comMarketScopeResponse.getRequestCommercialMarketScope().add(requestCommercialMarketScope);
					  req.getRequestCommercialMarketScope().add(requestCommercialMarketScope); 
					  requestRepository.save(req); 
					  CommercialMarketScope comMarketReponse = commercialMarketScopeRepository.save(comMarketScopeResponse); 
					  logger.info("Saved Commercial Market " + comMarketReponse.getId()); 
					  requestCommercialMarketScopeRepo.save(requestCommercialMarketScope); 
					  logger.info("Saved requestCommercialMarketScopeRepo"); 					  
				  }
		  
		  }
		  
		  ProjectOutputDetails projectOutputDetails =
				  projectMigrationService.createProject(UserName, Password, response,
				  projectInputDetail, market.getDisplayName(),
				  market.getBusinessUnit().getDisplayName(), requestData, taskData, noOfLinkedMarkets);
		  
		  for(GovernanceMilestone governanceMilestone : governanceMilestoneList) {
			
					GovernanceMilestone governanceMilestoneResponse = governanceMilestoneRepository.save(governanceMilestone); 
					
					  logger.info("Gov Milestone Id : " + governanceMilestoneResponse.getId());
					  RequestGovernanceMilestone requestRequestGovernanceMilestone = new RequestGovernanceMilestone(); 
					  Request req = requestRepository.findById(response.getId()).orElse(null); 
					  logger.info(" inside gov milestone req id " + req.getProjectName());
					  req.setRequestID("EM".concat(String.valueOf(req.getId())).concat("R"));
					  req.setMpmProjectId(projectOutputDetails.getMpmProjectId());
					  requestRequestGovernanceMilestone.setGovernanceMilestone(governanceMilestoneResponse); 
					  requestRequestGovernanceMilestone.setRequestGovMilestone(req);
					  requestRequestGovernanceMilestone.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
					  
					  logger.info("hello success ..");
					  governanceMilestoneResponse.getRequestGovernanceMilestone().add(requestRequestGovernanceMilestone);
					  req.getRequestGovernanceMilestone().add(requestRequestGovernanceMilestone);
					  logger.info("hello success come ..");
					  logger.info(" inside gov milestone before saving " + req.getRequestID());
					  requestRepository.save(req);
					  logger.info(" inside gov milestone saved request " + req.getRequestID());
					  GovernanceMilestone governanceMilestoneNewResponse = governanceMilestoneRepository.save(governanceMilestoneResponse); 
					  logger.info(" Saved gov milestone " +governanceMilestoneNewResponse.getId());
					  RequestGovernanceMilestoneRepo.save(requestRequestGovernanceMilestone);
					  logger.info(" Saved requestRequestGovernanceMilestone");
					  }
		  

		List<MPMTaskSchema> taskSchemaList = taskMigrationService.createTask(requestData, taskData, 
				projectOutputDetails.getMpmProjectId(), projectOutputDetails.getProjectTeamList());
	
		deliverableMigrationService.createDeliverable(taskSchemaList, projectOutputDetails.getMpmProjectId());
		String projectItemId = projectIndex.indexProject(projectOutputDetails.getMpmProject(), response, projectInputDetail,  
				projectOutputDetails.getOtmmFolderId(),
				env.getProperty("npd.npdProject.itemId").concat(".").concat(String.valueOf(projectOutputDetails.getNpdProjectId()))
				, market.getDisplayName(), businessUnit.getDisplayName(), taskSchemaList, projectOutputDetails.getNpdProject());
		 
		workflowRulesService.createWorkflowRules(projectOutputDetails.getMpmProjectId(), 
				Integer.parseInt(request.getProjectClassifierData().getProjectClassificationNumber()));
		
		MaxId max = new MaxId();
		max.setProjectItemId(projectItemId);
		max.setTaskSchemaList(taskSchemaList);
		max.setRequestId(response.getId());
		max.setProjectId(projectOutputDetails.getMpmProjectId());
	
//		MaxId max = new MaxId();
//		max.setRequestId(response.getId());
		return max;
	}
	
	public void copyProjectToTasks(String projectItemId, List<MPMTaskSchema> taskSchemaList) throws Exception {
		
		SolrClient client = solrIndexingService.startIndexerClient();
		solrIndexingService.updateTaskOnProjectEdit(client, "", projectItemId ,taskSchemaList );
		logger.info("Project copied to Task Successfully.." +""); 
		solrIndexingService.closeIndexerClient(client);
		
	}
	

	@Async
//	@Transactional
	public Future<Integer> migrateAll(RequestData requestData, TaskDetails taskData, ProjectDetails projectInputDetail, 
			String UserName, String Password) throws SolrException, SolrServerException, Exception {
		AuditLog auditLogResponse = new AuditLog();
		try{
			MaxId max = createRequest(requestData, taskData, projectInputDetail, UserName, Password);
			copyProjectToTasks(max.getProjectItemId(), max.getTaskSchemaList());
			AuditLog auditLog = new AuditLog();
//			auditLog.setId(auditLogRepository.getCurrentVal());
//			auditLog.setReferenceid(EventUserModal.getRequestList().get(0).getReferenceId());
			auditLog.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
			auditLog.setRequestId(max.getRequestId());
			auditLog.setProjectId(max.getProjectId());
//			auditLog.setReferenceid("EM".concat(String.valueOf(max.getRequestId())).concat("R"));
			auditLog.setReferenceid(requestData.getReferenceId());
			auditLog.setStatus("Success");
			auditLog.setMessage("Success");
		
			 auditLogResponse = auditLogRepository.save(auditLog);
			 
			 logger.info("Reference Id " +requestData.getReferenceId() + " Request Id " +  max.getRequestId() 
			 	+ " Project id " + max.getProjectId());
			 return new AsyncResult<Integer>(auditLogResponse.getId());
		}catch(Exception e) {
			logger.error("Request Reference Id "+ requestData.getReferenceId());
			logger.error("Exception info ", e);
			e.printStackTrace();
			AuditLog auditLog = new AuditLog();
//			auditLog.setId(auditLogRepository.getCurrentVal());
			auditLog.setReferenceid(requestData.getReferenceId());
			auditLog.setStatus("Failed"); 
			auditLog.setMessage(e.getMessage() + e.getStackTrace());
			auditLog.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
		
			try {
				auditLogResponse = auditLogRepository.save(auditLog);
			}catch(Exception ee) {
				logger.error("Audit exception failed ", ee);
			}
			
			logger.info("Failed Audit Log " + auditLog);
			logger.info("Added " + auditLogResponse.getId() + auditLogResponse);
			return new AsyncResult<Integer>(auditLogResponse.getId());
		}
		
		
	}
	
	@Async
	public Future<Integer> call(String referenceId, String requestType ) throws InterruptedException {
		String inputRequestType = requestType;
		String requestTypeValue = "";
		logger.info("Reference Id  " + referenceId + " Input Request Type : " + requestType);
//		String requestTypeValue = "";
		if (inputRequestType.equals("Lift & Shift SKU") || inputRequestType.equals("On-Pack Promo")
				|| inputRequestType.equals("Market Extension") || inputRequestType.equals("Innovation / Adaption")
				|| inputRequestType.equals("Manufacturing Extension")
				|| inputRequestType.equals("Innovation / Adaption + Reg.")
				|| inputRequestType.equals("Market Extension + Reg.") 
				|| inputRequestType.equals("Manufacturing Extension + Reg.")
				|| inputRequestType.equals("On-Pack Promo + Reg.")) {
			requestTypeValue = requestTypeValue.concat("Commercial");
			logger.info("Type value commercial " + requestTypeValue);

		} else if (inputRequestType.equals("Technical Change") ||
				inputRequestType.equals("Technical Change + Reg.")) {

			requestTypeValue = requestTypeValue.concat("Technical");
			logger.info("Type value technical " + requestTypeValue);
			}else {
				requestTypeValue = requestTypeValue.concat("");
				logger.info("No value  " + requestTypeValue);
			}
		
		AuditLog auditLog = new AuditLog();
//		auditLog.setId(auditLogRepository.getCurrentVal());
		auditLog.setReferenceid(referenceId);
		auditLog.setStatus(!util.isNullOrEmpty(requestTypeValue) ? "Success" : "Failed"); 
		auditLog.setMessage(requestType.concat(" - ").concat(requestTypeValue));
		auditLog.setOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
		AuditLog auditLogResponse = auditLogRepository.save(auditLog);
//		MPMTask task = new MPMTask();
//		task.setId(id);
//		MPMTask response = MPMTaskRepository.save(task);
//		logger.info("Task Added " + response.getId());
	    return new AsyncResult<Integer>(auditLogResponse.getId());
	  } 
	
//	public void testing() {
//		Request request = new Request();
//		request.setRequestID("EM5907R");
//		request.setId(5907);
//		Request response = requestRepository.save(request);
//		logger.info(response);
//	}
	
}
