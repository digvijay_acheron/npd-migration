package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.Brand;

@Repository
public interface BrandRepository extends JpaRepository<Brand, Integer> {
	
	Brand findByDisplayName(String name);

}
