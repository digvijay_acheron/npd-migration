package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "shrdnpdlookupdomainmodelproject_detail")
@Entity
public class ReportingProjectDetail {
	
	@Id
    @Column(name = "Id", updatable = false)
    private Integer id;

    @Column(name = "displayname", updatable = false)
    private String displayName;
    
    @Column(name = "name", updatable = false)
    private String name;
    

}
