package com.monster.npd.migration.service;

import java.nio.charset.StandardCharsets;
import java.util.List;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.monster.npd.migration.model.APIHeaders;
import com.monster.npd.migration.model.FolderResourceContainer;
import com.monster.npd.migration.model.SecurityPolicyList;
import com.monster.npd.migration.model.Sessions;

@Service
public class OTMMAPIService {
	
//	private static final Logger logger = LoggerFactory.getLogger(OTMMAPIService.class);
	private static final Logger logger = LogManager.getLogger(OTMMAPIService.class);
	
	ObjectMapper objMapper = new ObjectMapper();
	
	public Sessions getSessions(String OTDSTicket) {
		RestTemplate restTemplate = new RestTemplate();
		String url = "https://mmqa.monsterenergy.com/otmmapi/v5/sessions";
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
		        .queryParam(APIHeaders.OTDSTICKET.getValue(), OTDSTicket);
		try {
			HttpEntity<Sessions> response = restTemplate.getForEntity(builder.toUriString(),Sessions.class);
			Sessions sessionsObj = response.getBody();
			String cookie = response.getHeaders().getFirst(HttpHeaders.SET_COOKIE);	
			sessionsObj.setJSessionID(cookie);
			return sessionsObj;
		} catch  (HttpServerErrorException|HttpClientErrorException e) {
			logger.error("Failed while Authentication with error" + e.getResponseBodyAsString());
			throw new HttpServerErrorException(e.getStatusCode());
		}
	} 
	
	public Sessions createSession(String UserName, String Password) {

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders requestHeaders = new HttpHeaders();
//		String url = env.getProperty("otmm.server.host") + "/otmmapi/" + env.getProperty("otmm.server.version")
//				+ "/sessions";
		String url = "https://mmqa.monsterenergy.com/otmmapi/v5/sessions";

		logger.error("sessions url " + url);
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> requestFormData = new LinkedMultiValueMap<String, String>();
		requestFormData.add("username", UserName);
		requestFormData.add("password", Password);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(
				requestFormData, requestHeaders);

		try {
			HttpEntity<Sessions> response = restTemplate.postForEntity(url, request, Sessions.class);
			Sessions sessionsObj = response.getBody();
			String cookie = response.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
			sessionsObj.setJSessionID(cookie);
			return sessionsObj;

		} catch (HttpServerErrorException | HttpClientErrorException e) {
			logger.error("Failed while Authentication with error" + e.getResponseBodyAsString());
			throw new HttpServerErrorException(e.getStatusCode());
		}

	}
	
	public String createFolder(String otdsTicket, Sessions sessions, FolderResourceContainer folderResourceContainer, String parent_folder_id) throws JsonProcessingException {
		RestTemplate restTemplate = new RestTemplate();		
		HttpHeaders requestHeaders = new HttpHeaders();
		String url = "http://mmdqa.monsterenergy.com/otmmapi/v5/folders/" + parent_folder_id;
		logger.error("create folder url is :" + url);
	    requestHeaders.add(APIHeaders.CONTENT_TYPE.getValue(), APIHeaders.APPLICATION_JSON.getValue()); 
	    requestHeaders.add(APIHeaders.X_REQUESTED_BY.getValue(), sessions.getSessionResource().getSession().getId());
	    requestHeaders.add(APIHeaders.COOKIE.getValue(), sessions.getJSessionID());
//	    requestHeaders.add(APIHeaders.OTDSTICKET.getValue(), otdsTicket);
	    
	    requestHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		requestHeaders.add(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.name());
		
		List<SecurityPolicyList> security_policy_list = folderResourceContainer.getFolder_resource().getFolder().getSecurity_policy_list();
		
		if (security_policy_list.isEmpty()) {
			throw new IllegalArgumentException("Security Policy cannot be empty");
		}
	    
	    HttpEntity<FolderResourceContainer> request = new HttpEntity<FolderResourceContainer>(folderResourceContainer,
				requestHeaders);
	    try { 	
	    System.out.println("Request is  " + objMapper.writeValueAsString(request));	
	    HttpEntity<String> response = restTemplate.postForEntity(url, request, String.class);
	    logger.error("Request is  " + response);
//	    String asset_id = response.getBody().getFolder_resource().getFolder().getAsset_id();
	    return response.getBody();
		
	   } catch (HttpServerErrorException|HttpClientErrorException e) {
			logger.error("Failed while creation of Folder with error " + e.getResponseBodyAsString());
			throw new HttpServerErrorException(e.getStatusCode());
	   }
	}

}
