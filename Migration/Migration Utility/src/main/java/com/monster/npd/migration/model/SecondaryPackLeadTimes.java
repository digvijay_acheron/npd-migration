package com.monster.npd.migration.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@Table(name = "shrdnpdlookupdomainmodelsecondary_pack_lead_times")
@Entity
public class SecondaryPackLeadTimes {

		@Id
	    @Column(name = "Id", updatable = false)
	    private Integer id;

	    @Column(name = "packtype", updatable = false)
	    private String packType;
	    
	    @Column(name = "developmenttime", updatable = false)
	    private Integer developmentTime;
	    
	    @Column(name = "proofingtime", updatable = false)
	    private Integer proofingTime;
	    
	    @Column(name = "isdefault", updatable = false)
	    private Boolean isDefault;
	    
}