package com.monster.npd.migration.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NPDProjectSecondaryPackId implements Serializable {
	
	private Integer npdProject;
    private Integer secondaryPack;
    
    public int hashCode() {
    	  return (int)(npdProject + secondaryPack);
    	 }

    	 public boolean equals(Object object) {
    	  if (object instanceof NPDProjectSecondaryPackId) {
    		  NPDProjectSecondaryPackId otherId = (NPDProjectSecondaryPackId) object;
    	   return (otherId.npdProject == this.npdProject) && (otherId.secondaryPack == this.secondaryPack);
    	  }
    	  return false;
    	 }

}
