package com.monster.npd.migration.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestCommercialMarketScopeId implements Serializable {
	private Integer request;
    private Integer commercialMarketScope;
    
    public int hashCode() {
    	  return (int)(request + commercialMarketScope);
    	 }

    	 public boolean equals(Object object) {
    	  if (object instanceof RequestCommercialMarketScopeId) {
    		  RequestCommercialMarketScopeId otherId = (RequestCommercialMarketScopeId) object;
    	   return (otherId.request == this.request) && (otherId.commercialMarketScope == this.commercialMarketScope);
    	  }
    	  return false;
    	 }
}
