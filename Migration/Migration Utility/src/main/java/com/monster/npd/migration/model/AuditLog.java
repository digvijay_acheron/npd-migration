package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "shrdnpdmigrationaudit_log")
public class AuditLog {
	
	@Id
	@GeneratedValue(generator = "audit_log_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "audit_log_seq", sequenceName = "audit_log_seq",allocationSize=1)
    @Column(name = "Id")
    private Integer id;

    @Column(name = "referenceid")
    private String referenceid;
    
    @Column(name = "status")
    private String status;
    
    @Column(name = "requestid")
    private Integer requestId;
    
    @Column(name = "projectid")
    private Integer projectId;
    
    @Column(name = "message")
    private String message;
    
    @Column(name = "s_organizationid")
    private int organizationid;

}
