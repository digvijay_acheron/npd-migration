package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Table(name = "SHRDAcheronMPMCoreMPM_Priority")
@Entity
@ToString
public class MPMPriority {
	
		@Id
	    @Column(name = "id", updatable = false)
	    private Integer id;

	    @Column(name = "name", updatable = false)
	    private String name;
	    
	    @Column(name = "is_active", updatable = false)
	    private String is_active;
	    
	    @Column(name = "is_default", updatable = false)
	    private String is_default; 
	       
	    @Column(name = "s_organizationid", updatable = false)
	    private int organizationid; 
	        
	    @OneToOne
	    @Fetch(FetchMode.JOIN)
	    @JoinColumn(name = "r_po_category_level_id", referencedColumnName = "Id", insertable = false, updatable = false)
	    private MPMCategoryLevel categoryLevel;

}
