package com.monster.npd.migration.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestGovernanceMilestoneId implements Serializable {
	private Integer requestGovMilestone;
    private Integer governanceMilestone;
    
    public int hashCode() {
    	  return (int)(requestGovMilestone + governanceMilestone);
    	 }

    	 public boolean equals(Object object) {
    	  if (object instanceof RequestGovernanceMilestoneId) {
    		  RequestGovernanceMilestoneId otherId = (RequestGovernanceMilestoneId) object;
    	   return (otherId.requestGovMilestone == this.requestGovMilestone) && (otherId.governanceMilestone == this.governanceMilestone);
    	  }
    	  return false;
    	 }
}