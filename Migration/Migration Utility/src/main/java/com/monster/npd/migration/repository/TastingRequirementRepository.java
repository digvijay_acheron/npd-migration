package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.TastingRequirement;

@Repository
public interface TastingRequirementRepository extends JpaRepository<TastingRequirement, Integer> {
	
	TastingRequirement findByTastingRequirement(String name);

}
