package com.monster.npd.migration.model;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Table(name = "shrdnpdsubmissionnpd_project")
@Entity
@ToString
public class NPDProject {
	
	@Id
	@GeneratedValue(generator = "npd_project_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "npd_project_seq", sequenceName = "npd_project_seq",allocationSize=1)
    @Column(name = "id")
    private Integer id;
        
    @Column(name = "r_po_request_id")
    private Integer r_po_request_id;
    
    @Column(name = "isfgproject")
    private Boolean isfgproject;
    
    @Column(name = "opsaligneddpweek")
    private String opsaligneddpweek;
    
    @Column(name = "opsaligneddpyear")
    private String opsaligneddpyear;
    
    @Column(name = "timelinerisk")
    private String timelinerisk;
    
    @Column(name = "risksummary")
    private String risksummary;
    
    @Column(name = "statusandcriticalitems")
    private String statusandcriticalitems;
    
    @Column(name = "trialandorcommercialproductionincluded")
    private Boolean trialandorcommercialproductionincluded;
    
    @Column(name = "programmemanagercp1phasedelay")
    private Integer programmemanagercp1phasedelay;
    
    @Column(name = "npdwasontrackfororiginalcp1date")
    private Boolean npdwasontrackfororiginalcp1date;
    
    @Column(name = "bottlerpushedoutproductionwithoutlaunchimpact")
    private Boolean bottlerpushedoutproductionwithoutlaunchimpact;
    
    @Column(name = "meccommercialbottlerrequesteddelaytolaunch")
    private Boolean meccommercialbottlerrequesteddelaytolaunch;
    
    @Column(name = "othertbc")
    private Boolean othertbc;
    
    @Column(name = "other2tbc")
    private Boolean other2tbc;
    
    @Column(name = "hasthechangebeenalignedviacpmtg")
    private Boolean hasthechangebeenalignedviacpmtg;
    
    @Column(name = "briefdescriptionofdelayprogrammemanager")
    private String briefdescriptionofdelayprogrammemanager;
    
    @Column(name = "targetcp2date")
    private String targetcp2date;
    
    @Column(name = "comtechopsagreeddpweek")
    private String comtechopsagreeddpweek;
    
    @Column(name = "rag")
    private String rag;
    
    @Column(name = "projecthealth")
    private String projecthealth;
    
    @Column(name = "cp1reviseddate")
    private String cp1reviseddate;
    
    @Column(name = "noofweeksincrease")
    private Integer noofweeksincrease;
    
    @Column(name = "eantargetweek")
    private String eantargetweek;
    
    @Column(name = "eanactualweek")
    private String eanactualweek;
    
    @Column(name = "prodinfotargetweek")
    private String prodinfotargetweek;
    
    @Column(name = "prodinfoactualweek")
    private String prodinfoactualweek;
    
    @Column(name = "labeluploadedactualdate")
    private String labeluploadedactualdate;
    
    @Column(name = "bottlerspecificcomments")
    private String bottlerspecificcomments;
    
    @Column(name = "bottlerandcantargetweek")
    private String bottlerandcantargetweek;
    
    @Column(name = "bottlerandcanactualweek")
    private String bottlerandcanactualweek;
    
    
    @Column(name = "itemnumber")
    private String itemnumber;
    
    @Column(name = "numberofcancompaniesrequired")
    private Integer numberofcancompaniesrequired;
    
    @Column(name = "nameofcancompany")
    private String nameofcancompany;
    
    @Column(name = "poreceivedactualweek")
    private String poreceivedactualweek;
    
    @Column(name = "palletlabelreceivedtargetweek")
    private String palletlabelreceivedtargetweek;
    
    @Column(name = "palletlabelreceivedactualweek")
    private String palletlabelreceivedactualweek;
    
    @Column(name = "palletlabelsenttargetweek")
    private String palletlabelsenttargetweek;
    
    @Column(name = "palletlabelsentactualweek")
    private String palletlabelsentactualweek;
    
    @Column(name = "requestid")
    private Integer requestid;
    
    @Column(name = "pmslacktime")
    private String pmslacktime;
    
    @Column(name = "currentcommercialaligneddate")
    private String currentcommercialaligneddate;
    
    @Column(name = "artworkjobid")
    private String artworkjobid;
    
    @Column(name = "artworkjobstatus")
    private String artworkjobstatus;
    
    @Column(name = "artworkjobname")
    private String artworkjobname;
    
    @Column(name = "projectbusinessid")
    private String projectbusinessid;
    
    @Column(name = "mpmprojectid")
    private String mpmprojectid;
    
    @Column(name = "projectfinalclassification")
    private Integer projectfinalclassification;
    
    @Column(name = "trialdateconfirmedwithbottler")
    private String trialdateconfirmedwithbottler;
    
    @Column(name = "nsvy1annualised")
    private String nsvy1annualised;
    
    @Column(name = "volumey1annualised")
    private String volumey1annualised;
    
    @Column(name = "volume1st3months")
    private String volume1st3months;
    
    @Column(name = "cp1agreementdate")
    private String cp1agreementdate;
    
    @Column(name = "leadtimemarket")
    private Integer leadtimemarket;
    
    @Column(name = "cp1actualdate")
    private String cp1actualdate;
    
    @Column(name = "cp2actualdate")
    private String cp2actualdate;
    
    @Column(name = "cp3actualdate")
    private String cp3actualdate;
    
    @Column(name = "cp4actualdate")
    private String cp4actualdate;
    
    @Column(name = "pmreportingother1")
    private String pmreportingother1;
    
    @Column(name = "pmreportingother2")
    private String pmreportingother2;
    
    @Column(name = "finalmanufacturingsite")
    private String finalmanufacturingsite;
    
    @Column(name = "finalmanufacturingsiteid")
    private Integer finalmanufacturingsiteid;
    
    @Column(name = "finalmanufacturinglocation")
    private String finalmanufacturinglocation;
    
    @Column(name = "finalmanufacturinglocationid")
    private Integer finalmanufacturinglocationid;
    
    @Column(name = "finalmanufacturinglocationitemid")
    private String finalmanufacturinglocationitemid;
    
    
    @Column(name = "winshuttlenumber")
    private String winshuttlenumber;
    
    @Column(name = "fgmaterialcode")
    private String fgmaterialcode;
    
    @Column(name = "formulahbcnumber")
    private String formulahbcnumber;
    
    @Column(name = "hbcissuancetype")
    private String hbcissuancetype;
    
    @Column(name = "cansleeveprimarybottlematerial")
    private String cansleeveprimarybottlematerial;
    
    @Column(name = "palletlabelformat")
    private String palletlabelformat;
    
    @Column(name = "traylabelformat")
    private String traylabelformat;
    
    @Column(name = "trialmaterialsconfirmeddeliveryweek")
    private String trialmaterialsconfirmeddeliveryweek;
    
    @Column(name = "finalagreed1stprofconfirmedwithbottler")
    private String finalagreed1stprofconfirmedwithbottler;
    
    @Column(name = "firstprodmaterialsconfirmeddelivery")
    private String firstprodmaterialsconfirmeddelivery;
    
    @Column(name = "trialorfirstprodincludedinplan")
    private String trialorfirstprodincludedinplan;
    
    @Column(name = "earliesttrialor1stproddate")
    private String earliesttrialor1stproddate;
    
    @Column(name = "bomsentactualdate")
    private String bomsentactualdate;
    
    @Column(name = "localbominsap")
    private String localbominsap;
    
    @Column(name = "allowproductionschedulelessthan8weeks")
    private Boolean allowproductionschedulelessthan8weeks;
    
    @Column(name = "poreceived")
    private String poreceived;
    
    @Column(name = "possiblenotificationrequirement")
    private String possiblenotificationrequirement;
    
    @Column(name = "requiresprimarycanaw")
    private Boolean requiresprimarycanaw;
    
    @Column(name = "canprimaryawstart")
    private Boolean canprimaryawstart;
    
    @Column(name = "cansecondaryawstart")
    private Boolean cansecondaryawstart;
    
    @Column(name = "requiressecondaryaw")
    private Boolean requiressecondaryaw;
    
    @Column(name = "allawtaskscompleted")
    private Boolean allawtaskscompleted;
    
    @Column(name = "regulatorycomments")
    private String regulatorycomments;
    
    @Column(name = "howmanypieces")
    private Integer howmanypieces;
    
    @Column(name = "secondarypackrequired")
    private String secondarypackrequired;
    
    @Column(name = "noofpiecesrequired")
    private Integer noofpiecesrequired;
    
    @Column(name = "siteauditrequired")
    private Boolean siteauditrequired;
    
    @Column(name = "whowillaudit")
    private String whowillaudit;
    
    @Column(name = "auditdate")
    private String auditdate;
    
    @Column(name = "siteapproved")
    private Boolean siteapproved;
    
    @Column(name = "hbcrpemsebcreated")
    private String hbcrpemsebcreated;
    
    @Column(name = "plantspecificreapplicationonly")
    private String plantspecificreapplicationonly;
    
    @Column(name = "plantspecificresponsibleforcompletion")
    private String plantspecificresponsibleforcompletion;
    
    @Column(name = "plantspecificcomments")
    private String plantspecificcomments;
    
    @Column(name = "trialsupervisionquality")
    private String trialsupervisionquality;
    
    @Column(name = "trialsupervisionnpd")
    private String trialsupervisionnpd;
    
    @Column(name = "onsiteremotenotrequired")
    private String onsiteremotenotrequired;
    
    @Column(name = "trialprotocolwrittenby")
    private String trialprotocolwrittenby;
    
    @Column(name = "protocolissued")
    private String protocolissued;
    
    @Column(name = "tastingrequirement")
    private String tastingrequirement;
    
    @Column(name = "qaqcreleasedate")
    private String qaqcreleasedate;
    
    @Column(name = "newprojecttype")
    private String newprojecttype;
    
    @Column(name = "newfg")
    private Boolean newfg;
    
    @Column(name = "newrdformula")
    private Boolean newrdformula;
    
    @Column(name = "newhbcformula")
    private Boolean newhbcformula;
    
    @Column(name = "newprimarypackaging")
    private Boolean newprimarypackaging;
    
    @Column(name = "newsecondarypackaging")
    private String newsecondarypackaging;
    
    @Column(name = "newclassificationrational")
    private String newclassificationrational;
    
    @Column(name = "techorqualsupervisionrequired")
    private String techorqualsupervisionrequired;
    
    @Column(name = "cancompanies")
    private String cancompanies;
    
    @Column(name = "cancompanyids")
    private String cancompanyids;
    
    @Column(name = "istask4completed")
    private Boolean istask4completed;
    
    @Column(name = "istask5completed")
    private Boolean istask5completed;
    
    @Column(name = "regionaltechnicalmanagerid")
    private Integer regionaltechnicalmanagerid;
    
    @Column(name = "regionaltechnicalmanagername")
    private String regionaltechnicalmanagername;
    
    @Column(name = "opsplannerid")
    private Integer opsplannerid;
    
    @Column(name = "opsplannername")
    private String opsplannername;
    
    @Column(name = "gfgid")
    private Integer gfgid;
    
    @Column(name = "gfgname")
    private String gfgname;
    
    @Column(name = "projectspecialistid")
    private Integer projectspecialistid;
    
    @Column(name = "projectspecialistname")
    private String projectspecialistname;
    
    @Column(name = "regulatoryleadid")
    private Integer regulatoryleadid;
    
    @Column(name = "regulatoryleadname")
    private String regulatoryleadname;
    
    @Column(name = "commercialleadid")
    private Integer commercialleadid;
    
    @Column(name = "commercialleadname")
    private String commercialleadname;
    
    @Column(name = "regionaltechnicalmanageritemid")
    private String regionaltechnicalmanageritemid;
    
    @Column(name = "opsplanneritemid")
    private String opsplanneritemid;
    
    @Column(name = "gfgitemid")
    private String gfgitemid;
    
    @Column(name = "projectspecialistitemid")
    private String projectspecialistitemid;
    
    @Column(name = "regulatoryleaditemid")
    private String regulatoryleaditemid;
    
    @Column(name = "commercialleaditemid")
    private String commercialleaditemid;
    
    @Column(name = "printsupplier")
    private String printsupplier;
    
    @Column(name = "printsupplieritemid")
    private String printsupplieritemid;
    
    @Column(name = "whowillaudititemid")
    private String whowillaudititemid;
    
    @Column(name = "trialsupervisionnpditemid")
    private String trialsupervisionnpditemid;
    
    @Column(name = "trialsupervisionqualityitemid")
    private String trialsupervisionqualityitemid;
    
    @Column(name = "trialprotocolwrittenbyitemid")
    private String trialprotocolwrittenbyitemid;
    
    @Column(name = "tastingrequirementitemid")
    private String tastingrequirementitemid;
    
    @Column(name = "leadmarketid")
    private String leadmarketid;
    
    @Column(name = "leadmarket")
    private String leadmarket;
    
    @Column(name = "governanceapproach")
    private String governanceapproach;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "pmreportingprojecttype")
    private String pmreportingprojecttype;
    
    @Column(name = "pmreportingprojecttypeitemid")
    private String pmreportingprojecttypeitemid;
    
    @Column(name = "pmreportingsubprojecttype")
    private String pmreportingsubprojecttype;
    
    @Column(name = "pmreportingsubprojecttypeitemid")
    private String pmreportingsubprojecttypeitemid;
    
    @Column(name = "pmreportingprojectdetail")
    private String pmreportingprojectdetail;
    
    @Column(name = "pmreportingprojectdetailitemid")
    private String pmreportingprojectdetailitemid;
    
    @Column(name = "pmreportingdeliveryquarter")
    private String pmreportingdeliveryquarter;
    
    @Column(name = "pmreportingdeliveryquarteritemid")
    private String pmreportingdeliveryquarteritemid;
    
    @Column(name = "nooflinkedmarkets")
    private String nooflinkedmarkets;
    
    @Column(name = "pmreportingcomments")
    private String pmreportingcomments;
    
    @Column(name = "opspm")
    private String opspm;
    
    @Column(name = "opspmitemid")
    private String opspmitemid;
    
    @Column(name = "corppm")
    private String corppm;
    
    @Column(name = "corppmitemid")
    private String corppmitemid;
    
    @Column(name = "e2epm")
    private String e2epm;
    
    @Column(name = "e2epmitemid")
    private String e2epmitemid;
    
    @Column(name = "newregdossier")
    private Boolean newregdossier;
    
    @Column(name = "newpreproduction")
    private Boolean newpreproduction;
    
    @Column(name = "newpostproduction")
    private Boolean newpostproduction;
    
    @Column(name = "issecondarypackagingdevelopmentactive")
    private Boolean issecondarypackagingdevelopmentactive;
    
    @Column(name = "issecondarypackagingproofingactive")
    private Boolean issecondarypackagingproofingactive;
    
    @Column(name = "trialorcommentsoractionsrequired")
    private String trialorcommentsoractionsrequired;
    
    @Column(name = "newprojecttypeitemid")
    private String newprojecttypeitemid;
    
    @Column(name = "finalprojectclassificationdescription")
    private String finalprojectclassificationdescription;
    
    @Column(name = "finalmanufacturingsiteitemid")
    private String finalmanufacturingsiteitemid;
    
    @Column(name = "bottlercommunicationstatus")
    private String bottlercommunicationstatus;
    
    @Column(name = "isplantspecificreapplicationonly")
    private Boolean isplantspecificreapplicationonly;
    
    @Column(name = "techqualwhowillcompleteitemid")
    private String techqualwhowillcompleteitemid;
    
    @Column(name = "techqualwhowillcomplete")
    private String techqualwhowillcomplete;
    
    @Column(name = "techqualcompleted")
    private String techqualcompleted;
    
    @Column(name = "techqualweekdue")
    private String techqualweekdue;
    
    @Column(name = "iscp1completed")
    private Boolean iscp1completed;
    
    @Column(name = "isprojectmigrationcompleted")
    private Boolean isprojectmigrationcompleted;
    
    @Column(name = "r_po_secondary_pack_lead_time_id")
    private String r_po_secondary_pack_lead_time_id;
    
//    @ManyToMany(fetch = FetchType.LAZY)
//    @NotFound(action = NotFoundAction.IGNORE)
//    @JoinTable(name = "shrdnpdsubmissionnpd_projectsecondary_pack",
//            joinColumns = {
//                    @JoinColumn(name = "npd_projectidA5E8B0A5F45E5D19", referencedColumnName = "Id",
//                            nullable = false)},
//            inverseJoinColumns = {
//                    @JoinColumn(name = "secondary_pack_id",referencedColumnName = "Id",
//                            nullable = false)})
//    private Set<SecondaryPack> secondaryPack = new HashSet<SecondaryPack>();
    
    @OneToMany(mappedBy = "npdProject", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<NPDProjectSecondaryPack> npdProjectSecondaryPack = new HashSet<NPDProjectSecondaryPack>();
    
    @Column(name = "s_organizationid")
    private int organizationid;
    
    
}
