package com.monster.npd.migration.repository;


import com.monster.npd.migration.model.ProjectType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectTypeRepository  extends JpaRepository<ProjectType, Integer> {
	
	ProjectType findByDisplayName(String name);

}
