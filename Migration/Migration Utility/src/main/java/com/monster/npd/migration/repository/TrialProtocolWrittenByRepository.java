package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.TrialProtocolWrittenBy;

@Repository
public interface TrialProtocolWrittenByRepository extends JpaRepository<TrialProtocolWrittenBy, Integer> {
	
	TrialProtocolWrittenBy findByUsers(String name);

}