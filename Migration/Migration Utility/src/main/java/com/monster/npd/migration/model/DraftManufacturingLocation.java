package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "shrdnpdlookupdomainmodeldraft_manufacturing_location")
public class DraftManufacturingLocation {
	
	@Id
    @Column(name = "Id", updatable = false)
    private Integer id;

    @Column(name = "isactive", updatable = false)
    private String isActive;
    
//    @Column(name = "value", updatable = false)
//    private String value;
    
    @OneToOne
    @JoinColumn(name = "R_PO_MARKET_Id", referencedColumnName = "Id", insertable = false, updatable = false)
    private Market market;


}
