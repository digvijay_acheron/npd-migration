package com.monster.npd.migration.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@Table(name = "o10opentextentityidentitycomponentsidentity")
@ToString
public class OpenTextIdentity {
	
	@Id
	@Column(name = "id", updatable = false)
	private Integer id;
	
	@Column(name = "name", updatable = false)
	private String name;
	
	@Column(name = "identitydisplayname", updatable = false)
	private String identityDisplayName;
	
	@Column(name = "display_name", updatable = false)
	private String displayName;
	
	@Column(name = "userid", updatable = false)
	private String userId;
	
	@Column(name = "fullname", updatable = false)
	private String fullName;

}
