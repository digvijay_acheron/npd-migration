package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "shrdnpdlookupdomainmodeltrial_supervision_quality")
@Entity
public class TrialSupervisionQuality {

	  @Id
	    @Column(name = "Id", updatable = false)
	    private String id;

	    @Column(name = "users", updatable = false)
	    private String users;
}