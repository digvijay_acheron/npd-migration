package com.monster.npd.migration.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.CommercialMarketScope;

@Repository
public interface CommercialMarketScopeRepository extends JpaRepository<CommercialMarketScope, Integer>{
	
	@Query("SELECT MAX(p.id) FROM CommercialMarketScope p")
    Integer maxCommercialMarketScopeId();
	
	@Query(value = "SELECT NEXT VALUE FOR dbo.comm_market_seq", nativeQuery = true)
    public Integer getCurrentVal();

}
