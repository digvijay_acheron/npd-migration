package com.monster.npd.migration.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProjectTeamList {
	
	OpenTextIdentity e2ePm;
	OpenTextIdentity corpPm;
	OpenTextIdentity opsPm;
	OpenTextIdentity rtm;
	OpenTextIdentity opsPlanner;
	OpenTextIdentity gfg;
	OpenTextIdentity projectSpecialist;
	OpenTextIdentity regLead;
	OpenTextIdentity commercialLead;
	

}
