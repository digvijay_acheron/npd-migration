package com.monster.npd.migration.model;

import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@SolrDocument(collection = "NPD")
@NoArgsConstructor
public class MPMDeliverableSchema {
	

	// Default required fields ---------
    @org.springframework.data.annotation.Id
    @Indexed(name = "ITEM_ID", type = "string")
    private String item_id;
    
    @Indexed(name = "CONTENT_TYPE", type = "string")
    private String content_type;
    
    @Indexed(name = "DELIVERABLE_PRIORITY_ID", type = "string")
    private Integer deliverable_priority_id;
    
    @Indexed(name = "DELIVERABLE_TYPE", type = "string")
    private String deliverable_type;
    
    @Indexed(name = "DELIVERABLE_DESCRIPTION", type = "string")
    private String deliverable_description;
    
    @Indexed(name = "DELIVERABLE_DUE_DATE", type = "string")
    private String deliverable_due_date;
    
    @Indexed(name = "IS_ACTIVE_DELIVERABLE", type = "string")
    private Boolean is_active_deliverable;
    
    @Indexed(name = "IS_DELETED_DELIVERABLE", type = "string")
    private Boolean is_deleted_deliverable;
    
    @Indexed(name = "IS_UPLOADED", type = "string")
    private Boolean is_uploaded;
    
    @Indexed(name = "ITERATION_COUNT", type = "string")
    private String iteration_count;
    @Indexed(name = "DELIVERABLE_NAME", type = "string")
    private String deliverable_name;
    @Indexed(name = "DELIVERABLE_ASSIGNMENT_TYPE", type = "string")
    private String deliverable_assignment_type;
    @Indexed(name = "DELIVERABLE_START_DATE", type = "string")
    private String deliverable_start_date;
    
    @Indexed(name = "UPLOADED_ASSET_ID", type = "string")
    private String uploaded_asset_id;
    
    @Indexed(name = "DELIVERABLE_APPROVER_ACTIVE_USER_ID", type = "string")
    private Integer deliverable_approver_active_user_id;
    
    @Indexed(name = "DELIVERABLE_APPROVER_ACTIVE_USER_CN", type = "string")
    private String deliverable_approver_active_user_cn;
    
    @Indexed(name = "DELIVERABLE_APPROVER_ACTIVE_USER_NAME", type = "string")
    private String deliverable_approver_active_user_name;
    
    @Indexed(name = "DELIVERABLE_APPROVER_ROLE_ID", type = "string")
    private Integer deliverable_approver_role_id;
    
    @Indexed(name = "DELIVERABLE_APPROVER_ROLE_NAME", type = "string")
    private String deliverable_approver_role_name;
    
    @Indexed(name = "DELIVERABLE_APPROVER_ID", type = "string")
    private Integer deliverable_approver_id;
    
    @Indexed(name = "DELIVERABLE_APPROVER_CN", type = "string")
    private String deliverable_approver_cn;
    
    @Indexed(name = "DELIVERABLE_APPROVER_NAME", type = "string")
    private String deliverable_approver_name;
    
    @Indexed(name = "DELIVERABLE_OWNER_ID", type = "string")
    private Integer deliverable_owner_id;
    
    @Indexed(name = "DELIVERABLE_OWNER_CN", type = "string")
    private String deliverable_owner_cn;
    
    @Indexed(name = "DELIVERABLE_OWNER_NAME", type = "string")
    private String deliverable_owner_name;
    
    @Indexed(name = "DELIVERABLE_ACTIVE_USER_ID", type = "string")
    private Integer deliverable_active_user_id;
    
    @Indexed(name = "DELIVERABLE_ACTIVE_USER_CN", type = "string")
    private String deliverable_active_user_cn;
    
    @Indexed(name = "DELIVERABLE_ACTIVE_USER_NAME", type = "string")
    private String deliverable_active_user_name;
    
    @Indexed(name = "DELIVERABLE_OWNER_ROLE_ID", type = "string")
    private Integer deliverable_owner_role_id;
    
    @Indexed(name = "DELIVERABLE_OWNER_ROLE_NAME", type = "string")
    private String deliverable_owner_role_name;
    
    @Indexed(name = "DELIVERABLE_PRIORITY_VALUE", type = "string")
    private String deliverable_priority_value;
    
    @Indexed(name = "PROJECT_ITEM_ID", type = "string")
    private String project_item_id;
    
    @Indexed(name = "DELIVERABLE_STATUS_ID", type = "string")
    private Integer deliverable_status_id;
    
    @Indexed(name = "DELIVERABLE_STATUS_VALUE", type = "string")
    private String deliverable_status_value;
    
    @Indexed(name = "DELIVERABLE_STATUS_TYPE", type = "string")
    private String deliverable_status_type;
    
    @Indexed(name = "TASK_ITEM_ID", type = "string")
    private String task_item_id;
    
    @Indexed(name = "ID", type = "string")
    private String id;
    
    @Indexed(name = "IS_PM_ASSIGNED", type = "string")
    private Boolean is_pm_assigned;
    
    @Indexed(name = "ORIGINAL_DELIVERABLE_START_DATE", type = "string")
    private String original_deliverable_start_date;
    
    @Indexed(name = "ORIGINAL_DELIVERABLE_DUE_DATE", type = "string")
    private String original_deliverable_due_date;
    
    @Indexed(name = "EXPECTED_DELIVERABLE_DURATION", type = "string")
    private Integer expected_deliverable_duration;
    
    @Indexed(name = "DELIVERABLE_TIME_SPENT", type = "string")
    private Integer deliverable_time_spent;
    
    @Indexed(name = "CAMPAIGN_ITEM_ID", type = "string")
    private String campaign_item_id;
    
    @Indexed(name = "DELIVERABLE_APPROVER_CN_facet", type = "string")
    private String deliverable_approver_cn_facet;
        

}
