package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "shrdnpdlookupdomainmodeltech_qual_users")
@Entity
public class TechQualUsers {

	  @Id
	    @Column(name = "Id", updatable = false)
	    private String id;

	    @Column(name = "users", updatable = false)
	    private String users;
}
