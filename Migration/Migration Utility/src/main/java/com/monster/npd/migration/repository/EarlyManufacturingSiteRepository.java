package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.EarlyManufacturingSite;

@Repository
public interface EarlyManufacturingSiteRepository extends JpaRepository<EarlyManufacturingSite, Integer> {
	
	EarlyManufacturingSite findByDisplayName(String name);

}
