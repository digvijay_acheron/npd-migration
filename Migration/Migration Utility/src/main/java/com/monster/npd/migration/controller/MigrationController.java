package com.monster.npd.migration.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.monster.npd.migration.model.Data;
import com.monster.npd.migration.model.MPMProject;
import com.monster.npd.migration.repository.GovernanceMilestoneRepository;
import com.monster.npd.migration.repository.MPMProjectRepository;
import com.monster.npd.migration.repository.MPMTaskRepository;
import com.monster.npd.migration.repository.NPDTaskRepository;
import com.monster.npd.migration.repository.RequestRepository;
import com.monster.npd.migration.repository.WorkflowRulesRepository;
import com.monster.npd.migration.service.EventUserModal;
import com.monster.npd.migration.service.MigrationService;
import com.monster.npd.migration.service.ProjectMigrationService;
import com.monster.npd.migration.service.RequestMigrationService;
import com.monster.npd.migration.service.WorkflowRulesService;

/**
 * 
 * @author JeevaR
 *
 */
@RestController
@EnableAsync
//@PropertySources({ @PropertySource(value = "classpath:application.yml")})
public class MigrationController {

	@Autowired
	MigrationService migrationService;	
	
	@Autowired
	RequestMigrationService requestMigrationService;
	
	@Autowired
	ProjectMigrationService projectMigrationService;
	
	@Autowired
	WorkflowRulesService workflowRulesService;

	@Autowired
	RequestRepository requestRepository;
	
	@Autowired
	GovernanceMilestoneRepository governanceMilestoneRepository;
	
	@Autowired
	MPMProjectRepository MPMProjectRepository;
	
	@Autowired
	MPMTaskRepository MPMTaskRepository;
	
	@Autowired
	NPDTaskRepository NPDTaskRepository;
	
	@Autowired
	WorkflowRulesRepository workflowRulesRepository;
	
	@Autowired
	Environment env;
	
	@Autowired
	EventUserModal eventUserModal;
	private static final Logger logger = LogManager.getLogger(MigrationController.class);
	
	public static int maxRequestId;
	public static int maxProjectId;
	public static int maxMPMTaskId;
	public static int maxNPDTaskId;
	public static int maxGovernanceMilestoneId;
	public static int maxWorkflowRuleId;
  

	/**
	 * Test Get Api
	 * 
	 * @return
	 */
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String test() {
		logger.info("Inside Testing");
		return "Tested Successfully";
	}

	/**
	 * Post Api to send data from spreadsheet to Appworks SOAP request
	 * 
	 * @param data - samlart pay load for post request
	 * 
	 * @return -return string
	 * @throws Exception
	 */
	@RequestMapping(value = "/migrate", method = RequestMethod.POST)
	public String searchIndexer(@RequestBody(required = true) Data data) throws Exception {
		String samlart = data.getSamlart();
		//eventUserModal.processAllSheets(env.getProperty("excel.path"), samlart);
		System.out.println("In");
		 migrationService.migrateData(samlart, data.getUsername(), data.getPassword());
//		migrationService.kickOffAsyncTasks();
//		requestMigrationService.testrequest();
//		projectMigrationService.createProject(data.getUsername(), data.getPassword());
//		 workflowRulesService.createWorkflowRules(111226, 80);
		return "Migrated";
	}

}
