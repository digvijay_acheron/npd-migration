package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "shrdnpdlookupdomainmodelearly_manufacturing_site")
public class EarlyManufacturingSite {
	@Id
    @Column(name = "Id", updatable = false)
    private Integer id;

    @Column(name = "displayname", updatable = false)
    private String displayName;
    
    @Column(name = "value", updatable = false)
    private String value;

    @Column(name = "name", updatable = false)
    private String name;
    
}
