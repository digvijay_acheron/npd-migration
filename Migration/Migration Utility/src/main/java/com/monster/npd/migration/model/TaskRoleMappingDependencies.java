package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "npd_vw_task_role_mapping_dependencies")
@Entity
public class TaskRoleMappingDependencies {

	  	@Id
//	  	@GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name = "Id", updatable = false)
	    private Integer id;

	  	
	    @Column(name = "task_id", updatable = false)
	    private Integer task_id;
	    
	    @Column(name = "task_name", updatable = false)
	    private String task_name;
	    
	    @Column(name = "task_description", updatable = false)
	    private String task_description;
	    
//	    @Column(name = "functional_role_id", updatable = false)
//	    private Integer functional_role_id;
	    
	    @OneToOne
	    @JoinColumn(name = "functional_role_id", referencedColumnName = "Id")
	    private OpenTextIdentity functional_role_id;
	    
	    @Column(name = "predecessor_task_id", updatable = false)
	    private Integer predecessor_task_id;
	    
	    @Column(name = "predecessor_task_name", updatable = false)
	    private String predecessor_task_name;
	    
	    @Column(name = "is_primary_dependency", updatable = false)
	    private Boolean is_primary_dependency;
	    
	    @Column(name = "dependency_sequence", updatable = false)
	    private Integer dependency_sequence;
	    
	    @Column(name = "predecessor_task_description", updatable = false)
	    private String predecessor_task_description;
	    
	    @Column(name = "predecessor_task_role_id", updatable = false)
	    private Integer predecessor_task_role_id;
}