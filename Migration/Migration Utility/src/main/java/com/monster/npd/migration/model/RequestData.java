package com.monster.npd.migration.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
/**
 * 
 * @author JeevaR
 *
 */
@Getter
@Setter
@ToString
public class RequestData {
	String projectName;
	String currentCheckPoint;
	String requestRationale;
	String requestStatus;
	String isSVPAligned;
	String requestID;
	String secondaryPackagingType;
	String isSecondaryPackaging;
	String newFGSAP;
	String newHBCFormula;
	String newPrimaryPackaging;
	String newRDFormula;
	String postLaunchAnalysis;
	String postProductionRegistration;
	String preProductionRegistartion;
	String registrationDossier;
	String technicalDescription;
	String projectAbout;
	String whyProject;
	String businessCaseComments;
	String numberOfLinkedMarkets;
	String progMangerComments;
	String r_PO_PROJECT_TYPE;
	String r_PO_PACKAGING_TYPE;
	String r_PO_REQUEST_TYPE;
	String r_PO_VARIANT_SKU;
	String r_PO_CORP_PM;
	String r_PO_DRAFT_MANUFACTURING_LOCATION;
	String r_PO_E2E_PM;
	String r_PO_EARLY_MANUFACTURING_SITE;
	String r_PO_OPS_PM;
	String r_PO_PLATFORMS;
	String r_PO_BRANDS;
	String r_PO_BUSINESS_UNIT;
	String r_PO_BOTTLER;
	String r_PO_CASE_PACK_SIZE;
	String r_PO_CONSUMER_UNIT_SIZE;
	String r_PO_SKU_DETAILS;
	String r_PO_PROJECT_CLASSIFICATION;
	String r_PO_CP_PROJECT_TYPE;
	String r_PO_PROJECT_SUB_TYPE;
	String r_PO_PROJECT_DETAIL;
	String r_PO_PM_DELIVERY_QUARTER;
	String cP0DecisionDate;
	String leadMarketTargetDPInWarehouse;
	String leadMarketId;
	String otherLinkedMarketsTargetDPInWarehouse;
	String otherLinkedMarketsId;
	String referenceId;
	String nSV;
	String volume1Year;
	String volume3Months;
	String requestedDate; //R
	String governanceApproach; //AQD
	
	String govMilestoneCP0TargetStartDate; //AZ
	String govMilestoneCP0DecisionDate; //R
	String govMilestoneCP1TargetStartDate; //BA
	String govMilestoneCP1DecisionDate; //FV
	String govMilestoneCP2TargetStartDate; //BB
	String govMilestoneCP2DecisionDate; //TS
	String govMilestoneCP3TargetStartDate; //BC
	String govMilestoneCP3DecisionDate; //XW
	String govMilestoneCP4TargetStartDate; //BD
	String govMilestoneCP4DecisionDate; //ABY
	
}
