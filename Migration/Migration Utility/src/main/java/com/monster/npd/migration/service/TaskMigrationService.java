package com.monster.npd.migration.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.monster.npd.migration.controller.MigrationController;
import com.monster.npd.migration.model.MPMStatus;
import com.monster.npd.migration.model.MPMTask;
import com.monster.npd.migration.model.MPMTaskSchema;
import com.monster.npd.migration.model.MPMTeamRole;
import com.monster.npd.migration.model.MaxId;
import com.monster.npd.migration.model.NPDTask;
import com.monster.npd.migration.model.OpenTextIdentity;
import com.monster.npd.migration.model.ProjectTeamList;
import com.monster.npd.migration.model.ReportingDeliveryQuarter;
import com.monster.npd.migration.model.RequestData;
import com.monster.npd.migration.model.TaskDetails;
import com.monster.npd.migration.model.TaskIndexingService;
import com.monster.npd.migration.model.TaskRoleMappingDependencies;
import com.monster.npd.migration.repository.MPMTaskRepository;
import com.monster.npd.migration.repository.NPDTaskRepository;
import com.monster.npd.migration.repository.TaskSolrRepository;

@Service
public class TaskMigrationService {
	
//private static final Logger logger = LoggerFactory.getLogger(TaskMigrationService.class);
	
	private static final Logger logger = LogManager.getLogger(TaskMigrationService.class);
	
	@Autowired
	private TaskSolrRepository taskSolrRepository;
	
	@Autowired
	private MPMTaskRepository MPMTaskRepository;
	
	@Autowired
	SolrIndexingService solrIndexingService;
	
	@Autowired
	Environment env;
	
	@Autowired
	UtilService util;
	
	@Autowired
	TaskIndexingService taskIndexingService;

	@Autowired
	NPDTaskRepository NPDTaskRepository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional
	public List<MPMTaskSchema> createTask(RequestData requestData, TaskDetails taskDetails, Integer projectId,
			ProjectTeamList projectTeamList) throws SolrException, SolrServerException, Exception {
		
//		int count= 250;
		
		Integer orgId = Integer.parseInt(env.getProperty("npd.org.id"));
		List<MPMTask> mpmTaskList = new ArrayList<MPMTask>();
		
		TaskRoleMappingDependencies taskRoleMapping2 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 2")).findAny().orElse(null);
	
		MaxId max2 = new MaxId();
		max2.setTaskRoleName(taskRoleMapping2.getFunctional_role_id().getName());		
		MPMTeamRole task2MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max2.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task2Owner = new OpenTextIdentity();
		OpenTextIdentity e2ePMOwner = new OpenTextIdentity();
		OpenTextIdentity corpPMOwner = new OpenTextIdentity();
		OpenTextIdentity opsPMOwner = new OpenTextIdentity();
		OpenTextIdentity rtmOwner = new OpenTextIdentity();
//		OpenTextIdentity opsPlannerOwner = new OpenTextIdentity();
//		OpenTextIdentity gfgOwner = new OpenTextIdentity();
		OpenTextIdentity projectSpecialistOwner = new OpenTextIdentity();
		OpenTextIdentity regLeadOwner = new OpenTextIdentity();
//		OpenTextIdentity commercialLeadOwner = new OpenTextIdentity();
		
		if(!util.isNullOrEmpty(projectTeamList.getE2ePm())) {
			e2ePMOwner = ConfigService.getUsersList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == projectTeamList.getE2ePm().getId()).findAny().orElse(null);
		}
		if(!util.isNullOrEmpty(projectTeamList.getCorpPm())) {
			corpPMOwner = ConfigService.getUsersList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == projectTeamList.getCorpPm().getId()).findAny().orElse(null);
		}
		if(!util.isNullOrEmpty(projectTeamList.getOpsPm())) {
			opsPMOwner = ConfigService.getUsersList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == projectTeamList.getOpsPm().getId()).findAny().orElse(null);
		}
		if(!util.isNullOrEmpty(projectTeamList.getRtm())) {
			rtmOwner = ConfigService.getUsersList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == projectTeamList.getRtm().getId()).findAny().orElse(null);
		}
//		if(!util.isNullOrEmpty(projectTeamList.getOpsPlanner())) {
//			opsPlannerOwner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == projectTeamList.getOpsPlanner().getId()).findAny().orElse(null);
//		}
//		if(!util.isNullOrEmpty(projectTeamList.getGfg())) {
//			gfgOwner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == projectTeamList.getGfg().getId()).findAny().orElse(null);
//		}
		if(!util.isNullOrEmpty(projectTeamList.getProjectSpecialist())) {
			projectSpecialistOwner = ConfigService.getUsersList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == projectTeamList.getProjectSpecialist().getId()).findAny().orElse(null);
		}
		if(!util.isNullOrEmpty(projectTeamList.getRegLead())) {
			regLeadOwner = ConfigService.getUsersList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == projectTeamList.getRegLead().getId()).findAny().orElse(null);
		}
//		if(!util.isNullOrEmpty(projectTeamList.getCommercialLead())) {
//			commercialLeadOwner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == projectTeamList.getCommercialLead().getId()).findAny().orElse(null);
//		}
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask2Duration()) && Integer.parseInt(taskDetails.getTask2Duration()) > 0
				&& !util.isNullOrEmpty(taskDetails.getTask2DueDate()) && !util.isNullOrEmpty(taskDetails.getTask2StartDate())) {
			
			MPMTask task2 = new MPMTask();
			task2.setName("TASK 2");
			task2.setDescription(taskRoleMapping2.getTask_description());
			task2.setDueDate(Date.valueOf(taskDetails.getTask2DueDate().split("T")[0]));
			
			task2.setIsApprovalTask(false);
			task2.setIsDeleted(false);
			task2.setProgress(0);
			task2.setStartDate(Date.valueOf(taskDetails.getTask2StartDate().split("T")[0]));
			task2.setAssignmentType("ROLE");
			task2.setTaskType("TASK_WITHOUT_DELIVERABLE");
			task2.setStatusId(taskDetails.getTask2IsCompleted().equalsIgnoreCase("true") 
					? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
					: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
			Boolean isActive = taskDetails.getTask2IsCompleted().equalsIgnoreCase("true") ? false : 
					taskDetails.getTask2IsActive().equalsIgnoreCase("true") ? true: false;
			task2.setIsActive(isActive);
			task2.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
			task2.setProjectId(projectId);
			task2.setTeamRoleId(task2MPMRole);
			task2.setOwnerId(e2ePMOwner);
			task2.setActiveUserId(e2ePMOwner);
			task2.setExpectedDuration(0);
			task2.setOriginalStartDate(Date.valueOf(taskDetails.getTask2DueDate().split("T")[0]));
			task2.setOriginalDueDate(Date.valueOf(taskDetails.getTask2DueDate().split("T")[0]));
			task2.setTaskDuration(Integer.parseInt(taskDetails.getTask2Duration()) / 7);
			task2.setTaskDurationType("weeks");
//			task2.setId(MPMTaskRepository.getCurrentVal());
			task2.setIsBulkUpdate(false);
			task2.setIsOnHold(false);
			task2.setIsMilestone(false);
			task2.setIsSubproject(false);
			task2.setOrganizationid(orgId);
			mpmTaskList.add(task2);
//			MPMTask task2Response = MPMTaskRepository.save(task2);
//			entityManager.flush();
//			mpmTaskList.add(task2Response);
		}
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask3Duration()) && Integer.parseInt(taskDetails.getTask3Duration()) > 0
				&& !util.isNullOrEmpty(taskDetails.getTask3DueDate()) && !util.isNullOrEmpty(taskDetails.getTask3StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping3 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 3")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping3.getFunctional_role_id().getName());	
			
			MPMTeamRole task3MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
			
//			OpenTextIdentity task3Owner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task3 = new MPMTask();
		task3.setName("TASK 3");
		task3.setDescription(taskRoleMapping3.getTask_description());
		task3.setDueDate(Date.valueOf(taskDetails.getTask3DueDate().split("T")[0]));
		task3.setIsApprovalTask(false);
		task3.setIsDeleted(false);
		task3.setProgress(0);
		task3.setStartDate(Date.valueOf(taskDetails.getTask3StartDate().split("T")[0]));
		task3.setAssignmentType("ROLE");
		task3.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task3.setStatusId(taskDetails.getTask3IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		Boolean isActive = taskDetails.getTask3IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask3IsActive().equalsIgnoreCase("true") ? true: false;
		task3.setIsActive(isActive);
		task3.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task3.setProjectId(projectId);
		task3.setTeamRoleId(task3MPMRole);
		task3.setOwnerId(e2ePMOwner);
		task3.setActiveUserId(e2ePMOwner);
		task3.setExpectedDuration(0);
		task3.setOriginalStartDate(Date.valueOf(taskDetails.getTask3DueDate().split("T")[0]));
		task3.setOriginalDueDate(Date.valueOf(taskDetails.getTask3DueDate().split("T")[0]));
		task3.setTaskDuration(Integer.parseInt(taskDetails.getTask3Duration()));
		task3.setTaskDurationType("weeks");
//		task3.setId(MPMTaskRepository.getCurrentVal());
		task3.setIsBulkUpdate(false);
		task3.setIsOnHold(false);
		task3.setIsMilestone(false);
		task3.setIsSubproject(false);
		task3.setOrganizationid(orgId);
		mpmTaskList.add(task3);
//		logger.info("duration " + task3.getTaskDuration());
//		MPMTask task3Response = MPMTaskRepository.save(task3);
//		entityManager.flush();
//		mpmTaskList.add(task3Response);
		}
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask4Duration()) && Integer.parseInt(taskDetails.getTask4Duration()) > 0
				&& !util.isNullOrEmpty(taskDetails.getTask4DueDate()) && !util.isNullOrEmpty(taskDetails.getTask4StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping4 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 4")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping4.getFunctional_role_id().getName());	
			
			MPMTeamRole task4MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
			
//			OpenTextIdentity task4Owner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
			
		MPMTask task4 = new MPMTask();
		task4.setName("TASK 4");
		task4.setDescription(taskRoleMapping4.getTask_description());
		task4.setDueDate(Date.valueOf(taskDetails.getTask4DueDate().split("T")[0]));
		
		task4.setIsApprovalTask(false);
		task4.setIsDeleted(false);
		task4.setProgress(0);
		task4.setStartDate(Date.valueOf(taskDetails.getTask4StartDate().split("T")[0]));
		task4.setAssignmentType("ROLE");
		task4.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task4.setStatusId(taskDetails.getTask4IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		Boolean isActive = taskDetails.getTask4IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask4IsActive().equalsIgnoreCase("true") ? true: false;
		task4.setIsActive(isActive);
		task4.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task4.setProjectId(projectId);
		task4.setTeamRoleId(task4MPMRole);
		task4.setOwnerId(corpPMOwner);
		task4.setActiveUserId(corpPMOwner);
		task4.setExpectedDuration(0);
		task4.setOriginalStartDate(Date.valueOf(taskDetails.getTask4DueDate().split("T")[0]));
		task4.setOriginalDueDate(Date.valueOf(taskDetails.getTask4DueDate().split("T")[0]));
		task4.setTaskDuration(Integer.parseInt(taskDetails.getTask4Duration()));
		task4.setTaskDurationType("weeks");
//		task4.setId(MPMTaskRepository.getCurrentVal());
		task4.setIsBulkUpdate(false);
		task4.setIsOnHold(false);
		task4.setIsMilestone(false);
		task4.setIsSubproject(false);
		task4.setOrganizationid(orgId);
		mpmTaskList.add(task4);
//		MPMTask task4Response = MPMTaskRepository.save(task4);
//		entityManager.flush();
//		mpmTaskList.add(task4Response);
		}
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask5Duration()) && Integer.parseInt(taskDetails.getTask5Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask5DueDate()) && !util.isNullOrEmpty(taskDetails.getTask5StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping5 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 5")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping5.getFunctional_role_id().getName());	
			
			MPMTeamRole task5MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
//			OpenTextIdentity task5Owner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task5 = new MPMTask();
		task5.setName("TASK 5");
		task5.setDescription(taskRoleMapping5.getTask_description());
		task5.setDueDate(Date.valueOf(taskDetails.getTask5DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask5IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask5IsActive().equalsIgnoreCase("true") ? true: false;
		task5.setIsActive(isActive);
		task5.setIsApprovalTask(false);
		task5.setIsDeleted(false);
		task5.setProgress(0);
		task5.setStartDate(Date.valueOf(taskDetails.getTask5StartDate().split("T")[0]));
		task5.setAssignmentType("ROLE");
		task5.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task5.setStatusId(taskDetails.getTask5IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task5.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task5.setProjectId(projectId);
		task5.setTeamRoleId(task5MPMRole);
		task5.setOwnerId(opsPMOwner);
		task5.setActiveUserId(opsPMOwner);
		task5.setExpectedDuration(0);
		task5.setOriginalStartDate(Date.valueOf(taskDetails.getTask5DueDate().split("T")[0]));
		task5.setOriginalDueDate(Date.valueOf(taskDetails.getTask5DueDate().split("T")[0]));
		task5.setTaskDuration(Integer.parseInt(taskDetails.getTask5Duration()));
		task5.setTaskDurationType("weeks");
//		task5.setId(MPMTaskRepository.getCurrentVal());
		task5.setIsBulkUpdate(false);
		task5.setIsOnHold(false);
		task5.setIsMilestone(false);
		task5.setIsSubproject(false);
		task5.setOrganizationid(orgId);
		mpmTaskList.add(task5);
//		MPMTask task5Response = MPMTaskRepository.save(task5);
//		entityManager.flush();
//		mpmTaskList.add(task5Response);
		}
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask6Duration()) && Integer.parseInt(taskDetails.getTask6Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask6DueDate()) && !util.isNullOrEmpty(taskDetails.getTask6StartDate())) {
		
		TaskRoleMappingDependencies taskRoleMapping6 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 6")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping6.getFunctional_role_id().getName());	
		
		MPMTeamRole task6MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task6Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task6 = new MPMTask();
		task6.setName("TASK 6");
		task6.setDescription(taskRoleMapping6.getTask_description());
		task6.setDueDate(Date.valueOf(taskDetails.getTask6DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask6IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask6IsActive().equalsIgnoreCase("true") ? true: false;
		task6.setIsActive(isActive);
		task6.setIsApprovalTask(false);
		task6.setIsDeleted(false);
		task6.setProgress(0);
		task6.setStartDate(Date.valueOf(taskDetails.getTask6StartDate().split("T")[0]));
		task6.setAssignmentType("ROLE");
		task6.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task6.setStatusId(taskDetails.getTask6IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task6.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task6.setProjectId(projectId);
		task6.setTeamRoleId(task6MPMRole);
		task6.setOwnerId(rtmOwner);
		task6.setActiveUserId(rtmOwner);
		task6.setExpectedDuration(0);
		task6.setOriginalStartDate(Date.valueOf(taskDetails.getTask6DueDate().split("T")[0]));
		task6.setOriginalDueDate(Date.valueOf(taskDetails.getTask6DueDate().split("T")[0]));
		task6.setTaskDuration(Integer.parseInt(taskDetails.getTask6Duration()));
		task6.setTaskDurationType("weeks");
//		task6.setId(MPMTaskRepository.getCurrentVal());
		task6.setIsBulkUpdate(false);
		task6.setIsOnHold(false);
		task6.setIsMilestone(false);
		task6.setIsSubproject(false);
		task6.setOrganizationid(orgId);
		mpmTaskList.add(task6);
//		MPMTask task6Response = MPMTaskRepository.save(task6);
//		entityManager.flush();
//		mpmTaskList.add(task6Response);
		}
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask7Duration()) && Integer.parseInt(taskDetails.getTask7Duration()) > 0
				&& !util.isNullOrEmpty(taskDetails.getTask7DueDate()) && !util.isNullOrEmpty(taskDetails.getTask7StartDate())) {
		
		TaskRoleMappingDependencies taskRoleMapping7 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 7")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping7.getFunctional_role_id().getName());	
		
		MPMTeamRole task7MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
		OpenTextIdentity task7Owner = ConfigService.getUsersList().stream()
				.filter(obj -> Objects.nonNull(obj.getId()))
				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task7 = new MPMTask();
		task7.setName("TASK 7");
		task7.setDescription(taskRoleMapping7.getTask_description());
		task7.setDueDate(Date.valueOf(taskDetails.getTask7DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask7IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask7IsActive().equalsIgnoreCase("true") ? true: false;
		task7.setIsActive(isActive);
		task7.setIsApprovalTask(false);
		task7.setIsDeleted(false);
		task7.setProgress(0);
		task7.setStartDate(Date.valueOf(taskDetails.getTask7StartDate().split("T")[0]));
		task7.setAssignmentType("ROLE");
		task7.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task7.setStatusId(taskDetails.getTask7IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task7.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task7.setProjectId(projectId);
		task7.setTeamRoleId(task7MPMRole);
		task7.setOwnerId(task7Owner);
		task7.setActiveUserId(task7Owner);
		task7.setExpectedDuration(0);
		task7.setOriginalStartDate(Date.valueOf(taskDetails.getTask7DueDate().split("T")[0]));
		task7.setOriginalDueDate(Date.valueOf(taskDetails.getTask7DueDate().split("T")[0]));
		task7.setTaskDuration(Integer.parseInt(taskDetails.getTask7Duration()));
		task7.setTaskDurationType("weeks");
//		task7.setId(MPMTaskRepository.getCurrentVal());
		task7.setIsBulkUpdate(false);
		task7.setIsOnHold(false);
		task7.setIsMilestone(false);
		task7.setIsSubproject(false);
		task7.setOrganizationid(orgId);
		mpmTaskList.add(task7);
//		MPMTask task7Response = MPMTaskRepository.save(task7);
//		entityManager.flush();
//		mpmTaskList.add(task7Response);
		}
		
	
		
		if(!util.isNullOrEmpty(taskDetails.getTask9Duration()) && Integer.parseInt(taskDetails.getTask9Duration()) > 0
				&& !util.isNullOrEmpty(taskDetails.getTask9DueDate()) && !util.isNullOrEmpty(taskDetails.getTask9StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping9 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 9")).findAny().orElse(null);
		
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping9.getFunctional_role_id().getName());	
			
			MPMTeamRole task9MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task9Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task9 = new MPMTask();
		task9.setName("TASK 9");
		task9.setDescription(taskRoleMapping9.getTask_description());
		task9.setDueDate(Date.valueOf(taskDetails.getTask9DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask9IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask9IsActive().equalsIgnoreCase("true") ? true: false;
		task9.setIsActive(isActive);
		task9.setIsApprovalTask(false);
		task9.setIsDeleted(false);
		task9.setProgress(0);
		task9.setStartDate(Date.valueOf(taskDetails.getTask9StartDate().split("T")[0]));
		task9.setAssignmentType("ROLE");
		task9.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task9.setStatusId(taskDetails.getTask9IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task9.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task9.setProjectId(projectId);
		task9.setTeamRoleId(task9MPMRole);
		task9.setOwnerId(projectSpecialistOwner);
		task9.setActiveUserId(projectSpecialistOwner);
		task9.setExpectedDuration(0);
		task9.setOriginalStartDate(Date.valueOf(taskDetails.getTask9DueDate().split("T")[0]));
		task9.setOriginalDueDate(Date.valueOf(taskDetails.getTask9DueDate().split("T")[0]));
		task9.setTaskDuration(Integer.parseInt(taskDetails.getTask9Duration()));
		task9.setTaskDurationType("weeks");
//		task9.setId(MPMTaskRepository.getCurrentVal());
		task9.setIsBulkUpdate(false);
		task9.setIsOnHold(false);
		task9.setIsMilestone(false);
		task9.setIsSubproject(false);
		task9.setOrganizationid(orgId);
		mpmTaskList.add(task9);
//		MPMTask task9Response = MPMTaskRepository.save(task9);
//		entityManager.flush();
//		mpmTaskList.add(task9Response);
		}
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask10Duration()) && Integer.parseInt(taskDetails.getTask10Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask10DueDate()) && !util.isNullOrEmpty(taskDetails.getTask10StartDate())) {
		
		TaskRoleMappingDependencies taskRoleMapping10 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 10")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping10.getFunctional_role_id().getName());	
		
		MPMTeamRole task10MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task10Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task10 = new MPMTask();
		task10.setName("TASK 10");
		task10.setDescription(taskRoleMapping10.getTask_description());
		task10.setDueDate(Date.valueOf(taskDetails.getTask10DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask10IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask10IsActive().equalsIgnoreCase("true") ? true: false;
		task10.setIsActive(isActive);
		task10.setIsApprovalTask(false);
		task10.setIsDeleted(false);
		task10.setProgress(0);
		task10.setStartDate(Date.valueOf(taskDetails.getTask10StartDate().split("T")[0]));
		task10.setAssignmentType("ROLE");
		task10.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task10.setStatusId(taskDetails.getTask10IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task10.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task10.setProjectId(projectId);
		task10.setTeamRoleId(task10MPMRole);
		task10.setOwnerId(e2ePMOwner);
		task10.setActiveUserId(e2ePMOwner);
		task10.setExpectedDuration(0);
		task10.setOriginalStartDate(Date.valueOf(taskDetails.getTask10DueDate().split("T")[0]));
		task10.setOriginalDueDate(Date.valueOf(taskDetails.getTask10DueDate().split("T")[0]));
		task10.setTaskDuration(Integer.parseInt(taskDetails.getTask10Duration()));
		task10.setTaskDurationType("weeks");
//		task10.setId(MPMTaskRepository.getCurrentVal());
		task10.setIsBulkUpdate(false);
		task10.setIsOnHold(false);
		task10.setIsMilestone(false);
		task10.setIsSubproject(false);
		task10.setOrganizationid(orgId);
		mpmTaskList.add(task10);
//		MPMTask task10Response = MPMTaskRepository.save(task10);
//		entityManager.flush();
//		mpmTaskList.add(task10Response);
		}
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask11Duration()) && Integer.parseInt(taskDetails.getTask11Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask11DueDate()) && !util.isNullOrEmpty(taskDetails.getTask11StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping11 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 11")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping11.getFunctional_role_id().getName());	
		
		MPMTeamRole task11MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task11Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task11 = new MPMTask();
		task11.setName("TASK 11");
		task11.setDescription(taskRoleMapping11.getTask_description());
		task11.setDueDate(Date.valueOf(taskDetails.getTask11DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask11IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask11IsActive().equalsIgnoreCase("true") ? true: false;
		task11.setIsActive(isActive);
		task11.setIsApprovalTask(false);
		task11.setIsDeleted(false);
		task11.setProgress(0);
		task11.setStartDate(Date.valueOf(taskDetails.getTask11StartDate().split("T")[0]));
		task11.setAssignmentType("ROLE");
		task11.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task11.setStatusId(taskDetails.getTask11IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task11.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task11.setProjectId(projectId);
		task11.setTeamRoleId(task11MPMRole);
		task11.setOwnerId(corpPMOwner);
		task11.setActiveUserId(corpPMOwner);
		task11.setExpectedDuration(0);
		task11.setOriginalStartDate(Date.valueOf(taskDetails.getTask11DueDate().split("T")[0]));
		task11.setOriginalDueDate(Date.valueOf(taskDetails.getTask11DueDate().split("T")[0]));
		task11.setTaskDuration(Integer.parseInt(taskDetails.getTask11Duration()));
		task11.setTaskDurationType("weeks");
//		task11.setId(MPMTaskRepository.getCurrentVal());
		task11.setIsBulkUpdate(false);
		task11.setIsOnHold(false);
		task11.setIsMilestone(false);
		task11.setIsSubproject(false);
		task11.setOrganizationid(orgId);
		mpmTaskList.add(task11);
//		MPMTask task11Response = MPMTaskRepository.save(task11);
//		entityManager.flush();
//		mpmTaskList.add(task11Response);
		}
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask12Duration()) && Integer.parseInt(taskDetails.getTask12Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask12DueDate()) && !util.isNullOrEmpty(taskDetails.getTask12StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping12 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 12")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping12.getFunctional_role_id().getName());	
		
		MPMTeamRole task12MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task12Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task12 = new MPMTask();
		task12.setName("TASK 12");
		task12.setDescription(taskRoleMapping12.getTask_description());
		task12.setDueDate(Date.valueOf(taskDetails.getTask12DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask12IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask12IsActive().equalsIgnoreCase("true") ? true: false;
		task12.setIsActive(isActive);
		task12.setIsApprovalTask(false);
		task12.setIsDeleted(false);
		task12.setProgress(0);
		task12.setStartDate(Date.valueOf(taskDetails.getTask12StartDate().split("T")[0]));
		task12.setAssignmentType("ROLE");
		task12.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task12.setStatusId(taskDetails.getTask12IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task12.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task12.setProjectId(projectId);
		task12.setTeamRoleId(task12MPMRole);
		task12.setOwnerId(corpPMOwner);
		task12.setActiveUserId(corpPMOwner);
		task12.setExpectedDuration(0);
		task12.setOriginalStartDate(Date.valueOf(taskDetails.getTask12DueDate().split("T")[0]));
		task12.setOriginalDueDate(Date.valueOf(taskDetails.getTask12DueDate().split("T")[0]));
		task12.setTaskDuration(Integer.parseInt(taskDetails.getTask12Duration()));
		task12.setTaskDurationType("weeks");
//		task12.setId(MPMTaskRepository.getCurrentVal());
		task12.setIsBulkUpdate(false);
		task12.setIsOnHold(false);
		task12.setIsMilestone(false);
		task12.setIsSubproject(false);
		task12.setOrganizationid(orgId);
		mpmTaskList.add(task12);
//		MPMTask task12Response = MPMTaskRepository.save(task12);
//		entityManager.flush();
//		mpmTaskList.add(task12Response);
		}
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask13Duration()) && Integer.parseInt(taskDetails.getTask13Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask13DueDate()) && !util.isNullOrEmpty(taskDetails.getTask13StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping13 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 13")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping13.getFunctional_role_id().getName());	
		
		MPMTeamRole task13MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task13Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task13 = new MPMTask();
		task13.setName("TASK 13");
		task13.setDescription(taskRoleMapping13.getTask_description());
		task13.setDueDate(Date.valueOf(taskDetails.getTask13DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask13IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask13IsActive().equalsIgnoreCase("true") ? true: false;
		task13.setIsActive(isActive);
		task13.setIsApprovalTask(false);
		task13.setIsDeleted(false);
		task13.setProgress(0);
		task13.setStartDate(Date.valueOf(taskDetails.getTask13StartDate().split("T")[0]));
		task13.setAssignmentType("ROLE");
		task13.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task13.setStatusId(taskDetails.getTask13IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task13.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task13.setProjectId(projectId);
		task13.setTeamRoleId(task13MPMRole);
		task13.setOwnerId(corpPMOwner);
		task13.setActiveUserId(corpPMOwner);
		task13.setExpectedDuration(0);
		task13.setOriginalStartDate(Date.valueOf(taskDetails.getTask13DueDate().split("T")[0]));
		task13.setOriginalDueDate(Date.valueOf(taskDetails.getTask13DueDate().split("T")[0]));
		task13.setTaskDuration(Integer.parseInt(taskDetails.getTask13Duration()));
		task13.setTaskDurationType("weeks");
//		task13.setId(MPMTaskRepository.getCurrentVal());
		task13.setIsBulkUpdate(false);
		task13.setIsOnHold(false);
		task13.setIsMilestone(false);
		task13.setIsSubproject(false);
		task13.setOrganizationid(orgId);
		mpmTaskList.add(task13);
//		MPMTask task13Response = MPMTaskRepository.save(task13);
//		entityManager.flush();
//		mpmTaskList.add(task13Response);
		}
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask13aDuration()) && Integer.parseInt(taskDetails.getTask13aDuration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask13aDueDate()) && !util.isNullOrEmpty(taskDetails.getTask13aStartDate())) {
		TaskRoleMappingDependencies taskRoleMapping13a = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 13a")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping13a.getFunctional_role_id().getName());	
		
		MPMTeamRole task13aMPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task13aOwner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task13a = new MPMTask();
		task13a.setName("TASK 13A");
		task13a.setDescription(taskRoleMapping13a.getTask_description());
		task13a.setDueDate(Date.valueOf(taskDetails.getTask13aDueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask13aIsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask13aIsActive().equalsIgnoreCase("true") ? true: false;
		task13a.setIsActive(isActive);
		task13a.setIsApprovalTask(false);
		task13a.setIsDeleted(false);
		task13a.setProgress(0);
		task13a.setStartDate(Date.valueOf(taskDetails.getTask13aStartDate().split("T")[0]));
		task13a.setAssignmentType("ROLE");
		task13a.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task13a.setStatusId(taskDetails.getTask13aIsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task13a.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task13a.setProjectId(projectId);
		task13a.setTeamRoleId(task13aMPMRole);
		task13a.setOwnerId(corpPMOwner);
		task13a.setActiveUserId(corpPMOwner);
		task13a.setExpectedDuration(0);
		task13a.setOriginalStartDate(Date.valueOf(taskDetails.getTask13aDueDate().split("T")[0]));
		task13a.setOriginalDueDate(Date.valueOf(taskDetails.getTask13aDueDate().split("T")[0]));
		task13a.setTaskDuration(Integer.parseInt(taskDetails.getTask13aDuration()));
		task13a.setTaskDurationType("weeks");
//		task13a.setId(MPMTaskRepository.getCurrentVal());
		task13a.setIsBulkUpdate(false);
		task13a.setIsOnHold(false);
		task13a.setIsMilestone(false);
		task13a.setIsSubproject(false);
		task13a.setOrganizationid(orgId);
		mpmTaskList.add(task13a);
//		MPMTask task13aResponse = MPMTaskRepository.save(task13a);
//		entityManager.flush();
//		mpmTaskList.add(task13aResponse);
		}
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask14Duration()) && Integer.parseInt(taskDetails.getTask14Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask14DueDate()) && !util.isNullOrEmpty(taskDetails.getTask14StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping14 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 14")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping14.getFunctional_role_id().getName());	
		
		MPMTeamRole task14MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task14Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task14 = new MPMTask();
		task14.setName("TASK 14");
		task14.setDescription(taskRoleMapping14.getTask_description());
		task14.setDueDate(Date.valueOf(taskDetails.getTask14DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask14IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask14IsActive().equalsIgnoreCase("true") ? true: false;
		task14.setIsActive(isActive);
		task14.setIsApprovalTask(false);
		task14.setIsDeleted(false);
		task14.setProgress(0);
		task14.setStartDate(Date.valueOf(taskDetails.getTask14StartDate().split("T")[0]));
		task14.setAssignmentType("ROLE");
		task14.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task14.setStatusId(taskDetails.getTask14IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task14.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task14.setProjectId(projectId);
		task14.setTeamRoleId(task14MPMRole);
		task14.setOwnerId(projectSpecialistOwner);
		task14.setActiveUserId(projectSpecialistOwner);
		task14.setExpectedDuration(0);
		task14.setOriginalStartDate(Date.valueOf(taskDetails.getTask14DueDate().split("T")[0]));
		task14.setOriginalDueDate(Date.valueOf(taskDetails.getTask14DueDate().split("T")[0]));
		task14.setTaskDuration(Integer.parseInt(taskDetails.getTask14Duration()));
		task14.setTaskDurationType("weeks");
//		task14.setId(MPMTaskRepository.getCurrentVal());
		task14.setIsBulkUpdate(false);
		task14.setIsOnHold(false);
		task14.setIsMilestone(false);
		task14.setIsSubproject(false);
		task14.setOrganizationid(orgId);
		mpmTaskList.add(task14);
//		MPMTask task14Response = MPMTaskRepository.save(task14);
//		entityManager.flush();
//		mpmTaskList.add(task14Response);
		}
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask15Duration()) && Integer.parseInt(taskDetails.getTask15Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask15DueDate()) && !util.isNullOrEmpty(taskDetails.getTask15StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping15 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 15")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping15.getFunctional_role_id().getName());	
		
		MPMTeamRole task15MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task15Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task15 = new MPMTask();
		task15.setName("TASK 15");
		task15.setDescription(taskRoleMapping15.getTask_description());
		task15.setDueDate(Date.valueOf(taskDetails.getTask15DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask15IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask15IsActive().equalsIgnoreCase("true") ? true: false;
		task15.setIsActive(isActive);
		task15.setIsApprovalTask(false);
		task15.setIsDeleted(false);
		task15.setProgress(0);
		task15.setStartDate(Date.valueOf(taskDetails.getTask15StartDate().split("T")[0]));
		task15.setAssignmentType("ROLE");
		task15.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task15.setStatusId(taskDetails.getTask15IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task15.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task15.setProjectId(projectId);
		task15.setTeamRoleId(task15MPMRole);
		task15.setOwnerId(rtmOwner);
		task15.setActiveUserId(rtmOwner);
		task15.setExpectedDuration(0);
		task15.setOriginalStartDate(Date.valueOf(taskDetails.getTask15DueDate().split("T")[0]));
		task15.setOriginalDueDate(Date.valueOf(taskDetails.getTask15DueDate().split("T")[0]));
		task15.setTaskDuration(Integer.parseInt(taskDetails.getTask15Duration()));
		task15.setTaskDurationType("weeks");
//		task15.setId(MPMTaskRepository.getCurrentVal());
		task15.setIsBulkUpdate(false);
		task15.setIsOnHold(false);
		task15.setIsMilestone(false);
		task15.setIsSubproject(false);
		task15.setOrganizationid(orgId);
		mpmTaskList.add(task15);
//		MPMTask task15Response = MPMTaskRepository.save(task15);
//		entityManager.flush();
//		mpmTaskList.add(task15Response);
		}
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask16Duration()) && Integer.parseInt(taskDetails.getTask16Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask16DueDate()) && !util.isNullOrEmpty(taskDetails.getTask16StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping16 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 16")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping16.getFunctional_role_id().getName());	
		
		MPMTeamRole task16MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task16Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task16 = new MPMTask();
		task16.setName("TASK 16");
		task16.setDescription(taskRoleMapping16.getTask_description());
		task16.setDueDate(Date.valueOf(taskDetails.getTask16DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask16IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask16IsActive().equalsIgnoreCase("true") ? true: false;
		task16.setIsActive(isActive);
		task16.setIsApprovalTask(false);
		task16.setIsDeleted(false);
		task16.setProgress(0);
		task16.setStartDate(Date.valueOf(taskDetails.getTask16StartDate().split("T")[0]));
		task16.setAssignmentType("ROLE");
		task16.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task16.setStatusId(taskDetails.getTask16IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task16.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task16.setProjectId(projectId);
		task16.setTeamRoleId(task16MPMRole);
		task16.setOwnerId(corpPMOwner);
		task16.setActiveUserId(corpPMOwner);
		task16.setExpectedDuration(0);
		task16.setOriginalStartDate(Date.valueOf(taskDetails.getTask16DueDate().split("T")[0]));
		task16.setOriginalDueDate(Date.valueOf(taskDetails.getTask16DueDate().split("T")[0]));
		task16.setTaskDuration(Integer.parseInt(taskDetails.getTask16Duration()));
		task16.setTaskDurationType("weeks");
//		task16.setId(MPMTaskRepository.getCurrentVal());
		task16.setIsBulkUpdate(false);
		task16.setIsOnHold(false);
		task16.setIsMilestone(false);
		task16.setIsSubproject(false);
		task16.setOrganizationid(orgId);
		mpmTaskList.add(task16);
//		MPMTask task16Response = MPMTaskRepository.save(task16);
//		entityManager.flush();
//		mpmTaskList.add(task16Response);
		}
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask17Duration()) && Integer.parseInt(taskDetails.getTask17Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask17DueDate()) && !util.isNullOrEmpty(taskDetails.getTask17StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping17 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 17")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping17.getFunctional_role_id().getName());	
		
		MPMTeamRole task17MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task17Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task17 = new MPMTask();
		task17.setName("TASK 17");
		task17.setDescription(taskRoleMapping17.getTask_description());
		task17.setDueDate(Date.valueOf(taskDetails.getTask17DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask17IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask17IsActive().equalsIgnoreCase("true") ? true: false;
		task17.setIsActive(isActive);
		task17.setIsApprovalTask(false);
		task17.setIsDeleted(false);
		task17.setProgress(0);
		task17.setStartDate(Date.valueOf(taskDetails.getTask17StartDate().split("T")[0]));
		task17.setAssignmentType("ROLE");
		task17.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task17.setStatusId(taskDetails.getTask17IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task17.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task17.setProjectId(projectId);
		task17.setTeamRoleId(task17MPMRole);
		task17.setOwnerId(regLeadOwner);
		task17.setActiveUserId(regLeadOwner);
		task17.setExpectedDuration(0);
		task17.setOriginalStartDate(Date.valueOf(taskDetails.getTask17DueDate().split("T")[0]));
		task17.setOriginalDueDate(Date.valueOf(taskDetails.getTask17DueDate().split("T")[0]));
		task17.setTaskDuration(Integer.parseInt(taskDetails.getTask17Duration()));
		task17.setTaskDurationType("weeks");
//		task17.setId(MPMTaskRepository.getCurrentVal());
		task17.setIsBulkUpdate(false);
		task17.setIsOnHold(false);
		task17.setIsMilestone(false);
		task17.setIsSubproject(false);
		task17.setOrganizationid(orgId);
		mpmTaskList.add(task17);
//		MPMTask task17Response = MPMTaskRepository.save(task17);
//		entityManager.flush();
//		mpmTaskList.add(task17Response);
		}
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask18Duration()) && Integer.parseInt(taskDetails.getTask18Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask18DueDate()) && !util.isNullOrEmpty(taskDetails.getTask18StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping18 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 18")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping18.getFunctional_role_id().getName());	
		
		MPMTeamRole task18MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task18Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task18 = new MPMTask();
		task18.setName("TASK 18");
		task18.setDescription(taskRoleMapping18.getTask_description());
		task18.setDueDate(Date.valueOf(taskDetails.getTask18DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask18IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask18IsActive().equalsIgnoreCase("true") ? true: false;
		task18.setIsActive(isActive);
		task18.setIsApprovalTask(false);
		task18.setIsDeleted(false);
		task18.setProgress(0);
		task18.setStartDate(Date.valueOf(taskDetails.getTask18StartDate().split("T")[0]));
		task18.setAssignmentType("ROLE");
		task18.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task18.setStatusId(taskDetails.getTask18IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task18.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task18.setProjectId(projectId);
		task18.setTeamRoleId(task18MPMRole);
//		task18.setOwnerId(task18Owner);
//		task18.setActiveUserId(task18Owner);
		task18.setExpectedDuration(0);
		task18.setOriginalStartDate(Date.valueOf(taskDetails.getTask18DueDate().split("T")[0]));
		task18.setOriginalDueDate(Date.valueOf(taskDetails.getTask18DueDate().split("T")[0]));
		task18.setTaskDuration(Integer.parseInt(taskDetails.getTask18Duration()));
		task18.setTaskDurationType("weeks");
//		task18.setId(MPMTaskRepository.getCurrentVal());
		task18.setIsBulkUpdate(false);
		task18.setIsOnHold(false);
		task18.setIsMilestone(false);
		task18.setIsSubproject(false);
		task18.setOrganizationid(orgId);
		mpmTaskList.add(task18);
//		MPMTask task18Response = MPMTaskRepository.save(task18);
//		entityManager.flush();
//		mpmTaskList.add(task18Response);
		}
		
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask19Duration()) && Integer.parseInt(taskDetails.getTask19Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask19DueDate()) && !util.isNullOrEmpty(taskDetails.getTask19StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping19 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 19")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping19.getFunctional_role_id().getName());	
		
		MPMTeamRole task19MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task19Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		
		MPMTask task19 = new MPMTask();
		task19.setName("TASK 19");
		task19.setDescription(taskRoleMapping19.getTask_description());
		task19.setDueDate(Date.valueOf(taskDetails.getTask19DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask19IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask19IsActive().equalsIgnoreCase("true") ? true: false;
		task19.setIsActive(isActive);
		task19.setIsApprovalTask(false);
		task19.setIsDeleted(false);
		task19.setProgress(0);
		task19.setStartDate(Date.valueOf(taskDetails.getTask19StartDate().split("T")[0]));
		task19.setAssignmentType("ROLE");
		task19.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task19.setStatusId(taskDetails.getTask19IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task19.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task19.setProjectId(projectId);
		task19.setTeamRoleId(task19MPMRole);
		task19.setOwnerId(projectSpecialistOwner);
		task19.setActiveUserId(projectSpecialistOwner);
		task19.setExpectedDuration(0);
		task19.setOriginalStartDate(Date.valueOf(taskDetails.getTask19DueDate().split("T")[0]));
		task19.setOriginalDueDate(Date.valueOf(taskDetails.getTask19DueDate().split("T")[0]));
		task19.setTaskDuration(Integer.parseInt(taskDetails.getTask19Duration()));
		task19.setTaskDurationType("weeks");
//		task19.setId(MPMTaskRepository.getCurrentVal());
		task19.setIsBulkUpdate(false);
		task19.setIsOnHold(false);
		task19.setIsMilestone(false);
		task19.setIsSubproject(false);
		task19.setOrganizationid(orgId);
		mpmTaskList.add(task19);
//		MPMTask task19Response = MPMTaskRepository.save(task19);
//		entityManager.flush();
//		mpmTaskList.add(task19Response);
		}
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask20Duration()) && Integer.parseInt(taskDetails.getTask20Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask20DueDate()) && !util.isNullOrEmpty(taskDetails.getTask20StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping20 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 20")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping20.getFunctional_role_id().getName());	
		
		MPMTeamRole task20MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task20Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task20 = new MPMTask();
		task20.setName("TASK 20");
		task20.setDescription(taskRoleMapping20.getTask_description());
		task20.setDueDate(Date.valueOf(taskDetails.getTask20DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask20IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask20IsActive().equalsIgnoreCase("true") ? true: false;
		task20.setIsActive(isActive);
		task20.setIsApprovalTask(false);
		task20.setIsDeleted(false);
		task20.setProgress(0);
		task20.setStartDate(Date.valueOf(taskDetails.getTask20StartDate().split("T")[0]));
		task20.setAssignmentType("ROLE");
		task20.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task20.setStatusId(taskDetails.getTask20IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task20.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task20.setProjectId(projectId);
		task20.setTeamRoleId(task20MPMRole);
		task20.setOwnerId(regLeadOwner);
		task20.setActiveUserId(regLeadOwner);
		task20.setExpectedDuration(0);
		task20.setOriginalStartDate(Date.valueOf(taskDetails.getTask20DueDate().split("T")[0]));
		task20.setOriginalDueDate(Date.valueOf(taskDetails.getTask20DueDate().split("T")[0]));
		task20.setTaskDuration(Integer.parseInt(taskDetails.getTask20Duration()));
		task20.setTaskDurationType("weeks");
//		task20.setId(MPMTaskRepository.getCurrentVal());
		task20.setIsBulkUpdate(false);
		task20.setIsOnHold(false);
		task20.setIsMilestone(false);
		task20.setIsSubproject(false);
		task20.setOrganizationid(orgId);
		mpmTaskList.add(task20);
//		MPMTask task20Response = MPMTaskRepository.save(task20);
//		entityManager.flush();
//		mpmTaskList.add(task20Response);
		}
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask21Duration()) && Integer.parseInt(taskDetails.getTask21Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask21DueDate()) && !util.isNullOrEmpty(taskDetails.getTask21StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping21 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 21")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping21.getFunctional_role_id().getName());	
		
		MPMTeamRole task21MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task21Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task21 = new MPMTask();
		task21.setName("TASK 21");
		task21.setDescription(taskRoleMapping21.getTask_description());
		task21.setDueDate(Date.valueOf(taskDetails.getTask21DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask21IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask21IsActive().equalsIgnoreCase("true") ? true: false;
		task21.setIsActive(isActive);
		task21.setIsApprovalTask(false);
		task21.setIsDeleted(false);
		task21.setProgress(0);
		task21.setStartDate(Date.valueOf(taskDetails.getTask21StartDate().split("T")[0]));
		task21.setAssignmentType("ROLE");
		task21.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task21.setStatusId(taskDetails.getTask21IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task21.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task21.setProjectId(projectId);
		task21.setTeamRoleId(task21MPMRole);
		task21.setOwnerId(regLeadOwner);
		task21.setActiveUserId(regLeadOwner);
		task21.setExpectedDuration(0);
		task21.setOriginalStartDate(Date.valueOf(taskDetails.getTask21DueDate().split("T")[0]));
		task21.setOriginalDueDate(Date.valueOf(taskDetails.getTask21DueDate().split("T")[0]));
		task21.setTaskDuration(Integer.parseInt(taskDetails.getTask21Duration()));
		task21.setTaskDurationType("weeks");
//		task21.setId(MPMTaskRepository.getCurrentVal());
		task21.setIsBulkUpdate(false);
		task21.setIsOnHold(false);
		task21.setIsMilestone(false);
		task21.setIsSubproject(false);
		task21.setOrganizationid(orgId);
		mpmTaskList.add(task21);
//		MPMTask task21Response = MPMTaskRepository.save(task21);
//		entityManager.flush();
//		mpmTaskList.add(task21Response);
		}
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask22Duration()) && Integer.parseInt(taskDetails.getTask22Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask22DueDate()) && !util.isNullOrEmpty(taskDetails.getTask22StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping22 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 22")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping22.getFunctional_role_id().getName());	
		
		MPMTeamRole task22MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task22Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task22 = new MPMTask();
		task22.setName("TASK 22");
		task22.setDescription(taskRoleMapping22.getTask_description());
		task22.setDueDate(Date.valueOf(taskDetails.getTask22DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask22IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask22IsActive().equalsIgnoreCase("true") ? true: false;
		task22.setIsActive(isActive);
		task22.setIsApprovalTask(false);
		task22.setIsDeleted(false);
		task22.setProgress(0);
		task22.setStartDate(Date.valueOf(taskDetails.getTask22StartDate().split("T")[0]));
		task22.setAssignmentType("ROLE");
		task22.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task22.setStatusId(taskDetails.getTask22IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task22.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task22.setProjectId(projectId);
		task22.setTeamRoleId(task22MPMRole);
		task22.setOwnerId(regLeadOwner);
		task22.setActiveUserId(regLeadOwner);
		task22.setExpectedDuration(0);
		task22.setOriginalStartDate(Date.valueOf(taskDetails.getTask22DueDate().split("T")[0]));
		task22.setOriginalDueDate(Date.valueOf(taskDetails.getTask22DueDate().split("T")[0]));
		task22.setTaskDuration(Integer.parseInt(taskDetails.getTask22Duration()));
		task22.setTaskDurationType("weeks");
//		task22.setId(MPMTaskRepository.getCurrentVal());
		task22.setIsBulkUpdate(false);
		task22.setIsOnHold(false);
		task22.setIsMilestone(false);
		task22.setIsSubproject(false);
		task22.setOrganizationid(orgId);
		mpmTaskList.add(task22);
//		MPMTask task22Response = MPMTaskRepository.save(task22);
//		entityManager.flush();
//		mpmTaskList.add(task22Response);
		}
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask23Duration()) && Integer.parseInt(taskDetails.getTask23Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask23DueDate()) && !util.isNullOrEmpty(taskDetails.getTask23StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping23 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 23")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping23.getFunctional_role_id().getName());	
		
		MPMTeamRole task23MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task23Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task23 = new MPMTask();
		task23.setName("TASK 23");
		task23.setDescription(taskRoleMapping23.getTask_description());
		task23.setDueDate(Date.valueOf(taskDetails.getTask23DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask23IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask23IsActive().equalsIgnoreCase("true") ? true: false;
		task23.setIsActive(isActive);
		task23.setIsApprovalTask(false);
		task23.setIsDeleted(false);
		task23.setProgress(0);
		task23.setStartDate(Date.valueOf(taskDetails.getTask23StartDate().split("T")[0]));
		task23.setAssignmentType("ROLE");
		task23.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task23.setStatusId(taskDetails.getTask23IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task23.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task23.setProjectId(projectId);
		task23.setTeamRoleId(task23MPMRole);
		task23.setOwnerId(opsPMOwner);
		task23.setActiveUserId(opsPMOwner);
		task23.setExpectedDuration(0);
		task23.setOriginalStartDate(Date.valueOf(taskDetails.getTask23DueDate().split("T")[0]));
		task23.setOriginalDueDate(Date.valueOf(taskDetails.getTask23DueDate().split("T")[0]));
		task23.setTaskDuration(Integer.parseInt(taskDetails.getTask23Duration()));
		task23.setTaskDurationType("weeks");
//		task23.setId(MPMTaskRepository.getCurrentVal());
		task23.setIsBulkUpdate(false);
		task23.setIsOnHold(false);
		task23.setIsMilestone(false);
		task23.setIsSubproject(false);
		task23.setOrganizationid(orgId);
		mpmTaskList.add(task23);
//		MPMTask task23Response = MPMTaskRepository.save(task23);
//		entityManager.flush();
//		mpmTaskList.add(task23Response);
		}
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask24Duration()) && Integer.parseInt(taskDetails.getTask24Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask24DueDate()) && !util.isNullOrEmpty(taskDetails.getTask24StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping24 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 24")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping24.getFunctional_role_id().getName());	
		
		MPMTeamRole task24MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task24Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		
		MPMTask task24 = new MPMTask();
		task24.setName("TASK 24");
		task24.setDescription(taskRoleMapping24.getTask_description());
		task24.setDueDate(Date.valueOf(taskDetails.getTask24DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask24IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask24IsActive().equalsIgnoreCase("true") ? true: false;
		task24.setIsActive(isActive);
		task24.setIsApprovalTask(false);
		task24.setIsDeleted(false);
		task24.setProgress(0);
		task24.setStartDate(Date.valueOf(taskDetails.getTask24StartDate().split("T")[0]));
		task24.setAssignmentType("ROLE");
		task24.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task24.setStatusId(taskDetails.getTask24IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task24.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task24.setProjectId(projectId);
		task24.setTeamRoleId(task24MPMRole);
		task24.setOwnerId(opsPMOwner);
		task24.setActiveUserId(opsPMOwner);
		task24.setExpectedDuration(0);
		task24.setOriginalStartDate(Date.valueOf(taskDetails.getTask24DueDate().split("T")[0]));
		task24.setOriginalDueDate(Date.valueOf(taskDetails.getTask24DueDate().split("T")[0]));
		task24.setTaskDuration(Integer.parseInt(taskDetails.getTask24Duration()));
		task24.setTaskDurationType("weeks");
//		task24.setId(MPMTaskRepository.getCurrentVal());
		task24.setIsBulkUpdate(false);
		task24.setIsOnHold(false);
		task24.setIsMilestone(false);
		task24.setIsSubproject(false);
		task24.setOrganizationid(orgId);
		mpmTaskList.add(task24);
//		MPMTask task24Response = MPMTaskRepository.save(task24);
//		entityManager.flush();
//		mpmTaskList.add(task24Response);
		}
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask25Duration()) && Integer.parseInt(taskDetails.getTask25Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask25DueDate()) && !util.isNullOrEmpty(taskDetails.getTask25StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping25 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 25")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping25.getFunctional_role_id().getName());	
		
		MPMTeamRole task25MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task25Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		
		MPMTask task25 = new MPMTask();
		task25.setName("TASK 25");
		task25.setDescription(taskRoleMapping25.getTask_description());
		task25.setDueDate(Date.valueOf(taskDetails.getTask25DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask25IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask25IsActive().equalsIgnoreCase("true") ? true: false;
		task25.setIsActive(isActive);
		task25.setIsApprovalTask(false);
		task25.setIsDeleted(false);
		task25.setProgress(0);
		task25.setStartDate(Date.valueOf(taskDetails.getTask25StartDate().split("T")[0]));
		task25.setAssignmentType("ROLE");
		task25.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task25.setStatusId(taskDetails.getTask25IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task25.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task25.setProjectId(projectId);
		task25.setTeamRoleId(task25MPMRole);
		task25.setOwnerId(corpPMOwner);
		task25.setActiveUserId(corpPMOwner);
		task25.setExpectedDuration(0);
		task25.setOriginalStartDate(Date.valueOf(taskDetails.getTask25DueDate().split("T")[0]));
		task25.setOriginalDueDate(Date.valueOf(taskDetails.getTask25DueDate().split("T")[0]));
		task25.setTaskDuration(Integer.parseInt(taskDetails.getTask25Duration()));
		task25.setTaskDurationType("weeks");
//		task25.setId(MPMTaskRepository.getCurrentVal());
		task25.setIsBulkUpdate(false);
		task25.setIsOnHold(false);
		task25.setIsMilestone(false);
		task25.setIsSubproject(false);
		task25.setOrganizationid(orgId);
		mpmTaskList.add(task25);
//		MPMTask task25Response = MPMTaskRepository.save(task25);
//		entityManager.flush();
//		mpmTaskList.add(task25Response);
		}
		
	
		
		if(!util.isNullOrEmpty(taskDetails.getTask26Duration()) && Integer.parseInt(taskDetails.getTask26Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask26DueDate()) && !util.isNullOrEmpty(taskDetails.getTask26StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping26 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 26")).findAny().orElse(null);
		
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping26.getFunctional_role_id().getName());	
			
			MPMTeamRole task26MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task26Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task26 = new MPMTask();
		task26.setName("TASK 26");
		task26.setDescription(taskRoleMapping26.getTask_description());
		task26.setDueDate(Date.valueOf(taskDetails.getTask26DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask26IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask26IsActive().equalsIgnoreCase("true") ? true: false;
		task26.setIsActive(isActive);
		task26.setIsApprovalTask(false);
		task26.setIsDeleted(false);
		task26.setProgress(0);
		task26.setStartDate(Date.valueOf(taskDetails.getTask26StartDate().split("T")[0]));
		task26.setAssignmentType("ROLE");
		task26.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task26.setStatusId(taskDetails.getTask26IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task26.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task26.setProjectId(projectId);
		task26.setTeamRoleId(task26MPMRole);
		task26.setOwnerId(corpPMOwner);
		task26.setActiveUserId(corpPMOwner);
		task26.setExpectedDuration(0);
		task26.setOriginalStartDate(Date.valueOf(taskDetails.getTask26DueDate().split("T")[0]));
		task26.setOriginalDueDate(Date.valueOf(taskDetails.getTask26DueDate().split("T")[0]));
		task26.setTaskDuration(Integer.parseInt(taskDetails.getTask26Duration()));
		task26.setTaskDurationType("weeks");
//		task26.setId(MPMTaskRepository.getCurrentVal());
		task26.setIsBulkUpdate(false);
		task26.setIsOnHold(false);
		task26.setIsMilestone(false);
		task26.setIsSubproject(false);
		task26.setOrganizationid(orgId);
		mpmTaskList.add(task26);
//		MPMTask task26Response = MPMTaskRepository.save(task26);
//		entityManager.flush();
//		mpmTaskList.add(task26Response);
		}
		
	
		
		if(!util.isNullOrEmpty(taskDetails.getTask27Duration()) && Integer.parseInt(taskDetails.getTask27Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask27DueDate()) && !util.isNullOrEmpty(taskDetails.getTask27StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping27 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 27")).findAny().orElse(null);
		
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping27.getFunctional_role_id().getName());	
			
			MPMTeamRole task27MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task27Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task27 = new MPMTask();
		task27.setName("TASK 27");
		task27.setDescription(taskRoleMapping27.getTask_description());
		task27.setDueDate(Date.valueOf(taskDetails.getTask27DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask27IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask27IsActive().equalsIgnoreCase("true") ? true: false;
		task27.setIsActive(isActive);
		task27.setIsApprovalTask(false);
		task27.setIsDeleted(false);
		task27.setProgress(0);
		task27.setStartDate(Date.valueOf(taskDetails.getTask27StartDate().split("T")[0]));
		task27.setAssignmentType("ROLE");
		task27.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task27.setStatusId(taskDetails.getTask27IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task27.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task27.setProjectId(projectId);
		task27.setTeamRoleId(task27MPMRole);
		task27.setOwnerId(rtmOwner);
		task27.setActiveUserId(rtmOwner);
		task27.setExpectedDuration(0);
		task27.setOriginalStartDate(Date.valueOf(taskDetails.getTask27DueDate().split("T")[0]));
		task27.setOriginalDueDate(Date.valueOf(taskDetails.getTask27DueDate().split("T")[0]));
		task27.setTaskDuration(Integer.parseInt(taskDetails.getTask27Duration()));
		task27.setTaskDurationType("weeks");
//		task27.setId(MPMTaskRepository.getCurrentVal());
		task27.setIsBulkUpdate(false);
		task27.setIsOnHold(false);
		task27.setIsMilestone(false);
		task27.setIsSubproject(false);
		task27.setOrganizationid(orgId);
		mpmTaskList.add(task27);
//		MPMTask task27Response = MPMTaskRepository.save(task27);
//		entityManager.flush();
//		mpmTaskList.add(task27Response);
		}
		
		if(!util.isNullOrEmpty(taskDetails.getTask28Duration()) && Integer.parseInt(taskDetails.getTask28Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask28DueDate()) && !util.isNullOrEmpty(taskDetails.getTask28StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping28 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 28")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping28.getFunctional_role_id().getName());	
			
			MPMTeamRole task28MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
			
//			OpenTextIdentity task28Owner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
					
			MPMTask task28 = new MPMTask();
			task28.setName("TASK 28");
			task28.setDescription(taskRoleMapping28.getTask_description());
			task28.setDueDate(Date.valueOf(taskDetails.getTask28DueDate().split("T")[0]));
			Boolean isActive = taskDetails.getTask28IsCompleted().equalsIgnoreCase("true") ? false : 
				taskDetails.getTask28IsActive().equalsIgnoreCase("true") ? true: false;
			task28.setIsActive(isActive);
			task28.setIsApprovalTask(false);
			task28.setIsDeleted(false);
			task28.setProgress(0);
			task28.setStartDate(Date.valueOf(taskDetails.getTask28StartDate().split("T")[0]));
			task28.setAssignmentType("ROLE");
			task28.setTaskType("TASK_WITHOUT_DELIVERABLE");
			task28.setStatusId(taskDetails.getTask28IsCompleted().equalsIgnoreCase("true") 
					? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
					: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
			task28.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
			task28.setProjectId(projectId);
			task28.setTeamRoleId(task28MPMRole);
			task28.setOwnerId(e2ePMOwner);
			task28.setActiveUserId(e2ePMOwner);
			task28.setExpectedDuration(0);
			task28.setOriginalStartDate(Date.valueOf(taskDetails.getTask28DueDate().split("T")[0]));
			task28.setOriginalDueDate(Date.valueOf(taskDetails.getTask28DueDate().split("T")[0]));
			task28.setTaskDuration(Integer.parseInt(taskDetails.getTask28Duration()));
			task28.setTaskDurationType("weeks");
//			task28.setId(MPMTaskRepository.getCurrentVal());
			task28.setIsBulkUpdate(false);
			task28.setIsOnHold(false);
			task28.setIsMilestone(false);
			task28.setIsSubproject(false);
			task28.setOrganizationid(orgId);
			mpmTaskList.add(task28);
//			MPMTask task28Response = MPMTaskRepository.save(task28);
//			entityManager.flush();
//			mpmTaskList.add(task28Response);
			
			}
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask29Duration()) && Integer.parseInt(taskDetails.getTask29Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask29DueDate()) && !util.isNullOrEmpty(taskDetails.getTask29StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping29 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 29")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping29.getFunctional_role_id().getName());	
			
			MPMTeamRole task29MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
			
//			OpenTextIdentity task29Owner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
//					
			MPMTask task29 = new MPMTask();
			task29.setName("TASK 29");
			task29.setDescription(taskRoleMapping29.getTask_description());
			task29.setDueDate(Date.valueOf(taskDetails.getTask29DueDate().split("T")[0]));
			Boolean isActive = taskDetails.getTask29IsCompleted().equalsIgnoreCase("true") ? false : 
				taskDetails.getTask29IsActive().equalsIgnoreCase("true") ? true: false;
			task29.setIsActive(isActive);
			task29.setIsApprovalTask(false);
			task29.setIsDeleted(false);
			task29.setProgress(0);
			task29.setStartDate(Date.valueOf(taskDetails.getTask29StartDate().split("T")[0]));
			task29.setAssignmentType("ROLE");
			task29.setTaskType("TASK_WITHOUT_DELIVERABLE");
			task29.setStatusId(taskDetails.getTask29IsCompleted().equalsIgnoreCase("true") 
					? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
					: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
			task29.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
			task29.setProjectId(projectId);
			task29.setTeamRoleId(task29MPMRole);
			task29.setOwnerId(e2ePMOwner);
			task29.setActiveUserId(e2ePMOwner);
			task29.setExpectedDuration(0);
			task29.setOriginalStartDate(Date.valueOf(taskDetails.getTask29DueDate().split("T")[0]));
			task29.setOriginalDueDate(Date.valueOf(taskDetails.getTask29DueDate().split("T")[0]));
			task29.setTaskDuration(Integer.parseInt(taskDetails.getTask29Duration()));
			task29.setTaskDurationType("weeks");
//			task29.setId(MPMTaskRepository.getCurrentVal());
			task29.setIsBulkUpdate(false);
			task29.setIsOnHold(false);
			task29.setIsMilestone(false);
			task29.setIsSubproject(false);
			task29.setOrganizationid(orgId);
			mpmTaskList.add(task29);
//			MPMTask task29Response = MPMTaskRepository.save(task29);
//			entityManager.flush();
//			mpmTaskList.add(task29Response);
			
			}
		
		
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask30Duration()) && Integer.parseInt(taskDetails.getTask30Duration()) > 0 
		&& !util.isNullOrEmpty(taskDetails.getTask30DueDate()) ) {
			TaskRoleMappingDependencies taskRoleMapping30 = ConfigService.getTaskRoleMappingList().stream()
							.filter(obj -> Objects.nonNull(obj.getTask_name()))
							.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 30")).findAny().orElse(null);
					
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping30.getFunctional_role_id().getName());	
			
			MPMTeamRole task30MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//			OpenTextIdentity task30Owner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
			MPMTask task30 = new MPMTask();
			task30.setName("TASK 30");
			task30.setDescription(taskRoleMapping30.getTask_description());
			task30.setDueDate(Date.valueOf(taskDetails.getTask30DueDate().split("T")[0]));
			Boolean isActive = taskDetails.getTask30IsCompleted().equalsIgnoreCase("true") ? false : 
				taskDetails.getTask30IsActive().equalsIgnoreCase("true") ? true: false;
			task30.setIsActive(isActive);
			task30.setIsApprovalTask(false);
			task30.setIsDeleted(false);
			task30.setProgress(0);
			task30.setStartDate(Date.valueOf(taskDetails.getTask30DueDate().split("T")[0]));
			task30.setAssignmentType("ROLE");
			task30.setTaskType("TASK_WITHOUT_DELIVERABLE");
			Boolean isCompleted = !util.isNullOrEmpty(taskDetails.getTask30ActualDueDate()) ? true : false; 
					
			task30.setStatusId(isCompleted == true
					? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
					: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
			task30.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
			task30.setProjectId(projectId);
			task30.setTeamRoleId(task30MPMRole);
			task30.setOwnerId(projectSpecialistOwner);
			task30.setActiveUserId(projectSpecialistOwner);
			task30.setExpectedDuration(0);
			task30.setOriginalStartDate(Date.valueOf(taskDetails.getTask30DueDate().split("T")[0]));
			task30.setOriginalDueDate(Date.valueOf(taskDetails.getTask30DueDate().split("T")[0]));
			task30.setTaskDuration(1);
			task30.setTaskDurationType("weeks");
			task30.setIsBulkUpdate(false);
			task30.setIsOnHold(false);
			task30.setIsMilestone(false);
			task30.setIsSubproject(false);
			task30.setOrganizationid(orgId);
			mpmTaskList.add(task30);
//			MPMTask task30Response = MPMTaskRepository.save(task30);
//			entityManager.flush();
//			mpmTaskList.add(task30Response);
		}
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask31Duration()) && Integer.parseInt(taskDetails.getTask31Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask31DueDate()) && !util.isNullOrEmpty(taskDetails.getTask31StartDate())) {
		TaskRoleMappingDependencies taskRoleMapping31 = ConfigService.getTaskRoleMappingList().stream()
				.filter(obj -> Objects.nonNull(obj.getTask_name()))
				.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 31")).findAny().orElse(null);
		
		MaxId max = new MaxId();
		max.setTaskRoleName(taskRoleMapping31.getFunctional_role_id().getName());	
		
		MPMTeamRole task31MPMRole = ConfigService.getMPMTeamRoleList().stream()
				.filter(obj -> Objects.nonNull(obj.getName()))
				.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
		
//		OpenTextIdentity task31Owner = ConfigService.getUsersList().stream()
//				.filter(obj -> Objects.nonNull(obj.getId()))
//				.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		
		MPMTask task31 = new MPMTask();
		task31.setName("TASK 31");
		task31.setDescription(taskRoleMapping31.getTask_description());
		task31.setDueDate(Date.valueOf(taskDetails.getTask31DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask31IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask31IsActive().equalsIgnoreCase("true") ? true: false;
		task31.setIsActive(isActive);
		task31.setIsApprovalTask(false);
		task31.setIsDeleted(false);
		task31.setProgress(0);
		task31.setStartDate(Date.valueOf(taskDetails.getTask31StartDate().split("T")[0]));
		task31.setAssignmentType("ROLE");
		task31.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task31.setStatusId(taskDetails.getTask31IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task31.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task31.setProjectId(projectId);
		task31.setTeamRoleId(task31MPMRole);
		task31.setOwnerId(projectSpecialistOwner);
		task31.setActiveUserId(projectSpecialistOwner);
		task31.setExpectedDuration(0);
		task31.setOriginalStartDate(Date.valueOf(taskDetails.getTask31DueDate().split("T")[0]));
		task31.setOriginalDueDate(Date.valueOf(taskDetails.getTask31DueDate().split("T")[0]));
		task31.setTaskDuration(Integer.parseInt(taskDetails.getTask31Duration()));
		task31.setTaskDurationType("weeks");
//		task31.setId(MPMTaskRepository.getCurrentVal());
		task31.setIsBulkUpdate(false);
		task31.setIsOnHold(false);
		task31.setIsMilestone(false);
		task31.setIsSubproject(false);
		task31.setOrganizationid(orgId);
		mpmTaskList.add(task31);
//		MPMTask task31Response = MPMTaskRepository.save(task31);
//		entityManager.flush();
//		mpmTaskList.add(task31Response);
		}
		
		
		
		if(!util.isNullOrEmpty(taskDetails.getTask32Duration()) && Integer.parseInt(taskDetails.getTask32Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask32DueDate()) && !util.isNullOrEmpty(taskDetails.getTask32StartDate())) {
			
			TaskRoleMappingDependencies taskRoleMapping32 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 32")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping32.getFunctional_role_id().getName());	
			
			MPMTeamRole task32MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
//			
//			OpenTextIdentity task32Owner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		
		MPMTask task32 = new MPMTask();
		task32.setName("TASK 32");
		task32.setDescription(taskRoleMapping32.getTask_description());
		task32.setDueDate(Date.valueOf(taskDetails.getTask32DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask32IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask32IsActive().equalsIgnoreCase("true") ? true: false;
		task32.setIsActive(isActive);
		task32.setIsApprovalTask(false);
		task32.setIsDeleted(false);
		task32.setProgress(0);
		task32.setStartDate(Date.valueOf(taskDetails.getTask32StartDate().split("T")[0]));
		task32.setAssignmentType("ROLE");
		task32.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task32.setStatusId(taskDetails.getTask32IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task32.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task32.setProjectId(projectId);
		task32.setTeamRoleId(task32MPMRole);
		task32.setOwnerId(opsPMOwner);
		task32.setActiveUserId(opsPMOwner);
		task32.setExpectedDuration(0);
		task32.setOriginalStartDate(Date.valueOf(taskDetails.getTask32DueDate().split("T")[0]));
		task32.setOriginalDueDate(Date.valueOf(taskDetails.getTask32DueDate().split("T")[0]));
		task32.setTaskDuration(Integer.parseInt(taskDetails.getTask32Duration()));
		task32.setTaskDurationType("weeks");
//		task32.setId(MPMTaskRepository.getCurrentVal());
		task32.setIsBulkUpdate(false);
		task32.setIsOnHold(false);
		task32.setIsMilestone(false);
		task32.setIsSubproject(false);
		task32.setOrganizationid(orgId);
		mpmTaskList.add(task32);
//		MPMTask task32Response = MPMTaskRepository.save(task32);
//		entityManager.flush();
//		mpmTaskList.add(task32Response);
		}
		
		if(!util.isNullOrEmpty(taskDetails.getTask33Duration()) && Integer.parseInt(taskDetails.getTask33Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask33DueDate()) && !util.isNullOrEmpty(taskDetails.getTask33StartDate())) {
			
			TaskRoleMappingDependencies taskRoleMapping33 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 33")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping33.getFunctional_role_id().getName());	
			
			MPMTeamRole task33MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
			
			OpenTextIdentity task33Owner = ConfigService.getUsersList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task33 = new MPMTask();
		task33.setName("TASK 33");
		task33.setDescription(taskRoleMapping33.getTask_description());
		task33.setDueDate(Date.valueOf(taskDetails.getTask33DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask33IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask33IsActive().equalsIgnoreCase("true") ? true: false;
		task33.setIsActive(isActive);
		task33.setIsApprovalTask(false);
		task33.setIsDeleted(false);
		task33.setProgress(0);
		task33.setStartDate(Date.valueOf(taskDetails.getTask33StartDate().split("T")[0]));
		task33.setAssignmentType("ROLE");
		task33.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task33.setStatusId(taskDetails.getTask33IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task33.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task33.setProjectId(projectId);
		task33.setTeamRoleId(task33MPMRole);
//		task33.setOwnerId();
//		task33.setActiveUserId();
		task33.setExpectedDuration(0);
		task33.setOriginalStartDate(Date.valueOf(taskDetails.getTask33DueDate().split("T")[0]));
		task33.setOriginalDueDate(Date.valueOf(taskDetails.getTask33DueDate().split("T")[0]));
		task33.setTaskDuration(Integer.parseInt(taskDetails.getTask33Duration()));
		task33.setTaskDurationType("weeks");
//		task33.setId(MPMTaskRepository.getCurrentVal());
		task33.setIsBulkUpdate(false);
		task33.setIsOnHold(false);
		task33.setIsMilestone(false);
		task33.setIsSubproject(false);
		task33.setOrganizationid(orgId);
		mpmTaskList.add(task33);
//		MPMTask task33Response = MPMTaskRepository.save(task33);
//		entityManager.flush();
//		mpmTaskList.add(task33Response);
		}
		
		if(!util.isNullOrEmpty(taskDetails.getTask34Duration()) && Integer.parseInt(taskDetails.getTask34Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask34DueDate()) && !util.isNullOrEmpty(taskDetails.getTask34StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping34 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 34")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping34.getFunctional_role_id().getName());	
			
			MPMTeamRole task34MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
			
//			OpenTextIdentity task34Owner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task34 = new MPMTask();
		task34.setName("TASK 34");
		task34.setDescription(taskRoleMapping34.getTask_description());
		task34.setDueDate(Date.valueOf(taskDetails.getTask34DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask34IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask34IsActive().equalsIgnoreCase("true") ? true: false;
		task34.setIsActive(isActive);
		task34.setIsApprovalTask(false);
		task34.setIsDeleted(false);
		task34.setProgress(0);
		task34.setStartDate(Date.valueOf(taskDetails.getTask34StartDate().split("T")[0]));
		task34.setAssignmentType("ROLE");
		task34.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task34.setStatusId(taskDetails.getTask34IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task34.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task34.setProjectId(projectId);
		task34.setTeamRoleId(task34MPMRole);
		task34.setOwnerId(corpPMOwner);
		task34.setActiveUserId(corpPMOwner);
		task34.setExpectedDuration(0);
		task34.setOriginalStartDate(Date.valueOf(taskDetails.getTask34DueDate().split("T")[0]));
		task34.setOriginalDueDate(Date.valueOf(taskDetails.getTask34DueDate().split("T")[0]));
		task34.setTaskDuration(Integer.parseInt(taskDetails.getTask34Duration()));
		task34.setTaskDurationType("weeks");
//		task34.setId(MPMTaskRepository.getCurrentVal());
		task34.setIsBulkUpdate(false);
		task34.setIsOnHold(false);
		task34.setIsMilestone(false);
		task34.setIsSubproject(false);
		task34.setOrganizationid(orgId);
		mpmTaskList.add(task34);
//		MPMTask task34Response = MPMTaskRepository.save(task34);
//		entityManager.flush();
//		mpmTaskList.add(task34Response);
		}
		
		if(!util.isNullOrEmpty(taskDetails.getTask35Duration()) && Integer.parseInt(taskDetails.getTask35Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask35DueDate()) && !util.isNullOrEmpty(taskDetails.getTask35StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping35 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 35")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping35.getFunctional_role_id().getName());	
			
			MPMTeamRole task35MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
			
//			OpenTextIdentity task35Owner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task35 = new MPMTask();
		task35.setName("TASK 35");
		task35.setDescription(taskRoleMapping35.getTask_description());
		task35.setDueDate(Date.valueOf(taskDetails.getTask35DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask35IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask35IsActive().equalsIgnoreCase("true") ? true: false;
		task35.setIsActive(isActive);
		task35.setIsApprovalTask(false);
		task35.setIsDeleted(false);
		task35.setProgress(0);
		task35.setStartDate(Date.valueOf(taskDetails.getTask35StartDate().split("T")[0]));
		task35.setAssignmentType("ROLE");
		task35.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task35.setStatusId(taskDetails.getTask35IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task35.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task35.setProjectId(projectId);
		task35.setTeamRoleId(task35MPMRole);
		task35.setOwnerId(e2ePMOwner);
		task35.setActiveUserId(e2ePMOwner);
		task35.setExpectedDuration(0);
		task35.setOriginalStartDate(Date.valueOf(taskDetails.getTask35DueDate().split("T")[0]));
		task35.setOriginalDueDate(Date.valueOf(taskDetails.getTask35DueDate().split("T")[0]));
		task35.setTaskDuration(Integer.parseInt(taskDetails.getTask35Duration()));
		task35.setTaskDurationType("weeks");
//		task35.setId(MPMTaskRepository.getCurrentVal());
		task35.setIsBulkUpdate(false);
		task35.setIsOnHold(false);
		task35.setIsMilestone(false);
		task35.setIsSubproject(false);
		task35.setOrganizationid(orgId);
		mpmTaskList.add(task35);
//		MPMTask task35Response = MPMTaskRepository.save(task35);
//		entityManager.flush();
//		mpmTaskList.add(task35Response);
		}
		
		if(!util.isNullOrEmpty(taskDetails.getTask36Duration()) && Integer.parseInt(taskDetails.getTask36Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask36DueDate()) && !util.isNullOrEmpty(taskDetails.getTask36StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping36 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 36")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping36.getFunctional_role_id().getName());	
			
			MPMTeamRole task36MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
			
			OpenTextIdentity task36Owner = ConfigService.getUsersList().stream()
					.filter(obj -> Objects.nonNull(obj.getId()))
					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task36 = new MPMTask();
		task36.setName("TASK 36");
		task36.setDescription(taskRoleMapping36.getTask_description());
		task36.setDueDate(Date.valueOf(taskDetails.getTask36DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask36IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask36IsActive().equalsIgnoreCase("true") ? true: false;
		task36.setIsActive(isActive);
		task36.setIsApprovalTask(false);
		task36.setIsDeleted(false);
		task36.setProgress(0);
		task36.setStartDate(Date.valueOf(taskDetails.getTask36StartDate().split("T")[0]));
		task36.setAssignmentType("ROLE");
		task36.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task36.setStatusId(taskDetails.getTask36IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task36.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task36.setProjectId(projectId);
		task36.setTeamRoleId(task36MPMRole);
		task36.setOwnerId(opsPMOwner);
		task36.setActiveUserId(opsPMOwner);
		task36.setExpectedDuration(0);
		task36.setOriginalStartDate(Date.valueOf(taskDetails.getTask36DueDate().split("T")[0]));
		task36.setOriginalDueDate(Date.valueOf(taskDetails.getTask36DueDate().split("T")[0]));
		task36.setTaskDuration(Integer.parseInt(taskDetails.getTask36Duration()));
		task36.setTaskDurationType("weeks");
//		task36.setId(MPMTaskRepository.getCurrentVal());
		task36.setIsBulkUpdate(false);
		task36.setIsOnHold(false);
		task36.setIsMilestone(false);
		task36.setIsSubproject(false);
		task36.setOrganizationid(orgId);
		mpmTaskList.add(task36);
//		MPMTask task36Response = MPMTaskRepository.save(task36);
//		entityManager.flush();
//		mpmTaskList.add(task36Response);
		
		}
		
		if(!util.isNullOrEmpty(taskDetails.getTask37Duration()) && Integer.parseInt(taskDetails.getTask37Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask37DueDate()) && !util.isNullOrEmpty(taskDetails.getTask37StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping37 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 37")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping37.getFunctional_role_id().getName());	
			
			MPMTeamRole task37MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
			
//			OpenTextIdentity task37Owner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task37 = new MPMTask();
		task37.setName("TASK 37");
		task37.setDescription(taskRoleMapping37.getTask_description());
		task37.setDueDate(Date.valueOf(taskDetails.getTask37DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask37IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask37IsActive().equalsIgnoreCase("true") ? true: false;
		task37.setIsActive(isActive);
		task37.setIsApprovalTask(false);
		task37.setIsDeleted(false);
		task37.setProgress(0);
		task37.setStartDate(Date.valueOf(taskDetails.getTask37StartDate().split("T")[0]));
		task37.setAssignmentType("ROLE");
		task37.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task37.setStatusId(taskDetails.getTask37IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task37.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task37.setProjectId(projectId);
		task37.setTeamRoleId(task37MPMRole);
		task37.setOwnerId(opsPMOwner);
		task37.setActiveUserId(opsPMOwner);
		task37.setExpectedDuration(0);
		task37.setOriginalStartDate(Date.valueOf(taskDetails.getTask37DueDate().split("T")[0]));
		task37.setOriginalDueDate(Date.valueOf(taskDetails.getTask37DueDate().split("T")[0]));
		task37.setTaskDuration(Integer.parseInt(taskDetails.getTask37Duration()));
		task37.setTaskDurationType("weeks");
//		task37.setId(MPMTaskRepository.getCurrentVal());
		task37.setIsBulkUpdate(false);
		task37.setIsOnHold(false);
		task37.setIsMilestone(false);
		task37.setIsSubproject(false);
		task37.setOrganizationid(orgId);
		mpmTaskList.add(task37);
//		MPMTask task37Response = MPMTaskRepository.save(task37);
//		entityManager.flush();
//		mpmTaskList.add(task37Response);
		}
		
		if(!util.isNullOrEmpty(taskDetails.getTask38Duration()) && Integer.parseInt(taskDetails.getTask38Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask38DueDate()) && !util.isNullOrEmpty(taskDetails.getTask38StartDate())) {
			
			TaskRoleMappingDependencies taskRoleMapping38 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 38")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping38.getFunctional_role_id().getName());	
			
			MPMTeamRole task38MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
			
//			OpenTextIdentity task38Owner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task38 = new MPMTask();
		task38.setName("TASK 38");
		task38.setDescription(taskRoleMapping38.getTask_description());
		task38.setDueDate(Date.valueOf(taskDetails.getTask38DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask38IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask38IsActive().equalsIgnoreCase("true") ? true: false;
		task38.setIsActive(isActive);
		task38.setIsApprovalTask(false);
		task38.setIsDeleted(false);
		task38.setProgress(0);
		task38.setStartDate(Date.valueOf(taskDetails.getTask38StartDate().split("T")[0]));
		task38.setAssignmentType("ROLE");
		task38.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task38.setStatusId(taskDetails.getTask38IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task38.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task38.setProjectId(projectId);
		task38.setTeamRoleId(task38MPMRole);
		task38.setOwnerId(opsPMOwner);
		task38.setActiveUserId(opsPMOwner);
		task38.setExpectedDuration(0);
		task38.setOriginalStartDate(Date.valueOf(taskDetails.getTask38DueDate().split("T")[0]));
		task38.setOriginalDueDate(Date.valueOf(taskDetails.getTask38DueDate().split("T")[0]));
		task38.setTaskDuration(Integer.parseInt(taskDetails.getTask38Duration()));
		task38.setTaskDurationType("weeks");
//		task38.setId(MPMTaskRepository.getCurrentVal());
		task38.setIsBulkUpdate(false);
		task38.setIsOnHold(false);
		task38.setIsMilestone(false);
		task38.setIsSubproject(false);
		task38.setOrganizationid(orgId);
		mpmTaskList.add(task38);
//		MPMTask task38Response = MPMTaskRepository.save(task38);
//		entityManager.flush();
//		mpmTaskList.add(task38Response);
		}
		
		if(!util.isNullOrEmpty(taskDetails.getTask39Duration()) && Integer.parseInt(taskDetails.getTask39Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask39DueDate()) && !util.isNullOrEmpty(taskDetails.getTask39StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping39 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 39")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping39.getFunctional_role_id().getName());	
			
			MPMTeamRole task39MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
			
//			OpenTextIdentity task39Owner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task39 = new MPMTask();
		task39.setName("TASK 39");
		task39.setDescription(taskRoleMapping39.getTask_description());
		task39.setDueDate(Date.valueOf(taskDetails.getTask39DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask39IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask39IsActive().equalsIgnoreCase("true") ? true: false;
		task39.setIsActive(isActive);
		task39.setIsApprovalTask(false);
		task39.setIsDeleted(false);
		task39.setProgress(0);
		task39.setStartDate(Date.valueOf(taskDetails.getTask39StartDate().split("T")[0]));
		task39.setAssignmentType("ROLE");
		task39.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task39.setStatusId(taskDetails.getTask39IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task39.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task39.setProjectId(projectId);
		task39.setTeamRoleId(task39MPMRole);
		task39.setOwnerId(rtmOwner);
		task39.setActiveUserId(rtmOwner);
		task39.setExpectedDuration(0);
		task39.setOriginalStartDate(Date.valueOf(taskDetails.getTask39DueDate().split("T")[0]));
		task39.setOriginalDueDate(Date.valueOf(taskDetails.getTask39DueDate().split("T")[0]));
		task39.setTaskDuration(Integer.parseInt(taskDetails.getTask39Duration()));
		task39.setTaskDurationType("weeks");
//		task39.setId(MPMTaskRepository.getCurrentVal());
		task39.setIsBulkUpdate(false);
		task39.setIsOnHold(false);
		task39.setIsMilestone(false);
		task39.setIsSubproject(false);
		task39.setOrganizationid(orgId);
		mpmTaskList.add(task39);
//		MPMTask task39Response = MPMTaskRepository.save(task39);
//		entityManager.flush();
//		mpmTaskList.add(task39Response);
		}
		
		if(!util.isNullOrEmpty(taskDetails.getTask40Duration()) && Integer.parseInt(taskDetails.getTask40Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask40DueDate()) && !util.isNullOrEmpty(taskDetails.getTask40StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping40 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 40")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping40.getFunctional_role_id().getName());	
			
			MPMTeamRole task40MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
			
//			OpenTextIdentity task40Owner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task40 = new MPMTask();
		task40.setName("TASK 40");
		task40.setDescription(taskRoleMapping40.getTask_description());
		task40.setDueDate(Date.valueOf(taskDetails.getTask40DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask40IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask40IsActive().equalsIgnoreCase("true") ? true: false;
		task40.setIsActive(isActive);
		task40.setIsApprovalTask(false);
		task40.setIsDeleted(false);
		task40.setProgress(0);
		task40.setStartDate(Date.valueOf(taskDetails.getTask40StartDate().split("T")[0]));
		task40.setAssignmentType("ROLE");
		task40.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task40.setStatusId(taskDetails.getTask40IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task40.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task40.setProjectId(projectId);
		task40.setTeamRoleId(task40MPMRole);
		task40.setOwnerId(rtmOwner);
		task40.setActiveUserId(rtmOwner);
		task40.setExpectedDuration(0);
		task40.setOriginalStartDate(Date.valueOf(taskDetails.getTask40DueDate().split("T")[0]));
		task40.setOriginalDueDate(Date.valueOf(taskDetails.getTask40DueDate().split("T")[0]));
		task40.setTaskDuration(Integer.parseInt(taskDetails.getTask40Duration()));
		task40.setTaskDurationType("weeks");
//		task40.setId(MPMTaskRepository.getCurrentVal());
		task40.setIsBulkUpdate(false);
		task40.setIsOnHold(false);
		task40.setIsMilestone(false);
		task40.setIsSubproject(false);
		task40.setOrganizationid(orgId);
		mpmTaskList.add(task40);
//		MPMTask task40Response = MPMTaskRepository.save(task40);
//		entityManager.flush();
//		mpmTaskList.add(task40Response);
		}
		
		if(!util.isNullOrEmpty(taskDetails.getTask41Duration()) && Integer.parseInt(taskDetails.getTask41Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask41DueDate()) && !util.isNullOrEmpty(taskDetails.getTask41StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping41 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 41")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping41.getFunctional_role_id().getName());	
			
			MPMTeamRole task41MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
			
//			OpenTextIdentity task41Owner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
			
		MPMTask task41 = new MPMTask();
		task41.setName("TASK 41");
		task41.setDescription(taskRoleMapping41.getTask_description());
		task41.setDueDate(Date.valueOf(taskDetails.getTask41DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask41IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask41IsActive().equalsIgnoreCase("true") ? true: false;
		task41.setIsActive(isActive);
		task41.setIsApprovalTask(false);
		task41.setIsDeleted(false);
		task41.setProgress(0);
		task41.setStartDate(Date.valueOf(taskDetails.getTask41StartDate().split("T")[0]));
		task41.setAssignmentType("ROLE");
		task41.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task41.setStatusId(taskDetails.getTask41IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task41.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task41.setProjectId(projectId);
		task41.setTeamRoleId(task41MPMRole);
		task41.setOwnerId(regLeadOwner);
		task41.setActiveUserId(regLeadOwner);
		task41.setExpectedDuration(0);
		task41.setOriginalStartDate(Date.valueOf(taskDetails.getTask41DueDate().split("T")[0]));
		task41.setOriginalDueDate(Date.valueOf(taskDetails.getTask41DueDate().split("T")[0]));
		task41.setTaskDuration(Integer.parseInt(taskDetails.getTask41Duration()));
		task41.setTaskDurationType("weeks");
//		task41.setId(MPMTaskRepository.getCurrentVal());
		task41.setIsBulkUpdate(false);
		task41.setIsOnHold(false);
		task41.setIsMilestone(false);
		task41.setIsSubproject(false);
		task41.setOrganizationid(orgId);
		mpmTaskList.add(task41);
//		MPMTask task41Response = MPMTaskRepository.save(task41);
//		entityManager.flush();
//		mpmTaskList.add(task41Response);
		}
		
		if(!util.isNullOrEmpty(taskDetails.getTask42Duration()) && Integer.parseInt(taskDetails.getTask42Duration()) > 0 
				&& !util.isNullOrEmpty(taskDetails.getTask42DueDate()) && !util.isNullOrEmpty(taskDetails.getTask42StartDate())) {
			TaskRoleMappingDependencies taskRoleMapping42 = ConfigService.getTaskRoleMappingList().stream()
					.filter(obj -> Objects.nonNull(obj.getTask_name()))
					.filter(obj -> obj.getTask_name().equalsIgnoreCase("TASK 42")).findAny().orElse(null);
			
			MaxId max = new MaxId();
			max.setTaskRoleName(taskRoleMapping42.getFunctional_role_id().getName());	
			
			MPMTeamRole task42MPMRole = ConfigService.getMPMTeamRoleList().stream()
					.filter(obj -> Objects.nonNull(obj.getName()))
					.filter(obj -> obj.getName().equalsIgnoreCase(max.getTaskRoleName())).findAny().orElse(null);
			
//			OpenTextIdentity task42Owner = ConfigService.getUsersList().stream()
//					.filter(obj -> Objects.nonNull(obj.getId()))
//					.filter(obj -> obj.getId() == 49154).findAny().orElse(null);
		MPMTask task42 = new MPMTask();
		task42.setName("TASK 42");
		task42.setDescription(taskRoleMapping42.getTask_description());
		task42.setDueDate(Date.valueOf(taskDetails.getTask42DueDate().split("T")[0]));
		Boolean isActive = taskDetails.getTask42IsCompleted().equalsIgnoreCase("true") ? false : 
			taskDetails.getTask42IsActive().equalsIgnoreCase("true") ? true: false;
		task42.setIsActive(isActive);
		task42.setIsApprovalTask(false);
		task42.setIsDeleted(false);
		task42.setProgress(0);
		task42.setStartDate(Date.valueOf(taskDetails.getTask42StartDate().split("T")[0]));
		task42.setAssignmentType("ROLE");
		task42.setTaskType("TASK_WITHOUT_DELIVERABLE");
		task42.setStatusId(taskDetails.getTask42IsCompleted().equalsIgnoreCase("true") 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		task42.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		task42.setProjectId(projectId);
		task42.setTeamRoleId(task42MPMRole);
		task42.setOwnerId(opsPMOwner);
		task42.setActiveUserId(opsPMOwner);
		task42.setExpectedDuration(0);
		task42.setOriginalStartDate(Date.valueOf(taskDetails.getTask42DueDate().split("T")[0]));
		task42.setOriginalDueDate(Date.valueOf(taskDetails.getTask42DueDate().split("T")[0]));
		task42.setTaskDuration(Integer.parseInt(taskDetails.getTask42Duration()));
		task42.setTaskDurationType("weeks");
//		task42.setId(MPMTaskRepository.getCurrentVal());
		task42.setIsBulkUpdate(false);
		task42.setIsOnHold(false);
		task42.setIsMilestone(false);
		task42.setIsSubproject(false);
		task42.setOrganizationid(orgId);
		mpmTaskList.add(task42);
//		MPMTask task42Response = MPMTaskRepository.save(task42);
//		entityManager.flush();
//		mpmTaskList.add(task42Response);
		}

		Boolean isCP1Completed = true;
		if(Integer.parseInt(taskDetails.getTask2Duration()) > 0 ) {			
			isCP1Completed = taskDetails.getTask2IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask3Duration()) > 0 && isCP1Completed ==  true) {			
			isCP1Completed = taskDetails.getTask3IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask4Duration()) > 0 && isCP1Completed ==  true) {			
			isCP1Completed = taskDetails.getTask4IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask5Duration()) > 0 && isCP1Completed ==  true) {			
			isCP1Completed = taskDetails.getTask5IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask6Duration()) > 0 && isCP1Completed ==  true) {			
			isCP1Completed = taskDetails.getTask6IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask7Duration()) > 0 && isCP1Completed ==  true) {			
			isCP1Completed = taskDetails.getTask7IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		
		Boolean isCP2Completed = true;
		if(Integer.parseInt(taskDetails.getTask9Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask9IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask10Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask10IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask11Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask11IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask12Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask12IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask13Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask13IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask13aDuration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask13aIsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask14Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask14IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask15Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask15IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask16Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask16IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask17Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask17IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask18Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask18IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask19Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask19IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask20Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask20IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask21Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask21IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask22Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask22IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask23Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask23IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask24Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask24IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask25Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask25IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask26Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask26IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask27Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask27IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask28Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask28IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask29Duration()) > 0 && isCP2Completed ==  true) {			
			isCP2Completed = taskDetails.getTask29IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
//		if(Integer.parseInt(taskDetails.getTask30Duration()) > 0 && isCP2Completed ==  true) {			
//			isCP2Completed = taskDetails.getTask30IsCompleted().equalsIgnoreCase("true") ? true : false;
//		}
		
		Boolean isCP3Completed = true;
		if(Integer.parseInt(taskDetails.getTask31Duration()) > 0 && isCP3Completed ==  true) {			
			isCP3Completed = taskDetails.getTask31IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask32Duration()) > 0 && isCP3Completed ==  true) {			
			isCP3Completed = taskDetails.getTask32IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask33Duration()) > 0 && isCP3Completed ==  true) {			
			isCP3Completed = taskDetails.getTask33IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask34Duration()) > 0 && isCP3Completed ==  true) {			
			isCP3Completed = taskDetails.getTask34IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask35Duration()) > 0 && isCP3Completed ==  true) {			
			isCP3Completed = taskDetails.getTask35IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		
		Boolean isCP4Completed = true;
		if(Integer.parseInt(taskDetails.getTask36Duration()) > 0 && isCP4Completed ==  true) {			
			isCP4Completed = taskDetails.getTask36IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask37Duration()) > 0 && isCP4Completed ==  true) {			
			isCP4Completed = taskDetails.getTask37IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask38Duration()) > 0 && isCP4Completed ==  true) {			
			isCP4Completed = taskDetails.getTask38IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask39Duration()) > 0 && isCP4Completed ==  true) {			
			isCP4Completed = taskDetails.getTask39IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask40Duration()) > 0 && isCP4Completed ==  true) {			
			isCP4Completed = taskDetails.getTask40IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		if(Integer.parseInt(taskDetails.getTask41Duration()) > 0 && isCP4Completed ==  true) {			
			isCP4Completed = taskDetails.getTask41IsCompleted().equalsIgnoreCase("true") ? true : false;
		}
		
		
		

		
		MPMTask taskCP1 = new MPMTask();
		taskCP1.setName("CP1");
		taskCP1.setDescription("CP1");
		taskCP1.setDueDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP1DecisionDate()) 
				&& requestData.getGovMilestoneCP1DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP1DecisionDate().split("T")[0]) : null);
		taskCP1.setIsActive(false);
		taskCP1.setIsApprovalTask(false);
		taskCP1.setIsDeleted(false);
		taskCP1.setProgress(0);
		taskCP1.setStartDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP1DecisionDate()) 
				&& requestData.getGovMilestoneCP1DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP1DecisionDate().split("T")[0]) : null);
		taskCP1.setAssignmentType("ROLE");
		taskCP1.setTaskType("TASK_WITHOUT_DELIVERABLE");
		taskCP1.setStatusId(isCP1Completed == true 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		taskCP1.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		taskCP1.setProjectId(projectId);
		taskCP1.setTeamRoleId(task2MPMRole);
		taskCP1.setOwnerId(e2ePMOwner);
		taskCP1.setActiveUserId(e2ePMOwner);
		taskCP1.setExpectedDuration(0);
		taskCP1.setOriginalStartDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP1DecisionDate()) 
				&& requestData.getGovMilestoneCP1DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP1DecisionDate().split("T")[0]) : null);
		taskCP1.setOriginalDueDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP1DecisionDate()) 
				&& requestData.getGovMilestoneCP1DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP1DecisionDate().split("T")[0]) : null);
		taskCP1.setTaskDuration(1);
		taskCP1.setTaskDurationType("weeks");
//		taskCP1.setId(MPMTaskRepository.getCurrentVal());
		taskCP1.setIsBulkUpdate(false);
		taskCP1.setIsOnHold(false);
		taskCP1.setIsMilestone(false);
		taskCP1.setIsSubproject(false);
		taskCP1.setOrganizationid(orgId);
		mpmTaskList.add(taskCP1);
//		MPMTask taskCP1Response = MPMTaskRepository.save(taskCP1);
//		entityManager.flush();
//		mpmTaskList.add(taskCP1Response);
		
		MPMTask taskCP2 = new MPMTask();
		taskCP2.setName("CP2");
		taskCP2.setDescription("CP2");
		taskCP2.setDueDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP2DecisionDate()) 
				&& requestData.getGovMilestoneCP2DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP2DecisionDate().split("T")[0]) : null);
		taskCP2.setIsActive(false);
		taskCP2.setIsApprovalTask(false);
		taskCP2.setIsDeleted(false);
		taskCP2.setProgress(0);
		taskCP2.setStartDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP2DecisionDate()) 
				&& requestData.getGovMilestoneCP2DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP2DecisionDate().split("T")[0]) : null);
		taskCP2.setAssignmentType("ROLE");
		taskCP2.setTaskType("TASK_WITHOUT_DELIVERABLE");
		taskCP2.setStatusId(isCP2Completed == true 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		taskCP2.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		taskCP2.setProjectId(projectId);
		taskCP2.setTeamRoleId(task2MPMRole);
		taskCP2.setOwnerId(e2ePMOwner);
		taskCP2.setActiveUserId(e2ePMOwner);
		taskCP2.setExpectedDuration(0);
		taskCP2.setOriginalStartDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP2DecisionDate()) 
				&& requestData.getGovMilestoneCP2DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP2DecisionDate().split("T")[0]) : null);
		taskCP2.setOriginalDueDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP2DecisionDate()) 
				&& requestData.getGovMilestoneCP2DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP2DecisionDate().split("T")[0]) : null);
		taskCP2.setTaskDuration(1);
		taskCP2.setTaskDurationType("weeks");
//		taskCP2.setId(MPMTaskRepository.getCurrentVal());
		taskCP2.setIsBulkUpdate(false);
		taskCP2.setIsOnHold(false);
		taskCP2.setIsMilestone(false);
		taskCP2.setIsSubproject(false);
		taskCP2.setOrganizationid(orgId);
		mpmTaskList.add(taskCP2);
//		MPMTask taskCP2Response = MPMTaskRepository.save(taskCP2);
//		entityManager.flush();
//		mpmTaskList.add(taskCP2Response);
		
		MPMTask taskCP3 = new MPMTask();
		taskCP3.setName("CP3");
		taskCP3.setDescription("CP3");
		taskCP3.setDueDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP3DecisionDate()) 
				&& requestData.getGovMilestoneCP3DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP3DecisionDate().split("T")[0]) : null);
		taskCP3.setIsActive(false);
		taskCP3.setIsApprovalTask(false);
		taskCP3.setIsDeleted(false);
		taskCP3.setProgress(0);
		taskCP3.setStartDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP3DecisionDate()) 
				&& requestData.getGovMilestoneCP3DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP3DecisionDate().split("T")[0]) : null);
		taskCP3.setAssignmentType("ROLE");
		taskCP3.setTaskType("TASK_WITHOUT_DELIVERABLE");
		taskCP3.setStatusId(isCP3Completed == true 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		taskCP3.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		taskCP3.setProjectId(projectId);
		taskCP3.setTeamRoleId(task2MPMRole);
		taskCP3.setOwnerId(e2ePMOwner);
		taskCP3.setActiveUserId(e2ePMOwner);
		taskCP3.setExpectedDuration(0);
		taskCP3.setOriginalStartDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP3DecisionDate()) 
				&& requestData.getGovMilestoneCP3DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP3DecisionDate().split("T")[0]) : null);
		taskCP3.setOriginalDueDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP3DecisionDate()) 
				&& requestData.getGovMilestoneCP3DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP3DecisionDate().split("T")[0]) : null);
		taskCP3.setTaskDuration(1);
		taskCP3.setTaskDurationType("weeks");
//		taskCP3.setId(MPMTaskRepository.getCurrentVal());
		taskCP3.setIsBulkUpdate(false);
		taskCP3.setIsOnHold(false);
		taskCP3.setIsMilestone(false);
		taskCP3.setIsSubproject(false);
		taskCP3.setOrganizationid(orgId);
		mpmTaskList.add(taskCP3);
//		MPMTask taskCP3Response = MPMTaskRepository.save(taskCP3);
//		entityManager.flush();
//		mpmTaskList.add(taskCP3Response);
		
		
		MPMTask taskCP4 = new MPMTask();
		taskCP4.setName("CP4");
		taskCP4.setDescription("CP4");
		taskCP4.setDueDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP4DecisionDate()) 
				&& requestData.getGovMilestoneCP4DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP4DecisionDate().split("T")[0]) : null);
		taskCP4.setIsActive(false);
		taskCP4.setIsApprovalTask(false);
		taskCP4.setIsDeleted(false);
		taskCP4.setProgress(0);
		taskCP4.setStartDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP4DecisionDate()) 
				&& requestData.getGovMilestoneCP4DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP4DecisionDate().split("T")[0]) : null);
		taskCP4.setAssignmentType("ROLE");
		taskCP4.setTaskType("TASK_WITHOUT_DELIVERABLE");
		taskCP4.setStatusId(isCP4Completed == true 
				? Integer.parseInt(env.getProperty("npd.task.completed.status.id")) 
				: Integer.parseInt(env.getProperty("npd.task.initial.status.id")));
		taskCP4.setPriorityId(Integer.parseInt(env.getProperty("npd.task.high.priority.id")));
		taskCP4.setProjectId(projectId);
		taskCP4.setTeamRoleId(task2MPMRole);
		taskCP4.setOwnerId(e2ePMOwner);
		taskCP4.setActiveUserId(e2ePMOwner);
		taskCP4.setExpectedDuration(0);
		taskCP4.setOriginalStartDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP4DecisionDate()) 
				&& requestData.getGovMilestoneCP4DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP4DecisionDate().split("T")[0]) : null);
		taskCP4.setOriginalDueDate(!util.isNullOrEmpty(requestData.getGovMilestoneCP4DecisionDate()) 
				&& requestData.getGovMilestoneCP4DecisionDate().indexOf("T") > 0 ? 
				Date.valueOf(requestData.getGovMilestoneCP4DecisionDate().split("T")[0]) : null);
		taskCP4.setTaskDuration(1);
		taskCP4.setTaskDurationType("weeks");
//		taskCP4.setId(MPMTaskRepository.getCurrentVal());
		taskCP4.setIsBulkUpdate(false);
		taskCP4.setIsOnHold(false);
		taskCP4.setIsMilestone(false);
		taskCP4.setIsSubproject(false);
		taskCP4.setOrganizationid(orgId);
		mpmTaskList.add(taskCP4);
//		MPMTask taskCP4Response = MPMTaskRepository.save(taskCP4);
//		entityManager.flush();
//		mpmTaskList.add(taskCP4Response);
//		entityManager.clear();
		
		List<MPMTask> taskResponse = MPMTaskRepository.saveAll(mpmTaskList);
		logger.info("Taskss Created Successfully");
		List<MPMTaskSchema> taskSchemaList = taskIndexingService.indexTask(taskResponse, 
				env.getProperty("npd.project.itemId").concat(".").concat(String.valueOf(projectId)) , taskDetails);
//		logger.info("Task indexer list " + taskSchemaList);
		
		return taskSchemaList;
	}
	
	
	
}
