package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "shrdacheronmpmcoreworkflow_rules")
@Entity
public class WorkflowRules {
	
	@Id
	@GeneratedValue(generator = "workflow_rules_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "workflow_rules_seq", sequenceName = "workflow_rules_seq",allocationSize=1)
    @Column(name = "Id", updatable = false)
    private Integer id;

    @Column(name = "r_po_project_id", updatable = false)
    private Integer r_po_project_id;
    
    @Column(name = "trigger_type", updatable = false)
    private String trigger_type;
    
    @Column(name = "target_type", updatable = false)
    private String target_type;
    
    @Column(name = "r_po_event_id", updatable = false)
    private Integer r_po_event_id;
    
    @Column(name = "is_initial_rule", updatable = false)
    private Boolean is_initial_rule;
    
    @Column(name = "r_po_target_task_id", updatable = false)
    private Integer r_po_target_task_id;
    
    @Column(name = "r_po_actions_id", updatable = false)
    private Integer r_po_actions_id;
    
    @Column(name = "r_po_status_id", updatable = false)
    private Integer r_po_status_id;
    
    @Column(name = "r_po_task_id", updatable = false)
    private Integer r_po_task_id;
    
    @Column(name = "s_organizationid", updatable = false)
    private Integer s_organizationid;

}
