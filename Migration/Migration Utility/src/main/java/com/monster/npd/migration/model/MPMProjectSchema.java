package com.monster.npd.migration.model;

import java.util.Date;

import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@SolrDocument(collection = "NPD")
@NoArgsConstructor
public class MPMProjectSchema {
	
	// Default required fields ---------
    @org.springframework.data.annotation.Id
    @Indexed(name = "ITEM_ID", type = "string")
    private String item_id;
    
    @Indexed(name = "CONTENT_TYPE", type = "string")
    private String content_type;
    
    @Indexed(name = "PROJECT_PRIORITY_ID", type = "string")
    private Integer PROJECT_PRIORITY_ID;
    
    @Indexed(name = "PROJECT_CAMPAIGN_ITEM_ID", type = "string")
    private String PROJECT_CAMPAIGN_ITEM_ID;
    
    @Indexed(name = "PR_REQUEST_ITEM_ID", type = "string")
    private String PR_REQUEST_ITEM_ID;
      
    @Indexed(name = "PROJECT_NAME", type = "string")
    private String PROJECT_NAME;
    
    @Indexed(name = "PROJECT_NAME_facet", type = "string")
    private String PROJECT_NAME_facet;

    @Indexed(name = "PROJECT_START_DATE", type = "string")
    private String PROJECT_START_DATE;
    
    @Indexed(name = "IS_ACTIVE_PROJECT", type = "boolean")
    private String IS_ACTIVE_PROJECT;
    
    @Indexed(name = "PR_BRAND_string", type = "string")
    private String PR_BRAND_string;
    
    @Indexed(name = "PR_BRAND_string_facet", type = "string")
    private String PR_BRAND_string_facet;
    
    @Indexed(name = "PROJECT_STATUS_ID", type = "string")
    private Integer PROJECT_STATUS_ID;
    
    @Indexed(name = "PROJECT_PRIORITY_VALUE", type = "string")
    private String PROJECT_PRIORITY_VALUE;
    
    @Indexed(name = "PROJECT_PRIORITY_VALUE_facet", type = "string")
    private String PROJECT_PRIORITY_VALUE_facet;
    
    @Indexed(name = "PR_PROJECT_TYPE", type = "string")
    private String PR_PROJECT_TYPE;
    
    @Indexed(name = "PR_PROJECT_TYPE_facet", type = "string")
    private String PR_PROJECT_TYPE_facet;
    
    @Indexed(name = "PR_MARKET", type = "string")
    private String PR_MARKET;
    
    @Indexed(name = "PR_MARKET_facet", type = "string")
    private String PR_MARKET_facet;
    
    @Indexed(name = "PR_BUSINESS_UNIT", type = "string")
    private String PR_BUSINESS_UNIT;
    
    @Indexed(name = "PR_BUSINESS_UNIT_facet", type = "string")
    private String PR_BUSINESS_UNIT_facet;
    
    @Indexed(name = "PR_BOTTLER", type = "string")
    private String PR_BOTTLER;
    
    @Indexed(name = "PR_BOTTLER_facet", type = "string")
    private String PR_BOTTLER_facet;
    
    @Indexed(name = "PR_PRODUCTION_SITE", type = "string")
    private String PR_PRODUCTION_SITE;
    
    @Indexed(name = "PR_PRODUCTION_SITE_facet", type = "string")
    private String PR_PRODUCTION_SITE_facet;
    
    @Indexed(name = "PR_PLATFORM", type = "string")
    private String PR_PLATFORM;
    
    @Indexed(name = "PR_PLATFORM_facet", type = "string")
    private String PR_PLATFORM_facet;
    
    @Indexed(name = "PR_IS_SVP_ALIGNED", type = "string")
    private String PR_IS_SVP_ALIGNED;
    
    @Indexed(name = "PR_COMMERCIAL_CATEGORISATION", type = "string")
    private String PR_COMMERCIAL_CATEGORISATION;
    
    @Indexed(name = "PR_COMMERCIAL_CATEGORISATION_facet", type = "string")
    private String PR_COMMERCIAL_CATEGORISATION_facet;
    
    @Indexed(name = "PR_POST_LAUNCH_ANALYSIS", type = "string")
    private String PR_POST_LAUNCH_ANALYSIS;
    
    @Indexed(name = "PR_VARIANT_facet", type = "string")
    private String PR_VARIANT_facet;
    
    @Indexed(name = "PR_VARIANT", type = "string")
    private String PR_VARIANT;
    
    @Indexed(name = "PR_SKU_DETAILS_facet", type = "string")
    private String PR_SKU_DETAILS_facet;
    
    @Indexed(name = "PR_SKU_DETAILS", type = "string")
    private String PR_SKU_DETAILS;
    
    @Indexed(name = "PR_CASE_PACK_SIZE", type = "string")
    private Integer PR_CASE_PACK_SIZE;
    
    @Indexed(name = "PR_CONSUMER_UNIT_SIZE", type = "string")
    private Integer PR_CONSUMER_UNIT_SIZE;
    
    @Indexed(name = "PR_PRIMARY_PACKAGING_TYPE_facet", type = "string")
    private String PR_PRIMARY_PACKAGING_TYPE_facet;
    
    @Indexed(name = "PR_PRIMARY_PACKAGING_TYPE", type = "string")
    private String PR_PRIMARY_PACKAGING_TYPE;
    
    @Indexed(name = "PR_SECONDARY_PACKAGING_TYPE_facet", type = "string")
    private String PR_SECONDARY_PACKAGING_TYPE_facet;
    
    @Indexed(name = "PR_SECONDARY_PACKAGING_TYPE", type = "string")
    private String PR_SECONDARY_PACKAGING_TYPE;
    
    @Indexed(name = "PR_EARLY_PROJECT_CLASSIFICATION_facet", type = "string")
    private String PR_EARLY_PROJECT_CLASSIFICATION_facet;
    
    @Indexed(name = "PR_EARLY_PROJECT_CLASSIFICATION", type = "string")
    private String PR_EARLY_PROJECT_CLASSIFICATION;
    
    @Indexed(name = "PR_EARLY_PROJECT_CLASSIFICATION_DESC_facet", type = "string")
    private String PR_EARLY_PROJECT_CLASSIFICATION_DESC_facet;
    
    @Indexed(name = "PR_EARLY_PROJECT_CLASSIFICATION_DESC", type = "string")
    private String PR_EARLY_PROJECT_CLASSIFICATION_DESC;
    
    @Indexed(name = "NPD_PROJECT_PROJECT_FINAL_PROJECT_CLASSIFICATION", type = "string")
    private String NPD_PROJECT_PROJECT_FINAL_PROJECT_CLASSIFICATION;
    
    @Indexed(name = "NPD_PROJECT_PROJECT_FINAL_PROJECT_CLASSIFICATION_DESC_facet", type = "string")
    private String NPD_PROJECT_PROJECT_FINAL_PROJECT_CLASSIFICATION_DESC_facet;
    
    @Indexed(name = "NPD_PROJECT_PROJECT_FINAL_PROJECT_CLASSIFICATION_DESC", type = "string")
    private String NPD_PROJECT_PROJECT_FINAL_PROJECT_CLASSIFICATION_DESC;
    
    @Indexed(name = "PR_REGION", type = "string")
    private String PR_REGION;
    
    @Indexed(name = "PR_REQUESTOR_facet", type = "string")
    private String PR_REQUESTOR_facet;
    
    @Indexed(name = "PR_REQUESTOR", type = "string")
    private String PR_REQUESTOR;
    
    @Indexed(name = "PR_CHECKPOINT_0_APPROVED_DATE", type = "string")
    private String PR_CHECKPOINT_0_APPROVED_DATE;
    
    @Indexed(name = "PROJECT_OWNER_ID", type = "string")
    private Integer PROJECT_OWNER_ID;
    
    @Indexed(name = "PROJECT_OWNER_CN", type = "string")
    private String PROJECT_OWNER_CN;
    
    @Indexed(name = "PR_REQUEST_ID_string_facet", type = "string")
    private String PR_REQUEST_ID_string_facet;
    
    @Indexed(name = "PR_REQUEST_ID_string", type = "string")
    private String PR_REQUEST_ID_string;
    
    @Indexed(name = "PROJECT_TYPE", type = "string")
    private String PROJECT_TYPE;
    
    @Indexed(name = "ID", type = "string")
    private String ID;
    
    @Indexed(name = "PROJECT_MILESTONE_PROGRESS_facet", type = "string")
    private String PROJECT_MILESTONE_PROGRESS_facet;
    
    @Indexed(name = "PROJECT_MILESTONE_PROGRESS", type = "string")
    private String PROJECT_MILESTONE_PROGRESS;
    
    @Indexed(name = "PR_REQUEST_TYPE", type = "string")
    private String PR_REQUEST_TYPE;
    
    @Indexed(name = "PR_REQUEST_TYPE_VALUE", type = "string")
    private String PR_REQUEST_TYPE_VALUE;
    
    @Indexed(name = "PROJECT_DESCRIPTION", type = "string")
    private String PROJECT_DESCRIPTION;
    
    @Indexed(name = "PROJECT_DUE_DATE", type = "string")
    private String PROJECT_DUE_DATE;
    
    @Indexed(name = "PROJECT_END_DATE", type = "string")
    private String PROJECT_END_DATE;
    
    @Indexed(name = "IS_DELETED_PROJECT", type = "string")
    private String IS_DELETED_PROJECT;
    
    @Indexed(name = "IS_STATNDARED_STATUS_PROJECT", type = "string")
    private String IS_STATNDARED_STATUS_PROJECT;
    
    @Indexed(name = "OTMM_FOLDER_ID", type = "string")
    private String OTMM_FOLDER_ID;
    
    @Indexed(name = "PROJECT_TEMPLATE_REF_ID", type = "string")
    private String PROJECT_TEMPLATE_REF_ID;
    
    @Indexed(name = "PROJECT_CATEGORY_ID", type = "string")
    private Integer PROJECT_CATEGORY_ID;
    
    @Indexed(name = "PROJECT_CATEGORY_VALUE", type = "string")
    private String PROJECT_CATEGORY_VALUE;
    
    @Indexed(name = "PROJECT_OWNER_NAME", type = "string")
    private String PROJECT_OWNER_NAME;
    
    @Indexed(name = "PROJECT_STATUS_VALUE_facet", type = "string")
    private String PROJECT_STATUS_VALUE_facet;
    
    @Indexed(name = "PROJECT_STATUS_VALUE", type = "string")
    private String PROJECT_STATUS_VALUE;
    
    @Indexed(name = "PROJECT_STATUS_TYPE", type = "string")
    private String PROJECT_STATUS_TYPE;
    
    @Indexed(name = "PROJECT_TEAM_VALUE", type = "string")
    private String PROJECT_TEAM_VALUE;
    
    @Indexed(name = "PROJECT_TEAM_ID", type = "string")
    private Integer PROJECT_TEAM_ID;
    
    @Indexed(name = "ORIGINAL_PROJECT_START_DATE", type = "string")
    private String ORIGINAL_PROJECT_START_DATE;
    
    @Indexed(name = "ORIGINAL_PROJECT_DUE_DATE", type = "string")
    private String ORIGINAL_PROJECT_DUE_DATE;
    
    @Indexed(name = "ACTUAL_PROJECT_START_DATE", type = "string")
    private String ACTUAL_PROJECT_START_DATE;
    
    @Indexed(name = "ACTUAL_PROJECT_DUE_DATE", type = "string")
    private String ACTUAL_PROJECT_DUE_DATE;
    
    @Indexed(name = "EXPECTED_PROJECT_DURATION", type = "string")
    private Integer EXPECTED_PROJECT_DURATION;
    
    @Indexed(name = "PROJECT_TIME_SPENT", type = "string")
    private Integer PROJECT_TIME_SPENT;
    
    @Indexed(name = "NPD_PROJECT_BOTTLER_COMMUNICATION_STATUS_facet", type = "string")
    private String NPD_PROJECT_BOTTLER_COMMUNICATION_STATUS_facet;
    
    @Indexed(name = "NPD_PROJECT_BOTTLER_COMMUNICATION_STATUS", type = "string")
    private String NPD_PROJECT_BOTTLER_COMMUNICATION_STATUS;
    
    @Indexed(name = "NPD_PROJECT_BOTTLER_PUSHED_OUT_PRODUCTION_facet", type = "string")
    private String NPD_PROJECT_BOTTLER_PUSHED_OUT_PRODUCTION_facet;
    

    @Indexed(name = "NPD_PROJECT_BOTTLER_PUSHED_OUT_PRODUCTION", type = "string")
    private String NPD_PROJECT_BOTTLER_PUSHED_OUT_PRODUCTION;
    
    @Indexed(name = "NPD_PROJECT_OPS_ALIGNED_DP_WEEK_facet", type = "string")
    private String NPD_PROJECT_OPS_ALIGNED_DP_WEEK_facet;
    
    @Indexed(name = "NPD_PROJECT_OPS_ALIGNED_DP_YEAR_facet", type = "string")
    private String NPD_PROJECT_OPS_ALIGNED_DP_YEAR_facet;
    
    @Indexed(name = "NPD_PROJECT_TIMELINE_RISK_facet", type = "string")
    private String NPD_PROJECT_TIMELINE_RISK_facet;
    
    @Indexed(name = "NPD_PROJECT_RISK_SUMMARY_facet", type = "string")
    private String NPD_PROJECT_RISK_SUMMARY_facet;
    
    @Indexed(name = "NPD_PROJECT_STATUS_CRITICAL_ITEMS_facet", type = "string")
    private String NPD_PROJECT_STATUS_CRITICAL_ITEMS_facet;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_COMMERCIAL_PROD_INCLUDED_facet", type = "string")
    private String NPD_PROJECT_TRIAL_COMMERCIAL_PROD_INCLUDED_facet;
    
    @Indexed(name = "NPD_PROJECT_OPS_ALIGNED_DP_WEEK", type = "string")
    private String NPD_PROJECT_OPS_ALIGNED_DP_WEEK;
    
    @Indexed(name = "NPD_PROJECT_OPS_ALIGNED_DP_YEAR", type = "string")
    private String NPD_PROJECT_OPS_ALIGNED_DP_YEAR;
    
    @Indexed(name = "NPD_PROJECT_TIMELINE_RISK", type = "string")
    private String NPD_PROJECT_TIMELINE_RISK;
    
    @Indexed(name = "NPD_PROJECT_RISK_SUMMARY", type = "string")
    private String NPD_PROJECT_RISK_SUMMARY;
    
    @Indexed(name = "NPD_PROJECT_STATUS_CRITICAL_ITEMS", type = "string")
    private String NPD_PROJECT_STATUS_CRITICAL_ITEMS;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_COMMERCIAL_PROD_INCLUDED", type = "string")
    private String NPD_PROJECT_TRIAL_COMMERCIAL_PROD_INCLUDED;
    
    @Indexed(name = "NPD_PROJECT_PROGRAM_MANAGER_SLACK_TIME", type = "string")
    private Integer NPD_PROJECT_PROGRAM_MANAGER_SLACK_TIME;
    
    @Indexed(name = "NPD_PROJECT_PM_CP1_PHASE_DELAY", type = "string")
    private Integer NPD_PROJECT_PM_CP1_PHASE_DELAY;
    
    @Indexed(name = "NPD_PROJECT_ON_TRACK_ORIGINAL_CP1_DATE_facet", type = "string")
    private String NPD_PROJECT_ON_TRACK_ORIGINAL_CP1_DATE_facet;
    
    @Indexed(name = "NPD_PROJECT_MEC_COMMERCIAL_BOTTLER_REQUESTED_facet", type = "string")
    private String NPD_PROJECT_MEC_COMMERCIAL_BOTTLER_REQUESTED_facet;
    
    @Indexed(name = "NPD_PROJECT_OTHER_TBC_facet", type = "string")
    private String NPD_PROJECT_OTHER_TBC_facet;
    
    @Indexed(name = "NPD_PROJECT_OTHER2_TBC_facet", type = "string")
    private String NPD_PROJECT_OTHER2_TBC_facet;
    
    @Indexed(name = "NPD_PROJECT_HAS_CHANGE_BEEN_ALIGNED_facet", type = "string")
    private String NPD_PROJECT_HAS_CHANGE_BEEN_ALIGNED_facet;
    
    @Indexed(name = "NPD_PROJECT_BRIEF_DESC_OF_DELAY_facet", type = "string")
    private String NPD_PROJECT_BRIEF_DESC_OF_DELAY_facet;
    
    @Indexed(name = "NPD_PROJECT_TARGET_CP2_DATE_facet", type = "string")
    private String NPD_PROJECT_TARGET_CP2_DATE_facet;
    
    @Indexed(name = "NPD_PROJECT_COM_TECH_OPS_AGREED_DP_WEEK_facet", type = "string")
    private String NPD_PROJECT_COM_TECH_OPS_AGREED_DP_WEEK_facet;
    
    @Indexed(name = "NPD_PROJECT_RAG_facet", type = "string")
    private String NPD_PROJECT_RAG_facet;
    
    @Indexed(name = "NPD_PROJECT_HEALTH_facet", type = "string")
    private String NPD_PROJECT_HEALTH_facet;
    
    @Indexed(name = "NPD_PROJECT_CP1_REVISED_DATE_facet", type = "string")
    private String NPD_PROJECT_CP1_REVISED_DATE_facet;
    
    @Indexed(name = "NPD_PROJECT_ON_TRACK_ORIGINAL_CP1_DATE", type = "string")
    private String NPD_PROJECT_ON_TRACK_ORIGINAL_CP1_DATE;
    
    @Indexed(name = "NPD_PROJECT_MEC_COMMERCIAL_BOTTLER_REQUESTED", type = "string")
    private String NPD_PROJECT_MEC_COMMERCIAL_BOTTLER_REQUESTED;
    
    @Indexed(name = "NPD_PROJECT_OTHER_TBC", type = "string")
    private String NPD_PROJECT_OTHER_TBC;
    
    @Indexed(name = "NPD_PROJECT_OTHER2_TBC", type = "string")
    private String NPD_PROJECT_OTHER2_TBC;
    
    @Indexed(name = "NPD_PROJECT_HAS_CHANGE_BEEN_ALIGNED", type = "string")
    private String NPD_PROJECT_HAS_CHANGE_BEEN_ALIGNED;
    
    @Indexed(name = "NPD_PROJECT_BRIEF_DESC_OF_DELAY", type = "string")
    private String NPD_PROJECT_BRIEF_DESC_OF_DELAY;
    
    @Indexed(name = "NPD_PROJECT_TARGET_CP2_DATE", type = "string")
    private String NPD_PROJECT_TARGET_CP2_DATE;
    
    @Indexed(name = "NPD_PROJECT_COM_TECH_OPS_AGREED_DP_WEEK", type = "string")
    private String NPD_PROJECT_COM_TECH_OPS_AGREED_DP_WEEK;
    
    @Indexed(name = "NPD_PROJECT_RAG", type = "string")
    private String NPD_PROJECT_RAG;
    
    @Indexed(name = "NPD_PROJECT_HEALTH", type = "string")
    private String NPD_PROJECT_HEALTH;
    
    @Indexed(name = "NPD_PROJECT_CP1_REVISED_DATE", type = "string")
    private String NPD_PROJECT_CP1_REVISED_DATE;
    
    @Indexed(name = "NPD_PROJECT_NO_OF_WEEKS_INCREASED", type = "string")
    private Integer NPD_PROJECT_NO_OF_WEEKS_INCREASED;
    
    @Indexed(name = "NPD_PROJECT_EAN_TARGET_WEEK_facet", type = "string")
    private String NPD_PROJECT_EAN_TARGET_WEEK_facet;
    
    @Indexed(name = "NPD_PROJECT_EAN_ACTUAL_WEEK_facet", type = "string")
    private String NPD_PROJECT_EAN_ACTUAL_WEEK_facet;
    
    @Indexed(name = "NPD_PROJECT_PROD_INFO_TARGET_WEEK_facet", type = "string")
    private String NPD_PROJECT_PROD_INFO_TARGET_WEEK_facet;
    
    @Indexed(name = "NPD_PROJECT_PROD_INFO_ACTUAL_WEEK_facet", type = "string")
    private String NPD_PROJECT_PROD_INFO_ACTUAL_WEEK_facet;
    
    @Indexed(name = "NPD_PROJECT_LABEL_UPLOADED_ACTUAL_DATE_facet", type = "string")
    private String NPD_PROJECT_LABEL_UPLOADED_ACTUAL_DATE_facet;
    
    @Indexed(name = "NPD_PROJECT_BOTTLER_SPECIFIC_COMMENTS_facet", type = "string")
    private String NPD_PROJECT_BOTTLER_SPECIFIC_COMMENTS_facet;
    
    @Indexed(name = "NPD_PROJECT_BOTTLER_CAN_TARGET_WEEK_facet", type = "string")
    private String NPD_PROJECT_BOTTLER_CAN_TARGET_WEEK_facet;
    
    @Indexed(name = "NPD_PROJECT_BOTTLER_CAN_ACTUAL_WEEK_facet", type = "string")
    private String NPD_PROJECT_BOTTLER_CAN_ACTUAL_WEEK_facet;
    
    @Indexed(name = "NPD_PROJECT_ITEM_NUMBER_facet", type = "string")
    private String NPD_PROJECT_ITEM_NUMBER_facet;
    
    @Indexed(name = "NPD_PROJECT_EAN_TARGET_WEEK", type = "string")
    private String NPD_PROJECT_EAN_TARGET_WEEK;
    
    @Indexed(name = "NPD_PROJECT_EAN_ACTUAL_WEEK", type = "string")
    private String NPD_PROJECT_EAN_ACTUAL_WEEK;
    
    @Indexed(name = "NPD_PROJECT_PROD_INFO_TARGET_WEEK", type = "string")
    private String NPD_PROJECT_PROD_INFO_TARGET_WEEK;
    
    @Indexed(name = "NPD_PROJECT_PROD_INFO_ACTUAL_WEEK", type = "string")
    private String NPD_PROJECT_PROD_INFO_ACTUAL_WEEK;
    
    @Indexed(name = "NPD_PROJECT_LABEL_UPLOADED_ACTUAL_DATE", type = "string")
    private String NPD_PROJECT_LABEL_UPLOADED_ACTUAL_DATE;
    
    @Indexed(name = "NPD_PROJECT_BOTTLER_SPECIFIC_COMMENTS", type = "string")
    private String NPD_PROJECT_BOTTLER_SPECIFIC_COMMENTS;
    
    @Indexed(name = "NPD_PROJECT_BOTTLER_CAN_TARGET_WEEK", type = "string")
    private String NPD_PROJECT_BOTTLER_CAN_TARGET_WEEK;
    
    @Indexed(name = "NPD_PROJECT_BOTTLER_CAN_ACTUAL_WEEK", type = "string")
    private String NPD_PROJECT_BOTTLER_CAN_ACTUAL_WEEK;
    
    @Indexed(name = "NPD_PROJECT_ITEM_NUMBER", type = "string")
    private String NPD_PROJECT_ITEM_NUMBER;
    
    
    @Indexed(name = "NPD_PROJECT_NUMBER_OF_CAN_COMPANIES", type = "string")
    private Integer NPD_PROJECT_NUMBER_OF_CAN_COMPANIES;
    
    @Indexed(name = "NPD_PROJECT_NAME_OF_CAN_COMPANY_facet", type = "string")
    private String NPD_PROJECT_NAME_OF_CAN_COMPANY_facet;
    
    @Indexed(name = "NPD_PROJECT_PO_RECEIVED_ACTUAL_WEEK_facet", type = "string")
    private String NPD_PROJECT_PO_RECEIVED_ACTUAL_WEEK_facet;
    
    @Indexed(name = "NPD_PROJECT_PALLET_LABEL_RECEIVED_TARGET_WEEK_facet", type = "string")
    private String NPD_PROJECT_PALLET_LABEL_RECEIVED_TARGET_WEEK_facet;
    
    @Indexed(name = "NPD_PROJECT_PALLET_LABEL_RECEIVED_ACTUAL_WEEK_facet", type = "string")
    private String NPD_PROJECT_PALLET_LABEL_RECEIVED_ACTUAL_WEEK_facet;
    
    @Indexed(name = "NPD_PROJECT_PALLET_LABEL_SENT_TARGET_WEEK_facet", type = "string")
    private String NPD_PROJECT_PALLET_LABEL_SENT_TARGET_WEEK_facet;
    
    @Indexed(name = "NPD_PROJECT_PALLET_LABEL_SENT_ACTUAL_WEEK_facet", type = "string")
    private String NPD_PROJECT_PALLET_LABEL_SENT_ACTUAL_WEEK_facet;
    
    @Indexed(name = "NPD_PROJECT_IS_FG_PROJECT_facet", type = "string")
    private String NPD_PROJECT_IS_FG_PROJECT_facet;
    
    @Indexed(name = "NPD_PROJECT_CURRENT_COMMERICAL_ALIGNED_DATE_facet", type = "string")
    private String NPD_PROJECT_CURRENT_COMMERICAL_ALIGNED_DATE_facet;
    
    @Indexed(name = "NPD_PROJECT_BUSINESS_ID_facet", type = "string")
    private String NPD_PROJECT_BUSINESS_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_JOB_ID_facet", type = "string")
    private String NPD_PROJECT_JOB_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_JOB_STATUS_facet", type = "string")
    private String NPD_PROJECT_JOB_STATUS_facet;
    
    @Indexed(name = "NPD_PROJECT_JOB_NAME_facet", type = "string")
    private String NPD_PROJECT_JOB_NAME_facet;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_DATE_CONFIRMED_WITH_BOTTLER_facet", type = "string")
    private String NPD_PROJECT_TRIAL_DATE_CONFIRMED_WITH_BOTTLER_facet;
    
    @Indexed(name = "NPD_PROJECT_SECONDARY_PACK_LEAD_TIME_facet", type = "string")
    private String NPD_PROJECT_SECONDARY_PACK_LEAD_TIME_facet;
    
    @Indexed(name = "NPD_PROJECT_NSV_Y1_ANNUALISED_facet", type = "string")
    private String NPD_PROJECT_NSV_Y1_ANNUALISED_facet;
    
    @Indexed(name = "NPD_PROJECT_VOLUME_Y1_ANNUALISED_facet", type = "string")
    private String NPD_PROJECT_VOLUME_Y1_ANNUALISED_facet;
    
    @Indexed(name = "NPD_PROJECT_VOLUME_1ST_3_MONTHS_facet", type = "string")
    private String NPD_PROJECT_VOLUME_1ST_3_MONTHS_facet;
    
    @Indexed(name = "NPD_PROJECT_CP1_AGREEMENT_DATE_facet", type = "string")
    private String NPD_PROJECT_CP1_AGREEMENT_DATE_facet;
    
    @Indexed(name = "NPD_PROJECT_LEAD_TIME_TARGET_facet", type = "string")
    private String NPD_PROJECT_LEAD_TIME_TARGET_facet;
    
    @Indexed(name = "NPD_PROJECT_CP1_ACTUAL_DATE_facet", type = "string")
    private String NPD_PROJECT_CP1_ACTUAL_DATE_facet;
    
    @Indexed(name = "NPD_PROJECT_CP2_ACTUAL_DATE_facet", type = "string")
    private String NPD_PROJECT_CP2_ACTUAL_DATE_facet;
    
    @Indexed(name = "NPD_PROJECT_CP3_ACTUAL_DATE_facet", type = "string")
    private String NPD_PROJECT_CP3_ACTUAL_DATE_facet;
    
    @Indexed(name = "NPD_PROJECT_CP4_ACTUAL_DATE_facet", type = "string")
    private String NPD_PROJECT_CP4_ACTUAL_DATE_facet;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_OTHER1_facet", type = "string")
    private String NPD_PROJECT_PM_REPORTING_OTHER1_facet;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_OTHER2_facet", type = "string")
    private String NPD_PROJECT_PM_REPORTING_OTHER2_facet;
    
    @Indexed(name = "NPD_PROJECT_FINAL_MANUFACTURING_SITE_facet", type = "string")
    private String NPD_PROJECT_FINAL_MANUFACTURING_SITE_facet;
    
    @Indexed(name = "NPD_PROJECT_NAME_OF_CAN_COMPANY", type = "string")
    private String NPD_PROJECT_NAME_OF_CAN_COMPANY;
    
    @Indexed(name = "NPD_PROJECT_PO_RECEIVED_ACTUAL_WEEK", type = "string")
    private String NPD_PROJECT_PO_RECEIVED_ACTUAL_WEEK;
    
    @Indexed(name = "NPD_PROJECT_PALLET_LABEL_RECEIVED_TARGET_WEEK", type = "string")
    private String NPD_PROJECT_PALLET_LABEL_RECEIVED_TARGET_WEEK;
    
    @Indexed(name = "NPD_PROJECT_PALLET_LABEL_RECEIVED_ACTUAL_WEEK", type = "string")
    private String NPD_PROJECT_PALLET_LABEL_RECEIVED_ACTUAL_WEEK;
    
    @Indexed(name = "NPD_PROJECT_PALLET_LABEL_SENT_TARGET_WEEK", type = "string")
    private String NPD_PROJECT_PALLET_LABEL_SENT_TARGET_WEEK;
    
    @Indexed(name = "NPD_PROJECT_PALLET_LABEL_SENT_ACTUAL_WEEK", type = "string")
    private String NPD_PROJECT_PALLET_LABEL_SENT_ACTUAL_WEEK;
    
    @Indexed(name = "NPD_PROJECT_IS_FG_PROJECT", type = "string")
    private String NPD_PROJECT_IS_FG_PROJECT;
    
    @Indexed(name = "NPD_PROJECT_CURRENT_COMMERICAL_ALIGNED_DATE", type = "string")
    private String NPD_PROJECT_CURRENT_COMMERICAL_ALIGNED_DATE;
    
    @Indexed(name = "NPD_PROJECT_BUSINESS_ID", type = "string")
    private String NPD_PROJECT_BUSINESS_ID;
    
    @Indexed(name = "NPD_PROJECT_JOB_ID", type = "string")
    private String NPD_PROJECT_JOB_ID;
    
    @Indexed(name = "NPD_PROJECT_JOB_STATUS", type = "string")
    private String NPD_PROJECT_JOB_STATUS;
    
    @Indexed(name = "NPD_PROJECT_JOB_NAME", type = "string")
    private String NPD_PROJECT_JOB_NAME;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_DATE_CONFIRMED_WITH_BOTTLER", type = "string")
    private String NPD_PROJECT_TRIAL_DATE_CONFIRMED_WITH_BOTTLER;
    
    @Indexed(name = "NPD_PROJECT_SECONDARY_PACK_LEAD_TIME", type = "string")
    private String NPD_PROJECT_SECONDARY_PACK_LEAD_TIME;
    
    @Indexed(name = "NPD_PROJECT_NSV_Y1_ANNUALISED", type = "string")
    private String NPD_PROJECT_NSV_Y1_ANNUALISED;
    
    @Indexed(name = "NPD_PROJECT_VOLUME_Y1_ANNUALISED", type = "string")
    private String NPD_PROJECT_VOLUME_Y1_ANNUALISED;
    
    @Indexed(name = "NPD_PROJECT_VOLUME_1ST_3_MONTHS", type = "string")
    private String NPD_PROJECT_VOLUME_1ST_3_MONTHS;
    
    @Indexed(name = "NPD_PROJECT_CP1_AGREEMENT_DATE", type = "string")
    private String NPD_PROJECT_CP1_AGREEMENT_DATE;
    
    @Indexed(name = "NPD_PROJECT_LEAD_TIME_TARGET", type = "string")
    private Integer NPD_PROJECT_LEAD_TIME_TARGET;
    
    @Indexed(name = "NPD_PROJECT_CP1_ACTUAL_DATE", type = "string")
    private String NPD_PROJECT_CP1_ACTUAL_DATE;
    
    @Indexed(name = "NPD_PROJECT_CP2_ACTUAL_DATE", type = "string")
    private String NPD_PROJECT_CP2_ACTUAL_DATE;
    
    @Indexed(name = "NPD_PROJECT_CP3_ACTUAL_DATE", type = "string")
    private String NPD_PROJECT_CP3_ACTUAL_DATE;
    
    @Indexed(name = "NPD_PROJECT_CP4_ACTUAL_DATE", type = "string")
    private String NPD_PROJECT_CP4_ACTUAL_DATE;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_OTHER1", type = "string")
    private String NPD_PROJECT_PM_REPORTING_OTHER1;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_OTHER2", type = "string")
    private String NPD_PROJECT_PM_REPORTING_OTHER2;
    
    @Indexed(name = "NPD_PROJECT_FINAL_MANUFACTURING_SITE", type = "string")
    private String NPD_PROJECT_FINAL_MANUFACTURING_SITE;
    
    @Indexed(name = "NPD_PROJECT_FINAL_MANUFACTURING_SITE_ID", type = "string")
    private Integer NPD_PROJECT_FINAL_MANUFACTURING_SITE_ID;
    
    @Indexed(name = "NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_facet", type = "string")
    private String NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_facet;
    
    @Indexed(name = "NPD_PROJECT_FINAL_MANUFACTURING_LOCATION", type = "string")
    private String NPD_PROJECT_FINAL_MANUFACTURING_LOCATION;
    
    @Indexed(name = "NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ID", type = "string")
    private Integer NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ID;
    
    @Indexed(name = "NPD_PROJECT_WINSHUTTLE_NUMBER_facet", type = "string")
    private String NPD_PROJECT_WINSHUTTLE_NUMBER_facet;
    
    @Indexed(name = "NPD_PROJECT_FG_MATERIAL_CODE_facet", type = "string")
    private String NPD_PROJECT_FG_MATERIAL_CODE_facet;
    
    @Indexed(name = "NPD_PROJECT_FORMULA_HBC_NUM_facet", type = "string")
    private String NPD_PROJECT_FORMULA_HBC_NUM_facet;
    
    @Indexed(name = "NPD_PROJECT_HBC_ISSUANCE_TYPE_facet", type = "string")
    private String NPD_PROJECT_HBC_ISSUANCE_TYPE_facet;
    
    @Indexed(name = "NPD_PROJECT_CAN_SLEEVE_PRIMARY_BOTTLE_MATERIAL_facet", type = "string")
    private String NPD_PROJECT_CAN_SLEEVE_PRIMARY_BOTTLE_MATERIAL_facet;
    
    @Indexed(name = "NPD_PROJECT_PALLET_LABEL_FORMAT_facet", type = "string")
    private String NPD_PROJECT_PALLET_LABEL_FORMAT_facet;
    
    @Indexed(name = "NPD_PROJECT_TRAY_LABEL_FORMAT_facet", type = "string")
    private String NPD_PROJECT_TRAY_LABEL_FORMAT_facet;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_MATERIALS_CONFIRMED_DELIVERY_WEEK_facet", type = "string")
    private String NPD_PROJECT_TRIAL_MATERIALS_CONFIRMED_DELIVERY_WEEK_facet;
    
    @Indexed(name = "NPD_PROJECT_FINAL_AGREED_1ST_PROD_CONFIRMED_WITH_BOTTLER_facet", type = "string")
    private String NPD_PROJECT_FINAL_AGREED_1ST_PROD_CONFIRMED_WITH_BOTTLER_facet;
    
    @Indexed(name = "NPD_PROJECT_1ST_PROD_MATERIALS_CONFIRMED_DELIVERY_WEEK_facet", type = "string")
    private String NPD_PROJECT_1ST_PROD_MATERIALS_CONFIRMED_DELIVERY_WEEK_facet;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_OR_FIRST_PROD_INCLUDED_IN_PLAN_facet", type = "string")
    private String NPD_PROJECT_TRIAL_OR_FIRST_PROD_INCLUDED_IN_PLAN_facet;
    
    @Indexed(name = "NPD_PROJECT_EARLIEST_TRIAL_OR_1ST_PROD_DATE_facet", type = "string")
    private String NPD_PROJECT_EARLIEST_TRIAL_OR_1ST_PROD_DATE_facet;
    
    @Indexed(name = "NPD_PROJECT_BOM_SENT_ACTUAL_DATE_facet", type = "string")
    private String NPD_PROJECT_BOM_SENT_ACTUAL_DATE_facet;
    
    @Indexed(name = "NPD_PROJECT_LOCAL_BOM_IN_SAP_facet", type = "string")
    private String NPD_PROJECT_LOCAL_BOM_IN_SAP_facet;
    
    @Indexed(name = "NPD_PROJECT_WINSHUTTLE_NUMBER", type = "string")
    private String NPD_PROJECT_WINSHUTTLE_NUMBER;
    
    @Indexed(name = "NPD_PROJECT_FG_MATERIAL_CODE", type = "string")
    private String NPD_PROJECT_FG_MATERIAL_CODE;
    
    @Indexed(name = "NPD_PROJECT_FORMULA_HBC_NUM", type = "string")
    private String NPD_PROJECT_FORMULA_HBC_NUM;
    
    @Indexed(name = "NPD_PROJECT_HBC_ISSUANCE_TYPE", type = "string")
    private String NPD_PROJECT_HBC_ISSUANCE_TYPE;
    
    @Indexed(name = "NPD_PROJECT_CAN_SLEEVE_PRIMARY_BOTTLE_MATERIAL", type = "string")
    private String NPD_PROJECT_CAN_SLEEVE_PRIMARY_BOTTLE_MATERIAL;
    
    @Indexed(name = "NPD_PROJECT_PALLET_LABEL_FORMAT", type = "string")
    private String NPD_PROJECT_PALLET_LABEL_FORMAT;
    
    @Indexed(name = "NPD_PROJECT_TRAY_LABEL_FORMAT", type = "string")
    private String NPD_PROJECT_TRAY_LABEL_FORMAT;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_MATERIALS_CONFIRMED_DELIVERY_WEEK", type = "string")
    private String NPD_PROJECT_TRIAL_MATERIALS_CONFIRMED_DELIVERY_WEEK;
    
    @Indexed(name = "NPD_PROJECT_FINAL_AGREED_1ST_PROD_CONFIRMED_WITH_BOTTLER", type = "string")
    private String NPD_PROJECT_FINAL_AGREED_1ST_PROD_CONFIRMED_WITH_BOTTLER;
    
    @Indexed(name = "NPD_PROJECT_1ST_PROD_MATERIALS_CONFIRMED_DELIVERY_WEEK", type = "string")
    private String NPD_PROJECT_1ST_PROD_MATERIALS_CONFIRMED_DELIVERY_WEEK;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_OR_FIRST_PROD_INCLUDED_IN_PLAN", type = "string")
    private String NPD_PROJECT_TRIAL_OR_FIRST_PROD_INCLUDED_IN_PLAN;
    
    @Indexed(name = "NPD_PROJECT_EARLIEST_TRIAL_OR_1ST_PROD_DATE", type = "string")
    private String NPD_PROJECT_EARLIEST_TRIAL_OR_1ST_PROD_DATE;
    
    @Indexed(name = "NPD_PROJECT_BOM_SENT_ACTUAL_DATE", type = "string")
    private String NPD_PROJECT_BOM_SENT_ACTUAL_DATE;
    
    @Indexed(name = "NPD_PROJECT_LOCAL_BOM_IN_SAP", type = "string")
    private String NPD_PROJECT_LOCAL_BOM_IN_SAP;
    
    @Indexed(name = "NPD_PROJECT_ALLOW_PROD_SCHEDULE_LESS_THAN_8_facet", type = "string")
    private String NPD_PROJECT_ALLOW_PROD_SCHEDULE_LESS_THAN_8_facet;
    
    @Indexed(name = "NPD_PROJECT_ALLOW_PROD_SCHEDULE_LESS_THAN_8", type = "string")
    private String NPD_PROJECT_ALLOW_PROD_SCHEDULE_LESS_THAN_8;
    
//    @Indexed(name = "NPD_PROJECT_PO_RECEIVED_ACTUAL_WEEK", type = "string")
//    private String NPD_PROJECT_PO_RECEIVED_ACTUAL_WEEK;
    
    @Indexed(name = "NPD_PROJECT_POSSIBLE_NOTF_REQUIREMENT_facet", type = "string")
    private String NPD_PROJECT_POSSIBLE_NOTF_REQUIREMENT_facet;
    
    @Indexed(name = "NPD_PROJECT_REQUIRES_PRIMARY_CAN_AW_facet", type = "string")
    private String NPD_PROJECT_REQUIRES_PRIMARY_CAN_AW_facet;
    
    @Indexed(name = "NPD_PROJECT_CAN_PRIMARY_AW_STARTED_facet", type = "string")
    private String NPD_PROJECT_CAN_PRIMARY_AW_STARTED_facet;
    
    @Indexed(name = "NPD_PROJECT_REQUIRES_SECONDARY_AW_facet", type = "string")
    private String NPD_PROJECT_REQUIRES_SECONDARY_AW_facet;
    
    @Indexed(name = "NPD_PROJECT_CAN_SECONDARY_AW_STARTED_facet", type = "string")
    private String NPD_PROJECT_CAN_SECONDARY_AW_STARTED_facet;
    
    @Indexed(name = "NPD_PROJECT_ALL_AW_TASKS_COMPLETED_facet", type = "string")
    private String NPD_PROJECT_ALL_AW_TASKS_COMPLETED_facet;
    
    @Indexed(name = "NPD_PROJECT_REGULATORY_COMMENTS_facet", type = "string")
    private String NPD_PROJECT_REGULATORY_COMMENTS_facet;
    
    @Indexed(name = "NPD_PROJECT_POSSIBLE_NOTF_REQUIREMENT", type = "string")
    private String NPD_PROJECT_POSSIBLE_NOTF_REQUIREMENT;
    
    @Indexed(name = "NPD_PROJECT_REQUIRES_PRIMARY_CAN_AW", type = "string")
    private String NPD_PROJECT_REQUIRES_PRIMARY_CAN_AW;
    
    @Indexed(name = "NPD_PROJECT_CAN_PRIMARY_AW_STARTED", type = "string")
    private String NPD_PROJECT_CAN_PRIMARY_AW_STARTED;
    
    @Indexed(name = "NPD_PROJECT_REQUIRES_SECONDARY_AW", type = "string")
    private String NPD_PROJECT_REQUIRES_SECONDARY_AW;
    
    @Indexed(name = "NPD_PROJECT_CAN_SECONDARY_AW_STARTED", type = "string")
    private String NPD_PROJECT_CAN_SECONDARY_AW_STARTED;
    
    @Indexed(name = "NPD_PROJECT_ALL_AW_TASKS_COMPLETED", type = "string")
    private String NPD_PROJECT_ALL_AW_TASKS_COMPLETED;
    
    @Indexed(name = "NPD_PROJECT_REGULATORY_COMMENTS", type = "string")
    private String NPD_PROJECT_REGULATORY_COMMENTS;
    
    @Indexed(name = "NPD_PROJECT_HOW_MANY_PIECES", type = "string")
    private Integer NPD_PROJECT_HOW_MANY_PIECES;
    
    @Indexed(name = "NPD_PROJECT_SECONDARY_PACK_REQUIRED_facet", type = "string")
    private String NPD_PROJECT_SECONDARY_PACK_REQUIRED_facet;
    
    @Indexed(name = "NPD_PROJECT_SECONDARY_PACK_REQUIRED", type = "string")
    private String NPD_PROJECT_SECONDARY_PACK_REQUIRED;
    
    @Indexed(name = "NPD_PROJECT_NO_OF_PIECES_REQUIRED", type = "string")
    private Integer NPD_PROJECT_NO_OF_PIECES_REQUIRED;
    
    @Indexed(name = "NPD_PROJECT_SITE_AUDIT_REQUIRED_facet", type = "string")
    private String NPD_PROJECT_SITE_AUDIT_REQUIRED_facet;
    
    @Indexed(name = "NPD_PROJECT_SITE_AUDIT_REQUIRED", type = "string")
    private String NPD_PROJECT_SITE_AUDIT_REQUIRED;
    
    @Indexed(name = "NPD_PROJECT_WHO_WILL_AUDIT", type = "string")
    private String NPD_PROJECT_WHO_WILL_AUDIT;
    
    @Indexed(name = "NPD_PROJECT_AUDIT_DATE_facet", type = "string")
    private String NPD_PROJECT_AUDIT_DATE_facet;
    
    @Indexed(name = "NPD_PROJECT_SITE_APPROVED_facet", type = "string")
    private String NPD_PROJECT_SITE_APPROVED_facet;
    
    @Indexed(name = "NPD_PROJECT_HBC_RPE_MSEB_CREATED_facet", type = "string")
    private String NPD_PROJECT_HBC_RPE_MSEB_CREATED_facet;
    
    @Indexed(name = "NPD_PROJECT_PLANT_SPECIFIC_REAPPLICATION_ONLY_facet", type = "string")
    private String NPD_PROJECT_PLANT_SPECIFIC_REAPPLICATION_ONLY_facet;
    
    @Indexed(name = "NPD_PROJECT_PLANT_SPECIFIC_RESPONSIBLE_FOR_COMPLETION_facet", type = "string")
    private String NPD_PROJECT_PLANT_SPECIFIC_RESPONSIBLE_FOR_COMPLETION_facet;
    
    @Indexed(name = "NPD_PROJECT_PLANT_SPECIFIC_COMMENTS_facet", type = "string")
    private String NPD_PROJECT_PLANT_SPECIFIC_COMMENTS_facet;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_SUPERVISION_QUALITY_facet", type = "string")
    private String NPD_PROJECT_TRIAL_SUPERVISION_QUALITY_facet;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_SUPERVISION_NPD_facet", type = "string")
    private String NPD_PROJECT_TRIAL_SUPERVISION_NPD_facet;
    
    @Indexed(name = "NPD_PROJECT_ONSITE_REMOTE_NOT_REQUIRED_facet", type = "string")
    private String NPD_PROJECT_ONSITE_REMOTE_NOT_REQUIRED_facet;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_PROTOCOL_WRITTEN_BY_facet", type = "string")
    private String NPD_PROJECT_TRIAL_PROTOCOL_WRITTEN_BY_facet;
    
    @Indexed(name = "NPD_PROJECT_PROTOCOL_ISSUED_facet", type = "string")
    private String NPD_PROJECT_PROTOCOL_ISSUED_facet;
    
    @Indexed(name = "NPD_PROJECT_TASTING_REQUIREMENT_facet", type = "string")
    private String NPD_PROJECT_TASTING_REQUIREMENT_facet;
    
    @Indexed(name = "NPD_PROJECT_AUDIT_DATE", type = "string")
    private String NPD_PROJECT_AUDIT_DATE;
    
    @Indexed(name = "NPD_PROJECT_SITE_APPROVED", type = "string")
    private String NPD_PROJECT_SITE_APPROVED;
    
    @Indexed(name = "NPD_PROJECT_HBC_RPE_MSEB_CREATED", type = "string")
    private String NPD_PROJECT_HBC_RPE_MSEB_CREATED;
    
    @Indexed(name = "NPD_PROJECT_PLANT_SPECIFIC_REAPPLICATION_ONLY", type = "string")
    private String NPD_PROJECT_PLANT_SPECIFIC_REAPPLICATION_ONLY;
    
    @Indexed(name = "NPD_PROJECT_PLANT_SPECIFIC_RESPONSIBLE_FOR_COMPLETION", type = "string")
    private String NPD_PROJECT_PLANT_SPECIFIC_RESPONSIBLE_FOR_COMPLETION;
    
    @Indexed(name = "NPD_PROJECT_PLANT_SPECIFIC_COMMENTS", type = "string")
    private String NPD_PROJECT_PLANT_SPECIFIC_COMMENTS;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_SUPERVISION_QUALITY", type = "string")
    private String NPD_PROJECT_TRIAL_SUPERVISION_QUALITY;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_SUPERVISION_NPD", type = "string")
    private String NPD_PROJECT_TRIAL_SUPERVISION_NPD;
    
    @Indexed(name = "NPD_PROJECT_ONSITE_REMOTE_NOT_REQUIRED", type = "string")
    private String NPD_PROJECT_ONSITE_REMOTE_NOT_REQUIRED;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_PROTOCOL_WRITTEN_BY", type = "string")
    private String NPD_PROJECT_TRIAL_PROTOCOL_WRITTEN_BY;
    
    @Indexed(name = "NPD_PROJECT_PROTOCOL_ISSUED", type = "string")
    private String NPD_PROJECT_PROTOCOL_ISSUED;
    
    @Indexed(name = "NPD_PROJECT_TASTING_REQUIREMENT", type = "string")
    private String NPD_PROJECT_TASTING_REQUIREMENT;
    
    @Indexed(name = "NPD_PROJECT_QA_QC_RELEASE_DATE", type = "string")
    private String NPD_PROJECT_QA_QC_RELEASE_DATE_facet;
    
    @Indexed(name = "NPD_PROJECT_NAME_OF_CAN_COMPANY_VALUES_facet", type = "string")
    private String NPD_PROJECT_NAME_OF_CAN_COMPANY_VALUES_facet;
    
    @Indexed(name = "NPD_PROJECT_NEW_PROJECT_TYPE_facet", type = "string")
    private String NPD_PROJECT_NEW_PROJECT_TYPE_facet;
    
    @Indexed(name = "NPD_PROJECT_NEW_FG_facet", type = "string")
    private String NPD_PROJECT_NEW_FG_facet;
    
    @Indexed(name = "NPD_PROJECT_NEW_RD_FORMULA_facet", type = "string")
    private String NPD_PROJECT_NEW_RD_FORMULA_facet;
    
    @Indexed(name = "NPD_PROJECT_NEW_HBC_FORMULA_facet", type = "string")
    private String NPD_PROJECT_NEW_HBC_FORMULA_facet;
    
    @Indexed(name = "NPD_PROJECT_NEW_PRIMARY_PACK_facet", type = "string")
    private String NPD_PROJECT_NEW_PRIMARY_PACK_facet;
    
    @Indexed(name = "NPD_PROJECT_NEW_SECONDARY_PACK_facet", type = "string")
    private String NPD_PROJECT_NEW_SECONDARY_PACK_facet;
    
    @Indexed(name = "NPD_PROJECT_NEW_CLASSIFICATION_RATIONAL_facet", type = "string")
    private String NPD_PROJECT_NEW_CLASSIFICATION_RATIONAL_facet;
    
    @Indexed(name = "NPD_PROJECT_QA_QC_RELEASE_DATE", type = "string")
    private String NPD_PROJECT_QA_QC_RELEASE_DATE;
    
    @Indexed(name = "NPD_PROJECT_NAME_OF_CAN_COMPANY_VALUES", type = "string")
    private String NPD_PROJECT_NAME_OF_CAN_COMPANY_VALUES;
    
    @Indexed(name = "NPD_PROJECT_NEW_PROJECT_TYPE", type = "string")
    private String NPD_PROJECT_NEW_PROJECT_TYPE;
    
    @Indexed(name = "NPD_PROJECT_NEW_FG", type = "string")
    private String NPD_PROJECT_NEW_FG;
    
    @Indexed(name = "NPD_PROJECT_NEW_RD_FORMULA", type = "string")
    private String NPD_PROJECT_NEW_RD_FORMULA;
    
    @Indexed(name = "NPD_PROJECT_NEW_HBC_FORMULA", type = "string")
    private String NPD_PROJECT_NEW_HBC_FORMULA;
    
    @Indexed(name = "NPD_PROJECT_NEW_PRIMARY_PACK", type = "string")
    private String NPD_PROJECT_NEW_PRIMARY_PACK;
    
    @Indexed(name = "NPD_PROJECT_NEW_SECONDARY_PACK", type = "string")
    private String NPD_PROJECT_NEW_SECONDARY_PACK;
    
    @Indexed(name = "NPD_PROJECT_NEW_CLASSIFICATION_RATIONAL", type = "string")
    private String NPD_PROJECT_NEW_CLASSIFICATION_RATIONAL;
    
    @Indexed(name = "NPD_PROJECT_TECHNICAL_OR_QUALITY_SUPERVISION_REQ_facet", type = "string")
    private String NPD_PROJECT_TECHNICAL_OR_QUALITY_SUPERVISION_REQ_facet;
    
    @Indexed(name = "NPD_PROJECT_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_TECHNICAL_OR_QUALITY_SUPERVISION_REQ", type = "string")
    private String NPD_PROJECT_TECHNICAL_OR_QUALITY_SUPERVISION_REQ;
    
    @Indexed(name = "NPD_PROJECT_ITEM_ID", type = "string")
    private String NPD_PROJECT_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_IS_TASK4_COMPLETED", type = "string")
    private String NPD_PROJECT_IS_TASK4_COMPLETED;
    
    @Indexed(name = "NPD_PROJECT_IS_TASK5_COMPLETED", type = "string")
    private String NPD_PROJECT_IS_TASK5_COMPLETED;
    
    @Indexed(name = "NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_ID", type = "string")
    private Integer NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_ID;
    
    @Indexed(name = "NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_NAME_facet", type = "string")
    private String NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_NAME_facet;
    
    @Indexed(name = "NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_NAME", type = "string")
    private String NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_NAME;
    
    @Indexed(name = "NPD_PROJECT_OPS_PLANNER_ID", type = "string")
    private Integer NPD_PROJECT_OPS_PLANNER_ID;
    
    @Indexed(name = "NPD_PROJECT_OPS_PLANNER_NAME_facet", type = "string")
    private String NPD_PROJECT_OPS_PLANNER_NAME_facet;
    
    @Indexed(name = "NPD_PROJECT_OPS_PLANNER_NAME", type = "string")
    private String NPD_PROJECT_OPS_PLANNER_NAME;
    
    @Indexed(name = "NPD_PROJECT_GFG_ID", type = "string")
    private Integer NPD_PROJECT_GFG_ID;
    
    @Indexed(name = "NPD_PROJECT_GFG_NAME_facet", type = "string")
    private String NPD_PROJECT_GFG_NAME_facet;
    
    @Indexed(name = "NPD_PROJECT_GFG_NAME", type = "string")
    private String NPD_PROJECT_GFG_NAME;
    
    @Indexed(name = "NPD_PROJECT_PR_SPECIALIST_ID", type = "string")
    private Integer NPD_PROJECT_PR_SPECIALIST_ID;
    
    @Indexed(name = "NPD_PROJECT_PR_SPECIALIST_NAME_facet", type = "string")
    private String NPD_PROJECT_PR_SPECIALIST_NAME_facet;
    
    @Indexed(name = "NPD_PROJECT_PR_SPECIALIST_NAME", type = "string")
    private String NPD_PROJECT_PR_SPECIALIST_NAME;
    
    @Indexed(name = "NPD_PROJECT_REGULATORY_LEAD_ID", type = "string")
    private Integer NPD_PROJECT_REGULATORY_LEAD_ID;
    
    @Indexed(name = "NPD_PROJECT_REGULATORY_LEAD_NAME_facet", type = "string")
    private String NPD_PROJECT_REGULATORY_LEAD_NAME_facet;
    
    @Indexed(name = "NPD_PROJECT_REGULATORY_LEAD_NAME", type = "string")
    private String NPD_PROJECT_REGULATORY_LEAD_NAME;
    
    @Indexed(name = "NPD_PROJECT_COMMERCIAL_LEAD_ID", type = "string")
    private Integer NPD_PROJECT_COMMERCIAL_LEAD_ID;
    
    @Indexed(name = "NPD_PROJECT_COMMERCIAL_LEAD_NAME_facet", type = "string")
    private String NPD_PROJECT_COMMERCIAL_LEAD_NAME_facet;
    
    @Indexed(name = "NPD_PROJECT_COMMERCIAL_LEAD_NAME", type = "string")
    private String NPD_PROJECT_COMMERCIAL_LEAD_NAME;
    
    @Indexed(name = "NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_ITEM_ID", type = "string")
    private String NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_OPS_PLANNER_ITEM_ID", type = "string")
    private String NPD_PROJECT_OPS_PLANNER_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_GFG_ITEM_ID", type = "string")
    private String NPD_PROJECT_GFG_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_PR_SPECIALIST_ITEM_ID", type = "string")
    private String NPD_PROJECT_PR_SPECIALIST_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_REGULATORY_LEAD_ITEM_ID", type = "string")
    private String NPD_PROJECT_REGULATORY_LEAD_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_COMMERCIAL_LEAD_ITEM_ID", type = "string")
    private String NPD_PROJECT_COMMERCIAL_LEAD_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_WHO_WILL_AUDIT_ITEM_ID", type = "string")
    private String NPD_PROJECT_WHO_WILL_AUDIT_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_PRINT_SUPPLIER_ITEM_ID", type = "string")
    private String NPD_PROJECT_PRINT_SUPPLIER_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_PRINT_SUPPLIER", type = "string")
    private String NPD_PROJECT_PRINT_SUPPLIER;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_SUPERVISION_NPD_ITEM_ID", type = "string")
    private String NPD_PROJECT_TRIAL_SUPERVISION_NPD_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_SUPERVISION_QUALITY_ITEM_ID", type = "string")
    private String NPD_PROJECT_TRIAL_SUPERVISION_QUALITY_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_TASTING_REQUIREMENT_ITEM_ID", type = "string")
    private String NPD_PROJECT_TASTING_REQUIREMENT_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_PROTOCOL_WRITTEN_BY_ITEM_ID", type = "string")
    private String NPD_PROJECT_TRIAL_PROTOCOL_WRITTEN_BY_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_LEAD_MARKET_ID", type = "string")
    private String NPD_PROJECT_LEAD_MARKET_ID;
    
    @Indexed(name = "NPD_PROJECT_LEAD_MARKET", type = "string")
    private String NPD_PROJECT_LEAD_MARKET;
    
    @Indexed(name = "NPD_PROJECT_GOVERNANCE_APPROACH", type = "string")
    private String NPD_PROJECT_GOVERNANCE_APPROACH;
    
    @Indexed(name = "NPD_PROJECT_DESCRIPTION", type = "string")
    private String NPD_PROJECT_DESCRIPTION;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_PROJECT_TYPE", type = "string")
    private String NPD_PROJECT_PM_REPORTING_PROJECT_TYPE;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_PROJECT_TYPE_ITEM_ID", type = "string")
    private String NPD_PROJECT_PM_REPORTING_PROJECT_TYPE_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_SUB_PROJECT_TYPE", type = "string")
    private String NPD_PROJECT_PM_REPORTING_SUB_PROJECT_TYPE;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_SUB_PROJECT_TYPE_ITEM_ID", type = "string")
    private String NPD_PROJECT_PM_REPORTING_SUB_PROJECT_TYPE_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_PROJECT_DETAIL", type = "string")
    private String NPD_PROJECT_PM_REPORTING_PROJECT_DETAIL;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_PROJECT_DETAIL_ITEM_ID", type = "string")
    private String NPD_PROJECT_PM_REPORTING_PROJECT_DETAIL_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_DELIVERY_QUARTER", type = "string")
    private String NPD_PROJECT_PM_REPORTING_DELIVERY_QUARTER;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_DELIVERY_QUARTER_ITEM_ID", type = "string")
    private String NPD_PROJECT_PM_REPORTING_DELIVERY_QUARTER_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_ITEM_ID", type = "string")
    private String NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_OPS_PLANNER_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_OPS_PLANNER_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_GFG_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_GFG_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_PR_SPECIALIST_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_PR_SPECIALIST_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_REGULATORY_LEAD_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_REGULATORY_LEAD_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_COMMERCIAL_LEAD_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_COMMERCIAL_LEAD_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_WHO_WILL_AUDIT_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_WHO_WILL_AUDIT_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_PRINT_SUPPLIER_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_PRINT_SUPPLIER_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_PRINT_SUPPLIER_facet", type = "string")
    private String NPD_PROJECT_PRINT_SUPPLIER_facet;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_SUPERVISION_NPD_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_TRIAL_SUPERVISION_NPD_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_SUPERVISION_QUALITY_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_TRIAL_SUPERVISION_QUALITY_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_TASTING_REQUIREMENT_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_TASTING_REQUIREMENT_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_PROTOCOL_WRITTEN_BY_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_TRIAL_PROTOCOL_WRITTEN_BY_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_LEAD_MARKET_ID_facet", type = "string")
    private String NPD_PROJECT_LEAD_MARKET_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_LEAD_MARKET_facet", type = "string")
    private String NPD_PROJECT_LEAD_MARKET_facet;
    
    @Indexed(name = "NPD_PROJECT_GOVERNANCE_APPROACH_facet", type = "string")
    private String NPD_PROJECT_GOVERNANCE_APPROACH_facet;
    
    @Indexed(name = "NPD_PROJECT_DESCRIPTION_facet", type = "string")
    private String NPD_PROJECT_DESCRIPTION_facet;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_PROJECT_TYPE_facet", type = "string")
    private String NPD_PROJECT_PM_REPORTING_PROJECT_TYPE_facet;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_PROJECT_TYPE_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_PM_REPORTING_PROJECT_TYPE_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_SUB_PROJECT_TYPE_facet", type = "string")
    private String NPD_PROJECT_PM_REPORTING_SUB_PROJECT_TYPE_facet;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_SUB_PROJECT_TYPE_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_PM_REPORTING_SUB_PROJECT_TYPE_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_PROJECT_DETAIL_facet", type = "string")
    private String NPD_PROJECT_PM_REPORTING_PROJECT_DETAIL_facet;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_PROJECT_DETAIL_ITEM_ID", type = "string")
    private String NPD_PROJECT_PM_REPORTING_PROJECT_DETAIL_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_DELIVERY_QUARTER", type = "string")
    private String NPD_PROJECT_PM_REPORTING_DELIVERY_QUARTER_facet;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_DELIVERY_QUARTER_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_PM_REPORTING_DELIVERY_QUARTER_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_NO_OF_LINKED_MARKETS", type = "string")
    private String NPD_PROJECT_NO_OF_LINKED_MARKETS;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_COMMENTS", type = "string")
    private String NPD_PROJECT_PM_REPORTING_COMMENTS;
    
    @Indexed(name = "NPD_PROJECT_NAME_OF_CAN_COMPANY_IDS", type = "string")
    private String NPD_PROJECT_NAME_OF_CAN_COMPANY_IDS;
    
    @Indexed(name = "NPD_PROJECT_OPS_PM_ITEM_ID", type = "string")
    private String NPD_PROJECT_OPS_PM_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_OPS_PM_NAME", type = "string")
    private String NPD_PROJECT_OPS_PM_NAME;
    
    @Indexed(name = "NPD_PROJECT_CORP_PM_ITEM_ID", type = "string")
    private String NPD_PROJECT_CORP_PM_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_CORP_PM_NAME", type = "string")
    private String NPD_PROJECT_CORP_PM_NAME;
    
    @Indexed(name = "NPD_PROJECT_E2E_PM_ITEM_ID", type = "string")
    private String NPD_PROJECT_E2E_PM_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_E2E_PM_NAME", type = "string")
    private String NPD_PROJECT_E2E_PM_NAME;
    
    @Indexed(name = "NPD_PROJECT_NEW_REGISTRATION_DOSSIER", type = "string")
    private String NPD_PROJECT_NEW_REGISTRATION_DOSSIER;
    
    @Indexed(name = "NPD_PROJECT_NEW_PRE_PRODUCTION", type = "string")
    private String NPD_PROJECT_NEW_PRE_PRODUCTION;
    
    @Indexed(name = "NPD_PROJECT_NEW_POST_PRODUCTION", type = "string")
    private String NPD_PROJECT_NEW_POST_PRODUCTION;
    
    @Indexed(name = "NPD_PROJECT_NEW_PROJECT_TYPE_ID", type = "string")
    private String NPD_PROJECT_NEW_PROJECT_TYPE_ID;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_OR_COMMENTS_OR_ACTIONS_REQUIRED", type = "string")
    private String NPD_PROJECT_TRIAL_OR_COMMENTS_OR_ACTIONS_REQUIRED;
    
    @Indexed(name = "NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ITEM_ID", type = "string")
    private String NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_NEW_PROJECT_TYPE_ITEM_ID", type = "string")
    private String NPD_PROJECT_NEW_PROJECT_TYPE_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_FINAL_MANUFACTURING_SITE_ITEM_ID", type = "string")
    private String NPD_PROJECT_FINAL_MANUFACTURING_SITE_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_TECH_QUAL_WHO_WILL_COMPLETE_ITEM_ID", type = "string")
    private String NPD_PROJECT_TECH_QUAL_WHO_WILL_COMPLETE_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_TECH_QUAL_WHO_WILL_COMPLETE", type = "string")
    private String NPD_PROJECT_TECH_QUAL_WHO_WILL_COMPLETE;
    
    @Indexed(name = "NPD_PROJECT_TECH_QUAL_COMPLETED", type = "string")
    private String NPD_PROJECT_TECH_QUAL_COMPLETED;
    
    @Indexed(name = "NPD_PROJECT_TECH_QUAL_WEEK_DUE", type = "string")
    private String NPD_PROJECT_TECH_QUAL_WEEK_DUE;
    
    @Indexed(name = "PR_EARLY_PROJECT_MANUFACTURING_LOCATION", type = "string")
    private String PR_EARLY_PROJECT_MANUFACTURING_LOCATION;
    
    @Indexed(name = "PR_EARLY_PROJECT_MANUFACTURING_LOCATION_SITE", type = "string")
    private String PR_EARLY_PROJECT_MANUFACTURING_LOCATION_SITE;
    
    @Indexed(name = "PR_EARLY_PROJECT_TYPE", type = "string")
    private String PR_EARLY_PROJECT_TYPE;
    
    @Indexed(name = "IS_ON_HOLD", type = "string")
    private String IS_ON_HOLD;
    
    @Indexed(name = "IS_CUSTOM_WORKFLOW", type = "string")
    private String IS_CUSTOM_WORKFLOW;
    
    @Indexed(name = "NPD_PROJECT_IS_CP1_COMPLETED", type = "string")
    private String NPD_PROJECT_IS_CP1_COMPLETED;
    
    @Indexed(name = "NPD_PROJECT_NO_OF_LINKED_MARKETS_facet", type = "string")
    private String NPD_PROJECT_NO_OF_LINKED_MARKETS_facet;
    
    @Indexed(name = "NPD_PROJECT_PM_REPORTING_COMMENTS_facet", type = "string")
    private String NPD_PROJECT_PM_REPORTING_COMMENTS_facet;
    
    @Indexed(name = "NPD_PROJECT_NAME_OF_CAN_COMPANY_IDS_facet", type = "string")
    private String NPD_PROJECT_NAME_OF_CAN_COMPANY_IDS_facet;
    
    @Indexed(name = "NPD_PROJECT_OPS_PM_ITEM_ID", type = "string")
    private String NPD_PROJECT_OPS_PM_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_OPS_PM_NAME_facet", type = "string")
    private String NPD_PROJECT_OPS_PM_NAME_facet;
    
    @Indexed(name = "NPD_PROJECT_CORP_PM_ITEM_ID", type = "string")
    private String NPD_PROJECT_CORP_PM_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_CORP_PM_NAME_facet", type = "string")
    private String NPD_PROJECT_CORP_PM_NAME_facet;
    
    @Indexed(name = "NPD_PROJECT_E2E_PM_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_E2E_PM_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_E2E_PM_NAME_facet", type = "string")
    private String NPD_PROJECT_E2E_PM_NAME_facet;
    
    @Indexed(name = "NPD_PROJECT_NEW_REGISTRATION_DOSSIER_facet", type = "string")
    private String NPD_PROJECT_NEW_REGISTRATION_DOSSIER_facet;
    
    @Indexed(name = "NPD_PROJECT_NEW_PRE_PRODUCTION_facet", type = "string")
    private String NPD_PROJECT_NEW_PRE_PRODUCTION_facet;
    
    @Indexed(name = "NPD_PROJECT_NEW_POST_PRODUCTION_facet", type = "string")
    private String NPD_PROJECT_NEW_POST_PRODUCTION_facet;
    
    @Indexed(name = "NPD_PROJECT_NEW_PROJECT_TYPE_ID_facet", type = "string")
    private String NPD_PROJECT_NEW_PROJECT_TYPE_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_TRIAL_OR_COMMENTS_OR_ACTIONS_REQUIRED_facet", type = "string")
    private String NPD_PROJECT_TRIAL_OR_COMMENTS_OR_ACTIONS_REQUIRED_facet;
    
    @Indexed(name = "NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_NEW_PROJECT_TYPE_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_NEW_PROJECT_TYPE_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_FINAL_MANUFACTURING_SITE_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_FINAL_MANUFACTURING_SITE_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_TECH_QUAL_WHO_WILL_COMPLETE_ITEM_ID_facet", type = "string")
    private String NPD_PROJECT_TECH_QUAL_WHO_WILL_COMPLETE_ITEM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_TECH_QUAL_WHO_WILL_COMPLETE_facet", type = "string")
    private String NPD_PROJECT_TECH_QUAL_WHO_WILL_COMPLETE_facet;
    
    @Indexed(name = "NPD_PROJECT_TECH_QUAL_COMPLETED_facet", type = "string")
    private String NPD_PROJECT_TECH_QUAL_COMPLETED_facet;
    
    @Indexed(name = "NPD_PROJECT_TECH_QUAL_WEEK_DUE_facet", type = "string")
    private String NPD_PROJECT_TECH_QUAL_WEEK_DUE_facet;
    
    @Indexed(name = "PR_EARLY_PROJECT_MANUFACTURING_LOCATION_facet", type = "string")
    private String PR_EARLY_PROJECT_MANUFACTURING_LOCATION_facet;
    
    @Indexed(name = "PR_EARLY_PROJECT_MANUFACTURING_LOCATION_SITE_facet", type = "string")
    private String PR_EARLY_PROJECT_MANUFACTURING_LOCATION_SITE_facet;
    
    @Indexed(name = "PR_EARLY_PROJECT_TYPE_facet", type = "string")
    private String PR_EARLY_PROJECT_TYPE_facet;
    
    @Indexed(name = "IS_ON_HOLD_facet", type = "string")
    private String IS_ON_HOLD_facet;
    
    @Indexed(name = "NPD_PROJECT_E2E_PM_ID", type = "string")
    private String NPD_PROJECT_E2E_PM_ID;
    
    @Indexed(name = "NPD_PROJECT_CORP_PM_ID", type = "string")
    private String NPD_PROJECT_CORP_PM_ID;
    
    @Indexed(name = "NPD_PROJECT_OPS_PM_ID", type = "string")
    private String NPD_PROJECT_OPS_PM_ID;
    
    @Indexed(name = "NPD_PROJECT_E2E_PM_ID_facet", type = "string")
    private String NPD_PROJECT_E2E_PM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_CORP_PM_ID_facet", type = "string")
    private String NPD_PROJECT_CORP_PM_ID_facet;
    
    @Indexed(name = "NPD_PROJECT_OPS_PM_ID_facet", type = "string")
    private String NPD_PROJECT_OPS_PM_ID_facet;
    
    @Indexed(name = "PROJECT_CATEGORY_METADATA_ID", type = "string")
    private Integer PROJECT_CATEGORY_METADATA_ID;
    
    @Indexed(name = "PROJECT_CATEGORY_METADATA", type = "string")
    private String PROJECT_CATEGORY_METADATA;
    
    @Indexed(name = "PROJECT_CATEGORY_METADATA_facet", type = "string")
    private String PROJECT_CATEGORY_METADATA_facet;
    
    @Indexed(name = "NPD_PROJECT_EARLY_MANUFACTURING_LOCATION_ITEM_ID", type = "string")
    private String NPD_PROJECT_EARLY_MANUFACTURING_LOCATION_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_EARLY_MANUFACTURING_SITE_ITEM_ID", type = "string")
    private String NPD_PROJECT_EARLY_MANUFACTURING_SITE_ITEM_ID;
    
    @Indexed(name = "NPD_PROJECT_EARLY_PROJECT_TYPE_ITEM_ID", type = "string")
    private String NPD_PROJECT_EARLY_PROJECT_TYPE_ITEM_ID;
    
    
    
    
    public MPMProjectSchema(String itemId) {
		super();
		this.item_id = itemId;
	}

}
