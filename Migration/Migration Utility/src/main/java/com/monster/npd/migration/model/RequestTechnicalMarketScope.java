package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@Table(name = "shrdnpdsubmissionrequesttechnical_market_scope")
@Entity
@IdClass(RequestTechnicalMarketScopeId.class) 
public class RequestTechnicalMarketScope {
	
	
	@Id
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RequestidA9403B7340C3F66B")
    private Request requestTech;

	 @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "technical_market_scope_id")
    private TechnicalMarketScope technicalMarketScope;

    
    @Column(name = "s_organizationid")
    private int organizationid;

}