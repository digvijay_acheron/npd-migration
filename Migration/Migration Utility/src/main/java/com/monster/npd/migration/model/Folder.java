package com.monster.npd.migration.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Folder {
	

	String asset_id;
	String name;
	Metadata metadata;
	
	@JsonProperty("container_type_id")
	String container_type_id;
	
	@JsonProperty("metadata_model_id")
	String metadata_model_id;
	
	@JacksonXmlElementWrapper(useWrapping = false)
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	List<SecurityPolicyList> security_policy_list = new ArrayList<>();
	
}