package com.monster.npd.migration.service;

import java.util.List;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.monster.npd.migration.model.ProjectDetails;
import com.monster.npd.migration.model.RequestData;
import com.monster.npd.migration.model.TaskDetails;

/**
 * 
 * @author JeevaR
 *
 */
@Service
public class MigrationUtilityService {

	@Autowired
	Environment env;

	@Autowired
	private UtilService utilService;
	List<RequestData> requestList;

	private static final Logger logger = LogManager.getLogger(MigrationUtilityService.class);

	/**
	 * method to call soap Api in Appworks
	 * 
	 * @param samlart
	 *
	 * @param start   - index of the next value to be passed to the soap Api request
	 * @return - return soap message
	 */

	public SOAPMessage soapCall(String samlart, int start, List<RequestData> requestList) {
		this.requestList = requestList;
		SOAPConnectionFactory soapConnectionFactory;
		String endPointURL = utilService.formAppworksGatewayUrl(samlart);
		if (utilService.isNullOrEmpty(endPointURL)) {
			return null;
		}
		try {
			soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(start), endPointURL);

			// Print the SOAP Response
			logger.info("Response SOAP Message:");
			System.out.println(utilService.convertSOAPMessageToString(soapResponse));

			return soapResponse;
		} catch (UnsupportedOperationException e) {
			e.printStackTrace();
		} catch (SOAPException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * method to create soap envelope using soap message
	 * 
	 * @param soapMessage
	 * @param start       - index of the next value to be passed to the soap Api
	 *                    request
	 * @throws SOAPException
	 */
	private void createSoapEnvelope(SOAPMessage soapMessage, int start) throws SOAPException {

		SOAPPart soapPart = soapMessage.getSOAPPart();

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();

		// SOAP Body
		// List<RequestData> requestList = EventUserModal.getRequestList();
		SOAPBody soapBody = envelope.getBody();
		SOAPElement handleMigration = soapBody.addChildElement("HandleMigration", null,
				"http://schemas.monster.npd.com/migration/bpm/1.0");
		SOAPElement requests = handleMigration.addChildElement("Requests");
		for (int index = start; index < start + 10 && index < requestList.size(); index++) {
			RequestData request = requestList.get(index);
			SOAPElement requestData = requests.addChildElement("RequestData");
			SOAPElement projectName = requestData.addChildElement("ProjectName");
			projectName.addTextNode(request.getProjectName());
			SOAPElement currentCheckPoint = requestData.addChildElement("CurrentCheckPoint");
			currentCheckPoint.addTextNode(" ");
			SOAPElement requestRationale = requestData.addChildElement("RequestRationale");
			requestRationale.addTextNode(" ");
			SOAPElement requestStatus = requestData.addChildElement("RequestStatus");
			requestStatus.addTextNode(request.getRequestStatus());
			SOAPElement isSVPAligned = requestData.addChildElement("IsSVPAligned");
			isSVPAligned.addTextNode(" ");
			SOAPElement requestID = requestData.addChildElement("RequestID");
			requestID.addTextNode(request.getRequestID());
			SOAPElement secondaryPackagingType = requestData.addChildElement("SecondaryPackagingType");
			secondaryPackagingType.addTextNode(" ");
			SOAPElement isSecondaryPackaging = requestData.addChildElement("IsSecondaryPackaging");
			isSecondaryPackaging.addTextNode(request.getIsSecondaryPackaging());
			SOAPElement newFGSAP = requestData.addChildElement("NewFGSAP");
			newFGSAP.addTextNode(request.getNewFGSAP());
			SOAPElement newHBCFormula = requestData.addChildElement("NewHBCFormula");
			newHBCFormula.addTextNode(request.getNewHBCFormula());
			SOAPElement newPrimaryPackaging = requestData.addChildElement("NewPrimaryPackaging");
			newPrimaryPackaging.addTextNode(request.getNewPrimaryPackaging());
			SOAPElement newRDFormula = requestData.addChildElement("NewRDFormula");
			newRDFormula.addTextNode(request.getNewRDFormula());
			SOAPElement postLaunchAnalysis = requestData.addChildElement("PostLaunchAnalysis");
			postLaunchAnalysis.addTextNode(" ");
			SOAPElement postProductionRegistration = requestData.addChildElement("PostProductionRegistration");
			postProductionRegistration.addTextNode(request.getPostProductionRegistration());
			SOAPElement preProductionRegistartion = requestData.addChildElement("PreProductionRegistartion");
			preProductionRegistartion.addTextNode(request.getPreProductionRegistartion());
			SOAPElement registrationDossier = requestData.addChildElement("RegistrationDossier");
			registrationDossier.addTextNode(request.getRegistrationDossier());
			SOAPElement technicalDescription = requestData.addChildElement("TechnicalDescription");
			technicalDescription.addTextNode(request.getTechnicalDescription());
			SOAPElement projectAbout = requestData.addChildElement("ProjectAbout");
			projectAbout.addTextNode(" ");
			SOAPElement whyProject = requestData.addChildElement("WhyProject");
			whyProject.addTextNode(" ");
			SOAPElement businessCaseComments = requestData.addChildElement("BusinessCaseComments");
			businessCaseComments.addTextNode(" ");
			SOAPElement numberOfLinkedMarkets = requestData.addChildElement("NumberOfLinkedMarkets");
			numberOfLinkedMarkets.addTextNode(request.getNumberOfLinkedMarkets());
			SOAPElement progMangerComments = requestData.addChildElement("ProgMangerComments");
			progMangerComments.addTextNode(" ");
			SOAPElement r_PO_PROJECT_TYPE = requestData.addChildElement("R_PO_PROJECT_TYPE");
			r_PO_PROJECT_TYPE.addTextNode(request.getR_PO_PROJECT_TYPE());
			SOAPElement r_PO_PACKAGING_TYPE = requestData.addChildElement("R_PO_PACKAGING_TYPE");
			r_PO_PACKAGING_TYPE.addTextNode(request.getR_PO_PACKAGING_TYPE());
			SOAPElement r_PO_REQUEST_TYPE = requestData.addChildElement("R_PO_REQUEST_TYPE");
			String requestType = request.getR_PO_REQUEST_TYPE();
			if (requestType.equals("Lift & Shift SKU") || requestType.equals("On-Pack Promo")
					|| requestType.equals("Market Extension") || requestType.equals("Innovation / Adaption")
					|| requestType.equals("Manufacturing Extension")
					|| requestType.equals("Innovation / Adaption + Reg.")
					|| requestType.equals("Market Extension + Reg.")
					|| requestType.equals("Manufacturing Extension + Reg.")
					|| requestType.equals("On-Pack Promo + Reg.")) {
				r_PO_REQUEST_TYPE.addTextNode("Commercial");
			} else if (requestType.equals("Technical Change") || requestType.equals("Technical Change + Reg.")) {
				r_PO_REQUEST_TYPE.addTextNode("Technical");
			} else {
				r_PO_REQUEST_TYPE.addTextNode("Operation");
			}
			SOAPElement r_PO_VARIANT_SKU = requestData.addChildElement("R_PO_VARIANT_SKU");
			r_PO_VARIANT_SKU.addTextNode(request.getR_PO_VARIANT_SKU());
			SOAPElement r_PO_CORP_PM = requestData.addChildElement("R_PO_CORP_PM");
			String corpPm = utilService.getPmOwnerIdentityId(request.getR_PO_CORP_PM());
			System.out.print(corpPm);
			r_PO_CORP_PM.addTextNode(corpPm);
			SOAPElement r_PO_DRAFT_MANUFACTURING_LOCATION = requestData
					.addChildElement("R_PO_DRAFT_MANUFACTURING_LOCATION");
			r_PO_DRAFT_MANUFACTURING_LOCATION.addTextNode(request.getR_PO_DRAFT_MANUFACTURING_LOCATION());
			SOAPElement r_PO_E2E_PM = requestData.addChildElement("R_PO_E2E_PM");
			String e2ePm = utilService.getPmOwnerIdentityId(request.getR_PO_E2E_PM());
			r_PO_E2E_PM.addTextNode(e2ePm);
			SOAPElement r_PO_EARLY_MANUFACTURING_SITE = requestData.addChildElement("R_PO_EARLY_MANUFACTURING_SITE");
			r_PO_EARLY_MANUFACTURING_SITE.addTextNode(request.getR_PO_EARLY_MANUFACTURING_SITE());
			SOAPElement r_PO_OPS_PM = requestData.addChildElement("R_PO_OPS_PM");
			String opsPm = utilService.getPmOwnerIdentityId(request.getR_PO_OPS_PM());
			r_PO_OPS_PM.addTextNode(opsPm);
			SOAPElement r_PO_PLATFORMS = requestData.addChildElement("R_PO_PLATFORMS");
			r_PO_PLATFORMS.addTextNode(request.getR_PO_PLATFORMS());
			SOAPElement r_PO_BRANDS = requestData.addChildElement("R_PO_BRANDS");
			r_PO_BRANDS.addTextNode(request.getR_PO_BRANDS());
			SOAPElement r_PO_BUSINESS_UNIT = requestData.addChildElement("R_PO_BUSINESS_UNIT");
			r_PO_BUSINESS_UNIT.addTextNode(request.getR_PO_BUSINESS_UNIT());
			SOAPElement r_PO_BOTTLER = requestData.addChildElement("R_PO_BOTTLER");
			r_PO_BOTTLER.addTextNode(request.getR_PO_BOTTLER());
			SOAPElement r_PO_CASE_PACK_SIZE = requestData.addChildElement("R_PO_CASE_PACK_SIZE");
			r_PO_CASE_PACK_SIZE.addTextNode(request.getR_PO_CASE_PACK_SIZE());
			SOAPElement r_PO_CONSUMER_UNIT_SIZE = requestData.addChildElement("R_PO_CONSUMER_UNIT_SIZE");
			r_PO_CONSUMER_UNIT_SIZE.addTextNode(request.getR_PO_CONSUMER_UNIT_SIZE());
			SOAPElement r_PO_SKU_DETAILS = requestData.addChildElement("R_PO_SKU_DETAILS");
			r_PO_SKU_DETAILS.addTextNode(request.getR_PO_SKU_DETAILS());
			SOAPElement r_PO_PROJECT_CLASSIFICATION = requestData.addChildElement("R_PO_PROJECT_CLASSIFICATION");
			r_PO_PROJECT_CLASSIFICATION.addTextNode(request.getR_PO_PROJECT_CLASSIFICATION());
			SOAPElement r_PO_CP_PROJECT_TYPE = requestData.addChildElement("R_PO_CP_PROJECT_TYPE");
			r_PO_CP_PROJECT_TYPE.addTextNode(request.getR_PO_CP_PROJECT_TYPE());
			SOAPElement r_PO_PROJECT_SUB_TYPE = requestData.addChildElement("R_PO_PROJECT_SUB_TYPE");
			r_PO_PROJECT_SUB_TYPE.addTextNode(request.getR_PO_PROJECT_SUB_TYPE());
			SOAPElement r_PO_PROJECT_DETAIL = requestData.addChildElement("R_PO_PROJECT_DETAIL");
			r_PO_PROJECT_DETAIL.addTextNode(request.getR_PO_PROJECT_DETAIL());
			SOAPElement r_PO_PM_DELIVERY_QUARTER = requestData.addChildElement("R_PO_PM_DELIVERY_QUARTER");
			r_PO_PM_DELIVERY_QUARTER.addTextNode(request.getR_PO_PM_DELIVERY_QUARTER());
			SOAPElement cP0DecisionDate = requestData.addChildElement("CP0DecisionDate");
			cP0DecisionDate.addTextNode(request.getCP0DecisionDate());
			SOAPElement leadMarket = requestData.addChildElement("LeadMarket");
			SOAPElement targetDPInWarehouse = leadMarket.addChildElement("TargetDPInWarehouse");
			targetDPInWarehouse.addTextNode(request.getLeadMarketTargetDPInWarehouse());
			SOAPElement id = leadMarket.addChildElement("Id");
			id.addTextNode(request.getLeadMarketId());
			SOAPElement otherLinkedMarket = requestData.addChildElement("OtherLinkedMarkets");

			String[] otherLinkedMarkets = StringUtils.split(request.getOtherLinkedMarketsId(), ",/");

			for (int indexOfMarket = 0; indexOfMarket < otherLinkedMarkets.length; indexOfMarket++) {
				SOAPElement market = otherLinkedMarket.addChildElement("Market");
				SOAPElement marketId = market.addChildElement("Id");
				marketId.addTextNode(otherLinkedMarkets[indexOfMarket]);
				SOAPElement marketTargetDPInWarehouse = market.addChildElement("TargetDPInWarehouse");
				marketTargetDPInWarehouse.addTextNode(request.getOtherLinkedMarketsTargetDPInWarehouse());
			}

			SOAPElement referenceId = requestData.addChildElement("ReferenceId");
			referenceId.addTextNode(request.getReferenceId());
			requestData = createTaskDetails(requestData, index);
			requestData = createProjectDetails(requestData, index);
			SOAPElement nsv = requestData.addChildElement("NSV");
			nsv.addTextNode(request.getNSV());
			SOAPElement volume1Year = requestData.addChildElement("Volume1Year");
			volume1Year.addTextNode(request.getVolume1Year());
			SOAPElement volume3Months = requestData.addChildElement("Volume3Months");
			volume3Months.addTextNode(request.getVolume3Months());
		}
		soapMessage.saveChanges();

	}

	/**
	 * method to create project details soap request xml tags
	 * 
	 * @param requestData tag
	 * @param start
	 * @return project Details in requestData tag
	 * @throws SOAPException
	 */
	private SOAPElement createProjectDetails(SOAPElement requestData, int start) throws SOAPException {
		ProjectDetails projectDetail = EventUserModal.getProjectDetailsList().get(start);
		SOAPElement projectDetails = requestData.addChildElement("ProjectDetails");
		SOAPElement projectEndDate = projectDetails.addChildElement("projectEndDate");
		projectEndDate.addTextNode(projectDetail.getProjectEndDate());
		SOAPElement bottlerCommunicationStatus = projectDetails.addChildElement("bottlerCommunicationStatus");
		bottlerCommunicationStatus.addTextNode(projectDetail.getBottlerCommunicationStatus());
		SOAPElement projectDescription = projectDetails.addChildElement("projectDescription");
		projectDescription.addTextNode(projectDetail.getProjectDescription());
//		SOAPElement fGOrConcModel = projectDetails.addChildElement("FGOrConcModel");
//		fGOrConcModel.addTextNode(projectDetail.getFGOrConcModel());
		SOAPElement techOpsAgreedLatestView = projectDetails.addChildElement("techOpsAgreedLatestView");
		techOpsAgreedLatestView.addTextNode(projectDetail.getTechOpsAgreedLatestView());
		SOAPElement comTechOpsAlignedDate = projectDetails.addChildElement("comTechOpsAlignedDate");
		comTechOpsAlignedDate.addTextNode(projectDetail.getComTechOpsAlignedDate());
		SOAPElement timelineRisk = projectDetails.addChildElement("timelineRisk");
		timelineRisk.addTextNode(projectDetail.getTimelineRisk());
//		SOAPElement projectHealth = projectDetails.addChildElement("ProjectHealth");
//		projectHealth.addTextNode(projectDetail.getProjectHealth());
		SOAPElement riskSummary = projectDetails.addChildElement("riskSummary");
		riskSummary.addTextNode(projectDetail.getRiskSummary());
		SOAPElement statusAndCriticalItems = projectDetails.addChildElement("statusAndCriticalItems");
		statusAndCriticalItems.addTextNode(projectDetail.getStatusAndCriticalItems());
		SOAPElement manufacturingSite = projectDetails.addChildElement("finalManufacturingSite");
		manufacturingSite.addTextNode(projectDetail.getFinalManufacturingSite());
		SOAPElement countryOfManufacture = projectDetails.addChildElement("finalCountryOfManufacture");
		countryOfManufacture.addTextNode(projectDetail.getFinalCountryOfManufacture());
		SOAPElement projectType = projectDetails.addChildElement("finalProjectType");
		projectType.addTextNode(projectDetail.getFinalProjectType());
		SOAPElement newFG = projectDetails.addChildElement("finalNewFG");
		newFG.addTextNode(projectDetail.getFinalNewFG());
		SOAPElement newRDFormula = projectDetails.addChildElement("finalnewRDFormula");
		newRDFormula.addTextNode(projectDetail.getFinalnewRDFormula());
		SOAPElement newHBCFormula = projectDetails.addChildElement("finalNewHBCFormula");
		newHBCFormula.addTextNode(projectDetail.getFinalNewHBCFormula());
		SOAPElement newPrimaryPackaging = projectDetails.addChildElement("finalNewPrimaryPackaging");
		newPrimaryPackaging.addTextNode(projectDetail.getFinalNewPrimaryPackaging());
		SOAPElement secondaryPackaging = projectDetails.addChildElement("finalSecondaryPackaging");
		secondaryPackaging.addTextNode(projectDetail.getFinalSecondaryPackaging());
		SOAPElement registrationDossier = projectDetails.addChildElement("finalRegistrationDossier");
		registrationDossier.addTextNode(" ");
		SOAPElement preProductionRegistartion = projectDetails.addChildElement("finalPreProductionRegistartion");
		preProductionRegistartion.addTextNode(" ");
		SOAPElement postProductionRegistration = projectDetails.addChildElement("finalPostProductionRegistration");
		postProductionRegistration.addTextNode(" ");
		SOAPElement finalProjectClassification = projectDetails.addChildElement("finalProjectClassification");
		finalProjectClassification.addTextNode(projectDetail.getFinalProjectClassification());
		SOAPElement finalProjectClassificationDesc = projectDetails.addChildElement("finalProjectClassificationDesc");
		finalProjectClassificationDesc.addTextNode(" ");
		SOAPElement classificationRational = projectDetails.addChildElement("classificationRational");
		classificationRational.addTextNode(projectDetail.getClassificationRational());
		SOAPElement deliveryWeek = projectDetails.addChildElement("CP1DeliveryWeek");
		deliveryWeek.addTextNode(projectDetail.getCP1DeliveryWeek());
		SOAPElement leadTimeTargetWks = projectDetails.addChildElement("leadTimeTargetWks");
		leadTimeTargetWks.addTextNode(" ");
		SOAPElement programManagerSlackTime = projectDetails.addChildElement("programManagerSlackTime");
		programManagerSlackTime.addTextNode(projectDetail.getProgramManagerSlackTime());
		SOAPElement programManagerCP1PhaseDelay = projectDetails.addChildElement("programManagerCP1PhaseDelay");
		programManagerCP1PhaseDelay.addTextNode(projectDetail.getProgramManagerCP1PhaseDelay());
		SOAPElement cP1RevisedDate = projectDetails.addChildElement("CP1RevisedDate");
		cP1RevisedDate.addTextNode(projectDetail.getCP1RevisedDate());
		SOAPElement noOfWeeksIncreaseVsPrior = projectDetails.addChildElement("noOfWeeksIncreaseVsPrior");
		noOfWeeksIncreaseVsPrior.addTextNode(projectDetail.getNoOfWeeksIncreaseVsPrior());
		SOAPElement nPDWasOnTrackForOriginalCp1Date = projectDetails.addChildElement("NPDWasOnTrackForOriginalCp1Date");
		nPDWasOnTrackForOriginalCp1Date.addTextNode(projectDetail.getNPDWasOnTrackForOriginalCp1Date());
		SOAPElement bottlerPushedOutProductionWithoutLaunchImpact = projectDetails
				.addChildElement("bottlerPushedOutProductionWithoutLaunchImpact");
		bottlerPushedOutProductionWithoutLaunchImpact
				.addTextNode(projectDetail.getBottlerPushedOutProductionWithoutLaunchImpact());
		SOAPElement mECCommercialBottlerRequestedDelayToLaunch = projectDetails
				.addChildElement("MECCommercialBottlerRequestedDelayToLaunch");
		mECCommercialBottlerRequestedDelayToLaunch
				.addTextNode(projectDetail.getMECCommercialBottlerRequestedDelayToLaunch());
		SOAPElement otherTbc = projectDetails.addChildElement("otherTbc");
		otherTbc.addTextNode(projectDetail.getOtherTbc());
		SOAPElement other2Tbc = projectDetails.addChildElement("other2Tbc");
		other2Tbc.addTextNode(projectDetail.getOther2Tbc());
		SOAPElement hasTheChangeBeenAlignedViaCPMtg = projectDetails.addChildElement("hasTheChangeBeenAlignedViaCPMtg");
		hasTheChangeBeenAlignedViaCPMtg.addTextNode(projectDetail.getHasTheChangeBeenAlignedViaCPMtg());
		SOAPElement briefDescOfDelay = projectDetails.addChildElement("briefDescOfDelay");
		briefDescOfDelay.addTextNode(projectDetail.getBriefDescOfDelay());
		SOAPElement other1 = projectDetails.addChildElement("Other1");
		other1.addTextNode(projectDetail.getOther1());
		SOAPElement other2 = projectDetails.addChildElement("Other2");
		other2.addTextNode(projectDetail.getOther2());
		SOAPElement comments = projectDetails.addChildElement("comments");
		comments.addTextNode(projectDetail.getComments());
		SOAPElement winshuttleProjectNumber = projectDetails.addChildElement("winshuttleProjectNumber");
		winshuttleProjectNumber.addTextNode(projectDetail.getWinshuttleProjectNumber());
		SOAPElement fGMaterialCode = projectDetails.addChildElement("fGMaterialCode");
		fGMaterialCode.addTextNode(projectDetail.getFGMaterialCode());
		SOAPElement formulaHBC = projectDetails.addChildElement("formulaHBC");
		formulaHBC.addTextNode(projectDetail.getFormulaHBC());
		SOAPElement hBCIssuanceType = projectDetails.addChildElement("hBCIssuanceType");
		hBCIssuanceType.addTextNode(projectDetail.getHBCIssuanceType());
		SOAPElement canSleeveOrPrimaryBottler = projectDetails.addChildElement("canSleeveOrPrimaryBottler");
		canSleeveOrPrimaryBottler.addTextNode(projectDetail.getCanSleeveOrPrimaryBottler());
		SOAPElement palletLabelFormat = projectDetails.addChildElement("palletLabelFormat");
		palletLabelFormat.addTextNode(projectDetail.getPalletLabelFormat());
		SOAPElement trayLabelFormat = projectDetails.addChildElement("trayLabelFormat");
		trayLabelFormat.addTextNode(projectDetail.getTrayLabelFormat());
		SOAPElement regionTechManager = projectDetails.addChildElement("regionalTechnicalManager");
		String regionalTechManager = utilService.getPmOwnerIdentityId(projectDetail.getRegionTechManager());
		regionTechManager.addTextNode(regionalTechManager);
		SOAPElement oPSPlanner = projectDetails.addChildElement("OPSPlanner");
		oPSPlanner.addTextNode(projectDetail.getOPSPlanner());
		SOAPElement gFG = projectDetails.addChildElement("GFG");
		String gfgLead = utilService.getPmOwnerIdentityId(projectDetail.getGFG());
		gFG.addTextNode(gfgLead);
		SOAPElement projectSpecialist = projectDetails.addChildElement("projectSpecialist");
		String projSpeclead = utilService.getPmOwnerIdentityId(projectDetail.getProjectSpecialist());
		projectSpecialist.addTextNode(projSpeclead);
		SOAPElement regulatoryLead = projectDetails.addChildElement("regulatoryLead");
		String reguLead = utilService.getPmOwnerIdentityId(projectDetail.getRegulatoryLead());
		regulatoryLead.addTextNode(reguLead);
		SOAPElement commercialLead = projectDetails.addChildElement("commercialLead");
		String commLead = utilService.getPmOwnerIdentityId(projectDetail.getCommercialLead());
		commercialLead.addTextNode(commLead);
		SOAPElement eANCodeSharedWithBottlerTarget = projectDetails.addChildElement("eANCodeSharedWithBottlerTarget");
		eANCodeSharedWithBottlerTarget.addTextNode(projectDetail.getEANCodeSharedWithBottlerTarget());
		SOAPElement eANCodesSharedWithBottlerActual = projectDetails.addChildElement("eANCodesSharedWithBottlerActual");
		eANCodesSharedWithBottlerActual.addTextNode(projectDetail.getEANCodesSharedWithBottlerActual());
		SOAPElement prodInfoSheetSharedTarget = projectDetails.addChildElement("prodInfoSheetSharedTarget");
		prodInfoSheetSharedTarget.addTextNode("");
		SOAPElement prodInfoSheetSharedActual = projectDetails.addChildElement("ProdInfoSheetSharedActual");
		prodInfoSheetSharedActual.addTextNode("");
		SOAPElement labelUplaodedToCanCompanyDate = projectDetails.addChildElement("labelUplaodedToCanCompanyDate");
		labelUplaodedToCanCompanyDate.addTextNode(projectDetail.getLabelUplaodedToCanCompanyDate());
		SOAPElement bottlerSpecificComments = projectDetails.addChildElement("bottlerSpecificComments");
		bottlerSpecificComments.addTextNode(projectDetail.getBottlerSpecificComments());
		SOAPElement posiibleNotificationRequirement = projectDetails.addChildElement("posiibleNotificationRequirement");
		posiibleNotificationRequirement.addTextNode(" ");
		SOAPElement jobID = projectDetails.addChildElement("jobID");
		jobID.addTextNode(" ");
		SOAPElement regulatoryComments = projectDetails.addChildElement("regulatoryComments");
		regulatoryComments.addTextNode(" ");
		SOAPElement requiresPrimaryCanAW = projectDetails.addChildElement("requiresPrimaryCanAW");
		requiresPrimaryCanAW.addTextNode(" ");
		SOAPElement primaryCanAWAvailableToStarted = projectDetails.addChildElement("primaryCanAWAvailableToStarted");
		primaryCanAWAvailableToStarted.addTextNode(" ");
		SOAPElement requiresSecCanAW = projectDetails.addChildElement("requiresSecCanAW");
		requiresSecCanAW.addTextNode(" ");
		SOAPElement seconCansAWAvailableToStarted = projectDetails.addChildElement("secCansAWAvailableToStarted");
		seconCansAWAvailableToStarted.addTextNode(" ");
		SOAPElement areAllAWTasksCompletedDevelopmentRouting = projectDetails.addChildElement("areAllAWTasksCompleted");
		areAllAWTasksCompletedDevelopmentRouting.addTextNode(" ");
		SOAPElement howManyPieces = projectDetails.addChildElement("howManyPieces");
		howManyPieces.addTextNode(" ");
		SOAPElement whatItemsDieLineAvai = projectDetails.addChildElement("whatItemsDieLineAvai");
		whatItemsDieLineAvai.addTextNode(" ");
		SOAPElement noOfPiecesRequired = projectDetails.addChildElement("noOfPiecesRequired");
		noOfPiecesRequired.addTextNode(projectDetail.getNoOfPiecesRequired());
		SOAPElement secondaryPackDetails = projectDetails.addChildElement("SecondaryPackDetails");
		SOAPElement secondaryPack1 = secondaryPackDetails.addChildElement("SecondaryPack");

		SOAPElement require1 = secondaryPack1.addChildElement("required");
		require1.addTextNode(projectDetail.getRequire1());
		SOAPElement regsNeeded1 = secondaryPack1.addChildElement("regsNeeded");
		regsNeeded1.addTextNode(projectDetail.getRegsNeeded1());
		SOAPElement packFormat1 = secondaryPack1.addChildElement("packFormat");
		packFormat1.addTextNode(projectDetail.getPackFormat1());
		SOAPElement mPMJobNo1 = secondaryPack1.addChildElement("MPMJobNo");
		mPMJobNo1.addTextNode(projectDetail.getMPMJobNo1());
		SOAPElement printSupplier1 = secondaryPack1.addChildElement("printSupplier");
		printSupplier1.addTextNode(projectDetail.getPrintSupplier1());
		SOAPElement approvedDielineAvailable1 = secondaryPack1.addChildElement("approvedDielineAvailable");
		approvedDielineAvailable1.addTextNode(projectDetail.getApprovedDielineAvailable1());
		SOAPElement secPackItemNumber1 = secondaryPack1.addChildElement("secPackItemNumber");
		secPackItemNumber1.addTextNode(projectDetail.getSecPackItemNumber1());
		SOAPElement colourApproval1 = secondaryPack1.addChildElement("colourApproval");
		colourApproval1.addTextNode(projectDetail.getColourApproval1());
		SOAPElement invoiceApproval1 = secondaryPack1.addChildElement("invoiceApproval");
		invoiceApproval1.addTextNode(projectDetail.getInvoiceApproval1());
		SOAPElement statusComments1 = secondaryPack1.addChildElement("statusComments");
		statusComments1.addTextNode(projectDetail.getStatusComments1());
		SOAPElement dateDevelopmentCompleted1 = secondaryPack1.addChildElement("dateDevelopmentCompleted");
		dateDevelopmentCompleted1.addTextNode(projectDetail.getDateDevelopmentCompleted1());
		SOAPElement dateProofingCompletedSecondaryPackaging1 = secondaryPack1
				.addChildElement("dateProofingCompletedSecPackaging");
		dateProofingCompletedSecondaryPackaging1
				.addTextNode(projectDetail.getDateProofingCompletedSecondaryPackaging1());

		SOAPElement secondaryPack2 = secondaryPackDetails.addChildElement("SecondaryPack");

		SOAPElement require2 = secondaryPack2.addChildElement("required");
		require2.addTextNode(projectDetail.getRequire2());
		SOAPElement regsNeeded2 = secondaryPack2.addChildElement("regsNeeded");
		regsNeeded2.addTextNode(projectDetail.getRegsNeeded2());
		SOAPElement packFormat2 = secondaryPack2.addChildElement("packFormat");
		packFormat2.addTextNode(projectDetail.getPackFormat2());
		SOAPElement mPMJobNo2 = secondaryPack2.addChildElement("MPMJobNo");
		mPMJobNo2.addTextNode(projectDetail.getMPMJobNo2());
		SOAPElement printSupplier2 = secondaryPack2.addChildElement("printSupplier");
		printSupplier2.addTextNode(projectDetail.getPrintSupplier2());
		SOAPElement approvedDielineAvailable2 = secondaryPack2.addChildElement("approvedDielineAvailable");
		approvedDielineAvailable2.addTextNode(projectDetail.getApprovedDielineAvailable2());
		SOAPElement secPackItemNumber2 = secondaryPack2.addChildElement("secPackItemNumber");
		secPackItemNumber2.addTextNode(projectDetail.getSecPackItemNumber2());
		SOAPElement colourApproval2 = secondaryPack2.addChildElement("colourApproval");
		colourApproval2.addTextNode(projectDetail.getColourApproval2());
		SOAPElement invoiceApproval2 = secondaryPack2.addChildElement("invoiceApproval");
		invoiceApproval2.addTextNode(projectDetail.getInvoiceApproval2());
		SOAPElement statusComments2 = secondaryPack2.addChildElement("statusComments");
		statusComments2.addTextNode(projectDetail.getStatusComments2());
		SOAPElement dateDevelopmentCompleted2 = secondaryPack2.addChildElement("dateDevelopmentCompleted");
		dateDevelopmentCompleted2.addTextNode(projectDetail.getDateDevelopmentCompleted2());
		SOAPElement dateProofingCompletedSecondaryPackaging2 = secondaryPack2
				.addChildElement("dateProofingCompletedSecPackaging");
		dateProofingCompletedSecondaryPackaging2
				.addTextNode(projectDetail.getDateProofingCompletedSecondaryPackaging2());

		SOAPElement secondaryPack3 = secondaryPackDetails.addChildElement("SecondaryPack");

		SOAPElement require3 = secondaryPack3.addChildElement("required");
		require3.addTextNode(projectDetail.getRequire3());
		SOAPElement regsNeeded3 = secondaryPack3.addChildElement("regsNeeded");
		regsNeeded3.addTextNode(projectDetail.getRegsNeeded3());
		SOAPElement packFormat3 = secondaryPack3.addChildElement("packFormat");
		packFormat3.addTextNode(projectDetail.getPackFormat3());
		SOAPElement mPMJobNo3 = secondaryPack3.addChildElement("MPMJobNo");
		mPMJobNo3.addTextNode(projectDetail.getMPMJobNo3());
		SOAPElement printSupplier3 = secondaryPack3.addChildElement("printSupplier");
		printSupplier3.addTextNode(projectDetail.getPrintSupplier3());
		SOAPElement approvedDielineAvailable3 = secondaryPack3.addChildElement("approvedDielineAvailable");
		approvedDielineAvailable3.addTextNode(projectDetail.getApprovedDielineAvailable3());
		SOAPElement secPackItemNumber3 = secondaryPack3.addChildElement("secPackItemNumber");
		secPackItemNumber3.addTextNode(projectDetail.getSecPackItemNumber3());
		SOAPElement colourApproval3 = secondaryPack3.addChildElement("colourApproval");
		colourApproval3.addTextNode(projectDetail.getColourApproval3());
		SOAPElement invoiceApproval3 = secondaryPack3.addChildElement("invoiceApproval");
		invoiceApproval3.addTextNode(projectDetail.getInvoiceApproval3());
		SOAPElement statusComments3 = secondaryPack3.addChildElement("statusComments");
		statusComments3.addTextNode(projectDetail.getStatusComments3());
		SOAPElement dateDevelopmentCompleted3 = secondaryPack3.addChildElement("dateDevelopmentCompleted");
		dateDevelopmentCompleted3.addTextNode(projectDetail.getDateDevelopmentCompleted3());
		SOAPElement dateProofingCompletedSecondaryPackaging3 = secondaryPack3
				.addChildElement("dateProofingCompletedSecPackaging");
		dateProofingCompletedSecondaryPackaging3
				.addTextNode(projectDetail.getDateProofingCompletedSecondaryPackaging3());

		SOAPElement technicalOrQualitySupervisionRequired = projectDetails
				.addChildElement("technicalOrQualitySupervisionRequired");
		technicalOrQualitySupervisionRequired.addTextNode("");
		SOAPElement siteAuditRequired = projectDetails.addChildElement("siteAuditRequired");
		siteAuditRequired.addTextNode(" ");

		SOAPElement hBCRPEMESBCreated = projectDetails.addChildElement("HBCRPEMESBCreated");
		hBCRPEMESBCreated.addTextNode(" ");
		SOAPElement plantSpecificBatchingInstructionsReapplicationOnly = projectDetails
				.addChildElement("plantSpecificBatchingInstructionsReapplicationOnly");
		plantSpecificBatchingInstructionsReapplicationOnly.addTextNode("");
		SOAPElement weekDue = projectDetails.addChildElement("weekDue");
		weekDue.addTextNode(" ");
		SOAPElement whoWillComplete = projectDetails.addChildElement("whoWillComplete");
		whoWillComplete.addTextNode(" ");
		SOAPElement siteSetupCompleted = projectDetails.addChildElement("siteSetupCompleted");
		siteSetupCompleted.addTextNode(" ");
		SOAPElement plantSpecificBatchingInstructionsComments = projectDetails
				.addChildElement("plantSpecificBatchingInstructionsComments");
		plantSpecificBatchingInstructionsComments.addTextNode("");
		SOAPElement earliestTrialOr1stProd = projectDetails.addChildElement("earliestTrialOr1stProd");
		earliestTrialOr1stProd.addTextNode(projectDetail.getEarliestTrialOr1stProd());
		SOAPElement quality = projectDetails.addChildElement("trialQuality");
		quality.addTextNode(" ");
		SOAPElement nPD = projectDetails.addChildElement("trialNPD");
		nPD.addTextNode(" ");
		SOAPElement onsiteRemoteNotRequired = projectDetails.addChildElement("onsiteRemoteNotRequired");
		onsiteRemoteNotRequired.addTextNode(" ");
		SOAPElement trialProtocolWrittenBy = projectDetails.addChildElement("trialProtocolWrittenBy");
		trialProtocolWrittenBy.addTextNode(" ");
		SOAPElement protocolIssued = projectDetails.addChildElement("protocolIssued");
		protocolIssued.addTextNode(" ");
		SOAPElement tastingRequirement = projectDetails.addChildElement("tastingRequirement");
		tastingRequirement.addTextNode(" ");
		SOAPElement qAQAReleaseDate = projectDetails.addChildElement("QAQAReleaseDate");
		qAQAReleaseDate.addTextNode(" ");
		SOAPElement trialOverallComments = projectDetails.addChildElement("trialOverallComments");
		trialOverallComments.addTextNode(" ");
		SOAPElement trialMaterialsConfirmedDeliveryWeek = projectDetails
				.addChildElement("trialMaterialsConfirmedDeliveryWeek");
		trialMaterialsConfirmedDeliveryWeek.addTextNode("");
		SOAPElement istProdMaterialsConfirmed = projectDetails.addChildElement("IstProdMaterialsConfirmed");
		istProdMaterialsConfirmed.addTextNode(" ");
		SOAPElement finalAgreed1stProd = projectDetails.addChildElement("finalAgreed1stProd");
		finalAgreed1stProd.addTextNode(" ");
		SOAPElement trialOrFirstProductionIncludedInThePlan = projectDetails
				.addChildElement("trialOrFirstProductionIncludedInThePlan");
		trialOrFirstProductionIncludedInThePlan.addTextNode(" ");
		SOAPElement allowProdSchedule = projectDetails.addChildElement("allowProdSchedule");
		allowProdSchedule.addTextNode(projectDetail.getAllowProdSchedule());
		SOAPElement localBOMInSAp = projectDetails.addChildElement("localBOMInSAP");
		localBOMInSAp.addTextNode(" ");
		SOAPElement palletLabelReceivedTarget = projectDetails.addChildElement("palletLabelReceivedTarget");
		palletLabelReceivedTarget.addTextNode(" ");
		SOAPElement palletLabelReceivedActual = projectDetails.addChildElement("palletLabelReceivedActual");
		palletLabelReceivedActual.addTextNode(projectDetail.getPalletLabelReceivedActual());
		SOAPElement palletLabelSentTarget = projectDetails.addChildElement("palletLabelSentTarget");
		palletLabelSentTarget.addTextNode(" ");
		SOAPElement palletLabelSentActual = projectDetails.addChildElement("palletLabelSentActual");
		palletLabelSentActual.addTextNode(projectDetail.getPalletLabelSentActual());
		SOAPElement pOReceivedDate = projectDetails.addChildElement("POReceivedDate");
		pOReceivedDate.addTextNode(projectDetail.getPOReceivedDate());
		SOAPElement canSupplier = projectDetails.addChildElement("canSupplier");
		canSupplier.addTextNode(" ");
		SOAPElement targetWeek = projectDetails.addChildElement("bottlerTargetWeek");
		targetWeek.addTextNode(" ");
		SOAPElement actualWeek = projectDetails.addChildElement("bottlerActualWeek");
		actualWeek.addTextNode(" ");
		SOAPElement bottlerSpecificItemNumber = projectDetails.addChildElement("bottlerSpecificItemNumber");
		bottlerSpecificItemNumber.addTextNode(" ");
		SOAPElement noOfCanCompaniesReq = projectDetails.addChildElement("noOfCanCompaniesReq");
		noOfCanCompaniesReq.addTextNode(projectDetail.getNoOfCanCompaniesReq());
		SOAPElement nameOfCanCompanys = projectDetails.addChildElement("nameOfCanCompanys");
		nameOfCanCompanys.addTextNode(projectDetail.getNameOfCanCompanys());

		return requestData;
	}

	/**
	 * method to create task details soap request xml tags
	 * 
	 * @param requestData tag
	 * @return taskDetails in requestData tag
	 * @throws SOAPException
	 */
	private SOAPElement createTaskDetails(SOAPElement requestData, int index) throws SOAPException {
		TaskDetails taskDetail = EventUserModal.getTasksList().get(index);
		SOAPElement taskDetails = requestData.addChildElement("TaskDetails");

		SOAPElement task2 = taskDetails.addChildElement("Task2");
		SOAPElement task2StartDate = task2.addChildElement("StartDate");
		task2StartDate.addTextNode(taskDetail.getTask2StartDate());
		SOAPElement task2DueDate = task2.addChildElement("DueDate");
		task2DueDate.addTextNode(taskDetail.getTask2DueDate());
		SOAPElement task2ActualStartDate = task2.addChildElement("ActualStartDate");
		task2ActualStartDate.addTextNode(taskDetail.getTask2ActualStartDate());
		SOAPElement task2ActualDueDate = task2.addChildElement("ActualDueDate");
		task2ActualDueDate.addTextNode(taskDetail.getTask2ActualDueDate());
		SOAPElement task2IsActive = task2.addChildElement("IsActive");
		task2IsActive.addTextNode(taskDetail.getTask2IsActive());
		SOAPElement task2IsCompleted = task2.addChildElement("IsCompleted");
		task2IsCompleted.addTextNode(taskDetail.getTask2IsCompleted());
		SOAPElement task2Duration = task2.addChildElement("Duration");
		task2Duration.addTextNode(taskDetail.getTask2Duration());

		SOAPElement task3 = taskDetails.addChildElement("Task3");
		SOAPElement task3StartDate = task3.addChildElement("StartDate");
		task3StartDate.addTextNode(taskDetail.getTask3StartDate());
		SOAPElement task3DueDate = task3.addChildElement("DueDate");
		task3DueDate.addTextNode(taskDetail.getTask3DueDate());
		SOAPElement task3ActualStartDate = task3.addChildElement("ActualStartDate");
		task3ActualStartDate.addTextNode(taskDetail.getTask3ActualStartDate());
		SOAPElement task3ActualDueDate = task3.addChildElement("ActualDueDate");
		task3ActualDueDate.addTextNode(taskDetail.getTask3ActualDueDate());
		SOAPElement task3IsActive = task3.addChildElement("IsActive");
		task3IsActive.addTextNode(taskDetail.getTask3IsActive());
		SOAPElement task3IsCompleted = task3.addChildElement("IsCompleted");
		task3IsCompleted.addTextNode(taskDetail.getTask3IsCompleted());
		SOAPElement task3Duration = task3.addChildElement("Duration");
		task3Duration.addTextNode(taskDetail.getTask3Duration());
		SOAPElement task3CustomFields = task3.addChildElement("CustomFields");
		SOAPElement task3NewFG = task3CustomFields.addChildElement("NewFG");
		task3NewFG.addTextNode(taskDetail.getTask3NewFG());
		SOAPElement task3NewRDFormula = task3CustomFields.addChildElement("NewRDFormula");
		task3NewRDFormula.addTextNode(taskDetail.getTask3NewRDFormula());
		SOAPElement task3NewHBCFormula = task3CustomFields.addChildElement("NewHBCFormula");
		task3NewHBCFormula.addTextNode(taskDetail.getTask3NewHBCFormula());
		SOAPElement task3NewPrimaryPackaging = task3CustomFields.addChildElement("NewPrimaryPackaging");
		task3NewPrimaryPackaging.addTextNode(taskDetail.getTask3NewPrimaryPackaging());
		SOAPElement task3NewSecondaryPackaging = task3CustomFields.addChildElement("NewSecondaryPackaging");
		task3NewSecondaryPackaging.addTextNode(taskDetail.getTask3NewSecondaryPackaging());
		SOAPElement task3FinalClassification = task3CustomFields.addChildElement("FinalClassification");
		task3FinalClassification.addTextNode(taskDetail.getTask3FinalClassification());
		SOAPElement task3FinalCountryOfManufacture = task3CustomFields.addChildElement("FinalCountryOfManufacture");
		task3FinalCountryOfManufacture.addTextNode(taskDetail.getTask3FinalCountryOfManufacture());
		SOAPElement task3FinalSite = task3CustomFields.addChildElement("FinalSite");
		task3FinalSite.addTextNode("");
		SOAPElement task3FinalProjectType = task3CustomFields.addChildElement("FinalProjectType");
		task3FinalProjectType.addTextNode(taskDetail.getTask3FinalProjectType());

		SOAPElement task4 = taskDetails.addChildElement("Task4");
		SOAPElement task4StartDate = task4.addChildElement("StartDate");
		task4StartDate.addTextNode(taskDetail.getTask4StartDate());
		SOAPElement task4DueDate = task4.addChildElement("DueDate");
		task4DueDate.addTextNode(taskDetail.getTask4DueDate());
		SOAPElement task4ActualStartDate = task4.addChildElement("ActualStartDate");
		task4ActualStartDate.addTextNode(taskDetail.getTask4ActualStartDate());
		SOAPElement task4ActualDueDate = task4.addChildElement("ActualDueDate");
		task4ActualDueDate.addTextNode(taskDetail.getTask4ActualDueDate());
		SOAPElement task4IsActive = task4.addChildElement("IsActive");
		task4IsActive.addTextNode(taskDetail.getTask4IsActive());
		SOAPElement task4IsCompleted = task4.addChildElement("IsCompleted");
		task4IsCompleted.addTextNode(taskDetail.getTask4IsCompleted());
		SOAPElement task4Duration = task4.addChildElement("Duration");
		task4Duration.addTextNode(taskDetail.getTask4Duration());

		SOAPElement task5 = taskDetails.addChildElement("Task5");
		SOAPElement task5StartDate = task5.addChildElement("StartDate");
		task5StartDate.addTextNode(taskDetail.getTask5StartDate());
		SOAPElement task5DueDate = task5.addChildElement("DueDate");
		task5DueDate.addTextNode(taskDetail.getTask5DueDate());
		SOAPElement task5ActualStartDate = task5.addChildElement("ActualStartDate");
		task5ActualStartDate.addTextNode(taskDetail.getTask5ActualStartDate());
		SOAPElement task5ActualDueDate = task5.addChildElement("ActualDueDate");
		task5ActualDueDate.addTextNode(taskDetail.getTask5ActualDueDate());
		SOAPElement task5IsActive = task5.addChildElement("IsActive");
		task5IsActive.addTextNode(taskDetail.getTask5IsActive());
		SOAPElement task5IsCompleted = task5.addChildElement("IsCompleted");
		task5IsCompleted.addTextNode(taskDetail.getTask5IsCompleted());
		SOAPElement task5Duration = task5.addChildElement("Duration");
		task5Duration.addTextNode(taskDetail.getTask5Duration());

		SOAPElement task6 = taskDetails.addChildElement("Task6");
		SOAPElement task6StartDate = task6.addChildElement("StartDate");
		task6StartDate.addTextNode(taskDetail.getTask6StartDate());
		SOAPElement task6DueDate = task6.addChildElement("DueDate");
		task6DueDate.addTextNode(taskDetail.getTask6DueDate());
		SOAPElement task6ActualStartDate = task6.addChildElement("ActualStartDate");
		task6ActualStartDate.addTextNode(taskDetail.getTask6ActualStartDate());
		SOAPElement task6ActualDueDate = task6.addChildElement("ActualDueDate");
		task6ActualDueDate.addTextNode(taskDetail.getTask6ActualDueDate());
		SOAPElement task6IsActive = task6.addChildElement("IsActive");
		task6IsActive.addTextNode(taskDetail.getTask6IsActive());
		SOAPElement task6IsCompleted = task6.addChildElement("IsCompleted");
		task6IsCompleted.addTextNode(taskDetail.getTask6IsCompleted());
		SOAPElement task6Duration = task6.addChildElement("Duration");
		task6Duration.addTextNode(taskDetail.getTask6Duration());

		SOAPElement task7 = taskDetails.addChildElement("Task7");
		SOAPElement task7StartDate = task7.addChildElement("StartDate");
		task7StartDate.addTextNode(taskDetail.getTask7StartDate());
		SOAPElement task7DueDate = task7.addChildElement("DueDate");
		task7DueDate.addTextNode(taskDetail.getTask7DueDate());
		SOAPElement task7ActualStartDate = task7.addChildElement("ActualStartDate");
		task7ActualStartDate.addTextNode(taskDetail.getTask7ActualStartDate());
		SOAPElement task7ActualDueDate = task7.addChildElement("ActualDueDate");
		task7ActualDueDate.addTextNode(taskDetail.getTask7ActualDueDate());
		SOAPElement task7IsActive = task7.addChildElement("IsActive");
		task7IsActive.addTextNode(taskDetail.getTask7IsActive());
		SOAPElement task7IsCompleted = task7.addChildElement("IsCompleted");
		task7IsCompleted.addTextNode(taskDetail.getTask7IsCompleted());
		SOAPElement task7Duration = task7.addChildElement("Duration");
		task7Duration.addTextNode(taskDetail.getTask7Duration());
		SOAPElement task7CustomFields = task7.addChildElement("CustomFields");
		SOAPElement task7IsProcessAuthorityApproval = task7CustomFields.addChildElement("IsProcessAuthorityApproval");
		task7IsProcessAuthorityApproval.addTextNode(taskDetail.getTask7IsProcessAuthorityApproval());
		SOAPElement task7ScopingDocumentRequirement = task7CustomFields.addChildElement("ScopingDocumentRequirement");
		task7ScopingDocumentRequirement.addTextNode("");

		SOAPElement task9 = taskDetails.addChildElement("Task9");
		SOAPElement task9StartDate = task9.addChildElement("StartDate");
		task9StartDate.addTextNode(taskDetail.getTask9StartDate());
		SOAPElement task9DueDate = task9.addChildElement("DueDate");
		task9DueDate.addTextNode(taskDetail.getTask9DueDate());
		SOAPElement task9ActualStartDate = task9.addChildElement("ActualStartDate");
		task9ActualStartDate.addTextNode(taskDetail.getTask9ActualStartDate());
		SOAPElement task9ActualDueDate = task9.addChildElement("ActualDueDate");
		task9ActualDueDate.addTextNode(taskDetail.getTask9ActualDueDate());
		SOAPElement task9IsActive = task9.addChildElement("IsActive");
		task9IsActive.addTextNode(taskDetail.getTask9IsActive());
		SOAPElement task9IsCompleted = task9.addChildElement("IsCompleted");
		task9IsCompleted.addTextNode(taskDetail.getTask9IsCompleted());
		SOAPElement task9Duration = task9.addChildElement("Duration");
		task9Duration.addTextNode(taskDetail.getTask9Duration());
		SOAPElement task9CustomFields = task9.addChildElement("CustomFields");
		SOAPElement task9WinshuttleProjectNo = task9CustomFields.addChildElement("WinshuttleProjectNo");
		task9WinshuttleProjectNo.addTextNode("");

		SOAPElement task10 = taskDetails.addChildElement("Task10");
		SOAPElement task10StartDate = task10.addChildElement("StartDate");
		task10StartDate.addTextNode(taskDetail.getTask10StartDate());
		SOAPElement task10DueDate = task10.addChildElement("DueDate");
		task10DueDate.addTextNode(taskDetail.getTask10DueDate());
		SOAPElement task10ActualStartDate = task10.addChildElement("ActualStartDate");
		task10ActualStartDate.addTextNode(taskDetail.getTask10ActualStartDate());
		SOAPElement task10ActualDueDate = task10.addChildElement("ActualDueDate");
		task10ActualDueDate.addTextNode(taskDetail.getTask10ActualDueDate());
		SOAPElement task10IsActive = task10.addChildElement("IsActive");
		task10IsActive.addTextNode(taskDetail.getTask10IsActive());
		SOAPElement task10IsCompleted = task10.addChildElement("IsCompleted");
		task10IsCompleted.addTextNode(taskDetail.getTask10IsCompleted());
		SOAPElement task10Duration = task10.addChildElement("Duration");
		task10Duration.addTextNode(taskDetail.getTask10Duration());
		SOAPElement task10CustomFields = task10.addChildElement("CustomFields");
		SOAPElement task10RequiredComplete = task10CustomFields.addChildElement("RequiredComplete");
		task10RequiredComplete.addTextNode("");

		SOAPElement task11 = taskDetails.addChildElement("Task11");
		SOAPElement task11StartDate = task11.addChildElement("StartDate");
		task11StartDate.addTextNode(taskDetail.getTask11StartDate());
		SOAPElement task11DueDate = task11.addChildElement("DueDate");
		task11DueDate.addTextNode(taskDetail.getTask11DueDate());
		SOAPElement task11ActualStartDate = task11.addChildElement("ActualStartDate");
		task11ActualStartDate.addTextNode(taskDetail.getTask11ActualStartDate());
		SOAPElement task11ActualDueDate = task11.addChildElement("ActualDueDate");
		task11ActualDueDate.addTextNode(taskDetail.getTask11ActualDueDate());
		SOAPElement task11IsActive = task11.addChildElement("IsActive");
		task11IsActive.addTextNode(taskDetail.getTask11IsActive());
		SOAPElement task11IsCompleted = task11.addChildElement("IsCompleted");
		task11IsCompleted.addTextNode(taskDetail.getTask11IsCompleted());
		SOAPElement task11Duration = task11.addChildElement("Duration");
		task11Duration.addTextNode(taskDetail.getTask11Duration());

		SOAPElement task12 = taskDetails.addChildElement("Task12");
		SOAPElement task12StartDate = task12.addChildElement("StartDate");
		task12StartDate.addTextNode(taskDetail.getTask12StartDate());
		SOAPElement task12DueDate = task12.addChildElement("DueDate");
		task12DueDate.addTextNode(taskDetail.getTask12DueDate());
		SOAPElement task12ActualStartDate = task12.addChildElement("ActualStartDate");
		task12ActualStartDate.addTextNode(taskDetail.getTask12ActualStartDate());
		SOAPElement task12ActualDueDate = task12.addChildElement("ActualDueDate");
		task12ActualDueDate.addTextNode(taskDetail.getTask12ActualDueDate());
		SOAPElement task12IsActive = task12.addChildElement("IsActive");
		task12IsActive.addTextNode(taskDetail.getTask12IsActive());
		SOAPElement task12IsCompleted = task12.addChildElement("IsCompleted");
		task12IsCompleted.addTextNode(taskDetail.getTask12IsCompleted());
		SOAPElement task12Duration = task12.addChildElement("Duration");
		task12Duration.addTextNode(taskDetail.getTask12Duration());

		SOAPElement task13 = taskDetails.addChildElement("Task13");
		SOAPElement task13StartDate = task13.addChildElement("StartDate");
		task13StartDate.addTextNode(taskDetail.getTask13StartDate());
		SOAPElement task13DueDate = task13.addChildElement("DueDate");
		task13DueDate.addTextNode(taskDetail.getTask13DueDate());
		SOAPElement task13ActualStartDate = task13.addChildElement("ActualStartDate");
		task13ActualStartDate.addTextNode(taskDetail.getTask13ActualStartDate());
		SOAPElement task13ActualDueDate = task13.addChildElement("ActualDueDate");
		task13ActualDueDate.addTextNode(taskDetail.getTask13ActualDueDate());
		SOAPElement task13IsActive = task13.addChildElement("IsActive");
		task13IsActive.addTextNode(taskDetail.getTask13IsActive());
		SOAPElement task13IsCompleted = task13.addChildElement("IsCompleted");
		task13IsCompleted.addTextNode(taskDetail.getTask13IsCompleted());
		SOAPElement task13Duration = task13.addChildElement("Duration");
		task13Duration.addTextNode(taskDetail.getTask13Duration());
		SOAPElement task13CustomFields = task13.addChildElement("CustomFields");
		SOAPElement task13IsTransactionSchemeAnalysisRequired = task13CustomFields
				.addChildElement("IsTransactionSchemeAnalysisRequired");
		task13IsTransactionSchemeAnalysisRequired
				.addTextNode(taskDetail.getTask13IsTransactionSchemeAnalysisRequired());
		SOAPElement task13IsNewRMCosting = task13CustomFields.addChildElement("IsNewRMCosting");
		task13IsNewRMCosting.addTextNode(taskDetail.getTask13IsNewRMCosting());

		SOAPElement task13a = taskDetails.addChildElement("Task13a");
		SOAPElement task13aStartDate = task13a.addChildElement("StartDate");
		task13aStartDate.addTextNode(taskDetail.getTask13aStartDate());
		SOAPElement task13aDueDate = task13a.addChildElement("DueDate");
		task13aDueDate.addTextNode(taskDetail.getTask13aDueDate());
		SOAPElement task13aActualStartDate = task13a.addChildElement("ActualStartDate");
		task13aActualStartDate.addTextNode(taskDetail.getTask13aActualStartDate());
		SOAPElement task13aActualDueDate = task13a.addChildElement("ActualDueDate");
		task13aActualDueDate.addTextNode(taskDetail.getTask13aActualDueDate());
		SOAPElement task13aIsActive = task13a.addChildElement("IsActive");
		task13aIsActive.addTextNode(taskDetail.getTask13aIsActive());
		SOAPElement task13aIsCompleted = task13a.addChildElement("IsCompleted");
		task13aIsCompleted.addTextNode(taskDetail.getTask13aIsCompleted());
		SOAPElement task13aDuration = task13a.addChildElement("Duration");
		task13aDuration.addTextNode(taskDetail.getTask13aDuration());

		SOAPElement task14 = taskDetails.addChildElement("Task14");
		SOAPElement task14StartDate = task14.addChildElement("StartDate");
		task14StartDate.addTextNode(taskDetail.getTask14StartDate());
		SOAPElement task14DueDate = task14.addChildElement("DueDate");
		task14DueDate.addTextNode(taskDetail.getTask14DueDate());
		SOAPElement task14ActualStartDate = task14.addChildElement("ActualStartDate");
		task14ActualStartDate.addTextNode(taskDetail.getTask14ActualStartDate());
		SOAPElement task14ActualDueDate = task14.addChildElement("ActualDueDate");
		task14ActualDueDate.addTextNode(taskDetail.getTask14ActualDueDate());
		SOAPElement task14IsActive = task14.addChildElement("IsActive");
		task14IsActive.addTextNode(taskDetail.getTask14IsActive());
		SOAPElement task14IsCompleted = task14.addChildElement("IsCompleted");
		task14IsCompleted.addTextNode(taskDetail.getTask14IsCompleted());
		SOAPElement task14Duration = task14.addChildElement("Duration");
		task14Duration.addTextNode(taskDetail.getTask14Duration());

		SOAPElement task15 = taskDetails.addChildElement("Task15");
		SOAPElement task15StartDate = task15.addChildElement("StartDate");
		task15StartDate.addTextNode(taskDetail.getTask15StartDate());
		SOAPElement task15DueDate = task15.addChildElement("DueDate");
		task15DueDate.addTextNode(taskDetail.getTask15DueDate());
		SOAPElement task15ActualStartDate = task15.addChildElement("ActualStartDate");
		task15ActualStartDate.addTextNode(taskDetail.getTask15ActualStartDate());
		SOAPElement task15ActualDueDate = task15.addChildElement("ActualDueDate");
		task15ActualDueDate.addTextNode(taskDetail.getTask15ActualDueDate());
		SOAPElement task15IsActive = task15.addChildElement("IsActive");
		task15IsActive.addTextNode(taskDetail.getTask15IsActive());
		SOAPElement task15IsCompleted = task15.addChildElement("IsCompleted");
		task15IsCompleted.addTextNode(taskDetail.getTask15IsCompleted());
		SOAPElement task15Duration = task15.addChildElement("Duration");
		task15Duration.addTextNode(taskDetail.getTask15Duration());

		SOAPElement task16 = taskDetails.addChildElement("Task16");
		SOAPElement task16StartDate = task16.addChildElement("StartDate");
		task16StartDate.addTextNode(taskDetail.getTask16StartDate());
		SOAPElement task16DueDate = task16.addChildElement("DueDate");
		task16DueDate.addTextNode(taskDetail.getTask16DueDate());
		SOAPElement task16ActualStartDate = task16.addChildElement("ActualStartDate");
		task16ActualStartDate.addTextNode(taskDetail.getTask16ActualStartDate());
		SOAPElement task16ActualDueDate = task16.addChildElement("ActualDueDate");
		task16ActualDueDate.addTextNode(taskDetail.getTask16ActualDueDate());
		SOAPElement task16IsActive = task16.addChildElement("IsActive");
		task16IsActive.addTextNode(taskDetail.getTask16IsActive());
		SOAPElement task16IsCompleted = task16.addChildElement("IsCompleted");
		task16IsCompleted.addTextNode(taskDetail.getTask16IsCompleted());
		SOAPElement task16Duration = task16.addChildElement("Duration");
		task16Duration.addTextNode(taskDetail.getTask16Duration());
		SOAPElement task16CustomFields = task16.addChildElement("CustomFields");
		SOAPElement task16IssuanceType = task16CustomFields.addChildElement("IssuanceType");
		task16IssuanceType.addTextNode("");

		SOAPElement task17 = taskDetails.addChildElement("Task17");
		SOAPElement task17StartDate = task17.addChildElement("StartDate");
		task17StartDate.addTextNode(taskDetail.getTask17StartDate());
		SOAPElement task17DueDate = task17.addChildElement("DueDate");
		task17DueDate.addTextNode(taskDetail.getTask17DueDate());
		SOAPElement task17ActualStartDate = task17.addChildElement("ActualStartDate");
		task17ActualStartDate.addTextNode(taskDetail.getTask17ActualStartDate());
		SOAPElement task17ActualDueDate = task17.addChildElement("ActualDueDate");
		task17ActualDueDate.addTextNode(taskDetail.getTask17ActualDueDate());
		SOAPElement task17IsActive = task17.addChildElement("IsActive");
		task17IsActive.addTextNode(taskDetail.getTask17IsActive());
		SOAPElement task17IsCompleted = task17.addChildElement("IsCompleted");
		task17IsCompleted.addTextNode(taskDetail.getTask17IsCompleted());
		SOAPElement task17Duration = task17.addChildElement("Duration");
		task17Duration.addTextNode(taskDetail.getTask17Duration());

		SOAPElement task18 = taskDetails.addChildElement("Task18");
		SOAPElement task18StartDate = task18.addChildElement("StartDate");
		task18StartDate.addTextNode(taskDetail.getTask18StartDate());
		SOAPElement task18DueDate = task18.addChildElement("DueDate");
		task18DueDate.addTextNode(taskDetail.getTask18DueDate());
		SOAPElement task18ActualStartDate = task18.addChildElement("ActualStartDate");
		task18ActualStartDate.addTextNode(taskDetail.getTask18ActualStartDate());
		SOAPElement task18ActualDueDate = task18.addChildElement("ActualDueDate");
		task18ActualDueDate.addTextNode(taskDetail.getTask18ActualDueDate());
		SOAPElement task18IsActive = task18.addChildElement("IsActive");
		task18IsActive.addTextNode(taskDetail.getTask18IsActive());
		SOAPElement task18IsCompleted = task18.addChildElement("IsCompleted");
		task18IsCompleted.addTextNode(taskDetail.getTask18IsCompleted());
		SOAPElement task18Duration = task18.addChildElement("Duration");
		task18Duration.addTextNode(taskDetail.getTask18Duration());

		SOAPElement task19 = taskDetails.addChildElement("Task19");
		SOAPElement task19StartDate = task19.addChildElement("StartDate");
		task19StartDate.addTextNode(taskDetail.getTask19StartDate());
		SOAPElement task19DueDate = task19.addChildElement("DueDate");
		task19DueDate.addTextNode(taskDetail.getTask19DueDate());
		SOAPElement task19ActualStartDate = task19.addChildElement("ActualStartDate");
		task19ActualStartDate.addTextNode(taskDetail.getTask19ActualStartDate());
		SOAPElement task19ActualDueDate = task19.addChildElement("ActualDueDate");
		task19ActualDueDate.addTextNode(taskDetail.getTask19ActualDueDate());
		SOAPElement task19IsActive = task19.addChildElement("IsActive");
		task19IsActive.addTextNode(taskDetail.getTask19IsActive());
		SOAPElement task19IsCompleted = task19.addChildElement("IsCompleted");
		task19IsCompleted.addTextNode(taskDetail.getTask19IsCompleted());
		SOAPElement task19Duration = task19.addChildElement("Duration");
		task19Duration.addTextNode(taskDetail.getTask19Duration());

		SOAPElement task20 = taskDetails.addChildElement("Task20");
		SOAPElement task20StartDate = task20.addChildElement("StartDate");
		task20StartDate.addTextNode(taskDetail.getTask20StartDate());
		SOAPElement task20DueDate = task20.addChildElement("DueDate");
		task20DueDate.addTextNode(taskDetail.getTask20DueDate());
		SOAPElement task20ActualStartDate = task20.addChildElement("ActualStartDate");
		task20ActualStartDate.addTextNode(taskDetail.getTask20ActualStartDate());
		SOAPElement task20ActualDueDate = task20.addChildElement("ActualDueDate");
		task20ActualDueDate.addTextNode(taskDetail.getTask20ActualDueDate());
		SOAPElement task20IsActive = task20.addChildElement("IsActive");
		task20IsActive.addTextNode(taskDetail.getTask20IsActive());
		SOAPElement task20IsCompleted = task20.addChildElement("IsCompleted");
		task20IsCompleted.addTextNode(taskDetail.getTask20IsCompleted());
		SOAPElement task20Duration = task20.addChildElement("Duration");
		task20Duration.addTextNode(taskDetail.getTask20Duration());

		SOAPElement task21 = taskDetails.addChildElement("Task21");
		SOAPElement task21StartDate = task21.addChildElement("StartDate");
		task21StartDate.addTextNode(taskDetail.getTask21StartDate());
		SOAPElement task21DueDate = task21.addChildElement("DueDate");
		task21DueDate.addTextNode(taskDetail.getTask21DueDate());
		SOAPElement task21ActualStartDate = task21.addChildElement("ActualStartDate");
		task21ActualStartDate.addTextNode(taskDetail.getTask21ActualStartDate());
		SOAPElement task21ActualDueDate = task21.addChildElement("ActualDueDate");
		task21ActualDueDate.addTextNode(taskDetail.getTask21ActualDueDate());
		SOAPElement task21IsActive = task21.addChildElement("IsActive");
		task21IsActive.addTextNode(taskDetail.getTask21IsActive());
		SOAPElement task21IsCompleted = task21.addChildElement("IsCompleted");
		task21IsCompleted.addTextNode(taskDetail.getTask21IsCompleted());
		SOAPElement task21Duration = task21.addChildElement("Duration");
		task21Duration.addTextNode(taskDetail.getTask21Duration());

		SOAPElement task22 = taskDetails.addChildElement("Task22");
		SOAPElement task22StartDate = task22.addChildElement("StartDate");
		task22StartDate.addTextNode(taskDetail.getTask22StartDate());
		SOAPElement task22DueDate = task22.addChildElement("DueDate");
		task22DueDate.addTextNode(taskDetail.getTask22DueDate());
		SOAPElement task22ActualStartDate = task22.addChildElement("ActualStartDate");
		task22ActualStartDate.addTextNode(taskDetail.getTask22ActualStartDate());
		SOAPElement task22ActualDueDate = task22.addChildElement("ActualDueDate");
		task22ActualDueDate.addTextNode(taskDetail.getTask22ActualDueDate());
		SOAPElement task22IsActive = task22.addChildElement("IsActive");
		task22IsActive.addTextNode(taskDetail.getTask22IsActive());
		SOAPElement task22IsCompleted = task22.addChildElement("IsCompleted");
		task22IsCompleted.addTextNode(taskDetail.getTask22IsCompleted());
		SOAPElement task22Duration = task22.addChildElement("Duration");
		task22Duration.addTextNode(taskDetail.getTask22Duration());

		SOAPElement task23 = taskDetails.addChildElement("Task23");
		SOAPElement task23StartDate = task23.addChildElement("StartDate");
		task23StartDate.addTextNode(taskDetail.getTask23StartDate());
		SOAPElement task23DueDate = task23.addChildElement("DueDate");
		task23DueDate.addTextNode(taskDetail.getTask23DueDate());
		SOAPElement task23ActualStartDate = task23.addChildElement("ActualStartDate");
		task23ActualStartDate.addTextNode(taskDetail.getTask23ActualStartDate());
		SOAPElement task23ActualDueDate = task23.addChildElement("ActualDueDate");
		task23ActualDueDate.addTextNode(taskDetail.getTask23ActualDueDate());
		SOAPElement task23IsActive = task23.addChildElement("IsActive");
		task23IsActive.addTextNode(taskDetail.getTask23IsActive());
		SOAPElement task23IsCompleted = task23.addChildElement("IsCompleted");
		task23IsCompleted.addTextNode(taskDetail.getTask23IsCompleted());
		SOAPElement task23Duration = task23.addChildElement("Duration");
		task23Duration.addTextNode(taskDetail.getTask23Duration());

		SOAPElement task24 = taskDetails.addChildElement("Task24");
		SOAPElement task24StartDate = task24.addChildElement("StartDate");
		task24StartDate.addTextNode(taskDetail.getTask24StartDate());
		SOAPElement task24DueDate = task24.addChildElement("DueDate");
		task24DueDate.addTextNode(taskDetail.getTask24DueDate());
		SOAPElement task24ActualStartDate = task24.addChildElement("ActualStartDate");
		task24ActualStartDate.addTextNode(taskDetail.getTask24ActualStartDate());
		SOAPElement task24ActualDueDate = task24.addChildElement("ActualDueDate");
		task24ActualDueDate.addTextNode(taskDetail.getTask24ActualDueDate());
		SOAPElement task24IsActive = task24.addChildElement("IsActive");
		task24IsActive.addTextNode(taskDetail.getTask24IsActive());
		SOAPElement task24IsCompleted = task24.addChildElement("IsCompleted");
		task24IsCompleted.addTextNode(taskDetail.getTask24IsCompleted());
		SOAPElement task24Duration = task24.addChildElement("Duration");
		task24Duration.addTextNode(taskDetail.getTask24Duration());

		SOAPElement task25 = taskDetails.addChildElement("Task25");
		SOAPElement task25StartDate = task25.addChildElement("StartDate");
		task25StartDate.addTextNode(taskDetail.getTask25StartDate());
		SOAPElement task25DueDate = task25.addChildElement("DueDate");
		task25DueDate.addTextNode(taskDetail.getTask25DueDate());
		SOAPElement task25ActualStartDate = task25.addChildElement("ActualStartDate");
		task25ActualStartDate.addTextNode(taskDetail.getTask25ActualStartDate());
		SOAPElement task25ActualDueDate = task25.addChildElement("ActualDueDate");
		task25ActualDueDate.addTextNode(taskDetail.getTask25ActualDueDate());
		SOAPElement task25IsActive = task25.addChildElement("IsActive");
		task25IsActive.addTextNode(taskDetail.getTask25IsActive());
		SOAPElement task25IsCompleted = task25.addChildElement("IsCompleted");
		task25IsCompleted.addTextNode(taskDetail.getTask25IsCompleted());
		SOAPElement task25Duration = task25.addChildElement("Duration");
		task25Duration.addTextNode(taskDetail.getTask25Duration());

		SOAPElement task26 = taskDetails.addChildElement("Task26");
		SOAPElement task26StartDate = task26.addChildElement("StartDate");
		task26StartDate.addTextNode(taskDetail.getTask26StartDate());
		SOAPElement task26DueDate = task26.addChildElement("DueDate");
		task26DueDate.addTextNode(taskDetail.getTask26DueDate());
		SOAPElement task26ActualStartDate = task26.addChildElement("ActualStartDate");
		task26ActualStartDate.addTextNode(taskDetail.getTask26ActualStartDate());
		SOAPElement task26ActualDueDate = task26.addChildElement("ActualDueDate");
		task26ActualDueDate.addTextNode(taskDetail.getTask26ActualDueDate());
		SOAPElement task26IsActive = task26.addChildElement("IsActive");
		task26IsActive.addTextNode(taskDetail.getTask26IsActive());
		SOAPElement task26IsCompleted = task26.addChildElement("IsCompleted");
		task26IsCompleted.addTextNode(taskDetail.getTask26IsCompleted());
		SOAPElement task26Duration = task26.addChildElement("Duration");
		task26Duration.addTextNode(taskDetail.getTask26Duration());

		SOAPElement task27 = taskDetails.addChildElement("Task27");
		SOAPElement task27StartDate = task27.addChildElement("StartDate");
		task27StartDate.addTextNode(taskDetail.getTask27StartDate());
		SOAPElement task27DueDate = task27.addChildElement("DueDate");
		task27DueDate.addTextNode(taskDetail.getTask27DueDate());
		SOAPElement task27ActualStartDate = task27.addChildElement("ActualStartDate");
		task27ActualStartDate.addTextNode(taskDetail.getTask27ActualStartDate());
		SOAPElement task27ActualDueDate = task27.addChildElement("ActualDueDate");
		task27ActualDueDate.addTextNode(taskDetail.getTask27ActualDueDate());
		SOAPElement task27IsActive = task27.addChildElement("IsActive");
		task27IsActive.addTextNode(taskDetail.getTask27IsActive());
		SOAPElement task27IsCompleted = task27.addChildElement("IsCompleted");
		task27IsCompleted.addTextNode(taskDetail.getTask27IsCompleted());
		SOAPElement task27Duration = task27.addChildElement("Duration");
		task27Duration.addTextNode(taskDetail.getTask27Duration());

		SOAPElement task28 = taskDetails.addChildElement("Task28");
		SOAPElement task28StartDate = task28.addChildElement("StartDate");
		task28StartDate.addTextNode(taskDetail.getTask28StartDate());
		SOAPElement task28DueDate = task28.addChildElement("DueDate");
		task28DueDate.addTextNode(taskDetail.getTask28DueDate());
		SOAPElement task28ActualStartDate = task28.addChildElement("ActualStartDate");
		task28ActualStartDate.addTextNode(taskDetail.getTask28ActualStartDate());
		SOAPElement task28ActualDueDate = task28.addChildElement("ActualDueDate");
		task28ActualDueDate.addTextNode(taskDetail.getTask28ActualDueDate());
		SOAPElement task28IsActive = task28.addChildElement("IsActive");
		task28IsActive.addTextNode(taskDetail.getTask28IsActive());
		SOAPElement task28IsCompleted = task28.addChildElement("IsCompleted");
		task28IsCompleted.addTextNode(taskDetail.getTask28IsCompleted());
		SOAPElement task28Duration = task28.addChildElement("Duration");
		task28Duration.addTextNode(taskDetail.getTask28Duration());

		SOAPElement task29 = taskDetails.addChildElement("Task29");
		SOAPElement task29StartDate = task29.addChildElement("StartDate");
		task29StartDate.addTextNode(taskDetail.getTask29StartDate());
		SOAPElement task29DueDate = task29.addChildElement("DueDate");
		task29DueDate.addTextNode(taskDetail.getTask29DueDate());
		SOAPElement task29ActualStartDate = task29.addChildElement("ActualStartDate");
		task29ActualStartDate.addTextNode(taskDetail.getTask29ActualStartDate());
		SOAPElement task29ActualDueDate = task29.addChildElement("ActualDueDate");
		task29ActualDueDate.addTextNode(taskDetail.getTask29ActualDueDate());
		SOAPElement task29IsActive = task29.addChildElement("IsActive");
		task29IsActive.addTextNode(taskDetail.getTask29IsActive());
		SOAPElement task29IsCompleted = task29.addChildElement("IsCompleted");
		task29IsCompleted.addTextNode(taskDetail.getTask29IsCompleted());
		SOAPElement task29Duration = task29.addChildElement("Duration");
		task29Duration.addTextNode(taskDetail.getTask29Duration());

		SOAPElement task30 = taskDetails.addChildElement("Task30");
//		SOAPElement task30StartDate = task30.addChildElement("StartDate");
//		task30StartDate.addTextNode(taskDetail.getTask30StartDate());
		SOAPElement task30DueDate = task30.addChildElement("DueDate");
		task30DueDate.addTextNode(taskDetail.getTask30DueDate());
//		SOAPElement task30ActualStartDate = task30.addChildElement("ActualStartDate");
//		task30ActualStartDate.addTextNode(taskDetail.getTask30ActualStartDate());
		SOAPElement task30ActualDueDate = task30.addChildElement("ActualDueDate");
		task30ActualDueDate.addTextNode(taskDetail.getTask30ActualDueDate());
		SOAPElement task30IsActive = task30.addChildElement("IsActive");
		task30IsActive.addTextNode(taskDetail.getTask30IsActive());
//		SOAPElement task30IsCompleted = task30.addChildElement("IsCompleted");
//		task30IsCompleted.addTextNode(taskDetail.getTask30IsCompleted());
//		SOAPElement task30Duration = task30.addChildElement("Duration");
//		task30Duration.addTextNode(taskDetail.getTask30Duration());

		SOAPElement task31 = taskDetails.addChildElement("Task31");
		SOAPElement task31StartDate = task31.addChildElement("StartDate");
		task31StartDate.addTextNode(taskDetail.getTask31StartDate());
		SOAPElement task31DueDate = task31.addChildElement("DueDate");
		task31DueDate.addTextNode(taskDetail.getTask31DueDate());
		SOAPElement task31ActualStartDate = task31.addChildElement("ActualStartDate");
		task31ActualStartDate.addTextNode(taskDetail.getTask31ActualStartDate());
		SOAPElement task31ActualDueDate = task31.addChildElement("ActualDueDate");
		task31ActualDueDate.addTextNode(taskDetail.getTask31ActualDueDate());
		SOAPElement task31IsActive = task31.addChildElement("IsActive");
		task31IsActive.addTextNode(taskDetail.getTask31IsActive());
		SOAPElement task31IsCompleted = task31.addChildElement("IsCompleted");
		task31IsCompleted.addTextNode(taskDetail.getTask31IsCompleted());
		SOAPElement task31Duration = task31.addChildElement("Duration");
		task31Duration.addTextNode(taskDetail.getTask31Duration());
		SOAPElement task31CustomFields = task31.addChildElement("CustomFields");
		SOAPElement task31Pallet = task31CustomFields.addChildElement("Pallet");
		task31Pallet.addTextNode("");
		SOAPElement task31Tray = task31CustomFields.addChildElement("Tray");
		task31Tray.addTextNode("");

		SOAPElement task32 = taskDetails.addChildElement("Task32");
		SOAPElement task32StartDate = task32.addChildElement("StartDate");
		task32StartDate.addTextNode(taskDetail.getTask32StartDate());
		SOAPElement task32DueDate = task32.addChildElement("DueDate");
		task32DueDate.addTextNode(taskDetail.getTask32DueDate());
		SOAPElement task32ActualStartDate = task32.addChildElement("ActualStartDate");
		task32ActualStartDate.addTextNode(taskDetail.getTask32ActualStartDate());
		SOAPElement task32ActualDueDate = task32.addChildElement("ActualDueDate");
		task32ActualDueDate.addTextNode(taskDetail.getTask32ActualDueDate());
		SOAPElement task32IsActive = task32.addChildElement("IsActive");
		task32IsActive.addTextNode(taskDetail.getTask32IsActive());
		SOAPElement task32IsCompleted = task32.addChildElement("IsCompleted");
		task32IsCompleted.addTextNode(taskDetail.getTask32IsCompleted());
		SOAPElement task32Duration = task32.addChildElement("Duration");
		logger.info(taskDetail.getTask32Duration());
		task32Duration.addTextNode(taskDetail.getTask32Duration());

		SOAPElement task33 = taskDetails.addChildElement("Task33");
		SOAPElement task33StartDate = task33.addChildElement("StartDate");
		task33StartDate.addTextNode(taskDetail.getTask33StartDate());
		SOAPElement task33DueDate = task33.addChildElement("DueDate");
		task33DueDate.addTextNode(taskDetail.getTask33DueDate());
		SOAPElement task33ActualStartDate = task33.addChildElement("ActualStartDate");
		task33ActualStartDate.addTextNode(taskDetail.getTask33ActualStartDate());
		SOAPElement task33ActualDueDate = task33.addChildElement("ActualDueDate");
		task33ActualDueDate.addTextNode(taskDetail.getTask33ActualDueDate());
		SOAPElement task33IsActive = task33.addChildElement("IsActive");
		task33IsActive.addTextNode(taskDetail.getTask33IsActive());
		SOAPElement task33IsCompleted = task33.addChildElement("IsCompleted");
		task33IsCompleted.addTextNode(taskDetail.getTask33IsCompleted());
		SOAPElement task33Duration = task33.addChildElement("Duration");
		task33Duration.addTextNode(taskDetail.getTask33Duration());

		SOAPElement task34 = taskDetails.addChildElement("Task34");
		SOAPElement task34StartDate = task34.addChildElement("StartDate");
		task34StartDate.addTextNode(taskDetail.getTask34StartDate());
		SOAPElement task34DueDate = task34.addChildElement("DueDate");
		task34DueDate.addTextNode(taskDetail.getTask34DueDate());
		SOAPElement task34ActualStartDate = task34.addChildElement("ActualStartDate");
		task34ActualStartDate.addTextNode(taskDetail.getTask34ActualStartDate());
		SOAPElement task34ActualDueDate = task34.addChildElement("ActualDueDate");
		task34ActualDueDate.addTextNode(taskDetail.getTask34ActualDueDate());
		SOAPElement task34IsActive = task34.addChildElement("IsActive");
		task34IsActive.addTextNode(taskDetail.getTask34IsActive());
		SOAPElement task34IsCompleted = task34.addChildElement("IsCompleted");
		task34IsCompleted.addTextNode(taskDetail.getTask34IsCompleted());
		SOAPElement task34Duration = task34.addChildElement("Duration");
		task34Duration.addTextNode(taskDetail.getTask34Duration());

		SOAPElement task35 = taskDetails.addChildElement("Task35");
		SOAPElement task35StartDate = task35.addChildElement("StartDate");
		task35StartDate.addTextNode(taskDetail.getTask35StartDate());
		SOAPElement task35DueDate = task35.addChildElement("DueDate");
		task35DueDate.addTextNode(taskDetail.getTask35DueDate());
		SOAPElement task35ActualStartDate = task35.addChildElement("ActualStartDate");
		task35ActualStartDate.addTextNode(taskDetail.getTask35ActualStartDate());
		SOAPElement task35ActualDueDate = task35.addChildElement("ActualDueDate");
		task35ActualDueDate.addTextNode(taskDetail.getTask35ActualDueDate());
		SOAPElement task35IsActive = task35.addChildElement("IsActive");
		task35IsActive.addTextNode(taskDetail.getTask35IsActive());
		SOAPElement task35IsCompleted = task35.addChildElement("IsCompleted");
		task35IsCompleted.addTextNode(taskDetail.getTask35IsCompleted());
		SOAPElement task35Duration = task35.addChildElement("Duration");
		task35Duration.addTextNode(taskDetail.getTask35Duration());

		SOAPElement task36 = taskDetails.addChildElement("Task36");
		SOAPElement task36StartDate = task36.addChildElement("StartDate");
		task36StartDate.addTextNode(taskDetail.getTask36StartDate());
		SOAPElement task36DueDate = task36.addChildElement("DueDate");
		task36DueDate.addTextNode(taskDetail.getTask36DueDate());
		SOAPElement task36ActualStartDate = task36.addChildElement("ActualStartDate");
		task36ActualStartDate.addTextNode(taskDetail.getTask36ActualStartDate());
		SOAPElement task36ActualDueDate = task36.addChildElement("ActualDueDate");
		task36ActualDueDate.addTextNode(taskDetail.getTask36ActualDueDate());
		SOAPElement task36IsActive = task36.addChildElement("IsActive");
		task36IsActive.addTextNode(taskDetail.getTask36IsActive());
		SOAPElement task36IsCompleted = task36.addChildElement("IsCompleted");
		task36IsCompleted.addTextNode(taskDetail.getTask36IsCompleted());
		SOAPElement task36Duration = task36.addChildElement("Duration");
		task36Duration.addTextNode(taskDetail.getTask36Duration());
		SOAPElement task36CustomFields = task36.addChildElement("CustomFields");
		SOAPElement task36AllowProdSchedule = task36CustomFields.addChildElement("AllowProdSchedule");
		task36AllowProdSchedule.addTextNode(taskDetail.getTask36AllowProdSchedule());
		SOAPElement task36BOMSentActual = task36CustomFields.addChildElement("BOMSentActual");
		task36BOMSentActual.addTextNode("");
		SOAPElement task36WeeksDifference = task36CustomFields.addChildElement("WeeksDifference");
		task36WeeksDifference.addTextNode("");

		SOAPElement task37 = taskDetails.addChildElement("Task37");
		SOAPElement task37StartDate = task37.addChildElement("StartDate");
		task37StartDate.addTextNode(taskDetail.getTask37StartDate());
		SOAPElement task37DueDate = task37.addChildElement("DueDate");
		task37DueDate.addTextNode(taskDetail.getTask37DueDate());
		SOAPElement task37ActualStartDate = task37.addChildElement("ActualStartDate");
		task37ActualStartDate.addTextNode(taskDetail.getTask37ActualStartDate());
		SOAPElement task37ActualDueDate = task37.addChildElement("ActualDueDate");
		task37ActualDueDate.addTextNode(taskDetail.getTask37ActualDueDate());
		SOAPElement task37IsActive = task37.addChildElement("IsActive");
		task37IsActive.addTextNode(taskDetail.getTask37IsActive());
		SOAPElement task37IsCompleted = task37.addChildElement("IsCompleted");
		task37IsCompleted.addTextNode(taskDetail.getTask37IsCompleted());
		SOAPElement task37Duration = task37.addChildElement("Duration");
		task37Duration.addTextNode(taskDetail.getTask37Duration());

		SOAPElement task38 = taskDetails.addChildElement("Task38");
		SOAPElement task38StartDate = task38.addChildElement("StartDate");
		task38StartDate.addTextNode(taskDetail.getTask38StartDate());
		SOAPElement task38DueDate = task38.addChildElement("DueDate");
		task38DueDate.addTextNode(taskDetail.getTask38DueDate());
		SOAPElement task38ActualStartDate = task38.addChildElement("ActualStartDate");
		task38ActualStartDate.addTextNode(taskDetail.getTask38ActualStartDate());
		SOAPElement task38ActualDueDate = task38.addChildElement("ActualDueDate");
		task38ActualDueDate.addTextNode(taskDetail.getTask38ActualDueDate());
		SOAPElement task38IsActive = task38.addChildElement("IsActive");
		task38IsActive.addTextNode(taskDetail.getTask38IsActive());
		SOAPElement task38IsCompleted = task38.addChildElement("IsCompleted");
		task38IsCompleted.addTextNode(taskDetail.getTask38IsCompleted());
		SOAPElement task38Duration = task38.addChildElement("Duration");
		task38Duration.addTextNode(taskDetail.getTask38Duration());

		SOAPElement task39 = taskDetails.addChildElement("Task39");
		SOAPElement task39StartDate = task39.addChildElement("StartDate");
		task39StartDate.addTextNode(taskDetail.getTask39StartDate());
		SOAPElement task39DueDate = task39.addChildElement("DueDate");
		task39DueDate.addTextNode(taskDetail.getTask39DueDate());
		SOAPElement task39ActualStartDate = task39.addChildElement("ActualStartDate");
		task39ActualStartDate.addTextNode(taskDetail.getTask39ActualStartDate());
		SOAPElement task39ActualDueDate = task39.addChildElement("ActualDueDate");
		task39ActualDueDate.addTextNode(taskDetail.getTask39ActualDueDate());
		SOAPElement task39IsActive = task39.addChildElement("IsActive");
		task39IsActive.addTextNode(taskDetail.getTask39IsActive());
		SOAPElement task39IsCompleted = task39.addChildElement("IsCompleted");
		task39IsCompleted.addTextNode(taskDetail.getTask39IsCompleted());
		SOAPElement task39Duration = task39.addChildElement("Duration");
		task39Duration.addTextNode(taskDetail.getTask39Duration());
		SOAPElement task39CustomFields = task39.addChildElement("CustomFields");
		SOAPElement task39TrialDateConfirmedWithBottler = task39CustomFields
				.addChildElement("TrialDateConfirmedWithBottler");
		task39TrialDateConfirmedWithBottler.addTextNode(taskDetail.getTask39TrialDateConfirmedWithBottler());

		SOAPElement task40 = taskDetails.addChildElement("Task40");
		SOAPElement task40StartDate = task40.addChildElement("StartDate");
		task40StartDate.addTextNode(taskDetail.getTask40StartDate());
		SOAPElement task40DueDate = task40.addChildElement("DueDate");
		task40DueDate.addTextNode(taskDetail.getTask40DueDate());
		SOAPElement task40ActualStartDate = task40.addChildElement("ActualStartDate");
		task40ActualStartDate.addTextNode(taskDetail.getTask40ActualStartDate());
		SOAPElement task40ActualDueDate = task40.addChildElement("ActualDueDate");
		task40ActualDueDate.addTextNode(taskDetail.getTask40ActualDueDate());
		SOAPElement task40IsActive = task40.addChildElement("IsActive");
		task40IsActive.addTextNode(taskDetail.getTask40IsActive());
		SOAPElement task40IsCompleted = task40.addChildElement("IsCompleted");
		task40IsCompleted.addTextNode(taskDetail.getTask40IsCompleted());
		SOAPElement task40Duration = task40.addChildElement("Duration");
		task40Duration.addTextNode(taskDetail.getTask40Duration());

		SOAPElement task41 = taskDetails.addChildElement("Task41");
		SOAPElement task41StartDate = task41.addChildElement("StartDate");
		task41StartDate.addTextNode(taskDetail.getTask41StartDate());
		SOAPElement task41DueDate = task41.addChildElement("DueDate");
		task41DueDate.addTextNode(taskDetail.getTask41DueDate());
		SOAPElement task41ActualStartDate = task41.addChildElement("ActualStartDate");
		task41ActualStartDate.addTextNode(taskDetail.getTask41ActualStartDate());
		SOAPElement task41ActualDueDate = task41.addChildElement("ActualDueDate");
		task41ActualDueDate.addTextNode(taskDetail.getTask41ActualDueDate());
		SOAPElement task41IsActive = task41.addChildElement("IsActive");
		task41IsActive.addTextNode(taskDetail.getTask41IsActive());
		SOAPElement task41IsCompleted = task41.addChildElement("IsCompleted");
		task41IsCompleted.addTextNode(taskDetail.getTask41IsCompleted());
		SOAPElement task41Duration = task41.addChildElement("Duration");
		task41Duration.addTextNode(taskDetail.getTask41Duration());
		SOAPElement task41CustomFields = task41.addChildElement("CustomFields");
		SOAPElement task41FinalAgreedFirstProd = task41CustomFields.addChildElement("FinalAgreedFirstProd");
		task41FinalAgreedFirstProd.addTextNode("");

		SOAPElement task42 = taskDetails.addChildElement("Task42");
		SOAPElement task42StartDate = task42.addChildElement("StartDate");
		task42StartDate.addTextNode(taskDetail.getTask42StartDate());
		SOAPElement task42DueDate = task42.addChildElement("DueDate");
		task42DueDate.addTextNode(taskDetail.getTask42DueDate());
		SOAPElement task42ActualStartDate = task42.addChildElement("ActualStartDate");
		task42ActualStartDate.addTextNode(taskDetail.getTask42ActualStartDate());
		SOAPElement task42ActualDueDate = task42.addChildElement("ActualDueDate");
		task42ActualDueDate.addTextNode(taskDetail.getTask42ActualDueDate());
		SOAPElement task42IsActive = task42.addChildElement("IsActive");
		task42IsActive.addTextNode(taskDetail.getTask42IsActive());
		SOAPElement task42IsCompleted = task42.addChildElement("IsCompleted");
		task42IsCompleted.addTextNode(taskDetail.getTask42IsCompleted());
		SOAPElement task42Duration = task42.addChildElement("Duration");
		task42Duration.addTextNode(taskDetail.getTask42Duration());
		return requestData;

	}

	/**
	 * 
	 * @param start - index of the next value to be passed to the soap Api request
	 * @return - return soap message
	 * @throws Exception
	 */
	private SOAPMessage createSOAPRequest(int start) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();

		createSoapEnvelope(soapMessage, start);

		soapMessage.saveChanges();
		logger.info("GetUserRequest:");
		logger.info(utilService.convertSOAPMessageToString(soapMessage));

		return soapMessage;
	}

}
