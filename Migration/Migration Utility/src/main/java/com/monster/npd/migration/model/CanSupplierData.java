package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "shrdnpdlookupdomainmodelcan_supplier_data")
@Entity
public class CanSupplierData {
	@Id
    @Column(name = "Id")
    private Integer id;

    @Column(name = "cancompany")
    private String canCompany;
    
    @Column(name = "s_organizationid")
    private Integer sOrganizationId;

}
