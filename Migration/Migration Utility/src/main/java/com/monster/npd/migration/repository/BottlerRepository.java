package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.Bottler;

@Repository
public interface BottlerRepository extends JpaRepository<Bottler, Integer> {
	
	Bottler findByDisplayName(String name);

}
