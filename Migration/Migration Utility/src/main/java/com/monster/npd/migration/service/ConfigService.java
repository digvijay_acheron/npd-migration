package com.monster.npd.migration.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.monster.npd.migration.model.AuditUsers;
import com.monster.npd.migration.model.Bottler;
import com.monster.npd.migration.model.Brand;
import com.monster.npd.migration.model.BusinessUnit;
import com.monster.npd.migration.model.CPProjectType;
import com.monster.npd.migration.model.CanSupplierData;
import com.monster.npd.migration.model.CasePackSize;
import com.monster.npd.migration.model.ConsumerUnitSize;
import com.monster.npd.migration.model.DraftManufacturingLocation;
import com.monster.npd.migration.model.EarlyManufacturingSite;
import com.monster.npd.migration.model.GovernanceApproach;
import com.monster.npd.migration.model.MPMStatus;
import com.monster.npd.migration.model.MPMTeamRole;
import com.monster.npd.migration.model.Market;
import com.monster.npd.migration.model.OpenTextIdentity;
import com.monster.npd.migration.model.Platform;
import com.monster.npd.migration.model.PrimaryPackaging;
import com.monster.npd.migration.model.PrintSupplier;
import com.monster.npd.migration.model.ProjectClassifierData;
import com.monster.npd.migration.model.ProjectType;
import com.monster.npd.migration.model.ReportingDeliveryQuarter;
import com.monster.npd.migration.model.ReportingProjectDetail;
import com.monster.npd.migration.model.ReportingProjectSubType;
import com.monster.npd.migration.model.RequestType;
import com.monster.npd.migration.model.SKUDetails;
import com.monster.npd.migration.model.SecondaryPackLeadTimes;
import com.monster.npd.migration.model.TaskRoleMappingDependencies;
import com.monster.npd.migration.model.TastingRequirement;
import com.monster.npd.migration.model.TechQualUsers;
import com.monster.npd.migration.model.TrialProtocolWrittenBy;
import com.monster.npd.migration.model.TrialSupervisionNPD;
import com.monster.npd.migration.model.TrialSupervisionQuality;
import com.monster.npd.migration.model.VariantSKU;
import com.monster.npd.migration.repository.AuditLogRepository;
import com.monster.npd.migration.repository.AuditUsersRepository;
import com.monster.npd.migration.repository.BottlerRepository;
import com.monster.npd.migration.repository.BrandRepository;
import com.monster.npd.migration.repository.BusinessUnitRepository;
import com.monster.npd.migration.repository.CPProjectTypeRepository;
import com.monster.npd.migration.repository.CanSupplierDataRepository;
import com.monster.npd.migration.repository.CasePackSizeRepository;
import com.monster.npd.migration.repository.CommercialMarketScopeRepository;
import com.monster.npd.migration.repository.ConsumenrUnitSizeRepository;
import com.monster.npd.migration.repository.DeliveryQuarterRepository;
import com.monster.npd.migration.repository.DraftManufacturingLocationRepository;
import com.monster.npd.migration.repository.EarlyManufacturingSiteRepository;
import com.monster.npd.migration.repository.GovernanceApproachRepository;
import com.monster.npd.migration.repository.GovernanceMilestoneRepository;
import com.monster.npd.migration.repository.MPMDeliverableRepository;
import com.monster.npd.migration.repository.MPMProjectRepository;
import com.monster.npd.migration.repository.MPMStatusRepository;
import com.monster.npd.migration.repository.MPMTaskRepository;
import com.monster.npd.migration.repository.MPMTeamRoleRepository;
import com.monster.npd.migration.repository.MarketRepository;
import com.monster.npd.migration.repository.NPDProjectRepository;
import com.monster.npd.migration.repository.NPDTaskRepository;
import com.monster.npd.migration.repository.OpenTextIdentityRepository;
import com.monster.npd.migration.repository.PlatformRepository;
import com.monster.npd.migration.repository.PrimaryPackagingTypeRepository;
import com.monster.npd.migration.repository.PrintSupplierRepository;
import com.monster.npd.migration.repository.ProjectClassifierRepository;
import com.monster.npd.migration.repository.ProjectDetailRepository;
import com.monster.npd.migration.repository.ProjectSubTypeRepository;
import com.monster.npd.migration.repository.ProjectTypeRepository;
import com.monster.npd.migration.repository.RequestRepository;
import com.monster.npd.migration.repository.RequestTypeRepository;
import com.monster.npd.migration.repository.SKUDetailsRepository;
import com.monster.npd.migration.repository.SecondaryPackLeadTimeRepository;
import com.monster.npd.migration.repository.SecondaryPackLeadTimesRepository;
import com.monster.npd.migration.repository.SecondaryPackRepository;
import com.monster.npd.migration.repository.TaskRoleMappingRepository;
import com.monster.npd.migration.repository.TastingRequirementRepository;
import com.monster.npd.migration.repository.TechQualUserRepository;
import com.monster.npd.migration.repository.TechnicalMarketScopeRepository;
import com.monster.npd.migration.repository.TrialProtocolWrittenByRepository;
import com.monster.npd.migration.repository.TrialSupervisionNPDRepository;
import com.monster.npd.migration.repository.TrialSupervisionQualityRepository;
import com.monster.npd.migration.repository.VariantSKURepository;
import com.monster.npd.migration.repository.WorkflowRulesRepository;

@Service
public class ConfigService {
	
	@Autowired
	private BottlerRepository bottlerRepository;
	
	@Autowired
	private ProjectTypeRepository projectTypeRepository;
	
	@Autowired
	private PrimaryPackagingTypeRepository primaryPackagingTypeRepository;
	
	@Autowired
	private RequestTypeRepository requestTypeRepository;
	
	@Autowired
	private VariantSKURepository variantSKURepository;
	
	@Autowired
	private DraftManufacturingLocationRepository draftManufacturingLocationRepository;
	
	@Autowired
	private EarlyManufacturingSiteRepository earlyManufacturingSiteRepository;
	
	@Autowired
	private PlatformRepository platformRepository;
	
	@Autowired
	private BrandRepository brandRepository;
	
	@Autowired
	private BusinessUnitRepository businessUnitRepository;
	
	@Autowired
	private CasePackSizeRepository casePackSizeRepository;
	
	@Autowired
	private ConsumenrUnitSizeRepository consumenrUnitSizeRepository;
	
	@Autowired
	private SKUDetailsRepository SKUDetailsRepository;
	
	@Autowired
	private ProjectClassifierRepository projectClassifierRepository;
	
	@Autowired
	private CPProjectTypeRepository CPProjectTypeRepository;
	
	@Autowired
	private ProjectSubTypeRepository projectSubTypeRepository;
	
	@Autowired
	private ProjectDetailRepository projectDetailRepository;
	
	@Autowired
	private DeliveryQuarterRepository deliveryQuarterRepository;
	
	@Autowired
	private MarketRepository marketRepository;
	
	@Autowired
	private SecondaryPackLeadTimeRepository secondaryPackLeadTimeRepository;
	
	@Autowired
	private PrintSupplierRepository printSupplierRepository;
	
	@Autowired
	private TechQualUserRepository techQualUserRepository;
	
	@Autowired
	private TrialSupervisionNPDRepository trialSupervisionNPDRepository;
	
	@Autowired
	private TrialSupervisionQualityRepository trialSupervisionQualityRepository;
	
	@Autowired
	private TrialProtocolWrittenByRepository trialProtocolWrittenByRepository;
	
	@Autowired
	private TastingRequirementRepository tastingRequirementRepository;
	
	@Autowired
	private AuditUsersRepository auditUsersRepository;
	
	@Autowired
	OpenTextIdentityRepository openTextIdentityRepository;
	
	@Autowired
	TechnicalMarketScopeRepository technicalMarketScopeRepository;
	
	@Autowired
	CommercialMarketScopeRepository commercialMarketScopeRepository;
	
	@Autowired
	MPMStatusRepository MPMStatusRepository;
	
	@Autowired
	AuditLogRepository auditLogRepository;
	
	@Autowired
	RequestRepository requestRepository;
	
	@Autowired
	MPMProjectRepository MPMProjectRepository;
	
	@Autowired
	NPDProjectRepository NPDProjectRepository;
	
	@Autowired
	MPMTaskRepository MPMTaskRepository;
	
	@Autowired
	NPDTaskRepository NPDTaskRepository;
	
	@Autowired
	MPMDeliverableRepository MPMDeliverableRepository;
	
	@Autowired
	MPMTeamRoleRepository MPMTeamRoleRepository;
	
	@Autowired
	WorkflowRulesRepository workflowRulesRepository;
	
	@Autowired
	SecondaryPackLeadTimesRepository secondaryPackLeadTimesRepository;
	
	@Autowired
	SecondaryPackRepository secondaryPackRepository;
	
	@Autowired
	GovernanceMilestoneRepository governanceMilestoneRepository;
	
	@Autowired
	GovernanceApproachRepository governanceApproachRepository;
	
	@Autowired
	TaskRoleMappingRepository taskRoleMappingRepository;
	
	@Autowired
	CanSupplierDataRepository canSupplierDataRepository;
	
	
	@Autowired
	UtilService utilService;
	
	@Autowired
	Environment env;
	
	private static List<MPMStatus> MPMStatusList = new ArrayList<MPMStatus>();
	
	private static Integer maxAuditLogId = 0;
	private static Integer maxRequestId = 0;
	private static Integer maxMPMProjectId = 0;
	private static Integer maxNPDProjectId = 0;
	private static Integer maxMPMTaskId = 0;
	private static Integer maxNPDTaskId = 0;
	private static Integer maxDeliverableId = 0;
	private static Integer maxWorkflowRuleId = 0;
	private static Integer maxTechMarketScopeId = 0;
	private static Integer maxComMarketScopeId = 0;
	private static Integer maxSecondaryPackId = 0;
	private static Integer maxGovMilestoneId = 0;
	
	private static List<AuditUsers> AuditUsersList = new ArrayList<AuditUsers>();
	private static List<TechQualUsers> TechQualUsersList = new ArrayList<TechQualUsers>();
	private static List<TrialProtocolWrittenBy> TrialProtocolWrittenByList = new ArrayList<TrialProtocolWrittenBy>();
	private static List<TrialSupervisionNPD> TrialSupervisionNPDList = new ArrayList<TrialSupervisionNPD>();
	private static List<TrialSupervisionQuality> TrialSupervisionQualityList = new ArrayList<TrialSupervisionQuality>();
	private static List<TastingRequirement> TastingRequirementList = new ArrayList<TastingRequirement>();
	
	private static List<CanSupplierData> canSupplierDataList = new ArrayList<CanSupplierData>();
	
	private static List<MPMTeamRole> mpmTeamRoleList = new ArrayList<MPMTeamRole>();
	private static List<TaskRoleMappingDependencies> taskRoleMappingList = new ArrayList<TaskRoleMappingDependencies>();
	private static List<GovernanceApproach> governanceApproachList = new ArrayList<GovernanceApproach>();
	private static List<SecondaryPackLeadTimes> secondaryPackLeadTimesList = new ArrayList<SecondaryPackLeadTimes>();
	private static List<PrintSupplier> printSupplierList = new ArrayList<PrintSupplier>();
	
	private static List<PrimaryPackaging> primaryPackagingList = new ArrayList<PrimaryPackaging>();
	private static List<ProjectType> projectTypeList = new ArrayList<ProjectType>();
	private static List<Bottler> bottlerList = new ArrayList<Bottler>();
	
	private static List<RequestType> requestTypeList = new ArrayList<RequestType>();
	private static List<VariantSKU> variantSKUList = new ArrayList<>();
	private static List<DraftManufacturingLocation> draftManufacturingLocationList = new ArrayList<DraftManufacturingLocation>();
	private static List<EarlyManufacturingSite> earlyManufacturingSiteList = new ArrayList<EarlyManufacturingSite>();
	private static List<Platform> platformList = new ArrayList<Platform>();
	private static List<Brand> brandList = new ArrayList<Brand>();
	
	private static List<Market> marketList = new ArrayList<Market>();
	private static List<BusinessUnit> businessUnitList = new ArrayList<BusinessUnit>();
	private static List<CasePackSize> casePackSizeList = new ArrayList<CasePackSize>();
	private static List<ConsumerUnitSize> consumerUnitSizeList = new ArrayList<ConsumerUnitSize>();
	private static List<SKUDetails> SKUDetailsList = new ArrayList<SKUDetails>();
	private static List<ProjectClassifierData> projectClassifierList = new ArrayList<ProjectClassifierData>();
	
	private static List<CPProjectType> CPProjectTypeList = new ArrayList<CPProjectType>();
	private static List<ReportingProjectSubType> projectSubTypeList = new ArrayList<ReportingProjectSubType>();
	private static List<ReportingProjectDetail> projectDetailList = new ArrayList<ReportingProjectDetail>();
	private static List<ReportingDeliveryQuarter> deliveryQuarterList = new ArrayList<ReportingDeliveryQuarter>();
	private static List<OpenTextIdentity> usersList = new ArrayList<OpenTextIdentity>();

	
	public static Integer getMaxAuditLogId() {
		return maxAuditLogId++;
	}
	
	public static Integer getMaxRequestId() {
		return maxRequestId++;
	}
	
	public static Integer getMaxMPMProjectId() {
		return maxMPMProjectId++;
	}
	
	public static Integer getMaxNPDProjectId() {
		return maxNPDProjectId++;
	}
	
	public static Integer getMaxMPMTaskId() {
		return maxMPMTaskId++;
	}
	
	public static Integer getMaxNPDTaskId() {
		return maxNPDTaskId++;
	}
	
	public static Integer getMaxDeliverableId() {
		return maxDeliverableId++;
	}
	
	public static Integer getMaxWorkflowRuleId() {
		return maxWorkflowRuleId++;
	}
	
	public static Integer getMaxTechMarketScopeId() {
		return maxTechMarketScopeId++;
	}
	
	public static Integer getComMarketScopeId() {
		return maxComMarketScopeId++;
	}
	
	public static Integer getSecondaryPackId() {
		return maxSecondaryPackId++;
	}
	
	public static Integer getGovMilestoneId() {
		return maxGovMilestoneId++;
	}
	
	
	public static List<AuditUsers> getAuditUsersList() {
		return AuditUsersList;
	}
	public static List<TechQualUsers> getTechQualUsersList() {
		return TechQualUsersList;
	}
	public static List<TrialProtocolWrittenBy> getTrialProtocolWrittenByList() {
		return TrialProtocolWrittenByList;
	}
	public static List<TrialSupervisionNPD> getTrialSupervisionNPDList() {
		return TrialSupervisionNPDList;
	}
	public static List<TrialSupervisionQuality> getTrialSupervisionQualityList() {
		return TrialSupervisionQualityList;
	}
	public static List<TastingRequirement> getTastingRequirementList() {
		return TastingRequirementList;
	}
	
	public static List<CanSupplierData> getCanSupplierDataList() {
		return canSupplierDataList;
	}
	
	public static List<MPMTeamRole> getMPMTeamRoleList() {
		return mpmTeamRoleList;
	}
	
	public static List<TaskRoleMappingDependencies> getTaskRoleMappingList() {
		return taskRoleMappingList;
	}
	
	public static List<GovernanceApproach> getgovernanceApproachList() {
		return governanceApproachList;
	}
	
	public static List<SecondaryPackLeadTimes> getSecondaryPackLeadTimesList() {
		return secondaryPackLeadTimesList;
	}
	
	public static List<PrintSupplier> getPrintSupplierList() {
		return printSupplierList;
	}
	
	public static List<MPMStatus> getMPMStatusList() {
		return MPMStatusList;
	}
	
	public static List<PrimaryPackaging> getPrimaryPackagingList() {
		return primaryPackagingList;
	}
	
	public static List<ProjectType> getProjectTypeList() {
		return projectTypeList;
	}
	
	public static List<Bottler> getBottlerList() {
		return bottlerList;
	}
	
	public static List<RequestType> getRequestTypeList() {
		return requestTypeList;
	}
	
	public static List<VariantSKU> getVariantSKUList() {
		return variantSKUList;
	}
	
	public static List<DraftManufacturingLocation> getDraftManufacturingLocationList() {
		return draftManufacturingLocationList;
	}
	public static List<EarlyManufacturingSite> getEarlyManufacturingSiteList() {
		return earlyManufacturingSiteList;
	}
	public static List<Platform> getPlatformList() {
		return platformList;
	}
	public static List<Brand> getBrandList() {
		return brandList;
	}
	public static List<Market> getMarketList() {
		return marketList;
	}
	public static List<BusinessUnit> getBusinessUnitList() {
		return businessUnitList;
	}
	public static List<CasePackSize> getCasePackSizeList() {
		return casePackSizeList;
	}
	public static List<ConsumerUnitSize> getConsumerUnitSizeList() {
		return consumerUnitSizeList;
	}
	public static List<SKUDetails> getSKUDetailsList() {
		return SKUDetailsList;
	}
	
	public static List<ProjectClassifierData> getProjectClassifierList() {
		return projectClassifierList;
	}
	public static List<CPProjectType> getCPProjectTypeList() {
		return CPProjectTypeList;
	}
	public static List<ReportingProjectSubType> getProjectSubTypeList() {
		return projectSubTypeList;
	}
	
	public static List<ReportingProjectDetail> getProjectDetailList() {
		return projectDetailList;
	}
	public static List<ReportingDeliveryQuarter> getDeliveryQuarterList() {
		return deliveryQuarterList;
	}
	
	public static List<OpenTextIdentity> getUsersList() {
		return usersList;
	}

	
	public void getAllConfigs() {
//		maxAuditLogId = auditLogRepository.maxAuditLogId();
//		maxAuditLogId++;
		
//		maxRequestId = requestRepository.maxRequestId();
//		maxRequestId++;
//		
//		maxMPMProjectId = MPMProjectRepository.maxMPMProjectId();
//		maxMPMProjectId++;
//		
//		maxNPDProjectId = NPDProjectRepository.maxNPDProjectId();
//		maxNPDProjectId++;
//		
//		maxMPMTaskId = MPMTaskRepository.maxMPMTaskId();
//		maxMPMTaskId++;
//		
//		maxNPDTaskId = NPDTaskRepository.maxNPDTaskId();
//		maxNPDTaskId++;
//		
//		maxDeliverableId = MPMDeliverableRepository.maxDeliverableId();
//		maxDeliverableId++;
//		
//		maxWorkflowRuleId = workflowRulesRepository.maxWorkflowRuleId();
//		maxWorkflowRuleId++;
//		
//		maxTechMarketScopeId = technicalMarketScopeRepository.maxTechnicalMarketScopeId();
//		maxTechMarketScopeId++;
//		
//		maxComMarketScopeId = commercialMarketScopeRepository.maxCommercialMarketScopeId();
//		maxComMarketScopeId++;
//		
//		maxSecondaryPackId = secondaryPackRepository.maxSecondaryPackId();
//		maxSecondaryPackId++;
//		
//		maxGovMilestoneId = governanceMilestoneRepository.maxGovernanceMilestoneId();
//		maxGovMilestoneId++;
		
		AuditUsersList = auditUsersRepository.findAll();
		TechQualUsersList = techQualUserRepository.findAll();		
		TrialProtocolWrittenByList = trialProtocolWrittenByRepository.findAll();	
		TrialSupervisionNPDList = trialSupervisionNPDRepository.findAll();
		TrialSupervisionQualityList = trialSupervisionQualityRepository.findAll();	
		TastingRequirementList = tastingRequirementRepository.findAll();
		canSupplierDataList = canSupplierDataRepository.findAll();
		mpmTeamRoleList = MPMTeamRoleRepository.findAll();
		taskRoleMappingList = taskRoleMappingRepository.findAll();
		governanceApproachList = governanceApproachRepository.findAll();
		secondaryPackLeadTimesList = secondaryPackLeadTimesRepository.findAll();
		printSupplierList = printSupplierRepository.findAll();
		MPMStatusList = MPMStatusRepository.findByOrganizationid(Integer.parseInt(env.getProperty("npd.org.id")));
		primaryPackagingList = primaryPackagingTypeRepository.findAll();
		projectTypeList = projectTypeRepository.findAll();
		bottlerList = bottlerRepository.findAll();
		requestTypeList = requestTypeRepository.findAll();
		variantSKUList = variantSKURepository.findAll();
		draftManufacturingLocationList = draftManufacturingLocationRepository.findAll();
		earlyManufacturingSiteList = earlyManufacturingSiteRepository.findAll();		
		platformList = platformRepository.findAll();
		brandList = brandRepository.findAll();
		marketList = marketRepository.findAll();
		businessUnitList = businessUnitRepository.findAll();
		casePackSizeList = casePackSizeRepository.findAll();
		consumerUnitSizeList = consumenrUnitSizeRepository.findAll();		
		SKUDetailsList = SKUDetailsRepository.findAll();
		projectClassifierList = projectClassifierRepository.findAll();		
		CPProjectTypeList = CPProjectTypeRepository.findAll();
		projectSubTypeList = projectSubTypeRepository.findAll();
		projectDetailList = projectDetailRepository.findAll();
		deliveryQuarterList = deliveryQuarterRepository.findAll();		
		usersList = openTextIdentityRepository.findAll();
		
	} 


}
