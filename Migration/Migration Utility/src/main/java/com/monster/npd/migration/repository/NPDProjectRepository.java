package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.NPDProject;

@Repository
public interface NPDProjectRepository extends JpaRepository<NPDProject, Integer> {
	
	@Query("SELECT MAX(p.id) FROM NPDProject p")
    Integer maxNPDProjectId();
	
	@Query(value = "SELECT NEXT VALUE FOR dbo.npd_project_seq", nativeQuery = true)
    public Integer getCurrentVal();

}
