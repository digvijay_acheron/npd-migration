package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.WorkflowRules;

@Repository
public interface WorkflowRulesRepository extends JpaRepository<WorkflowRules, Integer> {
	
	@Query("SELECT MAX(p.id) FROM WorkflowRules p")
    Integer maxWorkflowRuleId();

	
	@Query(value = "SELECT NEXT VALUE FOR dbo.workflow_rules_seq", nativeQuery = true)
    public Integer getCurrentVal();
}
