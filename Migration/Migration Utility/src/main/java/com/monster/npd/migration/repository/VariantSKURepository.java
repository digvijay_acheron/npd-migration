package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.VariantSKU;

@Repository
public interface VariantSKURepository extends JpaRepository<VariantSKU, Integer> {
	
	VariantSKU findByDisplayName(String name);

}
