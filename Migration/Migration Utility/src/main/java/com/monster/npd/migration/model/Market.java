package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "shrdnpdlookupdomainmodelmarkets")
@Entity //O2NPDLookUpDomainModelMarket
public class Market {
	
    @Id
    @Column(name = "Id", updatable = false)
    private String id;

    @Column(name = "displayname", updatable = false)
    private String displayName;
    
    @Column(name = "name", updatable = false)
    private String name;
    
    
    @OneToOne
    @JoinColumn(name = "R_PO_BUSINESS_UNIT_Id", referencedColumnName = "Id", insertable = false, updatable = false)
    private BusinessUnit businessUnit;
    
    @Column(name = "conversionrateeuro", updatable = false)
    private Integer conversionRateEuro;
    
    @Column(name = "caselabelformat", updatable = false)
    private String caseLabelFormat;
    
    @Column(name = "palletlabelformat", updatable = false)
    private String palletLabelFormat;
    
    @Column(name = "productinforequiredweeks", updatable = false)
    private Integer productInfoRequiredWeeks;
    
    @Column(name = "regnotification", updatable = false)
    private Boolean regNotification;
    
    @Column(name = "regulatoryclassification", updatable = false)
    private String regulatoryClassification;
    
    @Column(name = "bottlerpalletlabelcodedp", updatable = false)
    private Integer bottlerPalletLabelCodeDP;
    
    

}
