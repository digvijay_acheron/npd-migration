package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "shrdnpdsubmissionrequestgovernance_milestone")
@Entity
@IdClass(RequestGovernanceMilestoneId.class) 
public class RequestGovernanceMilestone {
	
	@Id
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "requestidA8D18C068F996305")
    private Request requestGovMilestone;

	@Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "governance_milestone_id")
    private GovernanceMilestone governanceMilestone;

    
    @Column(name = "s_organizationid")
    private int organizationid;

}
