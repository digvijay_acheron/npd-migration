package com.monster.npd;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import com.monster.npd.migration.service.UtilService;

/**
 * 
 * @author JeevaR
 *
 */


//@PropertySources({ @PropertySource(value = "classpath:application.yml")})
@SpringBootApplication
public class MigrationUtilityApplication extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(MigrationUtilityApplication.class);
	}

	/**
	 * Main application void main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(MigrationUtilityApplication.class, args);
	}

	/**
	 * Main Application run method
	 */
//	@Override
//	public void run(String... args) throws Exception {
////		UtilService service = new UtilService();
////		UtilService.getValidDate("44593.00");
//		System.out.println(UtilService.getNextMonday("44593"));
//		System.out.println(UtilService.getNextSunday("44593"));
//		System.out.println(UtilService.getValidDate("44593"));
//		
//		
//		
//	}

}
