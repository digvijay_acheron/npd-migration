package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.NPDTask;

@Repository
public interface NPDTaskRepository extends JpaRepository<NPDTask, Integer> {
	
	@Query("SELECT MAX(p.id) FROM NPDTask p")
    Integer maxNPDTaskId();
	
	@Query(value = "SELECT NEXT VALUE FOR dbo.npd_task_seq", nativeQuery = true)
    public Integer getCurrentVal();

}
