package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@Table(name = "shrdnpdlookupdomainmodeltasting_requirement")
@Entity
public class TastingRequirement {

	  @Id
	    @Column(name = "Id", updatable = false)
	    private String id;

	    @Column(name = "tastingrequirement", updatable = false)
	    private String tastingRequirement;
}