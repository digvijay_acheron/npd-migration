package com.monster.npd.migration.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "shrdnpdsubmissionnpd_task")
public class NPDTask {
	
	@Id
	@GeneratedValue(generator = "npd_task_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "npd_task_seq", sequenceName = "npd_task_seq",allocationSize=1)
    @Column(name = "Id")
    private Integer id;

    @Column(name = "adjustment_point")
    private String adjustment_point;
    
    @Column(name = "task_id" )
    private Integer task_id;
    
    @Column(name = "canstart" )
    private Boolean canstart;
    
    @Column(name = "finalclassification" )
    private Integer finalclassification;
    
    @Column(name = "draftclassification" )
    private Integer draftclassification;
    
    @Column(name = "site" )
    private String site;
    
    @Column(name = "countryofmanufacture" )
    private String countryofmanufacture;
    
    @Column(name = "scopingdocumentrequirement" )
    private String scopingdocumentrequirement;
    
    @Column(name = "winshuttleproject" )
    private String winshuttleproject;
    
    @Column(name = "requiredcomplete" )
    private String requiredcomplete;
    
    @Column(name = "issuancetype")
    private String issuancetype;
    
    @Column(name = "pallet")
    private String pallet;
    
    @Column(name = "tray")
    private String tray;
    
    @Column(name = "isthisrequired")
    private String isthisrequired;
    
    @Column(name = "processauthorityapprovalearlyrdtrialrequired")
    private Boolean processAuthorityRequired;
    
    @Column(name = "transactionschemeanalysisrequired")
    private Boolean transactionSchemeAnalysisrequired;
    
    @Column(name = "nonproprietarymaterialcostingrequired")
    private Boolean nonProprietaryMaterialcostingRequired;
    
    @Column(name = "bomsentactual")
    private Date bomsentactual;
    
    @Column(name = "trialdateconfirmedwithbottler")
    private Date trialdateconfirmedwithbottler;
    
    @Column(name = "actualstartdate")
    private Date actualstartdate;
        
    @Column(name = "actualduedate")
    private Date actualduedate;
    
    @Column(name = "qcreleaseorinitialproductionscheduledifference")
    private Integer qcreleaseorinitialproductionscheduledifference;
    
    @Column(name = "allowproductionschedulelessthan8weeks")
    private Boolean allowproductionschedulelessthan8weeks;
    
    @Column(name = "task_item_id")
    private String task_item_id;
    
    @Column(name = "r_po_rm_user_id")
    private Integer r_po_rm_user_id;
    
    @Column(name = "r_po_rm_role_id")
    private Integer r_po_rm_role_id;
    
    @Column(name = "rmrolename")
    private String rmrolename;
    
    @Column(name = "rmusercn")
    private String rmusercn;
    
    @Column(name = "s_organizationid")
    private int organizationid;
    

}
