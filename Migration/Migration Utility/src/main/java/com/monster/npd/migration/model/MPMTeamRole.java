package com.monster.npd.migration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "shrdacheronmpmcorempm_team_role_mapping")
public class MPMTeamRole {
	
	@Id
    @Column(name = "Id", updatable = false)
    private Integer id;

    @Column(name = "r_po_team_id", updatable = false)
    private Integer r_po_team_id;
    
    @Column(name = "is_active", updatable = false)
    private Boolean is_active;
    
    @Column(name = "name", updatable = false)
    private String name;
    
    @Column(name = "otmm_sub_group_id", updatable = false)
    private String otmm_sub_group_id;
    
    @Column(name = "role_dn", updatable = false)
    private String role_dn;
    
    @Column(name = "is_external", updatable = false)
    private Boolean is_external;
    
    @Column(name = "r_po_cr_approver_role_id", updatable = false)
    private Integer r_po_cr_approver_role_id;
    
    @Column(name = "r_po_cr_viewer_role_id", updatable = false)
    private Integer r_po_cr_viewer_role_id;
    
    @Column(name = "s_organizationid", updatable = false)
    private Integer s_organizationid;

}
