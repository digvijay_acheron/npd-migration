package com.monster.npd.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monster.npd.migration.model.TechQualUsers;

@Repository
public interface TechQualUserRepository extends JpaRepository<TechQualUsers, Integer> {
	
	TechQualUsers findByUsers(String name);

}
